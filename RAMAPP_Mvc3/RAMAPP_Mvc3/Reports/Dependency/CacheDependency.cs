﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Threading;


namespace RAMAPP_Mvc3.Dependency
{
    

    // Base ASP.NET 1.1 class for custom cache dependencies
    public abstract class CacheDependency
    {
        // **************************************************************
        // The internal timer used to poll the data source
        protected Timer InternalTimer;

        // Seconds to wait between two successive polls 
        protected int Polling;

        // Name of the dependent cache key
        protected string DependentStorageKey;

        // **************************************************************
        // Last update
        public DateTime UtcLastModified;

        // **************************************************************
        // Class constructor
        public CacheDependency(string cacheKey)
        {
            // Store the name of the cache key to evict in case of changes
            DependentStorageKey = cacheKey;

            // Poll every 30 seconds by default
            Polling = 30;

            // Set the current time
            UtcLastModified = DateTime.Now;

            // Set up the timer
            if (InternalTimer == null)
            {
                int ms = Polling*1000;
                TimerCallback func = new 
                    TimerCallback(InternalTimerCallback);
                InternalTimer = new Timer(func, this, ms, ms);
            }
        }

        // **************************************************************
        // Built-in timer callback that fires an event to the caller
        private void InternalTimerCallback(object sender) 
        {
            CacheDependency dep = (CacheDependency) sender;
            if (HasChanged())
                NotifyDependencyChanged(dep);
        }

        // **************************************************************
        // Must-override member that determines if the monitored source 
        // has changed
        protected abstract bool HasChanged();

        // **************************************************************
        // Modify the helper key thus breaking the dependency in the Cache
        protected virtual void NotifyDependencyChanged(CacheDependency dep)
        {
            // Get the name of the helper key
            string key = 
                CacheHelper.GetHelperKeyName(DependentStorageKey);

            // Modify the date
            dep.UtcLastModified = DateTime.Now;

            // Overwrite the helper key to trigger eviction on the linked 
            // item
            HttpRuntime.Cache.Insert(key, dep);
        }
    }


}