﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ODRReport.aspx.cs" MasterPageFile="~/Views/Shared/RAM.Master" Inherits="RAMAPP_Mvc3.Reports.ODRReport" %>

<asp:Content ID="introhead" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .mainContent
        {
            font-size: .95em;
            font-family: Arial, Tahoma, "Lucida san" , Tahoma;
            line-height: 1.5;
            color: #333;
        }
        .tblHeading
        {
            background-color: #E6EAED;
            background-image: linear-gradient(#E6EAED, #D6DBE1);
            border-bottom: 1px solid #C0C1C5;
            border-top: 1px solid #D9DBDA;
        }
        .overlay
        {
            position: fixed;
            background-color: #eaeaea;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            -moz-opacity: 0.8;
            filter: alpha(opacity=80);
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
            z-index: 10000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
    <br />
    <div class="headerIntro">ODR Matrix Report</div>
    <br />
    <asp:ScriptManager ID="smMain" runat="server"></asp:ScriptManager>
    <div class="mainContent" style="overflow-x: auto; width: 1400px;">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upnlMain">
        <ProgressTemplate>
            <div class="overlay">
                <div style="height: 125px; width: 150px; position: fixed; top: 45%; left: 40%;">
                    <img src="../images/ajaxLoader2.gif" alt="Updating" style="padding-top: 10px;" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upnlMain" runat="server" UpdateMode="Conditional"><ContentTemplate>
        <table style="border-top: 1px solid #000000; width: 1390px;" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 100px; border: 1px solid #000000; padding: 1px; font-weight: bold;" class="tblHeading">
                    CompanyID
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px; font-weight: bold;" class="tblHeading">
                    Year
                </td>
                <td style="width: 85px; border: 1px solid #000000; padding: 1px; font-weight: bold;" class="tblHeading">
                    DatasetID
                </td>
                <td style="width: 298px; border: 1px solid #000000; padding: 1px; font-weight: bold;" class="tblHeading">
                    Site Name
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px; font-weight: bold;" class="tblHeading">
                    RAM EI Performance
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px; font-weight: bold;" class="tblHeading">
                    MAI Performance
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px; font-weight: bold;" class="tblHeading">
                    MCI Performance
                </td>
                <td style="width: 60px; border: 1px solid #000000; padding: 1px; font-weight: bold;" class="tblHeading">
                    Total Site Opp.
                </td>
                <td style="width: 180px; border: 1px solid #000000; padding: 1px; font-weight: bold;" class="tblHeading">
                    Worst Gap/$M
                </td>
                <td style="width: 180px; border: 1px solid #000000; padding: 1px; font-weight: bold;" class="tblHeading">
                    Best Gap/$M
                </td>
            </tr>
            <tr>
                <td style="width: 100px; border: 1px solid #000000; padding: 1px;">
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px;">
                    <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                        <asp:ListItem Text="-- SELECT --" Value="%"></asp:ListItem>
                        <asp:ListItem Text="2011" Value="2011"></asp:ListItem>
                        <asp:ListItem Text="2012" Value="2012"></asp:ListItem>
                        <asp:ListItem Text="2013" Value="2013"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 85px; border: 1px solid #000000; padding: 1px;">
                </td>
                <td style="width: 298px; border: 1px solid #000000; padding: 1px;">
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px;">
                    <asp:DropDownList ID="ddlRAMEI" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRAMEI_SelectedIndexChanged">
                        <asp:ListItem Text="-- SELECT --" Value="%"></asp:ListItem>
                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px;">
                    <asp:DropDownList ID="ddlMAI" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMAI_SelectedIndexChanged">
                        <asp:ListItem Text="-- SELECT --" Value="%"></asp:ListItem>
                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px;">
                    <asp:DropDownList ID="ddlMCI" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMCI_SelectedIndexChanged">
                        <asp:ListItem Text="-- SELECT --" Value="%"></asp:ListItem>
                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 60px; border: 1px solid #000000; padding: 1px;">
                </td>
                <td style="width: 180px; border: 1px solid #000000; padding: 1px;">
                </td>
                <td style="width: 180px; border: 1px solid #000000; padding: 1px;">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; border: 1px solid #000000; padding: 1px;">
                    <asp:LinkButton ID="lnkbtnSortCompanyID" runat="server" OnClick="lnkbtnSortCompanyID_Click">Sort</asp:LinkButton>
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px;">
                    <asp:LinkButton ID="lnkbtnSortStudyYear" runat="server" OnClick="lnkbtnSortStudyYear_Click">Sort</asp:LinkButton>
                </td>
                <td style="width: 85px; border: 1px solid #000000; padding: 1px;">
                    <asp:LinkButton ID="lnkbtnSortDatasetID" runat="server" OnClick="lnkbtnSortDatasetID_Click">Sort</asp:LinkButton>
                </td>
                <td style="width: 298px; border: 1px solid #000000; padding: 1px;">
                    <asp:LinkButton ID="lnkbtnSortFacilityName" runat="server" OnClick="lnkbtnSortFacilityName_Click">Sort</asp:LinkButton>
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px;">
                    <asp:LinkButton ID="lnkbtnSortRAMEI" runat="server" OnClick="lnkbtnSortRAMEI_Click">Sort</asp:LinkButton>
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px;">
                    <asp:LinkButton ID="lnkbtnSortMAI" runat="server" OnClick="lnkbtnSortMAI_Click">Sort</asp:LinkButton>
                </td>
                <td style="width: 110px; border: 1px solid #000000; padding: 1px;">
                    <asp:LinkButton ID="lnkbtnSortMCI" runat="server" OnClick="lnkbtnSortMCI_Click">Sort</asp:LinkButton>
                </td>
                <td style="width: 60px; border: 1px solid #000000; padding: 1px;">
                    <asp:LinkButton ID="lnkbtnSortTotalOpp" runat="server" OnClick="lnkbtnSortTotalOpp_Click">Sort</asp:LinkButton>
                </td>
                <td style="width: 180px; border: 1px solid #000000; padding: 1px;">
                    <asp:LinkButton ID="lnkbtnSortWorstGap" runat="server" OnClick="lnkbtnSortWorstGap_Click">Sort</asp:LinkButton>
                </td>
                <td style="width: 180px; border: 1px solid #000000; padding: 1px;">
                    <asp:LinkButton ID="lnkbtnSortBestGap" runat="server" OnClick="lnkbtnSortBestGap_Click">Sort</asp:LinkButton>
                </td>
            </tr>
        </table>
        <asp:ListView ID="lvODR" runat="server">
            <LayoutTemplate>
                <table runat="server" id="table1" style="border: 0px solid #000000; width: 1390px;" border="0" cellpadding="0" cellspacing="0">
                    <tr runat="server" id="itemPlaceholder">
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr id="Tr1" runat="server">
                    <td id="Td1" runat="server" style="width: 100px; border: 1px solid #000000; padding: 1px;">
                        <asp:Label ID="lblFacilityID" runat="server" Text='<%#Eval("FacilityID") %>' />
                    </td>
                    <td id="Td2" runat="server" style="width: 110px; border: 1px solid #000000; padding: 1px;">
                        <asp:Label ID="lblStudyYear" runat="server" Text='<%#Eval("StudyYear") %>' />
                    </td>
                    <td id="Td3" runat="server" style="width: 85px; border: 1px solid #000000; padding: 1px;">
                        <asp:Label ID="lblDatasetID" runat="server" Text='<%#Eval("DatasetID") %>' />
                    </td>
                    <td id="Td4" runat="server" style="width: 298px; border: 1px solid #000000; padding: 1px;">
                        <asp:Label ID="lblFacilityName" runat="server" Text='<%#Eval("FacilityName") %>' />
                    </td>
                    <td id="Td5" runat="server" style="width: 110px; border: 1px solid #000000; padding: 1px;">
                        <asp:Label ID="lblRAMEIPerformance" runat="server" Text='<%#Eval("RAMEIPerformance") %>' />
                    </td>
                    <td id="Td6" runat="server" style="width: 110px; border: 1px solid #000000; padding: 1px;">
                        <asp:Label ID="lblMAIPerformance" runat="server" Text='<%#Eval("MAIPerformance") %>' />
                    </td>
                    <td id="Td7" runat="server" style="width: 110px; border: 1px solid #000000; padding: 1px;">
                        <asp:Label ID="lblMCIPerformance" runat="server" Text='<%#Eval("MCIPerformance") %>' />
                    </td>
                    <td id="Td8" runat="server" style="width: 60px; border: 1px solid #000000; padding: 1px;">
                        <asp:Label ID="lblTotalOpp" runat="server" Text='<%#Eval("TotalOpp") %>' />
                    </td>
                    <td id="Td9" runat="server" style="width: 180px; border: 1px solid #000000; padding: 1px;">
                        <asp:Label ID="lblWorstGap" runat="server" Text='<%#Eval("WorstGapItem") %>' />
                    </td>
                    <td id="Td10" runat="server" style="width: 180px; border: 1px solid #000000; padding: 1px;">
                        <asp:Label ID="lblBestGap" runat="server" Text='<%#Eval("BestGapItem") %>' />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <table style="border: 1px solid #000000; width: 1390px;" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div style="float: right; padding-right: 1px;"><asp:Label ID="lblRecordCount" runat="server"></asp:Label></div>
                </td>
            </tr>
        </table>
        </ContentTemplate></asp:UpdatePanel>
    </div>
    </form>
</asp:Content>