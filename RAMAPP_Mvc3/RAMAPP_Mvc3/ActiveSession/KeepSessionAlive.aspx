﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KeepSessionAlive.aspx.cs"
    Inherits="RAMAPP_Mvc3.ActiveSession.KeepSessionAlive" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Keep Alive</title>
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
    <meta http-equiv="refresh" content="<%= "480;url="+ HttpContext.Current.Request.ServerVariables["SERVER_PROTOCOL"].Remove(HttpContext.Current.Request.ServerVariables["SERVER_PROTOCOL"].IndexOf("/")) + "s://"+ HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + Page.ResolveUrl("~/ActiveSession/KeepSessionAlive.aspx") +"?q=" + DateTime.Now.Ticks  %>" />
    <%--   <meta  http-equiv="refresh" content="<%= Convert.ToString((Session.Timeout * 60)/4) +";url=http://"+ HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + Page.ResolveUrl("~/ActiveSession/KeepSessionAlive.aspx") +"?q=" + DateTime.Now.Ticks  %>" />--%>
    <%--    <script src="../Scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/keepalive.js" type="text/javascript"></script>--%>
    <script type="text/javascript" language="javascript">
       window.status = "<%="Last refresh " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() %>";
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <%= DateTime.Now.ToLongTimeString() %>
    </div>
    </form>
</body>
</html>