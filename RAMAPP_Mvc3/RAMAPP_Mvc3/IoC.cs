using StructureMap;
namespace RAMAPP_Mvc3
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });

                         /*   x.For<RAMAPP_Mvc3.Repositories.App.INavigationRepository>()
                                .Use<RAMAPP_Mvc3.Repositories.Cache.CachedNavigationRepository>()
                               .Ctor<RAMAPP_Mvc3.Repositories.App.INavigationRepository>()
                               .Is<RAMAPP_Mvc3.Repositories.App.EfNavigationRepository>();*/


                            x.For<RAMAPP_Mvc3.Repositories.Cache.ICachedSiteUnitModelsRepository>()
                                 .Use<RAMAPP_Mvc3.Repositories.Cache.CachedSiteUnitModelsRepository>();
                            //    .Ctor<RAMAPP_Mvc3.Repositories.App.ISiteModelsRepository>()
                            //    .Is<RAMAPP_Mvc3.Repositories.App.SiteModelsRepository>();
                        });

            return ObjectFactory.Container;
        }
    }
}