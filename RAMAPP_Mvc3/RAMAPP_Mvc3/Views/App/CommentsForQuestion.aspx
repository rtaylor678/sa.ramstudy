﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="RAMAPP_Mvc3.Entity" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%
    
    
    var companySID = SessionUtil.GetCompanySID();
    var id = Convert.ToInt32(ViewContext.RouteData.Values["id"].ToString().Trim());
    var qid = ViewContext.RouteData.Values["qid"].ToString().Trim().Replace("___", ".");
    var userNo = ((LogIn)Session["UserInfo"]).UserNo;

    if (String.IsNullOrEmpty(qid))
    {
        Response.Write("Invalid Url");
    }

    using (var db = new RAMEntities())
    {
        try
        {
            var cm = (from h in db.Comments
                      join l in db.LogIn on h.UserNo equals l.UserNo
                      where h.DatasetID == id && h.QuestionInventoryID == qid
                      orderby h.CommentID descending
                      select
                          new
                              {
                                  h.UserNo,
                                  h.DatasetID,
                                  h.QuestionInventoryID,
                                  h.CommentID,
                                  h.Posted,
                                  h.CommentText,
                                  l.ScreenName
                              }).ToList();


            int f = 0;

            if (cm.Count == 0)
                Response.Write("There are not any comments");
            foreach (var item in cm)
            { %>
<div style="background: <%= (f++%2 == 0) ?  "white" : "white" %>; border: solid 1px #F5CCCD;
    min-width: 400px; max-width: 100%; margin: 2px 2px;">
    <div style="font-size: .85em; color: #991000; margin: 0px 5px; width: 95%;">
        POSTED BY:
        <%= item.ScreenName %>
        <div style="position: relative; top: 2px; float: right; display: inline;">
            <img style="cursor: pointer; <%= (userNo == item.UserNo) ? "display:hidden;": "" %>"
                alt="delete" src="<%= Page.ResolveUrl("~/images/comment_btn_delete.jpg") %>"
                align="middle" border="0" onclick="deleteComment(<%= item.CommentID %>,<%= item.DatasetID %>,'<%= item.QuestionInventoryID.Trim()%>');" />
        </div>
    </div>
    <div style="margin: 0px 10px; font-size: .90em; color: #555; border-top: 1px #F3E5E5 solid;
        min-width: 95%; min-height: 35px;">
        <%= item.CommentText %>
    </div>
    <div style="position: relative; bottom: 0px; font-size: .85em; color: #991000; min-width: 95%;
        height: auto; margin: 0px 5px;">
        <span>
            <%= item.Posted%></span>
        <%--<span style="float: right; cursor: pointer"><a href="javascript:ReplyToComment('')">
                REPLY</a>&nbsp;>
               
            </span--%>
    </div>
</div>
<% }

        }
        catch
        {
            Response.Write("Invalid Url");
        }


    }
%>
