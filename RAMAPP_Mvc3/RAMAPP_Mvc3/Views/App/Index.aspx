<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_App.Master" Inherits="System.Web.Mvc.ViewPage" %>




<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link href="<%= Page.ResolveUrl("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .style1
        {
            font-size: 16px;
            font-weight: 500;
        }
        .style2
        {
        	color:#333;
            font-size: .90em;
        	line-height: 1.6;
            font-family:Arial,Tahoma,"Lucida san", Tahoma;
        }
        .style4
        {
            color: #991000;
            
           
        }
        .style5
        {
            width: 50%;
        }
        .style6
        {
            font-size:  1em;
            border-top:1px #ccc solid;
            border-bottom:1px #ccc solid;
            font-family: Arial, Tahoma;
            color: #FF0000;
            padding: 10px;
        	background-color: cornsilk;
        }
        
        #content {

        	border: thin #ccc solid;
        	height: 100%;
        	min-width: 960px;
        }
        #wrapper 
        {
         
        	background: url('<%: Page.ResolveUrl("~/images/welcome.jpg") %>')  top right no-repeat;
           
        	height: 875px;
        	/*width: 1050px;*/
        	width: 100%;
        	min-width:1025px;
        	
        }
        #welcome
        {
        	width:825px;
        	background-color: rgba(255, 255, 255, .60);
        }
        .headerIntro
        {
           font-family: 'Arial'; font-size: 16px; color: #991000; text-transform: capitalize;  border-bottom: 1px solid wheat; min-width: 200px; max-width: 350px; padding-left: 8px; margin-top:0px; text-align: left; height: 20px; vertical-align:bottom;text-transform:capitalize;
        }
        #welcome ul {
        	
        	line-height: 1.7;
        	list-style: disc ;
        }
        #welcome ul li {
        	display: list-item ;
        	width: 325px;
        	padding-left: 5px;
        	
        	
        }
        
        /* #welcome ul li:before {
         	content: "\00BB ";
         	color: #991000;
         	padding-right: 8px;
         	
         }*/
         
    </style>

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID ="MainContent" runat="server">


<% if (Request["sectiontype"] == null)
   {  %>
   <div id="wrapper">
<table id="welcome" cellpadding="10" style="width:850px;height:100%;" align="left" >
<tr>
<td valign="top" class="style5">
    <span class="style1"><span class="style4" style="font-family:Arial;">Welcome to the International Study of Plant Reliability and Maintenance Effectiveness (RAM Study). We are pleased that you have elected to participate</span>.</span><br />
    <br />
&nbsp;<span class="style2">The first step in data entry is the creation of your 
    company, site and units. Your designated data coordinator is responsible for 
    creating these using the tools available through the Administration page of the 
    RAM Study web portal. Once these have been created, you can access them using 
    the MENU tab at the left of the screen. After clicking on the MENU tab you 
    should see your sites and units listed.</span><br />
    <br />
  <div class="headerIntro">Site Data</div>

   <p class="style2">
    Click on the name of the site for which you wish to enter data.  The data tabs for the site include:<br /> </p>
    <ul class="style2" style="padding-left:5px;list-style: disc ;">
    <li><span class="style4">Identification</span> (site name, location, key personnel, etc.)</li>
    <li><span class="style4">Characteristics</span>  (geographical size, number of workers, site replacement value, etc.)</li>
    <li><span class="style4">Costs</span> (direct and indirect maintenance costs, expenses and maintenance capital, routine and T/A, etc.)</li>
    <li><span class="style4">General</span>  (general maintenance such as scaffolding, painting, insulation and tracing)</li>
    <li><span class="style4">Craft Characteristics</span> (rotating, fixed and instrumentation and electrical equipment craft characteristics)</li>
    <li><span class="style4">MRO Inventory</span> (MRO storeroom warehouse inventory and work practices)</li>
    <li><span class="style4">Organization</span> (reliability and maintenance management, personnel and organization)</li>
    <li><span class="style4">Work Processes</span> (maintenance work processes, work orders, and backlog)</li>
    <li><span class="style4">Reliability</span>  (strategic plan, condition monitoring methods, use of contemporary proactive practices, etc.)</li>
<%--    <li><span class="style4">Site Turnarounds</span>  (site turnaround frequency and duration)</li>--%>
    <li><span class="style4">Study</span> (feedback on data time and resource requirements as well as data accuracy)</li>   
    </ul>
  
   


</td>
<td style="width:50%;" valign="top">
 <div class="headerIntro">Unit Data</div>

   <p class="style2">
    Click on the name of the unit for which you wish to enter data.  The data tabs for the unit include:</p>
<ul class="style2" style="padding-left:5px;list-style: disc ;">
<%--<li><span class="style4">Identification</span>  (unit name, product manufactured, etc.)</li>--%>
<li><span class="style4">Characteristics</span> (unit replacement value, production rate, process conditions, equipment types and counts)</li>
<li><span class="style4">Downtimes</span> (primary causes, turnaround and short overhaul data, etc.)</li>
<li><span class="style4">Reliability</span> (reliability processes, mean time between failures, etc.)</li>
<li><span class="style4">Costs</span> (unit maintenance costs and craft labor hours for routine and turnarounds)</li>
<li><span class="style4">Work Types</span> (corrective, preventive and predictive maintenance labor hours)</li>
</ul>


<p class="style2">
Your data will be saved automatically every few minutes or you may click on the SAVE button to save your data.  The system will prompt you to save your data when navigating to another input screen. You may also save your data at any time by clicking the SAVE button. The session will time-out after one hour of inactivity.  Click the LOGOUT button to logout of the system. You will be prompted again to save your data when you logout. 
</p>
<p class="style6">
Ensure Timely Input of Data to Prevent Delayed Results.
</p>


</td>
</tr>

</table>
</div>




<%} %>

</asp:Content>



