using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using MagicAjax.UI;

namespace RAM_MvcApp.Views
{
	/// <summary>
	/// Summary description for Note.
	/// </summary>
	public class Note : System.Web.UI.Page
    {
//        protected System.Data.SqlClient.SqlConnection sqlConnection1;
//        protected System.Data.SqlClient.SqlDataAdapter sdComments;
//        protected RAMStudy.dsComments dsComments1;
//        protected System.Web.UI.WebControls.TextBox txtOtherComments;
//        protected System.Data.SqlClient.SqlCommand sqlSelectCommand1;
//        protected System.Data.SqlClient.SqlCommand sqlInsertCommand1;
//        protected System.Data.SqlClient.SqlCommand sqlUpdateCommand1;
//        protected System.Data.SqlClient.SqlCommand sqlDeleteCommand1;
//        protected System.Web.UI.WebControls.DataGrid dgComments;
//        protected System.Web.UI.WebControls.Button btnSave;
//        protected System.Web.UI.WebControls.Label Label2;
//        protected System.Web.UI.WebControls.TextBox txtNote;
//        protected System.Web.UI.WebControls.LinkButton linkClose;
//        protected System.Web.UI.WebControls.Label Label1;
//        protected System.Web.UI.WebControls.Label lblResults;
//        protected System.Web.UI.HtmlControls.HtmlGenericControl othComments;

//        private void Page_Load(object sender, System.EventArgs e)
//        {   
			
			
//            if (!IsPostBack)
//            {
               			  
//                //this.Page.FindControl("Form1").Attributes["onSubmit"]+= ";btnSave.disabled=true;";

//                BindData();
////				for (int x=0;x < rows.Length; x++ )
////				{
////                    string user = rows[x]["NoteID"].ToString().Split('_')[0];
////				
////					string  posted ="<br>Posted ";
////
////					if (rows[x]["Posted"] != DBNull.Value)
////					{
////						posted = "<br>Posted on " + ((DateTime)rows[x]["Posted"]).ToString("g") ;
////						
////					}
////
////				     othComments.InnerHtml += "<font size='2'>  "+ rows[x]["Comment"].ToString() +
////						                      "</font>" + "<font size='1' color='gray'>  " + posted + 
////                                              " by " + user + "</font><br>";
////				}

//                // Put user code to initialize the page here
//                //linkClose.Attributes["onclick"]="window.close();";
//                txtNote.Attributes["onkeypress"]="return taLimit()";
//                txtNote.Attributes["onkeyup"]="return taCount()";
				
//            }
			
				
//        }

//        #region Web Form Designer generated code
//        override protected void OnInit(EventArgs e)
//        {
//            //
//            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
//            //
//            InitializeComponent();
//            base.OnInit(e);
//        }
		
//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {    
//            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
//            this.sdComments = new System.Data.SqlClient.SqlDataAdapter();
//            this.sqlDeleteCommand1 = new System.Data.SqlClient.SqlCommand();
//            this.sqlInsertCommand1 = new System.Data.SqlClient.SqlCommand();
//            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
//            this.sqlUpdateCommand1 = new System.Data.SqlClient.SqlCommand();
//            this.dsComments1 = new RAMStudy.dsComments();
//            ((System.ComponentModel.ISupportInitialize)(this.dsComments1)).BeginInit();
//            // 
//            // sqlConnection1
//            // 
//            this.sqlConnection1.ConnectionString = "packet size=4096;user id=saroy;password=carter;data source=\"10.10.41.13\";persist " +
//                "security info=False;initial catalog=RAM";
//            // 
//            // sdComments
//            // 
//            this.sdComments.DeleteCommand = this.sqlDeleteCommand1;
//            this.sdComments.InsertCommand = this.sqlInsertCommand1;
//            this.sdComments.SelectCommand = this.sqlSelectCommand1;
//            this.sdComments.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
//                                                                                                 new System.Data.Common.DataTableMapping("Table", "Comments", new System.Data.Common.DataColumnMapping[] {
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("CompanyID", "CompanyID"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("NoteID", "NoteID"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("Comment", "Comment"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("Posted", "Posted")})});
//            this.sdComments.UpdateCommand = this.sqlUpdateCommand1;
//            this.sdComments.RowUpdated += new System.Data.SqlClient.SqlRowUpdatedEventHandler(this.sdComments_RowUpdated);
//            // 
//            // sqlDeleteCommand1
//            // 
//            this.sqlDeleteCommand1.CommandText = @"DELETE FROM dbo.Comments WHERE (CompanyID = @Original_CompanyID) AND (NoteID = @Original_NoteID) AND (Comment = @Original_Comment OR @Original_Comment IS NULL AND Comment IS NULL) AND (Posted = @Original_Posted OR @Original_Posted IS NULL AND Posted IS NULL)";
//            this.sqlDeleteCommand1.Connection = this.sqlConnection1;
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_CompanyID", System.Data.SqlDbType.NVarChar, 9, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "CompanyID", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_NoteID", System.Data.SqlDbType.NVarChar, 60, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "NoteID", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Comment", System.Data.SqlDbType.NVarChar, 250, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Comment", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Posted", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Posted", System.Data.DataRowVersion.Original, null));
//            // 
//            // sqlInsertCommand1
//            // 
//            this.sqlInsertCommand1.CommandText = "INSERT INTO dbo.Comments(CompanyID, NoteID, Comment, Posted) VALUES (@CompanyID, " +
//                "@NoteID, @Comment, @Posted); SELECT CompanyID, NoteID, Comment, Posted FROM dbo." +
//                "Comments WHERE (CompanyID = @CompanyID) AND (NoteID = @NoteID)";
//            this.sqlInsertCommand1.Connection = this.sqlConnection1;
//            this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.NVarChar, 9, "CompanyID"));
//            this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@NoteID", System.Data.SqlDbType.NVarChar, 60, "NoteID"));
//            this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.NVarChar, 250, "Comment"));
//            this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Posted", System.Data.SqlDbType.DateTime, 4, "Posted"));
//            // 
//            // sqlSelectCommand1
//            // 
//            this.sqlSelectCommand1.CommandText = "SELECT CompanyID, NoteID, Comment, Posted FROM dbo.Comments";
//            this.sqlSelectCommand1.Connection = this.sqlConnection1;
//            // 
//            // sqlUpdateCommand1
//            // 
//            this.sqlUpdateCommand1.CommandText = @"UPDATE dbo.Comments SET CompanyID = @CompanyID, NoteID = @NoteID, Comment = @Comment, Posted = @Posted WHERE (CompanyID = @Original_CompanyID) AND (NoteID = @Original_NoteID) AND (Comment = @Original_Comment OR @Original_Comment IS NULL AND Comment IS NULL) AND (Posted = @Original_Posted OR @Original_Posted IS NULL AND Posted IS NULL); SELECT CompanyID, NoteID, Comment, Posted FROM dbo.Comments WHERE (CompanyID = @CompanyID) AND (NoteID = @NoteID)";
//            this.sqlUpdateCommand1.Connection = this.sqlConnection1;
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.NVarChar, 9, "CompanyID"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@NoteID", System.Data.SqlDbType.NVarChar, 60, "NoteID"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Comment", System.Data.SqlDbType.NVarChar, 250, "Comment"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Posted", System.Data.SqlDbType.DateTime, 4, "Posted"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_CompanyID", System.Data.SqlDbType.NVarChar, 9, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "CompanyID", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_NoteID", System.Data.SqlDbType.NVarChar, 60, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "NoteID", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Comment", System.Data.SqlDbType.NVarChar, 250, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Comment", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Posted", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Posted", System.Data.DataRowVersion.Original, null));
//            // 
//            // dsComments1
//            // 
//            this.dsComments1.DataSetName = "dsComments";
//            this.dsComments1.Locale = new System.Globalization.CultureInfo("en-US");
//            this.Load += new System.EventHandler(this.Page_Load);
//            ((System.ComponentModel.ISupportInitialize)(this.dsComments1)).EndInit();

//        }
//        #endregion

//        private void BindData()
//        {
//            DataRow[] rows;
//            if(sdComments.SelectCommand.CommandText.IndexOf("WHERE") == -1)
//                sdComments.SelectCommand.CommandText += " WHERE CompanyID='"+ HttpContext.Current.Session["CompanyID"].ToString().Trim() +"' AND NoteID Like '%"+ Request["Element"] +"%' ORDER BY Posted DESC" ;
//            dsComments1.EnforceConstraints = false;
//            dsComments1.Tables[0].BeginLoadData();
//            sdComments.Fill(dsComments1);
//            dsComments1.Tables[0].EndLoadData();

//            rows= new DataRow[dsComments1.Comments.Rows.Count];
//            dsComments1.Comments.Rows.CopyTo(rows,0);
//            dgComments.DataBind();
//        }
	
//        protected void btnSave_Click(object sender, System.EventArgs e)
//        {
//            string noteid;
////			int elmCommentCntByUser;

//            if (txtNote.Text.Trim().Length==0)
//            {
//                lblResults.Text="You must enter a comment before saving.";
//                return;
//            }

//            //((Button)sender).Enabled = false;
           
//            noteid = HttpContext.Current.Session["AppUser"] + Request["Element"]; 

////			if (dsComments1.Comments.Rows.Count ==0)
////			{
////			    if(sdComments.SelectCommand.CommandText.IndexOf("WHERE") == -1)
////					sdComments.SelectCommand.CommandText += " WHERE CompanyID='"+ HttpContext.Current.Session["CompanyID"].ToString().Trim() +"' AND NoteID Like '%"+ Request["Element"] +"%' ORDER BY Posted ASC" ;
////				dsComments1.EnforceConstraints = false;
////				dsComments1.Tables[0].BeginLoadData();
////				sdComments.Fill(dsComments1);
////				dsComments1.Tables[0].EndLoadData();
////			}

//            //elmCommentCntByUser = dsComments1.Comments.Rows.Count + 1;

//            noteid += "_" + DateTime.Now.ToUniversalTime().ToString();
			
		
//            DataRow row = dsComments1.Tables[0].NewRow();
//            row["CompanyID"]=HttpContext.Current.Session["CompanyID"];
//            row["NoteID"]= noteid;
//            row["Comment"]=txtNote.Text;
//            row["Posted"]=DateTime.Now;
//            dsComments1.Tables[0].Rows.Add(row);
//            sdComments.Update(dsComments1);
			
//            lblResults.Text="Your comment has been saved.";
//            BindData();
//            //Response.Write("<script language='javascript'>window.opener.document.getElementById('"+ Request["Element"] +"').src='images/icon_notepad_yw.gif';</script>");
//            //Response.Write("<script language='javascript'> {setTimeout('window.close()', 2000);}</script>");
//            Response.Write("<script language='javascript'> return false;</script>");
//        }

//        private void sdComments_RowUpdated(object sender, System.Data.SqlClient.SqlRowUpdatedEventArgs e)
//        {
		
//        }

		

//        private void dgComments_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
//        {
//            DataRowView drv = (DataRowView)e.Item.DataItem;
			
//            try
//            {
//                ImageButton imgBtn = (ImageButton)e.Item.FindControl("BtnDelete");
				
//                if (imgBtn != null && !drv.Row.IsNull("NoteID"))
//                   if(HttpContext.Current.Session["AppUser"].Equals( drv["NoteID"].ToString().Split('_')[0]))			
//                        imgBtn.Click +=new ImageClickEventHandler(imgBtn_Click);								
//                   else
//                        imgBtn.Visible = false;

//            }
//            catch(NullReferenceException){}
			
//        }
        
//        protected void editBtn_Click(object sender, System.Web.UI.ImageClickEventArgs e)
//        {

//        }

//        protected void replyBtn_Click(object sender, EventArgs  e)
//        {

//        }
//        protected void imgBtn_Click(object sender, System.Web.UI.ImageClickEventArgs e)
//        {
//            ImageButton imgBtn = (ImageButton) sender;
			
			
//            string sqlDel = "DELETE FROM COMMENTS WHERE NoteId='" + imgBtn.CommandArgument + "'";
//            try
//            {
//                if (sqlConnection1.State == ConnectionState.Closed)
//                    sqlConnection1.Open();

//                using(System.Data.SqlClient.SqlCommand  sc = new System.Data.SqlClient.SqlCommand(sqlDel,sqlConnection1))
//                {
//                    sc.ExecuteNonQuery();
//                } 
//                //System.Data.SqlClient.SqlCommand  sc = new System.Data.SqlClient.SqlCommand(sqlDel,sqlConnection1);
//                BindData();
//            }
//            finally
//            {
//                if (sqlConnection1.State == ConnectionState.Open)
//                    sqlConnection1.Close();

				
//            }

//        }

		
   }
}
