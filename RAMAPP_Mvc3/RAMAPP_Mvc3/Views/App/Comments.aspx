<%@ Page Language="c#" CodeBehind="Comments.aspx.cs" AutoEventWireup="false" Inherits="RAM_MvcApp.Views.Note" %>

<%@ Import Namespace="System.Data" %>
<html>
<head>
</head>
<body bgcolor="#ffffff">
    <form id="FormComment" runat="server">
    <asp:TextBox Style="z-index: 102; position: absolute; top: 272px; left: 24px" ID="txtNote"
        runat="server" MaxLength="250" TextMode="MultiLine" BorderStyle="Dotted" BorderWidth="1px"
        Height="48px" Width="384px"></asp:TextBox>
    <asp:LinkButton Style="z-index: 103; position: absolute; top: 16px; left: 392px"
        ID="linkClose" runat="server" Height="12px" Width="40px" Visible="False" ForeColor="Black"
        Font-Size="X-Small" Font-Names="Verdana">[X]</asp:LinkButton>
    <asp:Label Style="z-index: 104; position: absolute; top: 16px; left: 24px" ID="Label1"
        runat="server" Height="24px" Width="136px" Visible="False" Font-Size="XX-Small"
        Font-Names="Verdana" Font-Bold="True"> Comment(s)</asp:Label>
    <asp:Label Style="z-index: 105; position: absolute; top: 360px; left: 112px" ID="lblResults"
        runat="server" Height="32px" Width="280px" Font-Size="X-Small" Font-Names="Verdana"></asp:Label>
    <div style="z-index: 102; position: relative; top: 315px; left: 24px">
        <font size="2"><font size="1" face="Verdana">You have <b><span id="myCounter">250</span></b>
            characters remaining</font> </font>
    </div>
    <div style="border-bottom: slategray 1px groove; position: absolute; border-left: slategray 1px groove;
        overflow-x: hidden; overflow-y: auto; width: 400px; height: 225px; border-top: slategray 1px groove;
        top: 10px; border-right: slategray 1px groove; left: 24px">
        <asp:DataGrid ID="dgComments" runat="server" Height="152px" Width="380px" DataSource="<%# dsComments1 %>"
            AllowPaging="False" AllowSorting="False" ShowHeader="False" GridLines="Horizontal"
            AutoGenerateColumns="False" BackColor="LightCyan">
            <AlternatingItemStyle BackColor="Azure"></AlternatingItemStyle>
            <Columns>
                <asp:TemplateColumn SortExpression="Posted" HeaderText="Posted">
                    <ItemStyle Font-Names="Tahoma" ForeColor="#000FFF"></ItemStyle>
                    <ItemTemplate>
                        <div style="position: relative; font-size: 10px; color: #888; margin: 4px 2px;">
                            POSTED BY:
                            <%# ((DataRowView)Container.DataItem)["NoteID"].ToString().Split('_')[0]%>
                            <div style="position: relative; top: 0px; left: 240px; display: inline;">
                                <!--a  href="javascript:deleteComment('<%#  ((DataRowView)Container.DataItem)["NoteID"]  %>')"><img src="images/delete_icon.gif" /></a-->
                                <asp:ImageButton ID="BtnDelete" ImageUrl="images\delete_icon.gif" OnClick="imgBtn_Click"
                                    CommandArgument='<%#((DataRowView)Container.DataItem)["NoteID"] %>' runat="server">
                                </asp:ImageButton>
                            </div>
                        </div>
                        <div style="margin: 0px 5px; font-size: 12px; color: #000; border-top: 1px #BAD9D7 solid;">
                            <p>
                                <%# ((DataRowView)Container.DataItem)["Comment"]%></p>
                        </div>
                        <div style="font-size: 8px; color: #888; margin: 2px 2px;">
                            <%# ((DataRowView)Container.DataItem)["Posted"] %>
                            <div style="position: relative; top: 0px; left: 265px; display: inline;">
                                <asp:LinkButton ID="BtnReply" Text="Reply" runat="server" CommandName="Reply" CommandArgument='<%#((DataRowView)Container.DataItem)["NoteID"] %>'
                                    NAME="BtnReply" OnClick="replyBtn_Click" Font-Size="8">
                                </asp:LinkButton>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <asp:Button Style="z-index: 101; position: absolute; top: 360px; left: 24px" ID="btnSave"
        OnClick="btnSave_Click" runat="server" BorderStyle="None" Width="56px" BackColor="White"
        CausesValidation="False" EnableViewState="False"></asp:Button>
    <asp:Label Style="z-index: 108; position: absolute; top: 248px; left: 24px" ID="Label2"
        runat="server" Height="24px" Width="216px" Font-Size="XX-Small" Font-Names="Verdana"
        Font-Bold="True">Enter Comment Here</asp:Label>
    </form>
    <script language="Javascript" type="text/javascript">


        function taLimit() {
            var taObj = event.srcElement;
            if (taObj.value.length == taObj.maxLength * 1) return false;
        }

        function taCount() {

            var visCnt = document.getElementById("myCounter");
            var taObj = event.srcElement;
            //taObj.maxLength=250;
            if (taObj.value.length > taObj.maxLength * 1) taObj.value = taObj.value.substring(0, taObj.maxLength * 1);
            if (visCnt) visCnt.innerText = taObj.maxLength - taObj.value.length;
        }

        taObj = document.getElementById("txtNote");
        taObj.maxLength = 250;
        document.getElementById("myCounter").innerText = taObj.maxLength - taObj.value.length;
		 

    </script>
</body>
</html>
