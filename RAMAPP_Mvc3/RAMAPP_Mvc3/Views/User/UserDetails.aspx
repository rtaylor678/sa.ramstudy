﻿<%@ Page Title="User Details" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master"  Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UserDetails>" %>

<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
    <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/mvc/3.0/jquery.unobtrusive-ajax.min.js" type="text/javascript"></script>
    <style type="text/css">
        #CultureFormat
        {
            font-style: italic;
            font-size: x-small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
   
    <br />
    <br />
    <fieldset style="width: 700px;">
    <legend>User Information</legend>
        <% Html.EnableClientValidation(); %>
        <% Html.BeginForm("UpdateUser", "User", new { id = Model.UserID  });
           { %>
        <div class="grid_16">
            <span style="float: right;">
              <%--  <%:Html.Raw(Ajax.ActionLink("imagelink",
                                              "DeleteAppUser",
                                    new { id = Model.UserID },
                                    new AjaxOptions
                                    {
                                        Confirm = "You are deleting this user. Are you sure?",
                                        HttpMethod = "Delete",
                                        OnComplete = "JsonDeleteUser_OnComplete"
                                    }, new  { @style = Model.UserID.Equals(((RAMAPP_Mvc3.Entity.LogIn) Session["UserInfo"]).UserID)?"display:none;":""  })
                                    .ToHtmlString()
                                                   .Replace("imagelink", "<img src=\"http://cdn1.iconfinder.com/data/icons/vaga/user_delete.png\" border=\"no\"/> Delete User"))%>--%>
            </span>
        </div>
        <%--<div class="grid_3">
            Name :
        </div>
        <div class="grid_13">
        <span>
            <%: Html.LabelFor(model=>model.Firstname) %>
            <%: Html.EditorFor(model => model.Firstname, "TextTmpl", new { style = "width:130px" })%></span>
        <span>
            <%: Html.LabelFor(model=>model.Lastname) %>
            <%: Html.EditorFor(model => model.Lastname, "TextTmpl", new { style = "width:130px" })%></span>
        </div>--%>
       <%-- <p>
            &nbsp;</p>
        <div class="grid_3">
            Screen Name :
        </div>
        <div class="grid_13">
            <%: Html.HiddenFor(model=>model.ScreenName) %>
            <%: Html.DisplayFor(model => model.ScreenName, "TextTmpl", new { style = "width:130px" })%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_3">
            Password :
        </div>
        <div class="grid_13">
            <%: Html.HiddenFor(model=>model.Password) %>
            <%: Html.DisplayFor(model => model.Password, "TextTmpl", new { style = "width:130px" })%>
            &nbsp;
            <%: Ajax.ActionLink("Reset Password", "ResetPassword", null, new AjaxOptions { HttpMethod = "Post", Confirm = "You are resetting the user password. Are you sure?" }, new { @style="font-size:x-small;font-style:italic;" })%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_3">
            Email Address :
        </div>
        <div class="grid_13">
            <%: Html.LabelFor(model => model.EmailAddress,"Email Address")%>
            <%: Html.EditorFor(model => model.EmailAddress, "TextTmpl", new { style = "width:130px" })%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_3">
            Telephone Number :
        </div>
        <div class="grid_13">
            <%: Html.LabelFor(model => model.Telephone,"Telephone Number")%>
            <%: Html.EditorFor(model => model.Telephone, "TextTmpl", new { style = "width:130px" })%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_3">
            Job Title:
        </div>
        <div class="grid_13">
            <%: Html.LabelFor(model => model.JobTile)%>
            <%: Html.EditorFor(model => model.JobTile, "TextTmpl", new { style = "width:130px" })%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_3">
            Select Culture:
        </div>
        <div class="grid_13">
            <%: Html.DropDownListFor(model => model.Culture, CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                                                                        .OrderBy(ci=>ci.DisplayName)
                                                                        .Select(ci =>new SelectListItem() {Text=ci.EnglishName,Value=ci.Name }).Distinct()
                                    ,"Select Culture")%>
            <br />
            <br />
        </div>
        <div class="grid_3">
            &nbsp;
        </div>
        <div id="CultureFormat" class="grid_13">
            <font color="#990000" size="1px">(optional) default is US English</font>
            <br />
            Number format :<span id="NumberFormat"> 1,234,567.90</span><br />
            Date format : <span id="DateFormat">
                <%: DateTime.Now.ToLongDateString() %></span>
        </div>--%>
        <% }%>
         <%--<p>
            <span style="color: #990000; font-style: italic; margin: 0 auto;">
               <%:  TempData["UserCreateMsg"]  %>
             </span>
        </p>--%>
        <div id="alertBox">
        </div>
        <%: Html.HiddenFor(model=>model.UserNo) %>
        <%: Html.HiddenFor(model=>model.CompanyID) %>
        <input type="submit" title="Update" name="Update" value="Update" />
    </fieldset>
    <script type="text/javascript">
      //  $('#Culture').val('<%: Model.Culture != null ? Model.Culture.Trim() : "en" %>');

//        $('#Culture').click(function () {
//            var cultureSlt = $('#Culture').val();
//            $.post('<%= Html.Action("DisplayLocale","Admin") %>',
//				 { "locale": cultureSlt },
//				  function (response) {
//				      var $num = $('#NumberFormat');
//				      try {
//				          $num.html(response.localeNumber);
//				          $('#DateFormat').html(response.localeDate);
//				      }
//				      catch (error) {
//				          $num.html("Culture is not available ");
//				      }
//				  }, 'json');
//        });

       
   
        function JsonDeleteUser_OnComplete(response) {

            $('#alertBox').html(response.Message);
            $('#alertBox').dialog('open');
            window.location.href = '<%: Url.RouteUrl(new {action="Index",controller="Admin"}) %>';
        }
    </script>
</asp:Content>
