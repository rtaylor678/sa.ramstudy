﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master"
    Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UserDetails>" %>

<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
     
      
    <style type="text/css">
        #CultureFormat
        {
            font-style: italic;
            font-size: x-small;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Edit User Profile</h2>
   
    <fieldset style="width: 700px;">
        <br />
       <%-- <div style="color: #991000;"><%:TempData["UserCreateMsg"]%></div>--%>
        <legend>User Information</legend>
        <% Html.EnableClientValidation(); %>
      
        <% using (Html.BeginForm("UpdateUser", "User"))
           { %>
        <%: Html.ValidationSummary(true) %>
        <div class="grid_16">
            <span style="float: right;font-size:.85em;">
                <% if (!SessionUtil.GetCompanySID().Contains("04TEST"))
                   { %>
                <img  alt="delete" align=middle  src="<%=Page.ResolveUrl("~/images")%>/reset_pwd.gif"  /> 
                <%: Ajax.ActionLink("Reset Password", "ResetPassword","User", new {usr = Model.UserID, email = Model.EmailAddress }, new AjaxOptions { HttpMethod = "Post", Confirm = "You are resetting the user password. Click continue if you are sure.", OnComplete = "alert('Password reset instructions were just emailed to user.');" }, null)%>&nbsp;&nbsp;
                <%} %>
                <% if (!Model.UserID.Equals(((RAMAPP_Mvc3.Entity.LogIn)Session["UserInfo"]).UserID))
                   { %>
                 <img  alt="delete" align=middle  src="<%=Page.ResolveUrl("~/images")%>/x-delete-icon.png"  /> 
                <%: Ajax.ActionLink("Delete User",
                                     "DeleteLogin","User",
                                     new {userNo = Model.UserNo, userId = Model.UserID.Trim() },
                                     new AjaxOptions { HttpMethod = "Post", Confirm = "You are deleting the user. Click continue if you are sure.", OnComplete = "JsonDeleteUser_OnComplete" },
                                     null)
                                    
                %>
                <%} %>
            </span>
            <br />
             <div id="ReturnMsg" class="<%:  TempData["UserCreateMsgClass"] ?? String.Empty%>">
                      <span class="ResponseMsg"><%:   TempData["UserCreateMsg"] ?? String.Empty%></span>
              
              </div>
            <br />
             
         <%--   <span style="color: #990000; font-style: italic; margin: 0 auto;">
                <%:  ViewData["UserCreateMsg"]  %>
            </span>--%>
       
        </div>
        <div class="grid_5">
            Name :
        </div>
        <div class="grid_11">
            <span>
                <%: Html.LabelFor(model=>model.Firstname) %>
                <%: Html.EditorFor(model => model.Firstname, "TextTmpl", new {size="20" })%></span>
            <span style="margin-right:10px;">
                <%: Html.LabelFor(model=>model.Lastname) %>
                <%: Html.EditorFor(model => model.Lastname, "TextTmpl", new { size = "20" })%></span>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_5">
            Screen Name :
        </div>
        <div class="grid_11">
            <%: Html.HiddenFor(model=>model.ScreenName) %>
            <%: Html.DisplayFor(model => model.ScreenName, "TextTmpl", new { style = "width:130px" })%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_5">
            Email Address :
        </div>
        <div class="grid_11">
            <%: Html.LabelFor(model => model.EmailAddress,"Email Address")%>
            <%: Html.EditorFor(model => model.EmailAddress, "TextTmpl", new {size="35" })%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_5">
            Telephone Number :
        </div>
        <div class="grid_11">
            <%: Html.LabelFor(model => model.Telephone,"Telephone Number")%>
            <%: Html.EditorFor(model => model.Telephone, "TextTmpl", new { size = "35" })%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_5">
            Job Title:
        </div>
        <div class="grid_11">
            <%: Html.LabelFor(model => model.JobTile)%>
            <%: Html.EditorFor(model => model.JobTile, "TextTmpl", new { size = "35" })%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_5">
            Select Culture:
        </div>
        <div class="grid_11">
            <%: Html.DropDownListFor(model => model.Culture, CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                                                                        .OrderBy(ci=>ci.DisplayName)
                                                                        .Select(ci =>new SelectListItem() {Text=ci.EnglishName,Value=ci.Name }).Distinct()
                                    ,"Select Culture")%>
            <br />
            <br />
        </div>
        <div class="grid_5">
            &nbsp;
        </div>
        <div id="CultureFormat" class="grid_11">
            <font color="#990000" size="1px">(optional)</font>
            <br />
            Number format :<span id="NumberFormat"> 1,234,567.90</span><br />
            Date format : <span id="DateFormat">
                <%: DateTime.Now.ToLongDateString() %></span>
        </div>
       
        <div id="alertBox">
        </div>
        <%: Html.HiddenFor(model=>model.UserNo ) %>
        <%: Html.HiddenFor(model=>model.UserID ) %>
        <%: Html.HiddenFor(model=>model.Role ) %>
        <%: Html.HiddenFor(model=>model.CompanyID) %>
        <input type="submit" title="Update" name="Update" value="Update" />
        <%} %>
    </fieldset>
  
    <script type="text/javascript">
        $('#Culture').val('<%: Model.Culture != null ? Model.Culture.Trim() : "" %>');
        $('#Culture').click(function () {
            var cultureSlt = $('#Culture').val();
            $.post('<%:Page.ResolveUrl("~/Admin/DisplayLocale") %>',
        				 { "locale": cultureSlt },
        				  function (response) {
        				      var $num = $('#NumberFormat');
        				      try {
        				          $num.html(response.localeNumber);
        				          $('#DateFormat').html(response.localeDate);
        				      }
        				      catch (error) {
        				          $num.html("Culture is not available ");
        				      }
        				  }, 'json');
        });

        function JsonDeleteUser_OnComplete(response) {
            var $altBx = $('#alertBox');
            $altBx.html(response.Message)
            $altBx.dialog('open');
           
            window.location.href = '<%: Url.RouteUrl(new {action="Index",controller="Admin"}) %>';
        }			  
    </script>
</asp:Content>

