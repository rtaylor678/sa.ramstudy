﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UT_DTModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 02/05/2015 11:43:52  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_UT_DT").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("UT_DT", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_UT_DT" class="stContainer">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Turnarounds</small> </span></a></li>
            <li><a href='#step-2'>
                <label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br />
                    <small>Short Overhaul Characteristics</small> </span></a></li>
            <li><a href='#step-3'>
                <label class='stepNumber'>3</label><span class='stepDesc'>Section 3<br />
                    <small>Downtime Due to Reliability and Maintenance Causes</small> </span></a></li>
            <li><a href='#step-4'>
                <label class='stepNumber'>4</label><span class='stepDesc'>Section 4<br />
                    <small>Lost Production Due to Process/Regulatory Causes</small> </span></a></li>
            <li><a href='#step-5'>
                <label class='stepNumber'>5</label><span class='stepDesc'>Section 5<br />
                    <small>Maintenance completed during all non-maintenance outages</small> </span></a></li>
            <li><a href='#step-6'>
                <label class='stepNumber'>6</label><span class='stepDesc'>Section 6<br />
                    <small>Primary Causes of Downtime</small> </span></a></li>
            <li><a href='#step-7'>
                <label class='stepNumber'>7</label><span class='stepDesc'>Section 7<br />
                    <small>Principal Reason for Turnaround</small> </span></a></li>
            <li><a href='#step-8'>
                <label class='stepNumber'>8</label><span class='stepDesc'>Section 8<br />
                    <small>Turnaround Scope</small> </span></a></li>
            <li><a href='#step-9'>
                <label class='stepNumber'>9</label><span class='stepDesc'>Section 9<br />
                    <small>Turnaround Critical Path</small> </span></a></li>
             <li><a href='#step-10'>
                <label class='stepNumber'>10</label><span class='stepDesc'>Section 10<br />
                    <small>Turnaround Management</small> </span></a></li>
            <li><a href='#step-11'>
                <label class='stepNumber'>11</label><span class='stepDesc'>Section 11<br />
                    <small>Turnaround Strategy</small> </span></a></li>
<%--            <li><a href='#step-12'>
                <label class='stepNumber'>12</label><span class='stepDesc'>Section 12<br />
                    <small>Turnaround Work Execution</small> </span></a></li>
            <li><a href='#step-13'>
                <label class='stepNumber'>13</label><span class='stepDesc'>Section 13<br />
                    <small>Routine Work Execution</small> </span></a></li>--%>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle"><a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">Turnarounds</a></p>
            <p><i>Report the average characteristics of the last two scheduled <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/As.</a>.</i></p>



            <div class="questionblock" qid="QSTID_UREL_170">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_170' /></span> <span class='numbering'>1.</span> Average duration of operations shutdown and maintenance preparation (e.g., shut equipment down, decontamination, lock-out/tag-out, permitting, etc.) for last two <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/As</a>
                    <span>
                        <img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAAvgHrsShutdown,"DoubleTmpl",new {size="10" ,CalcTag="avgTADays" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TAAvgHrsShutdown)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_170.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_170.1' /></span> <span class='numbering'>2.</span> Average duration of maintenance preparation, execution, and closure (e.g., lock-out/tag-out, permitting, work execution, turnover to operations, etc.) for last two <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/As</a>
                    <span>
                        <img id="Img2" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAAvgHrsExecution,"DoubleTmpl",new {size="10" ,CalcTag="avgTADays" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TAAvgHrsExecution)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_170.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_170.2' /></span> <span class='numbering'>3.</span> Average duration of operations preparation and startup (e.g., lock-out/tag-out removal, system pressurization and testing, feedstock reintroduction, etc.) for last two <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/As</a>
                    <span>
                        <img id="Img3" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAAvgHrsStartup,"DoubleTmpl",new {size="10" ,CalcTag="avgTADays" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TAAvgHrsStartup)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_170.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_170.3' /></span> Average <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> duration between product-off and product-on for last two T/As
					 					 <span>
                                              <img id="Img4" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAAvgHrsDown,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.TAAvgHrsDown)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_170.6">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_170.6' /></span> <span class='numbering'>4.</span> Average planned <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> duration between product-off and product-on for last two T/As
					 					 <span>
                                              <img id="Img5" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAAvgPlanHrsDown,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TAAvgPlanHrsDown)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_170.4">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_170.4' /></span> <span class='numbering'>5.</span> Average interval between <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/As</a> for last two T/As
					 					 <span>
                                              <img id="Img6" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAAvgInterval,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.TAAvgInterval)%>
                </div>

                <div class="ReportInUnit">Months </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_170.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_170.5' /></span> Average Annualized Downtime for T/A's
					 					 <span>
                                              <img id="Img7" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAAvgAnnHrsDown,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.TAAvgAnnHrsDown)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-2">

            <p class="StepTitle"><a href="#" tooltip="Short unit outage scheduled in advance for maintenance purposes; may occur multiple times a year. Exclude outages for non-maintenance purposes such as cleaning or catalyst changes.">Short Overhaul</a> Characteristics</p>
            <p><i>Report the characteristics of <a href="#" tooltip="Short unit outage scheduled in advance for maintenance purposes; may occur multiple times a year. Exclude outages for non-maintenance purposes such as cleaning or catalyst changes.">short overhauls</a> necessary for plant maintenance and equipment repairs or replacement.</i></p>



            <div class="questionblock" qid="QSTID_UREL_210.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_210.1' /></span> <span class='numbering'>1.</span> Average <a href="#" tooltip="Short unit outage scheduled in advance for maintenance purposes; may occur multiple times a year. Exclude outages for non-maintenance purposes such as cleaning or catalyst changes.">short overhaul</a> duration between product-off and product-on over the previous two years
					 					 <span>
                                              <img id="Img8" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.ShortOHAvgHrsDown,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.ShortOHAvgHrsDown)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_210.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_210.2' /></span> <span class='numbering'>2.</span> Number of <a href="#" tooltip="Short unit outage scheduled in advance for maintenance purposes; may occur multiple times a year. Exclude outages for non-maintenance purposes such as cleaning or catalyst changes.">short overhauls</a> over the previous two years
					 					 <span>
                                              <img id="Img9" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.ShortOH2YrCnt,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.ShortOH2YrCnt)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_210.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_210.3' /></span> Average yearly downtime for <a href="#" tooltip="Short unit outage scheduled in advance for maintenance purposes; may occur multiple times a year. Exclude outages for non-maintenance purposes such as cleaning or catalyst changes.">short overhauls</a>
                    <span>
                        <img id="Img10" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.ShortOHAnnHrsDown,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.ShortOHAnnHrsDown)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-3">

            <p class="StepTitle"><a href="#" tooltip="Any event when the asset is down and not capable of performing its intended function. Examples include turnarounds, short duration outages, and unplanned outages.">Downtime</a> Due to <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> and Maintenance Causes</p>
            <p><i>Calculate the annual <a href="#" tooltip="Any event when the asset is down and not capable of performing its intended function. Examples include turnarounds, short duration outages, and unplanned outages.">downtime</a> due to each cause.  If unable to calculate the downtime, provide an estimate based on the best available information.</i></p>



            <div class="questionblock" qid="QSTID_UREL_100.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_100.1' /></span> <a href="#" tooltip="Scheduled maintenance is work that has been scheduled for execution. The maintenance schedule is used to track the completion of scheduled maintenance work within the process unit. The measure of performance associated with this practice is called maintenance schedule compliance and is a reflection of the organization’s ability to adhere to a defined schedule. Maintenance scheduling is performed at least a week in advance.">Scheduled Maintenance</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">Turnaround (T/A)</a> – <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5 years is divided by five to get the annualized cost of the turnaround.">Annualized</a>
                    <span>
                        <img id="Img11" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model._TAAvgAnnHrsDown,"DoubleTmpl",new {size="10" ,id="_TAAvgAnnHrsDown" ,CalcTag="sumAnnDt" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model._TAAvgAnnHrsDown)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_100.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_100.2' /></span> <a href="#" tooltip="Short unit outage scheduled in advance for maintenance purposes; may occur multiple times a year. Exclude outages for non-maintenance purposes such as cleaning or catalyst changes.">Scheduled Short Maintenance Overhauls</a>
                    <span>
                        <img id="Img12" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model._ShortOHAnnHrsDown,"DoubleTmpl",new {size="10" ,id="_ShortOHAnnHrsDown" ,CalcTag="sumAnnDt" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model._ShortOHAnnHrsDown)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_100.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_100.3' /></span> <span class='numbering'>1.</span> Unscheduled Maintenance Outage Due to Equipment <a href="#" tooltip="When an asset is unable to perform its required function even if a reduced operating state is still possible. PM/PdM tasks are not considered failures. However if through execution of a PM/PdM task the need for a Maintenance Event is identified, this work is to be considered a failure.">Failure</a>
                    <span>
                        <img id="Img13" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.LossHrsForUnschedMaint,"DoubleTmpl",new {size="10" ,CalcTag="sumAnnDt" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.LossHrsForUnschedMaint)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_100.4">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_100.4' /></span> <span class='numbering'>2.</span> <a href="#" tooltip="The equivalent downtime associated with operating at reduced rates attributable to reliability and maintenance causes such as a single pump failure in a parallel pump train which causes the unit to operate at below full capacity. The equivalent downtime is calculated by taking the total time the unit operates at below capacity times the percent of lost capacity. For example, a process unit runs at 60% of design capacity for 72 hours due to a pump failure. The equivalent downtime is 72 hours multiplied by 40% (100% - 60%) equals 28.8 equivalent hours of downtime. Exclude process rate reductions due to non-maintenance causes (e.g., catalyst aging, heat exchanger fouling, low sales or product demand, extremely low or high ambient temperatures, etc.).">Process Rate Reduction Loss</a> Due to <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> and Maintenance Causes
					 					 <span>
                                              <img id="Img14" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.LossHrsForProcessReliability,"DoubleTmpl",new {size="10" ,CalcTag="sumAnnDt" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.LossHrsForProcessReliability)%>
                </div>

                <div class="ReportInUnit">Equivalent Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div style="display: none;" class="questionblock" qid="QSTID_UREL_100.5X">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_100.5X' /></span> Total Production Losses Due to <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> and Maintenance Causes
					 				
				
														
								
					
                </div>

                <div class="ReportInUnit">Calculated </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_100.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_100.5' /></span> Total Production Losses Due to <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> and Maintenance Causes
					 					 <span>
                                              <img id="Img15" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.LossHrsTotalAnnProdLoss,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.LossHrsTotalAnnProdLoss)%>
                </div>

                <div class="ReportInUnit">Calculated </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div style="display: none;" class="questionblock" qid="QSTID_UREL_100.1X">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_100.1X' /></span> <a href="#" tooltip="Scheduled maintenance is work that has been scheduled for execution. The maintenance schedule is used to track the completion of scheduled maintenance work within the process unit. The measure of performance associated with this practice is called maintenance schedule compliance and is a reflection of the organization’s ability to adhere to a defined schedule. Maintenance scheduling is performed at least a week in advance.">Scheduled Maintenance</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">Turnaround (T/A)</a> – <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5 years is divided by five to get the annualized cost of the turnaround.">Annualized</a>





                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div style="display: none;" class="questionblock" qid="QSTID_UREL_100.2X">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_100.2X' /></span> <a href="#" tooltip="Short unit outage scheduled in advance for maintenance purposes; may occur multiple times a year. Exclude outages for non-maintenance purposes such as cleaning or catalyst changes.">Scheduled Short Maintenance Overhauls</a>





                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-4">

            <p class="StepTitle">Lost Production Due to Process/Regulatory Causes</p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_UREL_220">

                <span class='labelDesc'>Report unit lost production due to <a href="#" tooltip="An equipment inspection mandated by law or regulation. Government regulations that address inspection based on risk assessments such as API RP-580 or other equivalent Risk-Based Inspection methodologies.">regulatory inspections</a> or process-related outages that occurred each year. Report outages required by regulatory agencies (for example, <a href="#" tooltip="The predominant process pressure: Low = 150 psi or 10.5 kg/cm; Medium > 150 psi or 10.5 kg/cm and < 600 psi or 42.2 kg/cm; and High = 600 psi or 42.2 kg/cm.">pressure</a> vessel inspections, environmental testing, etc.), outages for furnace decoking, tower or equipment washing with an insignificant amount of maintenance support, and outages for catalyst regenerations/replacements not recorded as part of a <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnaround</a>. Report the total time down in hours (from product-out, to sustained, on-specification production of the major product streams to tankage) for <a href="#" tooltip="Any event when the asset is down and not capable of performing its intended function. Examples include turnarounds, short duration outages, and unplanned outages.">downtimes</a> caused by regulatory inspections or process-related outages.</span>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_220.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_220.1' /></span> <span class='numbering'>1.</span> Outages
					 					 <span>
                                              <img id="Img16" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.LostProductionOutages,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.LostProductionOutages)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_220.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_220.2' /></span> <span class='numbering'>2.</span> Rate Reductions (equivalent hours at full rates).
					 					 <span>
                                              <img id="Img17" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.LostProductionRateReductions,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.LostProductionRateReductions)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-5">

            <p class="StepTitle">Maintenance completed during all non-maintenance outages</p>
            <p><i>Estimate the amount of production displaced if maintenance completed during a non-maintenance-related outage required unit <a href="#" tooltip="Any event when the asset is down and not capable of performing its intended function. Examples include turnarounds, short duration outages, and unplanned outages.">downtime</a>.</i></p>



            <div class="questionblock" qid="QSTID_UREL_220.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_220.3' /></span> <span class='numbering'>1.</span> Maintenance <a href="#" tooltip="Any event when the asset is down and not capable of performing its intended function. Examples include turnarounds, short duration outages, and unplanned outages.">downtime</a> displaced (equivalent hours at full rates)
					 					 <span>
                                              <img id="Img18" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.NonMaintOutagesMaintEquivHours,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.NonMaintOutagesMaintEquivHours)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-6">

            <p class="StepTitle"><a href="#" tooltip="The primary cause is the root cause of an action or event. This term is used to assign the percent of downtime attributable to three equipment types: rotating equipment, fixed equipment, and instrumentation and electrical equipment. The sum of downtime for these three equipment types for each type of downtime (i.e., turnaround, short overhaul, and corrective maintenance) must equal 100% of downtime attributable to each of these reliability and maintenance downtimes. If there is more than one primary cause, the days required for each would be totaled and the proportionate time for each would be reported. This would also apply to additional scope from discovery work using the actual duration as the basis for determining the percentages.">Primary Causes</a> of <a href="#" tooltip="Any event when the asset is down and not capable of performing its intended function. Examples include turnarounds, short duration outages, and unplanned outages.">Downtime</a></p>
            <p><i>For each type of <a href="#" tooltip="Any event when the asset is down and not capable of performing its intended function. Examples include turnarounds, short duration outages, and unplanned outages.">downtime</a>, calculate the <a href="#" tooltip="The primary cause is the root cause of an action or event. This term is used to assign the percent of downtime attributable to three equipment types: rotating equipment, fixed equipment, and instrumentation and electrical equipment. The sum of downtime for these three equipment types for each type of downtime (i.e., turnaround, short overhaul, and corrective maintenance) must equal 100% of downtime attributable to each of these reliability and maintenance downtimes. If there is more than one primary cause, the days required for each would be totaled and the proportionate time for each would be reported. This would also apply to additional scope from discovery work using the actual duration as the basis for determining the percentages.">primary cause</a> contribution percentage for each equipment category.  If unable to calculate the contribution, provide an estimate based on the best available information. The total for each type of downtime should equal 100%.</i></p>



            <div class="questionblock" qid="">


                <div id='UT_REL_CS' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_120.1'><span class='numbering'>1.</span> <a href="#" tooltip="Equipment that turns on an axis and is designed to impart kinetic energy on a process material. Common examples include centrifugal pumps and compressors, electric motors, steam turbines, etc. Rotating equipment craft include machinery mechanics such as millwrights and rotating equipment condition monitoring technicians. Include process equipment and exclude non-process equipment.">Rotating Equipment</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_120.2'><span class='numbering'>2.</span> <a href="#" tooltip="Process equipment that is fixed versus rotating equipment, reciprocating equipment, or other moving equipment. Examples of fixed equipment include pipe, heat exchangers, process vessels, distillation towers, tanks, etc. Fixed equipment craft workers include pipefitters, welders, boilermakers, carpenters, painters, equipment operators, insulators, riggers, and other civil crafts. Include process equipment and exclude non-process equipment.">Fixed Equipment</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_120.3'><span class='numbering'>3.</span> Instrument or Electrical Equipment</li>
                        <li class='gridrowtext calctext' qid='QSTID_UREL_120.4'>Total</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Scheduled Maintenance Turnaround</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_120.1" /><%: Html.EditorFor(model => model.TACausePcnt_RE,"DoubleTmpl",new {size="10" ,CalcTag="sumSchTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_120.2" /><%: Html.EditorFor(model => model.TACausePcnt_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumSchTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_120.3" /><%: Html.EditorFor(model => model.TACausePcnt_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumSchTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_120.4" /><%: Html.EditorFor(model => model.TACausePcnt_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Scheduled Short Maintenance Overhauls</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_130.1" /><%: Html.EditorFor(model => model.ShortOHCausePcnt_RE,"DoubleTmpl",new {size="10" ,CalcTag="sumSchShrt" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_130.2" /><%: Html.EditorFor(model => model.ShortOHCausePcnt_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumSchShrt" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_130.3" /><%: Html.EditorFor(model => model.ShortOHCausePcnt_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumSchShrt" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_130.4" /><%: Html.EditorFor(model => model.ShortOHCausePcnt_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Unscheduled Maintenance Outages</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_140.1" /><%: Html.EditorFor(model => model.StoppagePcnt_RE,"DoubleTmpl",new {size="10" ,CalcTag="sumUnschOut" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_140.2" /><%: Html.EditorFor(model => model.StoppagePcnt_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumUnschOut" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_140.3" /><%: Html.EditorFor(model => model.StoppagePcnt_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumUnschOut" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_140.4" /><%: Html.EditorFor(model => model.StoppagePcnt_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rate Reductions due to RAM Causes</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_230.1" /><%: Html.EditorFor(model => model.RAMCausePcnt_RE,"DoubleTmpl",new {size="10" ,CalcTag="sumRRRAM" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_230.2" /><%: Html.EditorFor(model => model.RAMCausePcnt_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumRRRAM" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_230.3" /><%: Html.EditorFor(model => model.RAMCausePcnt_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumRRRAM" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_230.4" /><%: Html.EditorFor(model => model.RAMCausePcnt_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.TACausePcnt_RE)%> <%: Html.ValidationMessageFor(model => model.TACausePcnt_FP)%> <%: Html.ValidationMessageFor(model => model.TACausePcnt_IE)%> <%: Html.ValidationMessageFor(model => model.TACausePcnt_Tot)%> <%: Html.ValidationMessageFor(model => model.ShortOHCausePcnt_RE)%> <%: Html.ValidationMessageFor(model => model.ShortOHCausePcnt_FP)%> <%: Html.ValidationMessageFor(model => model.ShortOHCausePcnt_IE)%> <%: Html.ValidationMessageFor(model => model.ShortOHCausePcnt_Tot)%> <%: Html.ValidationMessageFor(model => model.StoppagePcnt_RE)%> <%: Html.ValidationMessageFor(model => model.StoppagePcnt_FP)%> <%: Html.ValidationMessageFor(model => model.StoppagePcnt_IE)%> <%: Html.ValidationMessageFor(model => model.StoppagePcnt_Tot)%> <%: Html.ValidationMessageFor(model => model.RAMCausePcnt_RE)%> <%: Html.ValidationMessageFor(model => model.RAMCausePcnt_FP)%> <%: Html.ValidationMessageFor(model => model.RAMCausePcnt_IE)%> <%: Html.ValidationMessageFor(model => model.RAMCausePcnt_Tot)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-7">

            <p class="StepTitle">Principal Reason for <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">Turnaround</a></p>
            <p><i>What was the principal reason for the <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a>? Score the reasons below, giving the highest score (5) for the most important and lowest (1) for the least important.</i></p>



            <div class="questionblock" qid="">


                <div id='UT_REL_TA_RSN' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_180'><span class='numbering'>1.</span> <a href="#" tooltip="An equipment inspection mandated by law or regulation. Government regulations that address inspection based on risk assessments such as API RP-580 or other equivalent Risk-Based Inspection methodologies.">Regulatory inspection</a> requirements</li>
                        <li class='gridrowtext' qid='QSTID_UREL_180.1'><span class='numbering'>2.</span> <a href="#" tooltip="An equipment inspection that the plant elected to perform, but was not mandated by law or regulation.">Discretionary need for inspection</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_180.2'><span class='numbering'>3.</span> <a href="#" tooltip="The inability of the production unit to run at design capacity, to make on-spec product, or to meet another manufacturing constraint that causes the unit to limit or cease production operations.">Deterioration of plant and/or performance</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_180.3'><span class='numbering'>4.</span> Unplanned equipment <a href="#" tooltip="When an asset is unable to perform its required function even if a reduced operating state is still possible. PM/PdM tasks are not considered failures. However if through execution of a PM/PdM task the need for a Maintenance Event is identified, this work is to be considered a failure.">failure</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_180.4'><span class='numbering'>5.</span> Drop in product demand</li>
                        <li class='gridrowtext' qid='QSTID_UREL_180.5'><span class='numbering'>6.</span> Regularly scheduled <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> set by the company</li>
                        <li class='gridrowtext' qid='QSTID_UREL_180.6'><span class='numbering'>7.</span> Capital projects</li>
                        <li class='gridrowtext' qid='QSTID_UREL_180.7'><span class='numbering'>8.</span> Contractor availability</li>
                        <li class='gridrowtext' qid='QSTID_UREL_180.8'><span class='numbering'>9.</span> Local weather conditions/season</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Importance</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_180" /><select id="TARegulatoryInspect" name="TARegulatoryInspect" width="30px"><option value='-1'>SELECT ONE</option>
                                <option value='0' <%= Model.TARegulatoryInspect != null ?(Model.TARegulatoryInspect.Equals("0")? "SELECTED" : String.Empty):String.Empty %>>Not a reason</option>
                                <option value='1' <%= Model.TARegulatoryInspect != null ?(Model.TARegulatoryInspect.Equals("1")? "SELECTED" : String.Empty):String.Empty %>>1 - Least Important</option>
                                <option value='2' <%= Model.TARegulatoryInspect != null ?(Model.TARegulatoryInspect.Equals("2")? "SELECTED" : String.Empty):String.Empty %>>2 - Less Important</option>
                                <option value='3' <%= Model.TARegulatoryInspect != null ?(Model.TARegulatoryInspect.Equals("3")? "SELECTED" : String.Empty):String.Empty %>>3 - Important</option>
                                <option value='4' <%= Model.TARegulatoryInspect != null ?(Model.TARegulatoryInspect.Equals("4")? "SELECTED" : String.Empty):String.Empty %>>4 - More Important</option>
                                <option value='5' <%= Model.TARegulatoryInspect != null ?(Model.TARegulatoryInspect.Equals("5")? "SELECTED" : String.Empty):String.Empty %>>5 - Most Important</option>
                            </select></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_180.1" /><select id="TADiscretionaryInspect" name="TADiscretionaryInspect" width="30px"><option value='-1'>SELECT ONE</option>
                                <option value='0' <%= Model.TADiscretionaryInspect != null ?(Model.TADiscretionaryInspect.Equals("0")? "SELECTED" : String.Empty):String.Empty %>>Not a reason</option>
                                <option value='1' <%= Model.TADiscretionaryInspect != null ?(Model.TADiscretionaryInspect.Equals("1")? "SELECTED" : String.Empty):String.Empty %>>1 - Least Important</option>
                                <option value='2' <%= Model.TADiscretionaryInspect != null ?(Model.TADiscretionaryInspect.Equals("2")? "SELECTED" : String.Empty):String.Empty %>>2 - Less Important</option>
                                <option value='3' <%= Model.TADiscretionaryInspect != null ?(Model.TADiscretionaryInspect.Equals("3")? "SELECTED" : String.Empty):String.Empty %>>3 - Important</option>
                                <option value='4' <%= Model.TADiscretionaryInspect != null ?(Model.TADiscretionaryInspect.Equals("4")? "SELECTED" : String.Empty):String.Empty %>>4 - More Important</option>
                                <option value='5' <%= Model.TADiscretionaryInspect != null ?(Model.TADiscretionaryInspect.Equals("5")? "SELECTED" : String.Empty):String.Empty %>>5 - Most Important</option>
                            </select></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_180.2" /><select id="TADeterioration" name="TADeterioration" width="30px"><option value='-1'>SELECT ONE</option>
                                <option value='0' <%= Model.TADeterioration != null ?(Model.TADeterioration.Equals("0")? "SELECTED" : String.Empty):String.Empty %>>Not a reason</option>
                                <option value='1' <%= Model.TADeterioration != null ?(Model.TADeterioration.Equals("1")? "SELECTED" : String.Empty):String.Empty %>>1 - Least Important</option>
                                <option value='2' <%= Model.TADeterioration != null ?(Model.TADeterioration.Equals("2")? "SELECTED" : String.Empty):String.Empty %>>2 - Less Important</option>
                                <option value='3' <%= Model.TADeterioration != null ?(Model.TADeterioration.Equals("3")? "SELECTED" : String.Empty):String.Empty %>>3 - Important</option>
                                <option value='4' <%= Model.TADeterioration != null ?(Model.TADeterioration.Equals("4")? "SELECTED" : String.Empty):String.Empty %>>4 - More Important</option>
                                <option value='5' <%= Model.TADeterioration != null ?(Model.TADeterioration.Equals("5")? "SELECTED" : String.Empty):String.Empty %>>5 - Most Important</option>
                            </select></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_180.3" /><select id="TAUnplannedFailure" name="TAUnplannedFailure" width="30px"><option value='-1'>SELECT ONE</option>
                                <option value='0' <%= Model.TAUnplannedFailure != null ?(Model.TAUnplannedFailure.Equals("0")? "SELECTED" : String.Empty):String.Empty %>>Not a reason</option>
                                <option value='1' <%= Model.TAUnplannedFailure != null ?(Model.TAUnplannedFailure.Equals("1")? "SELECTED" : String.Empty):String.Empty %>>1 - Least Important</option>
                                <option value='2' <%= Model.TAUnplannedFailure != null ?(Model.TAUnplannedFailure.Equals("2")? "SELECTED" : String.Empty):String.Empty %>>2 - Less Important</option>
                                <option value='3' <%= Model.TAUnplannedFailure != null ?(Model.TAUnplannedFailure.Equals("3")? "SELECTED" : String.Empty):String.Empty %>>3 - Important</option>
                                <option value='4' <%= Model.TAUnplannedFailure != null ?(Model.TAUnplannedFailure.Equals("4")? "SELECTED" : String.Empty):String.Empty %>>4 - More Important</option>
                                <option value='5' <%= Model.TAUnplannedFailure != null ?(Model.TAUnplannedFailure.Equals("5")? "SELECTED" : String.Empty):String.Empty %>>5 - Most Important</option>
                            </select></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_180.4" /><select id="TALowDemand" name="TALowDemand" width="30px"><option value='-1'>SELECT ONE</option>
                                <option value='0' <%= Model.TALowDemand != null ?(Model.TALowDemand.Equals("0")? "SELECTED" : String.Empty):String.Empty %>>Not a reason</option>
                                <option value='1' <%= Model.TALowDemand != null ?(Model.TALowDemand.Equals("1")? "SELECTED" : String.Empty):String.Empty %>>1 - Least Important</option>
                                <option value='2' <%= Model.TALowDemand != null ?(Model.TALowDemand.Equals("2")? "SELECTED" : String.Empty):String.Empty %>>2 - Less Important</option>
                                <option value='3' <%= Model.TALowDemand != null ?(Model.TALowDemand.Equals("3")? "SELECTED" : String.Empty):String.Empty %>>3 - Important</option>
                                <option value='4' <%= Model.TALowDemand != null ?(Model.TALowDemand.Equals("4")? "SELECTED" : String.Empty):String.Empty %>>4 - More Important</option>
                                <option value='5' <%= Model.TALowDemand != null ?(Model.TALowDemand.Equals("5")? "SELECTED" : String.Empty):String.Empty %>>5 - Most Important</option>
                            </select></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_180.5" /><select id="TAFixedSchedule" name="TAFixedSchedule" width="30px"><option value='-1'>SELECT ONE</option>
                                <option value='0' <%= Model.TAFixedSchedule != null ?(Model.TAFixedSchedule.Equals("0")? "SELECTED" : String.Empty):String.Empty %>>Not a reason</option>
                                <option value='1' <%= Model.TAFixedSchedule != null ?(Model.TAFixedSchedule.Equals("1")? "SELECTED" : String.Empty):String.Empty %>>1 - Least Important</option>
                                <option value='2' <%= Model.TAFixedSchedule != null ?(Model.TAFixedSchedule.Equals("2")? "SELECTED" : String.Empty):String.Empty %>>2 - Less Important</option>
                                <option value='3' <%= Model.TAFixedSchedule != null ?(Model.TAFixedSchedule.Equals("3")? "SELECTED" : String.Empty):String.Empty %>>3 - Important</option>
                                <option value='4' <%= Model.TAFixedSchedule != null ?(Model.TAFixedSchedule.Equals("4")? "SELECTED" : String.Empty):String.Empty %>>4 - More Important</option>
                                <option value='5' <%= Model.TAFixedSchedule != null ?(Model.TAFixedSchedule.Equals("5")? "SELECTED" : String.Empty):String.Empty %>>5 - Most Important</option>
                            </select></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_180.6" /><select id="TACptlProject" name="TACptlProject" width="30px"><option value='-1'>SELECT ONE</option>
                                <option value='0' <%= Model.TACptlProject != null ?(Model.TACptlProject.Equals("0")? "SELECTED" : String.Empty):String.Empty %>>Not a reason</option>
                                <option value='1' <%= Model.TACptlProject != null ?(Model.TACptlProject.Equals("1")? "SELECTED" : String.Empty):String.Empty %>>1 - Least Important</option>
                                <option value='2' <%= Model.TACptlProject != null ?(Model.TACptlProject.Equals("2")? "SELECTED" : String.Empty):String.Empty %>>2 - Less Important</option>
                                <option value='3' <%= Model.TACptlProject != null ?(Model.TACptlProject.Equals("3")? "SELECTED" : String.Empty):String.Empty %>>3 - Important</option>
                                <option value='4' <%= Model.TACptlProject != null ?(Model.TACptlProject.Equals("4")? "SELECTED" : String.Empty):String.Empty %>>4 - More Important</option>
                                <option value='5' <%= Model.TACptlProject != null ?(Model.TACptlProject.Equals("5")? "SELECTED" : String.Empty):String.Empty %>>5 - Most Important</option>
                            </select></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_180.7" /><select id="TAContAvail" name="TAContAvail" width="30px"><option value='-1'>SELECT ONE</option>
                                <option value='0' <%= Model.TAContAvail != null ?(Model.TAContAvail.Equals("0")? "SELECTED" : String.Empty):String.Empty %>>Not a reason</option>
                                <option value='1' <%= Model.TAContAvail != null ?(Model.TAContAvail.Equals("1")? "SELECTED" : String.Empty):String.Empty %>>1 - Least Important</option>
                                <option value='2' <%= Model.TAContAvail != null ?(Model.TAContAvail.Equals("2")? "SELECTED" : String.Empty):String.Empty %>>2 - Less Important</option>
                                <option value='3' <%= Model.TAContAvail != null ?(Model.TAContAvail.Equals("3")? "SELECTED" : String.Empty):String.Empty %>>3 - Important</option>
                                <option value='4' <%= Model.TAContAvail != null ?(Model.TAContAvail.Equals("4")? "SELECTED" : String.Empty):String.Empty %>>4 - More Important</option>
                                <option value='5' <%= Model.TAContAvail != null ?(Model.TAContAvail.Equals("5")? "SELECTED" : String.Empty):String.Empty %>>5 - Most Important</option>
                            </select></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_180.8" /><select id="TAWeather" name="TAWeather" width="30px"><option value='-1'>SELECT ONE</option>
                                <option value='0' <%= Model.TAWeather != null ?(Model.TAWeather.Equals("0")? "SELECTED" : String.Empty):String.Empty %>>Not a reason</option>
                                <option value='1' <%= Model.TAWeather != null ?(Model.TAWeather.Equals("1")? "SELECTED" : String.Empty):String.Empty %>>1 - Least Important</option>
                                <option value='2' <%= Model.TAWeather != null ?(Model.TAWeather.Equals("2")? "SELECTED" : String.Empty):String.Empty %>>2 - Less Important</option>
                                <option value='3' <%= Model.TAWeather != null ?(Model.TAWeather.Equals("3")? "SELECTED" : String.Empty):String.Empty %>>3 - Important</option>
                                <option value='4' <%= Model.TAWeather != null ?(Model.TAWeather.Equals("4")? "SELECTED" : String.Empty):String.Empty %>>4 - More Important</option>
                                <option value='5' <%= Model.TAWeather != null ?(Model.TAWeather.Equals("5")? "SELECTED" : String.Empty):String.Empty %>>5 - Most Important</option>
                            </select></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.TARegulatoryInspect)%> <%: Html.ValidationMessageFor(model => model.TADiscretionaryInspect)%> <%: Html.ValidationMessageFor(model => model.TADeterioration)%> <%: Html.ValidationMessageFor(model => model.TAUnplannedFailure)%> <%: Html.ValidationMessageFor(model => model.TALowDemand)%> <%: Html.ValidationMessageFor(model => model.TAFixedSchedule)%> <%: Html.ValidationMessageFor(model => model.TACptlProject)%> <%: Html.ValidationMessageFor(model => model.TAContAvail)%> <%: Html.ValidationMessageFor(model => model.TAWeather)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-8">

            <p class="StepTitle"><a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">Turnaround</a> Scope</p>
            <p><i>What percentage of the process unit equipment in each of the following categories was worked on during the last <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a>?</i></p>



            <div class="questionblock" qid="QSTID_UREL_190">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_190' /></span> <span class='numbering'>1.</span> Heat exchangers opened
					 					 <span>
                                              <img id="Img19" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAHeatExchOpened,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TAHeatExchOpened)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_190.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_190.1' /></span> <span class='numbering'>2.</span> <a href="#" tooltip="A valve integrated into a process control loop used to control flows within the process unit. Include automated block valves with a positioner. Do not include control valves that are integrated into other equipment (e.g., boilers). Include automated block valves with a positioner and automatic shut-off valves.">Control valves</a> overhauled
					 					 <span>
                                              <img id="Img20" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAControlValves,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TAControlValves)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_190.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_190.2' /></span> <span class='numbering'>3.</span> <a href="#" tooltip="A pressure safety valve installed in pressurized systems designed to relieve pressure and avoid a catastrophic failure. Exclude rupture diaphragms and disks, and safety valves installed on specific equipment items such as turbines, boilers, etc.">Pressure relief/safety valves</a> recalibrated
					 					 <span>
                                              <img id="Img21" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAValvesCalibrated,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TAValvesCalibrated)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_190.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_190.3' /></span> <span class='numbering'>4.</span> <a href="#" tooltip="A fired heat induction device used to transfer heat into water or a process fluid. This typically involves a fired chamber (i.e., boiler) with tubes containing the fluid to be heated.">Fired furnaces/boilers</a> repaired/overhauled
					 					 <span>
                                              <img id="Img22" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAFiredFurnaces,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TAFiredFurnaces)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_190.4">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_190.4' /></span> <span class='numbering'>5.</span> <a href="#" tooltip="Equipment used to complete a process such as a chemical reaction in a reactor.">Process vessels</a> opened
					 					 <span>
                                              <img id="Img23" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAValvesOpened,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TAValvesOpened)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_190.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_190.5' /></span> <span class='numbering'>6.</span> <a href="#" tooltip="Columnar equipment used to separate liquid mixtures based on the differences of boiling point. Includes both vacuum and atmospheric units that typically have internal trays or related separation devices.">Distillation towers</a> opened
					 					 <span>
                                              <img id="Img24" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TADistTowersOpened,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TADistTowersOpened)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_190.6">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_190.6' /></span> <span class='numbering'>7.</span> Process pumps (centrifugal and positive displacement) overhauled
					 					 <span>
                                              <img id="Img25" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAPumps,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TAPumps)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_190.7">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_190.7' /></span> <span class='numbering'>8.</span> Process compressors (reciprocating and rotary) overhauled
					 					 <span>
                                              <img id="Img26" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TACompressors,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.TACompressors)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-9">

            <p class="StepTitle"><a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">Turnaround</a> <a href="#" tooltip="The path of associated sub activities that identifies those elements that constrain the total time required to complete the overall activity. For example, in a plant or unit turnaround, these are the combined activities that determine the least amount of time required to complete the turnaround.">Critical Path</a></p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_UREL_200">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_200' /></span> <span class='numbering'>1.</span> Which of the items listed below was responsible for the <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> <a href="#" tooltip="The path of associated sub activities that identifies those elements that constrain the total time required to complete the overall activity. For example, in a plant or unit turnaround, these are the combined activities that determine the least amount of time required to complete the turnaround.">critical path</a> duration?
					 					 <span>
                                              <img id="Img27" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <select id="TACritPath" name="TACritPath" width="30px">
                        <option value='-1'>SELECT ONE</option>
                        <option value='RE' <%= Model.TACritPath != null ?(Model.TACritPath.Equals("RE")? "SELECTED" : String.Empty):String.Empty %>>Rotating equipment inspection/repair</option>
                        <option value='FP' <%= Model.TACritPath != null ?(Model.TACritPath.Equals("FP")? "SELECTED" : String.Empty):String.Empty %>>Fixed equipment (vessels, towers, safety valves, etc.) inspection/repair</option>
                        <option value='IE' <%= Model.TACritPath != null ?(Model.TACritPath.Equals("IE")? "SELECTED" : String.Empty):String.Empty %>>Instrument and Electrical Equipment inspection/repair</option>
                        <option value='CP' <%= Model.TACritPath != null ?(Model.TACritPath.Equals("CP")? "SELECTED" : String.Empty):String.Empty %>>Capital project work</option>
                        <option value='Op' <%= Model.TACritPath != null ?(Model.TACritPath.Equals("Op")? "SELECTED" : String.Empty):String.Empty %>>Operations work (e.g., catalyst changeout)</option>
                        <option value='Oth' <%= Model.TACritPath != null ?(Model.TACritPath.Equals("Oth")? "SELECTED" : String.Empty):String.Empty %>>Other</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.TACritPath)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_200.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_200.1' /></span> <span class='numbering'>2.</span> If other, please specify:
					 					 <span>
                                              <img id="Img28" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TACritPathOth,"TextTmpl",new {size="25"})%> <%: Html.ValidationMessageFor(model => model.TACritPathOth)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <div class="SubSectionTab" id="step-10">

            <p class="StepTitle">Turnaround Management</p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_UREL_300">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_300' /></span> <span class='numbering'>1.</span> How are your Turnarounds managed?
					 					 <span>
                                              <img id="Img29" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <select id="TAManagement" name="TAManagement" width="30px">
                        <option value='-1'>SELECT ONE</option>
                        <option value='Central management group in client organization' <%= Model.TAManagement != null ?(Model.TAManagement.Equals("Central management group in client organization")? "SELECTED" : String.Empty):String.Empty %>>1. Central management group in client organization</option>
                        <option value='Managed locally by unit team' <%= Model.TAManagement != null ?(Model.TAManagement.Equals("Managed locally by unit team")? "SELECTED" : String.Empty):String.Empty %>>2. Managed locally by unit team</option>
                        <option value='Other' <%= Model.TAManagement != null ?(Model.TAManagement.Equals("Other")? "SELECTED" : String.Empty):String.Empty %>>3. Other</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.TAManagement)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_UREL_300.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_300.1' /></span> <span class='numbering'>2.</span> If other, please specify:
					 					 <span>
                                              <img id="Img30" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.TAManagementOth,"TextTmpl",new {size="25"})%> <%: Html.ValidationMessageFor(model => model.TAManagementOth)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
<%--            <%: Html.HiddenFor(model=>model.CompanySID) %>--%>
        </div>
        <div class="SubSectionTab" id="step-11">
            <p class="StepTitle">Turnaround Strategy</p>
            <p><i>At what dates prior to the Turnaround are the following items frozen?</i></p>
            <div class="questionblock" qid="">
                <div id='UT_DT_TA_STG' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>Item</li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100'><span class='numbering'>1.</span> Turnaround strategy developed (planned) </li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100.1'><span class='numbering'>2.</span> Scheduled scope freeze</li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100.2'><span class='numbering'>3.</span> Actual Scope freeze (average of the turnarounds in the study)</li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100.3'><span class='numbering'>4.</span> Main contracts confirmed</li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100.4'><span class='numbering'>5.</span> Budget and duration confirmed</li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100.5'><span class='numbering'>6.</span> Initial Draft of Schedule Completion Planned</li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100.6'><span class='numbering'>7.</span> Initial Draft of Schedule Completion Actual (average of the turnarounds in the study)</li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100.7'><span class='numbering'>8.</span> Major materials procured and on site (planned)</li>
                        <li> <i>Answer as a percent of total scope</i></li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100.8'><span class='numbering'>9.</span> Non Discovery work added after the scope was frozen (%)</li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100.9'><span class='numbering'>10.</span> Discovery work added after scope was frozen (%)</li>
                        <li> <i>Indicate which best describes your work process</i></li>
                        <li class='gridrowtext' qid='QSTID_UDTS_110'><span class='numbering'>11.</span> Do you have a requirement for work that can be done on-line to be excluded from the Turnaround</li>
                        <li class='gridrowtext' qid='QSTID_UDTS_110.1'><span class='numbering'>12.</span> On Average how many main craft work hours per week do you work per turnaround based on 168 hours per week (on the critical path)</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc" style="text-align:center">Months prior to TAR</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100" /><%: Html.EditorFor(model => model.TAStrategyMon_TSD,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.1" /><%: Html.EditorFor(model => model.TAStrategyMon_SSF,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.2" /><%: Html.EditorFor(model => model.TAStrategyMon_ASF,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.3" /><%: Html.EditorFor(model => model.TAStrategyMon_MCC,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.4" /><%: Html.EditorFor(model => model.TAStrategyMon_BDC,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.5" /><%: Html.EditorFor(model => model.TAStrategyMon_IDSP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.6" /><%: Html.EditorFor(model => model.TAStrategyMon_IDSA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.7" /><%: Html.EditorFor(model => model.TAStrategyMon_MMP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.8" /><%: Html.EditorFor(model => model.TAStrategyMon_NDW,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.9" /><%: Html.EditorFor(model => model.TAStrategyMon_DWA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_110" /><br />
                            <select id="TAStrategyWP_RFW" name="TAStrategyWP_RFW" style="width:100px;"><option value='-1'>SELECT ONE</option>
                                <option value='No' <%= Model.TAStrategyWP_RFW != null ?(Model.TAStrategyWP_RFW.Equals("No")? "SELECTED" : String.Empty):String.Empty %>>No</option>
                                <option title="Yes but not applied or not typically done due to safety procedures" value='Yes but not applied or not typically done due to safety procedures' <%= Model.TAStrategyWP_RFW != null ?(Model.TAStrategyWP_RFW.Equals("Yes but not applied or not typically done due to safety procedures")? "SELECTED" : String.Empty):String.Empty %>>Yes but not applied or not typically done due to safety procedures</option>
                                <option title="Yes, but can be waived if justified economically" value='Yes, but can be waived if justified economically' <%= Model.TAStrategyWP_RFW != null ?(Model.TAStrategyWP_RFW.Equals("Yes, but can be waived if justified economically")? "SELECTED" : String.Empty):String.Empty %>>Yes, but can be waived if justified economically</option>
                                <option title="Yes and rigorously applied on technical grounds" value='Yes and rigorously applied on technical grounds' <%= Model.TAStrategyWP_RFW != null ?(Model.TAStrategyWP_RFW.Equals("Yes and rigorously applied on technical grounds")? "SELECTED" : String.Empty):String.Empty %>>Yes and rigorously applied on technical grounds</option>
                                <option title="Yes rigorously applied and audited" value='Yes rigorously applied and audited' <%= Model.TAStrategyWP_RFW != null ?(Model.TAStrategyWP_RFW.Equals("Yes rigorously applied and audited")? "SELECTED" : String.Empty):String.Empty %>>Yes rigorously applied and audited</option>
                            </select></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_110.1" /><%: Html.EditorFor(model => model.TAStrategyWP_MCW,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.TAStrategyMon_TSD)%> 
                <%: Html.ValidationMessageFor(model => model.TAStrategyMon_SSF)%> 
                <%: Html.ValidationMessageFor(model => model.TAStrategyMon_ASF)%> 
                <%: Html.ValidationMessageFor(model => model.TAStrategyMon_MCC)%> 
                <%: Html.ValidationMessageFor(model => model.TAStrategyMon_BDC)%> 
                <%: Html.ValidationMessageFor(model => model.TAStrategyMon_IDSP)%> 
                <%: Html.ValidationMessageFor(model => model.TAStrategyMon_IDSA)%> 
                <%: Html.ValidationMessageFor(model => model.TAStrategyMon_MMP)%> 
                <%: Html.ValidationMessageFor(model => model.TAStrategyMon_NDW)%>
                <%: Html.ValidationMessageFor(model => model.TAStrategyMon_DWA)%> 
                <%: Html.ValidationMessageFor(model => model.TAStrategyWP_RFW)%> 
                <%: Html.ValidationMessageFor(model => model.TAStrategyWP_MCW)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
<%--        <div class="SubSectionTab" id="step-12">
            <p class="StepTitle">Turnaround Work Execution</p>
            <p><i>How is the Turnaround work executed during  the TAR?  (Percent of hours, must total to 100%)</i></p>
            <div class="questionblock" qid="">
                <div id='UT_DT_TA_EXE' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>Delivery</li>
                        <li class='gridrowtext' qid='QSTID_UDTE_100'><span class='numbering'>1.</span> Client technicians </li>
                        <li class='gridrowtext' qid='QSTID_UDTE_100.1'><span class='numbering'>2.</span> Contractors on reimbursable contract</li>
                        <li class='gridrowtext' qid='QSTID_UDTE_100.2'><span class='numbering'>3.</span> Contractors on measured (Unit Rate) contract</li>
                        <li class='gridrowtext' qid='QSTID_UDTE_100.3'><span class='numbering'>4.</span> Contractors on fixed price contract</li>
                        <li class='gridrowtext' qid='QSTID_UDTE_100.4'><span class='numbering'>5.</span> Contractors on alliance or incentivised contract</li>
                        <li class='gridrowtext calctext' qid='QSTID_UDTE_100.5'><span class='numbering'>6.</span> Total</li>
                        <li class='gridrowtext' qid='QSTID_UDTS_100.6'><span class='numbering'>7.</span> If the Turnaround is performed by contractors, do you use the same contractor as for routine maintenance?</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc" style="text-align:center">Percent of Hours</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100" /><%: Html.EditorFor(model => model.TAWKEXEPCT_CT,"DoubleTmpl",new {size="10" ,CalcTag="sumDvryHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.1" /><%: Html.EditorFor(model => model.TAWKEXEPCT_CRC,"DoubleTmpl",new {size="10" ,CalcTag="sumDvryHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.2" /><%: Html.EditorFor(model => model.TAWKEXEPCT_CMC,"DoubleTmpl",new {size="10" ,CalcTag="sumDvryHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.3" /><%: Html.EditorFor(model => model.TAWKEXEPCT_CPC,"DoubleTmpl",new {size="10" ,CalcTag="sumDvryHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.4" /><%: Html.EditorFor(model => model.TAWKEXEPCT_CAC,"DoubleTmpl",new {size="10" ,CalcTag="sumDvryHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.5" /><%: Html.EditorFor(model => model.TAWKEXEPCT_TT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTS_100.6" /><br />
                            <select id="TAWKEXE_RM" name="TAWKEXE_RM" width="30px">
                                <option value='-1'>SELECT ONE</option>
                                <option value='No' <%= Model.TAWKEXE_RM != null ?(Model.TAWKEXE_RM.Equals("No")? "SELECTED" : String.Empty):String.Empty %>>No</option>
                                <option value='Yes' <%= Model.TAWKEXE_RM != null ?(Model.TAWKEXE_RM.Equals("Yes")? "SELECTED" : String.Empty):String.Empty %>>Yes</option>
                                <option value='Mixed' <%= Model.TAWKEXE_RM != null ?(Model.TAWKEXE_RM.Equals("Mixed")? "SELECTED" : String.Empty):String.Empty %>>Mixed</option>
                            </select></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.TAWKEXEPCT_CT)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKEXEPCT_CRC)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKEXEPCT_CMC)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKEXEPCT_CPC)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKEXEPCT_CAC)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKEXEPCT_TT)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKEXE_RM)%> 

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-13">
            <p class="StepTitle">Routine Work Execution</p>
            <p><i>How is routine work executed  (Percent of hours, must total to 100%))</i></p>
            <div class="questionblock" qid="">
                <div id='UT_DT_TA_RT' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>Delivery</li>
                        <li class='gridrowtext' qid='QSTID_UDTR_100'><span class='numbering'>1.</span> Client technicians </li>
                        <li class='gridrowtext' qid='QSTID_UDTR_100.1'><span class='numbering'>2.</span> Contractors on reimbursable contract</li>
                        <li class='gridrowtext' qid='QSTID_UDTR_100.2'><span class='numbering'>3.</span> Contractors on measured (Unit Rate) contract</li>
                        <li class='gridrowtext' qid='QSTID_UDTR_100.3'><span class='numbering'>4.</span> Contractors on fixed price contract</li>
                        <li class='gridrowtext' qid='QSTID_UDTR_100.4'><span class='numbering'>5.</span> Contractors on alliance or incentivised contract</li>
                        <li class='gridrowtext calctext' qid='QSTID_UDTR_100.5'><span class='numbering'>6.</span> Total</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc" style="text-align:center">Percent of Hours</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTR_100" /><%: Html.EditorFor(model => model.TAWKRoutCT_CT,"DoubleTmpl",new {size="10" ,CalcTag="sumRoutineHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTR_100.1" /><%: Html.EditorFor(model => model.TAWKRoutCT_CRC,"DoubleTmpl",new {size="10" ,CalcTag="sumRoutineHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTR_100.2" /><%: Html.EditorFor(model => model.TAWKRoutCT_CMC,"DoubleTmpl",new {size="10" ,CalcTag="sumRoutineHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTR_100.3" /><%: Html.EditorFor(model => model.TAWKRoutCT_CPC,"DoubleTmpl",new {size="10" ,CalcTag="sumRoutineHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTR_100.4" /><%: Html.EditorFor(model => model.TAWKRoutCT_CAC,"DoubleTmpl",new {size="10" ,CalcTag="sumRoutineHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UDTR_100.5" /><%: Html.EditorFor(model => model.TAWKRoutCT_TT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.TAWKRoutCT_CT)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKRoutCT_CRC)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKRoutCT_CMC)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKRoutCT_CPC)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKRoutCT_CAC)%> 
                <%: Html.ValidationMessageFor(model => model.TAWKRoutCT_TT)%> 

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
                        <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>--%>
        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>



<script type = "text/javascript" > 
    function loadHelpReferences() {
    var objHelpRefs = Section_HelpLst;
    for (i = objHelpRefs.length - 1; i >= 0; i--) {
        var qidNm = objHelpRefs[i]['HelpID'];
        if ($('li[qid^="' + qidNm + '"]').length) {
            var $elm = $('li[qid^="' + qidNm + '"]');
            $elm.append('<img id="Img31" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');
            $elm.find('img.tooltipBtn').css('visibility', 'visible');
        } else if ($('div[qid^="' + qidNm + '"]').length) {
            $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');
        }
    }
}

function formatResult(el, value) {
    var format = 'N';
    if (el.attr('NumFormat') == 'Int') format = 'n0';
    if (el.attr('DecPlaces') != null) {
        if (el.attr('NumFormat') == 'Number') format = 'n' + el.attr('DecPlaces');
    }
    if (value != undefined && isNaN(value)) value = 0;
    var tempval = Globalize.parseFloat(value.toString(), 'en');
    if (isFinite(tempval)) {
        el.val(Globalize.format(tempval, format));
        var idnm = '#_' + el.attr('id');
        var $f = $(idnm);
        if ($f.length) {
            $f.val(el.val());
        }
    } else {
        el.val('');
    }
}
$("input[CalcTag*='avgTADays']").sum({
    bind: "keyup",
    selector: "#TAAvgHrsDown",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#TAAvgHrsDown').attr('readonly', 'readonly');
$('#TAAvgHrsDown').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumAnnDt']").sum({
    bind: "keyup",
    selector: "#LossHrsTotalAnnProdLoss",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#LossHrsTotalAnnProdLoss').attr('readonly', 'readonly');
$('#LossHrsTotalAnnProdLoss').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumSchShrt']").sum({
    bind: "keyup",
    selector: "#ShortOHCausePcnt_Tot",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#ShortOHCausePcnt_Tot').attr('readonly', 'readonly');
$('#ShortOHCausePcnt_Tot').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumSchTA']").sum({
    bind: "keyup",
    selector: "#TACausePcnt_Tot",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#TACausePcnt_Tot').attr('readonly', 'readonly');
$('#TACausePcnt_Tot').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumUnschOut']").sum({
    bind: "keyup",
    selector: "#StoppagePcnt_Tot",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#StoppagePcnt_Tot').attr('readonly', 'readonly');
$('#StoppagePcnt_Tot').change(function() {
    $(this).validate();
});
$("input[CalcTag*='avgTADays'],#TAAvgInterval ").bind("keyup", function() {
    var $result = $("#TAAvgAnnHrsDown");
    $result.calc("v1/v2*12", {
        v1: $("#TAAvgHrsDown"),
        v2: $("#TAAvgInterval")
    });
    formatResult($result, $result.val()); /*$("#TAAvgAnnHrsDown").trigger('blur');*/
});
if ($("#TAAvgHrsDown").val() != null && !isNaN($("#TAAvgHrsDown").val()))
    if ($("#TAAvgInterval").val() != null && !isNaN($("#TAAvgInterval").val())) {
        $("#TAAvgHrsDown").trigger('keyup');
    } $("#ShortOHAvgHrsDown,#ShortOH2YrCnt ").bind("keyup", function() {
    var $result = $("#ShortOHAnnHrsDown");
    $result.calc("v1*v2/2", {
        v1: $("#ShortOHAvgHrsDown"),
        v2: $("#ShortOH2YrCnt")
    });
    formatResult($result, $result.val()); /*$("#ShortOHAnnHrsDown").trigger('blur');*/
});
if ($("#ShortOHAvgHrsDown").val() != null && !isNaN($("#ShortOHAvgHrsDown").val()))
    if ($("#ShortOH2YrCnt").val() != null && !isNaN($("#ShortOH2YrCnt").val())) {
        $("#ShortOHAvgHrsDown").trigger('keyup');
    } $("input[CalcTag*='sumRRRAM']").sum({
    bind: "keyup",
    selector: "#RAMCausePcnt_Tot",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#RAMCausePcnt_Tot').attr('readonly', 'readonly');
$('#RAMCausePcnt_Tot').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumDvryHrs']").sum({
        bind: "keyup",
        selector: "#TAWKEXEPCT_TT",
        oncalc: function (value, settings) {
            if (value != undefined && !isNaN(value)) {
                formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
            }
        }
    });
$('#TAWKEXEPCT_TT').attr('readonly', 'readonly');
$('#TAWKEXEPCT_TT').change(function () {
    $(this).validate();
});

$("input[CalcTag*='sumRoutineHrs']").sum({
    bind: "keyup",
    selector: "#TAWKRoutCT_TT",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#TAWKRoutCT_TT').attr('readonly', 'readonly');
$('#TAWKRoutCT_TT').change(function () {
    $(this).validate();
});
var secComments = Section_Comments;
var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';
for (i = secComments.length - 1; i >= 0; i--) {
    var qidNm = secComments[i]['CommentID'];
    if ($('img[qid="' + qidNm + '"]').length) {
        $('img[qid="' + qidNm + '"]').attr('src', redCmtImg);
    }
} <%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %> $('.rowdesc li div').prepend(function(index, html) {
    var gridId = $(this).closest('.grid').attr('id');
    if ($.trim(html).indexOf('nbsp', 0) == -1) {
        var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>';
    }
    $(this).html(newHTML);
});
$('img[suid]').click(function() {
    var suid = $(this).attr('suid');
    var qid = $(this).attr('qid');
    recordComment(suid, qid);
}); 

</script>
</asp:Content>
