﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="RAMAPP_Mvc3.Repositories.Cache" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%@ Import Namespace="RAMAPP_Mvc3.Navigation" %>
<!-- start navigation -->
<%
       
    var nav = new NavigationTree();
    string sitesUnitsMenu =  nav.GetNavigation(NavigationTree.NavigationType.Survey);
   
    
    /*
    var myList = new StringBuilder();
    var cachedNavigation = new CachedNavigationRepository();
    var sites = cachedNavigation.GetSiteUnitList();
    myList.Append("<ul id='ulOuter'>");


    foreach (var parent in sites)
    {



        myList.Append("<li class=' rightPointerNav'><strong>" +
            Html.ActionLink(parent.LockedImg + parent.SiteName, "Index", new { id = parent.DatasetID, sectiontype = "site" }, new { title = "Site Study Questions" }));
        myList.Append("</strong><div class='sep'>&nbsp;</div><div class='unitsContainer'><ul>");


        List<Units> childUnits = (List<Units>)parent.UnitsOfSite;
        if (childUnits.Count == 0)
        {
            myList.Append("<li>Please add units</li>");
        }
        else
        {
            myList.Append("<li class='subHeader'> UNITS </li>");
            foreach (var child in childUnits)
            {
               

                myList.Append("<li> " + Html.ActionLink(parent.LockedImg + child.UnitName, "Index", new { id = child.DatasetID, sectiontype = "unit", onclick = "sideMenuClick();" }, new { title = "Unit Study Questions" }) + "</li>");
                

            }

        }
        myList.Append("</ul></div></li> ");
    }

    myList.Append("</ul>");


    sitesUnitsMenu = myList.Replace("#lockimg#", "<img border=0 src=\"" + Url.Content("~/images/lockedsym.png") + "\" alt='' />").ToString();
            
   */
%>
<div style="text-align: center; background: #991000; height: 35px; width: 300px;">
    <h5 style="color: white; padding-top: 6px;">
        SITE AND UNIT SURVEY QUESTIONS</h5>
</div>
<p>
    <div style="padding: 5px; width: 250px; margin: 0 20px;">
        <img id="Img1" src="~/images/info.png" align="top" style="margin: 0 5px; display: inline-block;"
            runat="server" /><span style="display: inline-block; font-size: .90em; width: 200px;">Click
                site or unit name to open the questionnaire and answer the questions. </span>
    </div>
    <%: Html.Raw(sitesUnitsMenu.Length == 0 ? " Please add sites" : sitesUnitsMenu)%>
</p>
<!-- end navigation -->
