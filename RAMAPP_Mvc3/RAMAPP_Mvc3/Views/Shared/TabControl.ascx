﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<%if (ViewContext.RouteData.Values["id"] != null && Regex.IsMatch(Convert.ToString(ViewContext.RouteData.Values["id"]), @"^\d+") && Request["sectiontype"] != null)
  { %>

  
<div id="tabContainer" style="min-width:1200px; max-width:1400px;">

    <ul>
    <% if (Request["sectiontype"] == "corporate")
       {%>  
        <li>
            <%= Html.Raw(Html.ActionLink("Company spin", "CORP", new { id = Int16.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Company" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>
    <%} %>


    <% if (Request["sectiontype"]=="site") {%>  
        <li>
            <%= Html.Raw(Html.ActionLink("Identification spin", "ST_IDENT","Site", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Identification" }).ToHtmlString().Replace("spin","<span>&nbsp;</span>"))%>
        </li>
        <li>
            <%= Html.Raw(Html.ActionLink("Characteristics spin", "ST_CHAR", "Site", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Characteristics" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>  
        <li>
            <%= Html.Raw(Html.ActionLink("Costs spin", "ST_COST", "Site", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Cost" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li> 
        <li>
            <%= Html.Raw(Html.ActionLink("General spin", "ST_MAINT", "Site", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Maintenance" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>
      
        <li>
            <%= Html.Raw(Html.ActionLink("Craft Characteristics spin", "ST_CRAFTCHAR", "Site", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Work Characteristics" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li> 
        <li>
            <%= Html.Raw(Html.ActionLink("MRO Inventory spin", "ST_INVEN", "Site", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Spare Inventory" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>
        <li>
            <%= Html.Raw(Html.ActionLink("Organization spin", "ST_ORG", "Site", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Organization" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>
         <li>
            <%= Html.Raw(Html.ActionLink("Work Processes spin", "ST_PROCESS", "Site", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Work Processes" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>  
        <li>
            <%= Html.Raw(Html.ActionLink("Reliability spin", "ST_REL", "Site", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Reliability" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>
<%--          <li>
            <%= Html.Raw(Html.ActionLink("Site Turnarounds spin", "ST_MT_TA", "Site", new { id = Int16.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Maintenance Turnaround" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>--%>
        <li>
            <%= Html.Raw(Html.ActionLink("Study spin", "ST_STUDY", "Site", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Site Study" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>
        
         
       
       
       
    <%} %>


    <% if (Request["sectiontype"]=="unit") {%>  
        <li>
            <%= Html.Raw(Html.ActionLink("Characteristics spin", "UT_CHAR","Unit", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Unit Characteristics" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li> 
        <li>
            <%= Html.Raw(Html.ActionLink("Downtime spin", "UT_DT", "Unit", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Unit Downtime" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>
        <li>
            <%= Html.Raw(Html.ActionLink("Reliability spin", "UT_REL", "Unit", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Unit Reliability" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>
          <li>
            <%= Html.Raw(Html.ActionLink("Costs spin", "UT_COST", "Unit", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Unit Costs" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>
         
         <li>
            <%= Html.Raw(Html.ActionLink("Work Types spin", "UT_PROCESS", "Unit", new { id = int.Parse(ViewContext.RouteData.GetRequiredString("id")) }, new { title = "Unit Work Types" }).ToHtmlString().Replace("spin", "<span>&nbsp;</span>"))%>
        </li>
         
         
    <%} %>
    </ul>
    
</div>
<%} %>
