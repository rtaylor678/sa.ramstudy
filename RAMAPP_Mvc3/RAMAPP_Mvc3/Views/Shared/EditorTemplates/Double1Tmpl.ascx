﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<float?>" %>
<% this.ViewData.Add("style","text-align:right");
   System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CurrentCulture;
   ci.NumberFormat.NumberDecimalDigits = 1;
    %>
<%: Html.TextBox("", String.Format(ci,"{0:N}", Model),  this.ViewData.ToDictionary(c=>c.Key,c=>c.Value))%> 
