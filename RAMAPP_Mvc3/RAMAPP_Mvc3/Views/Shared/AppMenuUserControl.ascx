﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%
    var isAdmin = SessionUtil.IsAdmin();
    var isSiteAdmin = SessionUtil.IsSiteAdmin();
%>
 <script src="<%= Page.ResolveUrl("~/Scripts/clearbox.js") %>" type="text/javascript"></script>
<div class="dd_menu">
    <ul>
        <li><a href="<%= Url.RouteUrl(new{action="Index",controller="App"}) %>">Home</a></li>
        <%
            if (isAdmin || isSiteAdmin)
            {  %>
        <li><a href="<%=Url.RouteUrl(new {action="Index",controller="Admin"})%>">Administration</a></li>
        <% }
        %>
         <% if (Request["sectiontype"] == null)
           {  %>
        <!--<li><a href="#">Download</a>
            <ul>
                <li><a href="<%=Url.Content("~/Downloads/RAM Input 2013.xls")%>">Blank Input Spreadsheet</a></li>
                <li><a href="#">Entered Data (not available)</a></li>
            </ul>
        </li>-->
         <%} %>
        <% if (Request["sectiontype"] != null)
           {  %>
        <li><a href="#" onclick="SaveTabInput();return false;">Save</a></li>
        <%} %>
<%--        <li><a href="#">Training Modules</a>
            <ul>
             <li>
                          <a href="<%: Url.Content("~/Home/VideoPres?vid=0") %>"  title="Module 1 - Web Portal Administrator"   rel="clearbox[,,width=800,,height=580]">Module 1 - Web Portal Admin</a>
			
                        </li>
                        <li>
                          <a href="<%: Url.Content("~/Home/VideoPres?vid=1") %>"  title="Module 2 (Part 1) - Introduction"   rel="clearbox[,,width=800,,height=580]">Module 2 (Part 1) - Introduction</a> 
                        </li>
                        <li>
                         <a href="<%: Url.Content("~/Home/VideoPres?vid=2") %>"   title="Module 2 (Part 2) - Introduction"   rel="clearbox[,,width=800,,height=580]">Module 2 (Part 2) - Introduction</a> 
                        </li>
                        <li>
                         <a href="<%: Url.Content("~/Home/VideoPres?vid=3") %>"   title="Module 3 - Site & Unit Characteristics"   rel="clearbox[,,width=800,,height=580]">Module 3 - Site & Unit Characteristics</a> 
                        </li>
                          <li>
                         <a href="<%: Url.Content("~/Home/VideoPres?vid=4") %>"   title="Module 4 (Part 1) - Maintenance Costs"   rel="clearbox[,,width=800,,height=580]">Module 4 (Part 1) - Maintenance Costs</a> 
                        </li>

                        <li>
                         <a href="<%: Url.Content("~/Home/VideoPres?vid=5") %>"   title="Module 4 (Part 2) - Maintenance Capital"   rel="clearbox[,,width=800,,height=580]">Module 4 (Part 2) - Maintenance Capital</a> 
                        </li>

                        <li>
                         <a href="<%: Url.Content("~/Home/VideoPres?vid=6") %>"   title="Module 5 - Organization"   rel="clearbox[,,width=800,,height=580]">Module 5 - Organization</a> 
                        </li>
                         <li>
                         <a href="<%: Url.Content("~/Home/VideoPres?vid=7") %>"   title="Module 6 - MRO Inventory"   rel="clearbox[,,width=800,,height=580]">Module 6 - MRO Inventory</a> 
                        </li>
                         <li>
                         <a href="<%: Url.Content("~/Home/VideoPres?vid=8") %>"   title="Module 7 - Work Processes"   rel="clearbox[,,width=800,,height=580]">Module 7 - Work Processes</a> 
                        </li>
                          <li>
                         <a href="<%: Url.Content("~/Home/VideoPres?vid=9") %>"   title="Module 8 - Reliability"   rel="clearbox[,,width=800,,height=580]">Module 8 - Reliability</a> 
                        </li>
                         <li>
                         <a href="<%: Url.Content("~/Home/VideoPres?vid=10") %>"   title="Module 9 - Input Data Review"   rel="clearbox[,,width=800,height=580]">Module 9 - Input Data Review</a> 
                        </li>



            
            </ul>
        </li>--%>
        
        <li><a href="#">Help</a>
            <ul>
              
                <li><a href="<%=Url.Content("~/Downloads/RAM Study Glossary.pdf")%>" target="_blank">
                    Glossary</a></li>
                <li><a href="<%=Url.Content("~/Downloads/RAM Web Data Input Guide w Data Review Instructions.pdf") %>" target="_blank">
                    Web Portal Guide</a></li>
                <li><a href="<%=Url.Content("~/Downloads/RAM Instruction Guide for Data Input Spreadsheets.pdf") %>" target="_blank">
		    Input Data Guide</a></li>
                <%--<li><a href="<%=Url.Content("~/Downloads/RAM Study Schedule.pdf") %>" target="_blank">
                    Study Schedule</a></li>--%>
                <%--<li><a href="<%=Url.Content("~/Downloads/2011RAMStudyDataRequirements.pdf") %>" target="_blank">
                    Data Requirements Summary</a></li>--%>
               <%-- <li><a href="<%=Url.Content("~/Downloads/2011RAMStudyResourceRecommendations.pdf") %>"
                    target="_blank">Data Resource Recommendations</a></li>--%>
               <%-- <li><a href="<%=Url.Content("~/Downloads/MC Decision Tree Video Slides.pdf") %>" target="_blank">
                    Maintenance Capital Slides</a></li>--%>
                <%--<li><a href="http://solomononline.com/documents/RAM_Study_Portal/2011_Chem_Proc_Fam.xls"
                    target="_blank">Chemical Process Families</a></li>--%>
                <!--<li><a href="<%=Url.Content("~/Downloads/RAM Study Data Coordinator Webinar.pdf") %>"
                    target="_blank">2013 RAM Study Webinar Slides</a></li>-->
                <%--<li><a href="<%=Url.Content("~/Downloads/RAM Participant FAQs.pdf") %>" target="_blank">
                    Participant's FAQS</a></li>--%>
            </ul>
        </li>
        <% 
           
            var  admintst =  Session["Mode"]!= null ? Session["Mode"].ToString().Equals(SessionUtil.AdminKey()) : false;
            
           if(true)
            { 
        %>
            <li><a href="<%=Url.RouteUrl(new {action="DataInputReviewSummary",controller="Review"})%>">Input Data Review</a></li>
        
       <%  } %>
    </ul>
</div>
