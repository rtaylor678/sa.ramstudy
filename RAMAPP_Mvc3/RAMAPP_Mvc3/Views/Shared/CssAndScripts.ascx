﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CssAndScripts.ascx.cs" Inherits="RAMAPP_Mvc3.Views.Shared.CssAndScripts" %>
<%@ OutputCache Duration="120" VaryByParam="None"%>


     <link media="print" href="../../Content/print.css" rel="stylesheet" type="text/css" />
    <link media="all" href="~/Content/sIFR/all.css" type="text/css" rel="stylesheet" />
    <%-- <link media="screen" href="~/Content/sIFR/sIFR-screen.css" type="text/css" rel="stylesheet" />--%>
    <link media="screen" href="~/Content/fluid_grid.css" rel="stylesheet" type="text/css" />
    <link href="~/Content/sa_style.css" rel="stylesheet" type="text/css" />
    <%-- <link media="screen" href="~/Content/content.css" rel="stylesheet" />--%>
    <link media="screen" href="../../Content/jquery-ui-1.8.15.custom.css" type="text/css"
        rel="stylesheet" />
    <link media="screen" href="~/Content/confirm.css" rel="stylesheet" type="text/css" />
    <link media="screen" href="~/Content/flyoutmenu.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/Site.css" rel="stylesheet" type="text/css" />
    <link media="screen" href="~/Content/jquery.countdown.css" rel="stylesheet" type="text/css" />
    <link media="screen" href="../../Content/smart_tab_vertical.css" rel="stylesheet"
        type="text/css" />
    <link media="screen" href="../../Content/sidemenu.css" rel="stylesheet" type="text/css" />
    <link media="screen" href="../../Content/LayoutSection.css" rel="stylesheet" type="text/css" />
    <link media="screen" href="../../Content/submenu.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/jquery.qtip.min.css" rel="stylesheet" type="text/css" />