﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%
    
    var sitemenu = new StringBuilder();

    string companyId = "";
    const string sitetemplate = "<li><a href=\"/Admin/SiteDetail/{0}\">{1}</a>";
    const string approvaltemplate = "<li><a href=\"/Admin/ApproveData/{0}\"><img border=\"0\" src=\"http://cdn4.iconfinder.com/data/icons/checkout-icons/16x16/tick.png\"  alt=\"approval\" align=\"middle\"/>Approve</a></li>";
    const string unittemplate = "<li><a href=\"/Admin/UnitDetails/{0}\">{1}</a></li>";
    
    var userinfo = (RAMAPP_Mvc3.Entity.LogIn)HttpContext.Current.Session["UserInfo"];
    var companySID = SessionUtil.GetCompanySID();
    var isAdmin = SessionUtil.IsAdmin();
    var isSiteAdmin = SessionUtil.IsSiteAdmin();
    
    using (var db = new RAMAPP_Mvc3.Entity.RAMEntities())
    {


        if (userinfo != null)
        {
            //companyId = userinfo.CompanyID;
            var sites = (from s in db.SiteInfo
                         where s.CompanySID  == companySID &&
                         (isAdmin || db.SitePermissions.Any(p => p.UserID == userinfo.UserID && p.DatasetID == s.DatasetID && p.SecurityLevel==1))
                         select new
                         {
                             s.SiteName,
                             s.DatasetID,
                             units = (from u in db.UnitInfo
                                      where u.SiteDatasetID == s.DatasetID  
                                      select new { u.UnitName, u.DatasetID})
                         }).ToList();



            sitemenu.Append("<ul id=\"sidenav\">");
           
            foreach (var parent in sites)
            {
                sitemenu.Append(String.Format(sitetemplate, parent.DatasetID, parent.SiteName));
                sitemenu.Append("<ul>");
                sitemenu.Append(String.Format(approvaltemplate, parent.DatasetID));
                if (parent.units.ToList().Count == 0)
                {
                    //sitemenu.Append("<dd> No units found.</dd>");
                }
                else
                {
                   
                    sitemenu.Append("<li><a href='#'>Units</a><ul>");
                    
                    foreach (var child in parent.units.ToList())
                    {
                        if (child != null)
                            sitemenu.Append(String.Format(unittemplate, child.DatasetID, child.UnitName));
                    }
                    sitemenu.Append("</ul></li>");
                }
                sitemenu.Append("</ul>");
                sitemenu.Append("</li>");
            }

            sitemenu.Append("</ul>");
        }

    }

  
%>
<%--<link href="../../Content/jquery-ui-1.8.15.custom.css" rel="stylesheet" type="text/css" />
<script src="../../Scripts/jquery-1.6.1.min.js" type="text/javascript"></script>
<script src="../../Scripts/jquery-ui-1.8.15.custom.min.js" type="text/javascript"></script>--%>
<fieldset>
    <legend>Available Sites</legend>
    <div class="sidebarmenu">
        <%: Html.Raw(sitemenu.ToString().Length == 0 ? " No sites were found." : sitemenu.ToString())%>
    </div>
</fieldset>
<fieldset>
    <legend>Tools</legend>
    <% if (isAdmin)
       {  %>
    <p>
        <%: Html.ActionLink("Create Site", "SiteDetail","Admin") %></p>
    <% } %>
    <% if (isAdmin||isSiteAdmin)
       {%>
    <p>
        <%: Html.ActionLink("Create User", "CreateUser","Admin", new { id = companySID }) %></p>
    <% } %>
</fieldset>
