﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_DataCheck.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="display: block; position: absolute; left: 800px; color: #ccc; font-size: .85em;">
        <%: Html.ActionLink("Back to Facility Grades", "DataInputReviewSummary", "Admin")%>
    </div>
    <br />
    <p class="headerIntro">
        Input Review Details : <span style="color:#444;"><%: Session["ValidateFacility"]%></span></p>
    <div class="paging">
        <div class="grid_13">
           Page: <%= Html.Raw(  TempData["PageNav"].ToString()) %></div>
        <div class="grid_3">
        
            Page Size:
            <select id="pgSize" name="pgSize" onchange="window.location.href='<%= Url.RouteUrl(new {action="ViewGradeDetails",controller="Admin", id = this.RouteData.Values["id"], start = Convert.ToInt32(Request["start"]) }) %>&size=' + this.value">
               
                <option value='10'>10</option>
                <option value='20' selected="selected">20</option>
                <option value='50'>50</option>
                <option value='100'>100</option>
            </select>
            
        </div>
    </div>
    
    <br />
    <div>
<br />
        <%= Html.Raw(TempData["GradeDetails"].ToString()) %></div>
    <div class="paging">
       Page: <%= Html.Raw(  TempData["PageNav"].ToString()) %>
    </div>
    <script type="text/javascript">
        var min = Math.min;
        var max = Math.max;
        var round = Math.round;

        function get_color_for_height(startColor, endColor, height, row) {
            var scale = row / height;
            var r = startColor['red'] + scale * (endColor['red'] - startColor['red']);
            var b = startColor['blue'] + scale * (endColor['blue'] - startColor['blue']);
            var g = startColor['green'] + scale * (endColor['green'] - startColor['green']);

            return {
                r: round(min(255, max(0, r))),
                g: round(min(255, max(0, g))),
                b: round(min(255, max(0, b)))
            }
        }


       $(document).ready(function(){
    
        $('.gridHorizontal li').click(function(){
          
          if ($(this).hasClass("downPointerNav"))
            $.cookie("lastrowclicked",$(this).attr("id") );

        });
        
        if ($.cookie("lastrowclicked")!=null ){
           var target = '#'+$.cookie("lastrowclicked");
           $(target).focus();
           $(target).click();

         }

        $("ul.gridbody").each(function () {
            var scale = $(this).attr("scale");
            var color = null;
            if (scale != undefined) {

                if (scale <= 100 && scale > 75)
                    color = get_color_for_height({ red: 235, green: 255, blue: 100 }, { red: 0, green: 255, blue: 60 }, 100, scale);
                if (scale <= 75 && scale > 50)
                    color = get_color_for_height({ red: 255, green: 255, blue: 0 }, { red: 255, green: 255, blue: 100 }, 100, scale);
                if (scale <= 50 && scale > 25)
                    color = get_color_for_height({ red: 255, green: 69, blue: 0 }, { red: 255, green: 255, blue: 0 }, 100, scale);
                if (scale <= 25)
                    color = get_color_for_height({ red: 255, green: 98, blue: 98 }, { red: 255, green: 69, blue: 0 }, 100, scale);

                $(this).find('li:nth-child(4)').css('background-color', 'rgb(' + color['r'] + ',' + color['g'] + ',' + color['b'] + ')', 'background-color', 'rgba(' + color['r'] + ',' + color['g'] + ',' + color['b'] + ',0.60)');
                var fontsize = round(20 * (color['r'] + color['g'] + color['b']) / (3 * 255));

                // $(this).find('li:nth-child(4)').css('font-size', (25 - fontsize) + 'pt');
            }
        });

        <% if (Session["PageSize"]!=null){ %>
             $('#pgSize').val('<%= Session["PageSize"] %>');
        <% } %>
    
      var currentpage = <%= Convert.ToInt32(Request["start"])/Convert.ToInt32(Request["size"]) %> +2;
      $('.paging').find('span:nth-child('+ currentpage +') a').addClass('active');
     
      });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .headerIntro
        {
            font-family: 'Arial';
            font-size: 16px;
            color: #991000;
            text-transform: capitalize;
            border-bottom: 1px solid #ccc;
            min-width: 200px;
            max-width: 800px;
            padding-left: 0px;
            margin-top: 0px;
            text-align: left;
            height: 20px;
            vertical-align: bottom;
            text-transform: capitalize;
        }
        .gridHorizontal
        {
            margin: 5px 0;
            
        }
        
        .gridHorizontal .gridrowtext
        {
            text-align: left;
            width: 300px !important;
        }
        .gridHorizontal .columndesc
        {
            text-align: center;
        }
        .gridHorizontal ul li
        {
            font-size: .90em;
            min-height: 40px;
        }
        .gridHorizontal li div.unitsContainer
        {
            margin: 0 !important;
        }
        ul.summaryDesc
        {
            list-style: none;
            padding-left: 10px;
            text-align: left;
            width: 895px;
            margin: 0 !important;
        }
        ul.summaryDesc li
        {
            padding: 0;
            text-align: left;
        }
        .summaryContainer
        {
            background-color:wheat;
        }
        .checkMsg
        {
            text-indent: 3;
            display: block;
            width: 700;
            padding-bottom: 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>

