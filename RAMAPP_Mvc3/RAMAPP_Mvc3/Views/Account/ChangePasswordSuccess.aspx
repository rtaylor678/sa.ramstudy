﻿<%@Language="C#" MasterPageFile="~/Views/Shared/RAM_Profile.Master"  Inherits="System.Web.Mvc.ViewPage" %>


<asp:Content ID="changePasswordSuccessContent" ContentPlaceHolderID="MainContent" runat="server">
<div style="margin:auto auto;height:450px;">
    <h2>Change Password</h2>
    <p>&nbsp;</p><p>&nbsp;</p>
    <p>
        Your password has been changed.
    </p>
</div>
   <img src="~/images/arrow-return-180.png" runat="server" border="0" alt="return" align="middle"/> <%: Html.ActionLink("Return To Profile","Index","Profile") %>
</asp:Content>
