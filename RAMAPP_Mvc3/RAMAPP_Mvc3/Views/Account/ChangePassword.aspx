﻿<%@ Page Language="C#" MasterPageFile="../Shared/RAM_Profile.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ChangePasswordModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

 <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="changePasswordContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Change Password</h2>
    <p>
        Use the form below to change your password. 
    </p>
    <p>
        New passwords are required to be a minimum of <%= Html.Encode(ViewData["PasswordLength"]) %> characters in length.
    </p>

    <% using (Html.BeginForm()) { %>
        <%= Html.ValidationSummary(true, "Password change was unsuccessful. Please correct the errors and try again.") %>
        <div>
            <fieldset style="width:650px;">
                <legend>Enter New Password</legend>
                
                <div class="editor-label">
                    <%= Html.LabelFor(m => m.OldPassword) %>
                </div>
                <div class="editor-field">
                    <%= Html.PasswordFor(m => m.OldPassword) %>
                    <%= Html.ValidationMessageFor(m => m.OldPassword) %>
                </div>
                
                <div class="editor-label">
                    <%= Html.LabelFor(m => m.NewPassword) %>
                </div>
                <div class="editor-field">
                    <%= Html.PasswordFor(m => m.NewPassword) %>
                    <%= Html.ValidationMessageFor(m => m.NewPassword) %>
                </div>
                
                <div class="editor-label">
                    <%= Html.LabelFor(m => m.ConfirmPassword) %>
                </div>
                <div class="editor-field">
                    <%= Html.PasswordFor(m => m.ConfirmPassword) %>
                    <%= Html.ValidationMessageFor(m => m.ConfirmPassword) %>
                </div>
                
                <p>
                    <input type="submit" value="Change" />
                </p>
            </fieldset>
        </div>
    <% } %>

   <img class="returnLink" src="~/images/arrow-return-180.png" runat="server" border="0" alt="return" align="middle"/> <a href="javascript:history.go(-1)">Back</a>

  
</asp:Content>
