﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register Src="~/Views/Shared/SAIAdminMenuUserControl.ascx" TagName="SAIMenu"
    TagPrefix="uc" %>
<%@ Import Namespace="RAMAPP_Mvc3.Entity" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="head" align="center">
        RAM Study Administration Console
    </div>
    <br />
    <div id="box">
        <br />
        <ul style="list-style:square; margin:5% 25%;">
            <%
                var userinfo = (LogIn)Session["UserInfo"];
                if (userinfo.SecurityLevel < 32000)
                {
                    Response.Redirect(Page.ResolveUrl("~/Account/LogOff"));
                } 
                if (userinfo.SecurityLevel > 32000)
                { %>
            <li>
                <%: Html.ActionLink("Create a Company", "CreateCompany")%></li>
            <li>
                <%: Html.ActionLink("Create Data Coordinator", "AddCompanyCoordinator")%></li>
            <li>
                <%: Html.ActionLink("Assign New Coordinator", "AssignCoordinator")%></li>
            <%
                }
            %>
            <li>
                <%: Html.ActionLink("Companies in Study", "ViewCompanies")%></li>
            <li>
                <a href="<%= Page.ResolveUrl("~/Reports/ODRReport.aspx") %>">ODR Matrix Report</a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%= Page.ResolveUrl("~/Content/menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style type="text/css">
        #head
        {
            margin: 35% 15px;
            font-size: 24px;
            color: #991000;
            margin: auto auto;
            padding: auto 20px;
        }
        
        #box
        {
            margin: auto auto;
            padding: auto 20px;
            background-color: cornsilk;
            height: 100%;
            width: 55%;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
            border-radius: 12px;
            min-height: 300px;
            border: wheat 2px solid;
            behavior: url(PIE.htc);
        }
        #box ul
        {
            margin: 30px auto;
            padding: auto auto;
            list-style: square;
            line-height: 2.5;
            font-size: 14px;
            color: #800000;
        }
        #onlineUsr
        {
            font-size: .85em;
            color: #800000;
            border-right: 2px #eee solid;
            line-height: 1.5;
            width: 285px;
            min-height: 500px;
        }
        #onlineUsr ul
        {
            list-style: none;
            line-height: 1.5;
            color: #800000;
            width: 85%;
        }
        
        #onlineUsr ul li
        {
            display: inline-block;
        }
        .msal
        {
            font: .80em Verdana, Arial, Helvetica, sans-serif;
            color: #555;
        }
        .num
        {
            position: relative;
            float: left;
            max-width: 40px;
            padding-left: 2px;
            padding-top: 0;
            font: bold 25px Verdana, Arial, Helvetica, sans-serif;
        }
        .user
        {
            position: relative;
            float: left;
            text-align: left;
            padding-bottom: 5px;
            padding-top: 3px;
            padding-left: 5px;
            width: 78%;
        }
        
        div.navWindow, div.btnNavWindow
        {
            visibility: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
    <div id="onlineUsr">
        <div style="font-size: 15px; border-top: solid thin #eee; border-bottom: solid thin #eee;
            margin: auto auto; padding: 10px 0;">
            <div align="center">
                Users Online :
                <%: SessionChecker.GetOnlineUsersCount() %>
            </div>
        </div>
        <%  
            var dictControl = HttpRuntime.Cache["UsersCountSession"] as Dictionary<int, DateTime>;
            int cnt = 1;
            using (var db = new RAMEntities())
            {
                var cols = (from uo in dictControl.Keys
                            join l in db.LogIn on uo equals l.UserNo
                            join c in db.Company_LU on l.CompanyID equals c.CompanyID
                            where c.StudyYear == db.Company_LU.Max(y => y.StudyYear)
                            select new { c.CompanyName, l.FirstName, l.LastName, LoginTime = dictControl[uo], OnlineTime = (DateTime.Now - dictControl[uo]).Minutes }).OrderBy(t => t.LoginTime).ToList();
                   
        %>
        <ul>
            <% foreach (var i in cols)
               {  %>
            <li>
                <div class="num">
                    <%= cnt++ %></div>
                <div class="user">
                    <%= i.LastName + " ," + i.FirstName%><br />
                    <span class="msal">
                        <%=  i.CompanyName %></span><br />
                    <span class="msal">
                        <%=  i.LoginTime.ToString("MM/dd/yyyy h:mm tt")  + " " + i.OnlineTime +"min" %></span>
                </div>
            </li>
            <%}
            }%>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="RightContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc:SAIMenu ID="saiMenu" runat="server" />
</asp:Content>
