﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master"
    Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UserDetails>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h5>
        AssignCoordinator</h5>
    <% using (Html.BeginForm("AddCompanyCoordinator"))
       {
           using (var db = new RAMAPP_Mvc3.Entity.RAMEntities())
           {
               var slt = (from g in db.Company_LU
                          where g.StudyYear == 2011 && !g.CompanySID.ToUpper().Contains("TEST") 
                          select new SelectListItem() { Text = g.CompanyName.Trim(), Value = g.CompanyID.Trim() }).ToList();

    %>
    <%: Html.ValidationSummary(true) %>
    <%:Html.LabelFor(m=>m.CompanyID) %>
    <%: Html.DropDownListFor(m => m.CompanyID ,slt,"SELECT ONE") %>
    <%:Html.HiddenFor(m=>m.Role ,new{ value=2 }) %>
    <input type="submit" value="Assign Coordinator" />
    <% }
   } %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="RightContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
