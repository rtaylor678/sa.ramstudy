﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ContactUsModel>" %>
<%@ Register Src="~/Views/Shared/HomeMenuUserControl.ascx" TagName="HomeMenu" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<link href="<%= Page.ResolveUrl("~/Content/menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .bodyright
        {
            margin: 0.5em 0px 1em;
        }
        #loginLink
        {
            width: 120px;
            font-size: 19px;
            font-weight: bold;
        }
        #DateID
        {
            font-size: 9px;
        }
        #container
        {
            border-bottom: medium none;
            border-left: medium none;
            background-color: transparent;
            width: 550px;
           
            border-top: medium none;
            border-right: medium none;
        }
        .err
        {
        	color : Red;
        }
            .style4
        {
             font-size: .95em;
            font-family:Arial,Tahoma,"Lucida san", Tahoma;
            line-height:1.5;
            color:#333;
        }
        .editor-field
        {
        	margin: 10px 0;
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="MenuContent" runat="server">
    <uc:HomeMenu ID="homeMenu" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">



    <div style="min-height:200px;width:550px; margin: 30px 30px;">
        <div  class='headerIntro'>
            Contact Us</div>
        <p class="style4">
            We want to ensure that you realize all the benefits of the 2011 RAM Study and 
            look forward to the opportunity to work with you and your colleagues to ensure 
            your successful participation.
        </p>
        <p class="style4">
            Please submit your email to <a href="mailto:RAM@solomononline.com">
            RAM@solomononline.com</a> or you may email us using the form below. You 
            may also contact by phone at 972-739-1731.</p>
        <p class="style4">
            To use the form please enter your name, company, location, a title that describes
            your subject or question and write a brief description of your question or inquiry.
            Once complete, click on the <b>SEND</b> button to send your 
            email to us.</p>
            
      
        <% Html.EnableClientValidation(); %>
       
        
        <p>
        <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
     
         
            <p style="color:#991000;font-size:12pt;">Send Us Your Question or Request</p>
            
             <div class="editor-label">
                <%:  Html.LabelFor(model => model.authorsName) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.authorsName,new {size="54"}) %>
                <%: Html.ValidationMessageFor(model => model.authorsName) %>
            </div>
            
              <div class="editor-label">
                <%:  Html.LabelFor(model => model.companyName ) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.companyName, new { size = "54" })%>
                <%: Html.ValidationMessageFor(model => model.companyName)%>
            </div>

            <div class="editor-label">
                <%:  Html.LabelFor(model => model.location) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.location,new {size="54"}) %>
                <%: Html.ValidationMessageFor(model => model.location) %>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.emailAddress) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.emailAddress,new {size="54"}) %>
                <%: Html.ValidationMessageFor(model => model.emailAddress) %>
            </div>
            <div class="editor-label">
                <%: Html.LabelFor(model => model.emailSubject) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.emailSubject,new {size="54"}) %>
                <%: Html.ValidationMessageFor(model => model.emailSubject) %>
            </div>
            <div class="editor-label">
                <%: Html.LabelFor(model => model.emailBody) %>
            </div>
            <div class="editor-field">
                <%: Html.TextAreaFor(model => model.emailBody,8,42,null) %>
                <%: Html.ValidationMessageFor(model => model.emailBody) %>
            </div>
            <div class="editor-label">
                <%: (TempData["EmailStatus"] !=null)?
                    
                        "<b>" + TempData["EmailStatus"] + "</b>"
                    :
                    ""%>
            </div>
            <div>
                <input type="submit" value="Send" />
            </div>
       </p>
        <% } %>
       
      
    </div>
    
   
</asp:Content>

