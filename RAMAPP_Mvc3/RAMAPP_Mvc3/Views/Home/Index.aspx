﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/RAM.Master" Inherits="System.Web.Mvc.ViewPage"  %>
<%@ OutputCache duration="300" varybyparam="none"  %>

<%@ Register Src="~/Views/Shared/HomeMenuUserControl.ascx" TagName="HomeMenu" TagPrefix="uc" %>

<asp:Content ID="introhead" ContentPlaceHolderID="head" runat="server">
    <% var contentPath = Page.ResolveUrl("~/Content"); %>
 <link href="<%= contentPath %>/menu_style.css" rel="stylesheet" type="text/css" />
 <link href="<%= contentPath %>/sa_style.css" rel="stylesheet" type="text/css" />
   
  <style type="text/css">
  .bodyright{margin:.5em 0 1em}
#loginLink{width:120px;font-size:19px;font-weight:700}
#DateID{font-size:9px}
#container{border-bottom:medium none;border-left:medium none;width:450px;border-top:medium none;border-right:medium none;margin:0 auto}
#news li{font:85.5% "Lucida Sans Unicode" , "Bitstream Vera Sans" , "Trebuchet Unicode MS" , "Lucida Grande" , Verdana, Helvetica, sans-serif;margin:0;padding:0}
.style4{font-size:.95em;font-family:Arial,Tahoma, "Lucida san" , Tahoma;line-height:1.5;color:#333}
#sidebar{width:225px;min-height:500px;}
#sidebar ul {padding-left: 15px;list-style-position: outside; list-style-image: url("<%: Page.ResolveUrl("~/images")%>/red_bullet.gif");}
#sidebar ul li{margin-top: 8px;font-family:Arial ,Verdana,Tahoma, "Century Gothic"; padding: 5px 0;}
#sidebar legend{color:#991000;/*border-top:2px solid #eee;*/border-bottom:1px solid #eee ;display:block;width:98%;text-align:left;margin:15px 0 5px 0 ;padding:15px 0 5px  0 ;font-size:1.2em;}
#sidebar ul li:hover{	background: #991000;}
#sidebar  ul li:hover a {color:#FFF;}
#videoGrid{width:100%;margin:0;padding:0;}
#videoGrid a{display:block;width:100%;min-height:18px;font-size:.75em;text-align:center;font-weight:700;}
#videoGrid a:hover{color:#FFF;font-weight:700;}
#videoGrid li:hover {border:2px solid #991000;background-color:#991000;color:#FFF}
#videoGrid li:hover a{color:#FFF}
#videoGrid li{width:121px;min-height:120px;display:inline-block;vertical-align:top;margin:10px 5px;border:1px solid #d5d5d5; background:#d5d5d5 url(<%: Page.ResolveUrl("~/images")%>/csg-4f96d64931d8f.png) no-repeat top left;}
.headerIntro2{font-family:Arial;font-size:20px;color:#991000;min-width:200px;max-width:800px;padding-left:0;margin-top:0;text-align:left;height:20px;vertical-align:bottom;text-transform:capitalize}
.sprite-Module-1{ background-position: 0 0!important; width: 120px; height: 89px; } 
.sprite-Module-2-Part-1{ background-position: 0 -139px!important;width: 122px; height: 89px; } 
.sprite-Module-2-Part-2{ background-position: 0 -278px!important; width: 160px; height: 90px; } 
.sprite-Module-3{ background-position: 0 -418px!important; width: 120px; height: 90px; } 
.sprite-Module-4{ background-position: 0 -558px!important; width: 120px; height: 90px; } 
.sprite-Module-5{ background-position: 0 -698px!important; width: 120px; height: 90px; } 
.sprite-Module-6{ background-position: 0 -838px!important; width: 120px; height: 90px; } 
.sprite-Module-7{ background-position: 0 -978px!important; width: 120px; height: 90px; } 
.sprite-Module-8{ background-position: 0 -1118px!important; width: 120px; height: 90px; } 
.sprite-Module-9{ background-position: 0 -1258px!important; width: 159px; height: 90px; } 


  </style>
  
    <!--[if lte IE 7 ]> 
    <script type="text/javascript">
     
     document.location= '<%= Url.Action("SystemRequirements" ) %>';
   
    </script>
     <![endif]-->

          <%-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
            <script src="<%= Page.ResolveUrl("~/Scripts/clearbox.js") %>" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ContentPlaceHolderID="LoginContent" runat="server">
    <% Html.RenderPartial("LoginPartial"); %>
</asp:Content>
<asp:Content ContentPlaceHolderID="MenuContent" runat="server">
    <uc:HomeMenu ID="homeMenu" runat="server" />
 </asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <% var homePath = Page.ResolveUrl("~/Home"); %>
    <div style="margin: 30px 30px; width: 550px;">
        <p id="DateID" align="left">
            <%= DateTime.Today.ToString("dddd, dd MMMM ")%></p>
        <!--div id="container" -->
        <div class="headerIntro">
            RAM Study</div>
        <br />
        <br />
        <span class="style4">Solomon developed the International Study of Plant Reliability
            and Maintenance Effectiveness (RAM Study) in partnership with industry leaders to
            identify and quantify areas for improved reliability and maintenance performance.
            This study provides the most comprehensive set of reliability and maintenance performance
            benchmarks in the industry. Over 1,500 plants with more than 8,500 process units
            have participated in the RAM Study. </span>
        <br />
        <br />
        <div class="headerIntro2">
            Why Participate
        </div>
        <br />
        <br />
        <span class="style4">If your plant mechanical availability is not well above 96%, you
            have valuable untapped margin. If your maintenance cost as a percent of plant replacement
            value is not well below 1.3%, you are spending too much. To enjoy the benefits of
            sustainable improvement, these two indicators must be in balance. The RAM Study
            benchmarks these and other key performance indicators (KPIs), and highlights where
            there is imbalance. To ensure you are getting the most out of your assets, Solomon’s
            reliability indices compare your performance to top-quartile performers and reveals
            where you can increase capacity without costly capital investment. To ensure you
            are getting the most out of your resources, Solomon’s maintenance indices compare
            your performance to top-quartile performers to show where you can reduce costly
            expenses without sacrificing performance. </span>
<br />
        <br />
  
    
                  <%--<div class="headerIntro2">
          Training Modules
        </div> --%>
        <br />
        <br />
      <%-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>--%>
            <script src="<%= Page.ResolveUrl("~/Scripts") %>/clearbox.js" type="text/javascript"></script>
                    <ul id="videoGrid">
                        
                         <%--<li>
                          <a href="<%: homePath %>/VideoPres?vid=0"  title="Module 1 - Web Portal Administrator"   rel="clearbox[,,width=675,,height=500]">Module 1 - Web Portal Administrator</a> 
                        </li>
                        <li>
                          <a href="<%: homePath %>/VideoPres?vid=1"  title="Module 2 (Part 1) - Introduction"   rel="clearbox[,,width=675,,height=500]">Module 2 (Part 1) - Introduction</a> 
                        </li>
                        <li>
                         <a href="<%: homePath %>/VideoPres?vid=2"   title="Module 2 (Part 2) - Introduction"   rel="clearbox[,,width=675,,height=500]">Module 2 (Part 2) - Introduction</a> 
                        </li>
                        <li>
                         <a href="<%: homePath %>/VideoPres?vid=3"   title="Module 3 - Site & Unit Characteristics"   rel="clearbox[,,width=675,,height=500]">Module 3 - Site & Unit Characteristics</a> 
                        </li>
                          <li>
                         <a href="<%: homePath %>/VideoPres?vid=4"   title="Module 4 - Maintenance Costs"   rel="clearbox[,,width=675,,height=500]">Module 4 - Maintenance Costs</a> 
                        </li>
                        <li>
                         <a href="<%: homePath %>/VideoPres?vid=5"   title="Module 5 - Organization"   rel="clearbox[,,width=675,,height=500]">Module 5 - Organization</a> 
                        </li>
                         <li>
                         <a href="<%: homePath %>/VideoPres?vid=6"   title="Module 6 - MRO Inventory"   rel="clearbox[,,width=675,,height=500]">Module 6 - MRO Inventory</a> 
                        </li>
                         <li>
                         <a href="<%: homePath %>/VideoPres?vid=7"   title="Module 7 - Work Processes"   rel="clearbox[,,width=675,,height=500]">Module 7 - Work Processes</a> 
                        </li>
                          <li>
                         <a href="<%: homePath %>/VideoPres?vid=8"   title="Module 8 - Reliability"   rel="clearbox[,,width=675,,height=500]">Module 8 - Reliability</a> 
                        </li>
                         <li>
                         <a href="<%: homePath %>/VideoPres?vid=9"   title="Module 9 - Validation"   rel="clearbox[,,width=675,,height=500]">Module 9 - Validation</a> 
                        </li>--%>
                       
                    </ul>  
    </div>
    <div id="preloadImg">
    </div>
    <noscript>
        <div style="border: 1px #ccc solid; background-color: #eee; font-size: .85em; border: 1px #911000 solid;">
            <p align="justify">
                <img src="<%: Url.Content("~/images/info.png")%>" align="top" style="margin: 0 5px;
                    display: inline-block;" />
                <span style="position: relative; display: inline-block; width: 545px; text-align: justify;">
                    You must have both JavaScript and Cookies enabled in your browser in order to use
                    this application. See <a href="<%: homePath %>/SystemRequirements">
                        system requirements</a>. </span>
            </p>
        </div>
    </noscript>
            
     <script type="text/javascript">
         $(window).load(

          function () {

              // var $thumbs = new Array('Module-1.jpg', 'Module-2-Part-1.jpg', 'Module-2-Part-2.jpg', 'Module-3.jpg', 'Module-4.jpg', 'Module-5.jpg', 'Module-6.jpg', 'Module-7.jpg', 'Module-8.jpg', 'Module-9.jpg');
              var $thumbsNm = new Array('sprite-Module-1', 'sprite-Module-2-Part-1', 'sprite-Module-2-Part-2', 'sprite-Module-3', 'sprite-Module-4', 'sprite-Module-5', 'sprite-Module-6', 'sprite-Module-7', 'sprite-Module-8', 'sprite-Module-9');
              var $ls = $('#videoGrid li a');
              for (var t = $ls.length - 1; $lic = $ls[t], t >= 0; t--) {
                  //$($lic).parent().css('background', '#d5d5d5 url(<%=Page.ResolveUrl("~/images") %>/' + $thumbs[t] + ') no-repeat');
                  $($lic).parent().addClass($thumbsNm[t]);
                  //$($lic).prepend('<img src="<%=Page.ResolveUrl("~/images") %>/' + $thumbs[t] + '" width=120 height=90 border=1 style="position:realtive; z-index:-10;margin-bottom:4px;"/> <img class="pbtb" src="<%=Page.ResolveUrl("~/images") %>/playbutton_tb.png" style="position:relative;z-index:30;top: 5; left:15" />');
                  //$($lic).prepend('<img border=1 width=120 height=90  src="<%=Page.ResolveUrl("~/images") %>/playbutton _tb.png"  />');
                  $($lic).prepend('<img border=0 style="margin:30% 35%" src="<%=Page.ResolveUrl("~/images") %>/playbutton.png"  />');
              }
          });
    </script>
</asp:Content>



