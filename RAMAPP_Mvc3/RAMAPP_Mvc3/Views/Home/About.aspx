﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/RAM.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Src="~/Views/Shared/HomeMenuUserControl.ascx" TagName="HomeMenu" TagPrefix="uc" %>
<asp:Content ID="introhead" ContentPlaceHolderID="head" runat="server">
    <link href="<%= Page.ResolveUrl("~/Content/menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style type="">
  
       
        #container
        {
            border-bottom: medium none;
            border-left: medium none;
            background-color: transparent;
            margin: 0px auto;
            width: 550px;
            border-top: medium none;
            border-right: medium none;
        }
    .style1
        {
            color: #990000;
            font-family: "Century Gothic";
        }
        .style4
        {
            font-family: "Century Gothic";
            font-size: small;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" runat="server">
    <uc:HomeMenu ID="homeMenu" runat="server" />
</asp:Content>
<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="container" style="margin: 20px 20px;">
       <div  class='headerIntro'>
            About Us</div>
        <p  class="style4">
            Solomon Associates has produced the International Benchmarking Study of Reliability
            and Maintenance Effectiveness (RAM Study) every 2 years since 1996. Participating
            in the study has provided significant contributions to performance improvement initiatives
            and the results achieved. Worldwide, more than 2,500 process units have participated
            in the RAM Study.</p>
        <p  class="style4">
            Companies can now report and maintain data confidentially online, making entering
            and submitting your study data easier than ever. A company coordinator (website
            administrator) is able to limit website access to only the data contributors for
            each site. When a site's data contributors have reported all the data and cleared
            the data validation checks, the website administrator submits the data file to Solomon.
            The resulting performance and process analysis is then delivered.
        </p>
    </div>
</asp:Content>
