﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<RAMAPP_Mvc3.Models.LogOnModel>" %>
<%@ Import Namespace="System.Web.Security" %>
<%
            
    var cookieAspxFormsAuth = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
   // var m = new LogOnModel();
    if (cookieAspxFormsAuth != null && cookieAspxFormsAuth.Name.Length > 0)
    {
        try
        {
            var fat = FormsAuthentication.Decrypt(cookieAspxFormsAuth.Value);
            Model.EmailAddress = fat.Name;
            Model.RememberMe = fat.IsPersistent;
        }
        catch 
        { 
            //Do nothing
        }
    }
          
    using (Html.BeginForm("LoginPartial","Account"))
    { %>
<table border="0" cellpadding="1" cellspacing="1">
    <tr>
        <td>
            <%: Html.LabelFor(m => m.EmailAddress) %>
        </td>
        <td>
            <%: Html.TextBoxFor(m => m.EmailAddress)%>
            <%: Html.ValidationMessageFor(m => m.EmailAddress)%>
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            <%: Html.LabelFor(m => m.Password) %>
        </td>
        <td>
            <%: Html.PasswordFor(m => m.Password) %>
            <%: Html.ValidationMessageFor(m => m.Password) %>
        </td>
        <td> <input type="submit" value="Login" onclick="this.value='Verifying...'" /></td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td<%--<%= Html.CheckBoxFor(m => m.RememberMe)%>--%>
           <%-- <%= Html.LabelFor(m => m.RememberMe) %>--%></td>
        <td>&nbsp;</td>
    </tr>
</table>

<% } %>