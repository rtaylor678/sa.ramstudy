﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master"
         Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.SiteModel>" %>
<asp:Content ContentPlaceHolderID="head" ID="HeadContent" runat="server">
  <link href="<%= Url.Content("~/Content/dd_menu_style.css") %>" type="text/css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("CreateSite", "Site"))
       { %>
        <%: Html.ValidationSummary(true, "Error. Please try again.") %>
        <br />
        <br />
        <div style="color: #991000;">
            <%: TempData["CreateSiteMsg"] %></div>
        <fieldset style="height: 600px; width: 700px;">
            <legend>Create Site</legend>
            <br />
            <br />
            <div class="display-label grid_3">
                <%: Html.LabelFor(model => model.Name, "Enter Site Name") %>Site Name :</div>
            <div class="display-field grid_8">
                <%: Html.TextBoxFor(model => model.Name, new { placeholder = "Unit Name", size = "38" })%>
            </div>
            <br />
            <br />
            <%-- <div class="display-label grid_4"> Location : </div><div class="display-field grid_8">
            <%: Html.DropDownListFor(model => model.Location, CultureInfo.GetCultures(CultureTypes.SpecificCultures & ~CultureTypes.NeutralCultures)
                                                                                                            .Where(ci => ci.Name.Trim().Length > 0)
                                                                        .OrderBy(ci=>new RegionInfo(ci.LCID).EnglishName )
                                                                        .Select(ci =>{
                                                                                          var r = new RegionInfo(ci.LCID);
                                                                                     
                                                                                          return new SelectListItem() { Text = r.DisplayName, Value = r.TwoLetterISORegionName };
                                                                                   
                                                                                               
                                                                        }).Distinct(new RAMAPP_Mvc3.Models.SelectListItemComparer()),"Select Location")%>
           
        </div>--%>
            <%-- <br />
        <br />
        <%
           RegionInfo rg = null;
           try
           {
               rg = new RegionInfo(Model.Location ?? "");
           }
           catch
           {

               rg = null;
           }      
                            
        %>
        <div class="display-label grid_4"> Currency : </div><div class="display-field grid_8">
            <%: Html.DropDownListFor(model => model.Currency, CultureInfo.GetCultures(CultureTypes.SpecificCultures & ~CultureTypes.NeutralCultures)
                                                                      
                                                                        .Select(ci =>{
                                                                                      var r = new RegionInfo(ci.LCID);

                                                                                      return new SelectListItem() { Text = r.CurrencyEnglishName, Value = r.ISOCurrencySymbol, Selected = r.ISOCurrencySymbol.Equals(Model != null && Model.Currency != null ? Model.Currency.Trim()  : "") };
                                                                                         
                                                                        }).OrderBy(ci=> ci.Text).Distinct(new RAMAPP_Mvc3.Models.SelectListItemComparer()),"Select Currency")%>
        </div>--%>
            <%--   <br />
        <br />
         <div class="display-label grid_4"> <%: Html.LabelFor(model => model.ExchangeRate)%>Exchange Rate :</div>
         <div
            class="display-field grid_8">
            <%: Html.TextBoxFor(model => model.ExchangeRate)%>
        </div>--%>
            <br />
            <div class="grid_16">
                <input type="submit" name="CreateSite" value="Create Site" /></div>
            <br />
        </fieldset>
        <%: Html.HiddenFor(model => model.CompanySID) %>
    <% } %>
    <div>
        <%: Html.ActionLink("Back", "Index","Admin") %>
    </div>
</asp:Content>