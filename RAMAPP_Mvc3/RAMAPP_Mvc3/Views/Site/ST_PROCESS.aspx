﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_PROCESSModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 12/12/2013 10:02:08  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_ST_PROCESS").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("ST_PROCESS", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_ST_PROCESS" class="stContainer">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Maintenance Work Processes</small> </span></a></li>
            <li><a href='#step-2'>
                <label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br />
                    <small>Maintenance Work Orders</small> </span></a></li>
            <li><a href='#step-3'>
                <label class='stepNumber'>3</label><span class='stepDesc'>Section 3<br />
                    <small>Planned Maintenance Work Orders</small> </span></a></li>
            <li><a href='#step-4'>
                <label class='stepNumber'>4</label><span class='stepDesc'>Section 4<br />
                    <small>Scheduled Maintenance Work Orders</small> </span></a></li>
            <li><a href='#step-5'>
                <label class='stepNumber'>5</label><span class='stepDesc'>Section 5<br />
                    <small>Maintenance Work Orders Completed as Scheduled</small> </span></a></li>
            <li><a href='#step-6'>
                <label class='stepNumber'>6</label><span class='stepDesc'>Section 6<br />
                    <small>Emergency Maintenance Work Orders</small> </span></a></li>
            <li><a href='#step-7'>
                <label class='stepNumber'>7</label><span class='stepDesc'>Section 7<br />
                    <small>Planned Backlog</small> </span></a></li>
            <li><a href='#step-8'>
                <label class='stepNumber'>8</label><span class='stepDesc'>Section 8<br />
                    <small>Ready Backlog</small> </span></a></li>
            <li><a href='#step-9'>
                <label class='stepNumber'>9</label><span class='stepDesc'>Section 9<br />
                    <small>Maintenance Job Plans Library</small> </span></a></li>
            <li><a href='#step-10'>
                <label class='stepNumber'>10</label><span class='stepDesc'>Section 10<br />
                    <small>Schedule Compliance</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle">Maintenance Work Processes</p>
            <p><i></i></p>



            <div class="questionblock" qid="">


                <div id='ST_PROC_MT' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_PROC_100.1'><span class='numbering'>1.</span> Is there a documented <a href="#" tooltip="A model that represents the sequence of connected steps that result in the execution of a defined work process. For maintenance this model typically includes interconnected steps for work identification, work planning, work scheduling, work execution, and work closure.">maintenance workflow</a> process that is well defined?</li>
                        <li class='gridrowtext' qid='QSTID_PROC_101.1'><span class='numbering'>2.</span> What is the % of maintenance contracts tendered by type? </li>
                        <li class='gridrowtext' qid='QSTID_PROC_101.1'><span class='numbering'>&nbsp;&nbsp;&nbsp;</span> Time and materials (reimbursable) </li>
                        <li class='gridrowtext' qid='QSTID_PROC_101.2'><span class='numbering'>&nbsp;&nbsp;&nbsp;</span> Measured / Unit rates </li>
                        <li class='gridrowtext' qid='QSTID_PROC_101.3'><span class='numbering'>&nbsp;&nbsp;&nbsp;</span> Lump sum / fixed price </li>
                        <li class='gridrowtext' qid='QSTID_PROC_101.4'><span class='numbering'>&nbsp;&nbsp;&nbsp;</span> Alliance / incentivised </li>
                        <li class='gridrowtext' qid='QSTID_PROC_101.5'><span class='numbering'>&nbsp;&nbsp;&nbsp;</span> Other </li>
                        <li class='gridrowtext' qid='QSTID_PROC_101.6'><span class='numbering'>&nbsp;&nbsp;&nbsp;</span>  <a href="#" tooltip="The total should be equal to 100%" >TOTAL</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_102.1'><span class='numbering'>3.</span> How far in advance is work order planning completed? </li>
                        <li class='gridrowtext' qid='QSTID_PROC_102.2'><span class='numbering'>4.</span> How far in advance is work scheduled? </li>

<%--                        <li class='gridrowtext' qid='QSTID_PROC_100.2'><span class='numbering'>2.</span> Is there a documented <a href="#" tooltip="A model that represents the sequence of connected steps that result in the execution of a defined work process. For maintenance this model typically includes interconnected steps for work identification, work planning, work scheduling, work execution, and work closure.">maintenance workflow</a> process that is followed?</li>
                        <li class='gridrowtext' qid='QSTID_PROC_100.3'><span class='numbering'>3.</span> Is there a documented <a href="#" tooltip="A model that represents the sequence of connected steps that result in the execution of a defined work process. For maintenance this model typically includes interconnected steps for work identification, work planning, work scheduling, work execution, and work closure.">maintenance workflow</a> process that is assessed by an independent third party to determine how well the process is followed?</li>--%>
                    </ul>
                    <ul class="gridbody" style="width:120px">
                        <li class="columndesc">Routine</li>
                      <%--  <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_100.1" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocDefinedWorkflow_RT, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocDefinedWorkflow_RT, "N") %></td>
                                </tr>
                            </table>
                        </li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_PROC_100.7' /></span><br />
                            <select id="SiteDocDefinedWorkflow1_RT" name="SiteDocDefinedWorkflow1_RT" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='does not exist' <%= Model.SiteDocDefinedWorkflow1_RT != null ?(Model.SiteDocDefinedWorkflow1_RT.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                                <option title="exists but not documented" value='exists but not documented' <%= Model.SiteDocDefinedWorkflow1_RT != null ?(Model.SiteDocDefinedWorkflow1_RT.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but not documented</option>
                                <option value='exists and documented' <%= Model.SiteDocDefinedWorkflow1_RT != null ?(Model.SiteDocDefinedWorkflow1_RT.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and documented</option>
                                <option title="documented and training provided" value='documented and training provided' <%= Model.SiteDocDefinedWorkflow1_RT != null ?(Model.SiteDocDefinedWorkflow1_RT.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented and training provided</option>
                                <option title="Documented, training provided, and internally audited" value='Documented, training provided, and internally audited' <%= Model.SiteDocDefinedWorkflow1_RT != null ?((Model.SiteDocDefinedWorkflow1_RT.Equals("Documented, training provided, and internally audited") || (Model.SiteDocDefinedWorkflow1_RT.Equals("documented, training provided, and internally audited"))? "SELECTED" : String.Empty):String.Empty %>>Documented, training provided, and internally audited</option>
                                <option title="Documented, training provided, and both internally and externally audited" value='Documented, training provided, and both internally and externally audited' <%= Model.SiteDocDefinedWorkflow1_RT != null ?(Model.SiteDocDefinedWorkflow1_RT.Equals("Documented, training provided, and both internally and externally audited")? "SELECTED" : String.Empty):String.Empty %>>Documented, training provided, and both internally and externally audited</option>
                            </select>
                        </li>
                        <li class="columndesc">Percent</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_101.1" /><%: Html.EditorFor(model => model.SiteDocWorkflowPcntTM,"DoubleTmpl",new {size="10" ,CalcTag="sumPCTtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_101.2" /><%: Html.EditorFor(model => model.SiteDocWorkflowPcntMR,"DoubleTmpl",new {size="10" ,CalcTag="sumPCTtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_101.3" /><%: Html.EditorFor(model => model.SiteDocWorkflowPcntFP,"DoubleTmpl",new {size="10" ,CalcTag="sumPCTtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_101.4" /><%: Html.EditorFor(model => model.SiteDocWorkflowPcntAE,"DoubleTmpl",new {size="10" ,CalcTag="sumPCTtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_101.5" /><%: Html.EditorFor(model => model.SiteDocWorkflowPcntOT,"DoubleTmpl",new {size="10" ,CalcTag="sumPCTtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_101.6" /><%: Html.EditorFor(model => model.SiteDocWorkflowPcntTT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_102.1' /></span><br />
                            <select id="SiteDocWorkflowWorkPlan" name="SiteDocWorkflowWorkPlan" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='0 week' <%= Model.SiteDocWorkflowWorkPlan != null ?(Model.SiteDocWorkflowWorkPlan.Equals("0 week")? "SELECTED" : String.Empty):String.Empty %>>0 week</option>
                                <option value='2 weeks' <%= Model.SiteDocWorkflowWorkPlan != null ?(Model.SiteDocWorkflowWorkPlan.Equals("2 weeks")? "SELECTED" : String.Empty):String.Empty %>>2 weeks</option>
                                <option value='3 weeks' <%= Model.SiteDocWorkflowWorkPlan != null ?(Model.SiteDocWorkflowWorkPlan.Equals("3 weeks")? "SELECTED" : String.Empty):String.Empty %>>3 weeks</option>
                                <option value='4 weeks' <%= Model.SiteDocWorkflowWorkPlan != null ?(Model.SiteDocWorkflowWorkPlan.Equals("4 weeks")? "SELECTED" : String.Empty):String.Empty %>>4 weeks</option>
                            </select>
                        </li>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_102.2' /></span><br />
                            <select id="SiteDocWorkflowWorkSchedule" name="SiteDocWorkflowWorkSchedule" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='0 weeks' <%= Model.SiteDocWorkflowWorkSchedule != null ?(Model.SiteDocWorkflowWorkSchedule.Equals("0 weeks")? "SELECTED" : String.Empty):String.Empty %>>0 weeks</option>
                                <option value='1 week' <%= Model.SiteDocWorkflowWorkSchedule != null ?(Model.SiteDocWorkflowWorkSchedule.Equals("1 week")? "SELECTED" : String.Empty):String.Empty %>>1 week</option>
                                <option value='2 weeks' <%= Model.SiteDocWorkflowWorkSchedule != null ?(Model.SiteDocWorkflowWorkSchedule.Equals("2 weeks")? "SELECTED" : String.Empty):String.Empty %>>2 weeks</option>
                                <option value='3 weeks' <%= Model.SiteDocWorkflowWorkSchedule != null ?(Model.SiteDocWorkflowWorkSchedule.Equals("3 weeks")? "SELECTED" : String.Empty):String.Empty %>>3 weeks</option>
                            </select>
                        </li>

<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_100.2" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocWorkflowFollowed_RT, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocWorkflowFollowed_RT, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_100.3" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocWorkflowExtAssessed_RT, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocWorkflowExtAssessed_RT, "N") %></td>
                                </tr>
                            </table>
                        </li>--%>
                    </ul>
                    <ul class="gridbody" style="width:120px">
                        <li class="columndesc">Turnaround</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_100.4" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocDefinedWorkflow_TA, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocDefinedWorkflow_TA, "N") %></td>
                                </tr>
                            </table>
                        </li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_PROC_100.8' /></span><br />
                            <select id="SiteDocDefinedWorkflow1_TA" name="SiteDocDefinedWorkflow1_TA" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='does not exist' <%= Model.SiteDocDefinedWorkflow1_TA != null ?(Model.SiteDocDefinedWorkflow1_TA.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                                <option title="exists but not documented" value='exists but not documented' <%= Model.SiteDocDefinedWorkflow1_TA != null ?(Model.SiteDocDefinedWorkflow1_TA.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but not documented</option>
                                <option value='exists and documented' <%= Model.SiteDocDefinedWorkflow1_TA != null ?(Model.SiteDocDefinedWorkflow1_TA.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and documented</option>
                                <option title="documented and training provided" value='documented and training provided' <%= Model.SiteDocDefinedWorkflow1_TA != null ?(Model.SiteDocDefinedWorkflow1_TA.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented and training provided</option>
                                <option title="Documented, training provided, and internally audited" value='Documented, training provided, and internally audited' <%= Model.SiteDocDefinedWorkflow1_TA != null ?(Model.SiteDocDefinedWorkflow1_TA.Equals("Documented, training provided, and internally audited")? "SELECTED" : String.Empty):String.Empty %>>Documented, training provided, and internally audited</option>
                                <option title="Documented, training provided, and both internally and externally audited" value='Documented, training provided, and both internally and externally audited' <%= Model.SiteDocDefinedWorkflow1_TA != null ?(Model.SiteDocDefinedWorkflow1_TA.Equals("Documented, training provided, and both internally and externally audited")? "SELECTED" : String.Empty):String.Empty %>>Documented, training provided, and both internally and externally audited</option>
                            </select>
                        </li>

<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_100.5" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocWorkflowFollowed_TA, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocWorkflowFollowed_TA, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_100.6" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocWorkflowExtAssessed_TA, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteDocWorkflowExtAssessed_TA, "N") %></td>
                                </tr>
                            </table>
                        </li>--%>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteDocDefinedWorkflow_RT)%> 
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowFollowed_RT)%> 
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowExtAssessed_RT)%> 
                <%: Html.ValidationMessageFor(model => model.SiteDocDefinedWorkflow_TA)%> 
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowFollowed_TA)%> 
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowExtAssessed_TA)%>
                <%: Html.ValidationMessageFor(model => model.SiteDocDefinedWorkflow1_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteDocDefinedWorkflow1_TA)%>

                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowPcntTM)%> 
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowPcntMR)%> 
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowPcntFP)%> 
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowPcntAE)%>
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowPcntOT)%>
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowPcntTT)%>
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowWorkPlan)%>
                <%: Html.ValidationMessageFor(model => model.SiteDocWorkflowWorkSchedule)%>
                


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-2">

            <p class="StepTitle">Maintenance Work Orders</p>
            <p><i>Please include all of the completed work orders at the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">site</a> for the reporting period, including all routine and <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnarounds</a> that occurred during the reported period.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_PROC_MT_ALL' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_PROC_120.1'><span class='numbering'>1.</span> <a href="#" tooltip="Corrective maintenance is work done to restore the function of an asset after failure or when failure is imminent.">Corrective Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_120.2'><span class='numbering'>2.</span> <a href="#" tooltip="Time-based maintenance activities performed at predetermined frequencies with established methods, tools, and equipment designed to avoid failure or extend equipment or asset life. Examples include lubrication or periodic adjustments. Tasks such as equipment overhauls scheduled via PM are to be considered Maintenance Events and included in statistics as a failure.">Preventive Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_120.3'><span class='numbering'>3.</span> Predictive Maintenance (CBM)</li>
                        <li class='gridrowtext' qid='QSTID_PROC_120.4'><span class='numbering'>4.</span> <a href="#" tooltip="Equipment that is properly maintained will operate with minimal interruption reaching their full potential life cycle. In contrast, poorly maintained assets fail frequently and reach the end of their useful life prematurely. Consequently, it is necessary to capture and report in-kind replacement of equipment as maintenance cost to avoid masking poor maintenance practices. If this cost is capitalized in the accounting system, report this amount as maintenance cost (either Routine Maintenance Cost or Turnaround Maintenance Cost as applicable).     See also Solomon guide, illustration, and decision tree on Maintenance Capital.">Maintenance Capital</a></li>
                        <li class='gridrowtext calctext' qid='QSTID_PROC_120.5'>Total</li>
                        <li class='gridrowtext' qid='QSTID_PROC_122.1'><span class='numbering'>5.</span> Is maintenance rework measured?</li>
                        <li class='gridrowtext' qid='QSTID_PROC_122.2'><span class='numbering'>6.</span> # of Maintenance Rework Work Orders</li>
                        <li class='gridrowtext' qid='QSTID_PROC_122.5'><span class='numbering'>7.</span> % Rework</li>
                    </ul>
                    <ul class="gridbody1">
                        <li class="columndesc" style="border-left:#EEE 1px solid;">Rotating Equipment</li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_120.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_CORE,"IntTmpl",new {size="10" ,CalcTag="sumCorrWO sumREWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_120.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_PRORE,"IntTmpl",new {size="10" ,CalcTag="sumProWO sumREWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_120.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_PRERE,"IntTmpl",new {size="10" ,CalcTag="sumPredWO sumREWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_120.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_CAPRE,"IntTmpl",new {size="10" ,CalcTag="sumCapWO sumREWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_120.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_TTLRE,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_122.1" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkOrdersTotalOrders_RM, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkOrdersTotalOrders_RM, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_122.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_RWRE,"IntTmpl",new {size="10" ,CalcTag="sumRework", NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_122.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_RWRETT,"DoubleTmpl",new {size="10" , CalcTag="sumPercent" ,NumFormat="Number" ,DecPlaces="2",@class="ttlfld" ,ReadOnly="True"})%></li>

                    </ul>
                    <ul class="gridbody1">
                        <li class="columndesc" style="border-left:#EEE 1px solid;">Fixed Equipment</li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_120.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_COFP,"IntTmpl",new {size="10" ,CalcTag="sumCorrWO sumFPWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_120.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_PROFP,"IntTmpl",new {size="10" ,CalcTag="sumFPWO sumProWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_120.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_PREFP,"IntTmpl",new {size="10" ,CalcTag="sumFPWO sumPredWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_120.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_CAPFP,"IntTmpl",new {size="10" ,CalcTag="sumCapWO sumFPWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_121" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_TTLFP,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1" style="border-left:0px solid;"></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_122.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_RWFW,"IntTmpl",new {size="10" ,CalcTag="sumRework", NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_122.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_RWFWTT,"DoubleTmpl",new {size="10" , CalcTag="sumPercent" ,NumFormat="Number" ,DecPlaces="2",@class="ttlfld" ,ReadOnly="True"})%></li>
                         <li class="gridrow1" style="border-left:0px solid;"></li>
                    </ul>
                    <ul class="gridbody1">
                        <li class="columndesc" style="border-left:#EEE 1px solid;">Instrument & Electrical Equipment</li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_121.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_COIE,"IntTmpl",new {size="10" ,CalcTag="sumCorrWO sumEEWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_121.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_PROIE,"IntTmpl",new {size="10" ,CalcTag="sumEEWO sumProWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_121.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_PREIE,"IntTmpl",new {size="10" ,CalcTag="sumEEWO sumPredWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_121.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_CAPIE,"IntTmpl",new {size="10" ,CalcTag="sumCapWO sumEEWO sumTOTWO" ,NumFormat="Int"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_121.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_TTLIE,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1" style="border-left:0px solid;"></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_122.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_RWIEE,"IntTmpl",new {size="10" ,CalcTag="sumRework", NumFormat="Int"})%></li>
                        <li class="gridrow1" style="border-left:0px solid;">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_122.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_RWIEETT,"DoubleTmpl",new {size="10", CalcTag="sumPercent"  ,NumFormat="Number" ,DecPlaces="2",@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1" style="border-left:0px solid;"></li>
                    </ul>
                    <ul class="gridbody1">
                        <li class="columndesc" style="border-left:#EEE 1px solid;">Total</li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_121.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_COTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_121.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_PROTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow"1>
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_121.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_PRETOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_121.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_CAPTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_122" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_TTLTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1" style="border-left:0px solid;"></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_122.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_RWTT,"IntTmpl",new {size="10" ,  NumFormat="Int",@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_122.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalOrders_RWPCTTT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2",@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1" style="border-left:0px solid;"></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_CORE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_PRORE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_PRERE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_CAPRE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_TTLRE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_COFP)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_PROFP)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_PREFP)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_CAPFP)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_TTLFP)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_COIE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_PROIE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_PREIE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_CAPIE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_TTLIE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_COTOT)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_PROTOT)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_PRETOT)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_CAPTOT)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_TTLTOT)%>

                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_RM)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_RWRE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_RWFW)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_RWIEE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_RWTT)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_RWRETT)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_RWFWTT)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_RWIEETT)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalOrders_RWPCTTT)%>




                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-3">

            <p class="StepTitle">Planned Maintenance Work Orders</p>
            <p><i>Enter the total number of planned maintenance work orders completed during the reporting period for each equipment <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> category. Include both routine and <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnaround</a> work orders.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_PROC_MT_PLAN' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_PROC_140.1'><span class='numbering'>1.</span> <a href="#" tooltip="Corrective maintenance is work done to restore the function of an asset after failure or when failure is imminent.">Corrective Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_140.2'><span class='numbering'>2.</span> <a href="#" tooltip="Time-based maintenance activities performed at predetermined frequencies with established methods, tools, and equipment designed to avoid failure or extend equipment or asset life. Examples include lubrication or periodic adjustments. Tasks such as equipment overhauls scheduled via PM are to be considered Maintenance Events and included in statistics as a failure.">Preventive Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_140.3'><span class='numbering'>3.</span> Predictive Maintenance (CBM)</li>
                        <li class='gridrowtext' qid='QSTID_PROC_140.4'><span class='numbering'>4.</span> <a href="#" tooltip="Equipment that is properly maintained will operate with minimal interruption reaching their full potential life cycle. In contrast, poorly maintained assets fail frequently and reach the end of their useful life prematurely. Consequently, it is necessary to capture and report in-kind replacement of equipment as maintenance cost to avoid masking poor maintenance practices. If this cost is capitalized in the accounting system, report this amount as maintenance cost (either Routine Maintenance Cost or Turnaround Maintenance Cost as applicable).     See also Solomon guide, illustration, and decision tree on Maintenance Capital.">Maintenance Capital</a></li>
                        <li class='gridrowtext calctext' qid='QSTID_PROC_140.5'>Total</li>
                        <li class='gridrowtext' qid='QSTID_PROC_123.1'><span class='numbering'>5.</span> What is the % of Jobs Planned that the planner goes to the field for planning?</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_140.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_CORE,"IntTmpl",new {size="10" ,CalcTag="sumCorrPL sumREPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_140.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_PRORE,"IntTmpl",new {size="10" ,CalcTag="sumProPL sumREPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_140.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_PRERE,"IntTmpl",new {size="10" ,CalcTag="sumPredPL sumREPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_140.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_CAPRE,"IntTmpl",new {size="10" ,CalcTag="sumCapPL sumREPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_140.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_TTLRE,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow1">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_PROC_123.1' /></span><br />
                            <select id="SiteWorkOrdersJobPlan" name="SiteWorkOrdersJobPlan" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='0%' <%= Model.SiteWorkOrdersJobPlan != null ?(Model.SiteWorkOrdersJobPlan.Equals("0%")? "SELECTED" : String.Empty):String.Empty %>>0%</option>
                                <option value='>0% to 25%' <%= Model.SiteWorkOrdersJobPlan != null ?(Model.SiteWorkOrdersJobPlan.Equals(">0% to 25%")? "SELECTED" : String.Empty):String.Empty %>>>0% to 25%</option>
                                <option value='>25% to 50%' <%= Model.SiteWorkOrdersJobPlan != null ?(Model.SiteWorkOrdersJobPlan.Equals(">25% to 50%")? "SELECTED" : String.Empty):String.Empty %>>>25% to 50%</option>
                                <option value='>50% to 75%' <%= Model.SiteWorkOrdersJobPlan != null ?(Model.SiteWorkOrdersJobPlan.Equals(">50% to 75%")? "SELECTED" : String.Empty):String.Empty %>>>50% to 75%</option>
                                <option value='>75 to 100%' <%= Model.SiteWorkOrdersJobPlan != null ?(Model.SiteWorkOrdersJobPlan.Equals(">75 to 100%")? "SELECTED" : String.Empty):String.Empty %>>>75 to 100%</option>
                            </select>
                        </li>

                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_140.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_COFP,"IntTmpl",new {size="10" ,CalcTag="sumCorrPL sumFPPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_140.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_PROFP,"IntTmpl",new {size="10" ,CalcTag="sumFPPL sumProPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_140.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_PREFP,"IntTmpl",new {size="10" ,CalcTag="sumFPPL sumPredPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_140.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_CAPFP,"IntTmpl",new {size="10" ,CalcTag="sumCapPL sumFPPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_141" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_TTLFP,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_141.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_COIE,"IntTmpl",new {size="10" ,CalcTag="sumCorrPL sumEEPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_141.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_PROIE,"IntTmpl",new {size="10" ,CalcTag="sumEEPL sumProPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_141.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_PREIE,"IntTmpl",new {size="10" ,CalcTag="sumEEPL sumPredPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_141.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_CAPIE,"IntTmpl",new {size="10" ,CalcTag="sumCapPL sumEEPL sumTOTPL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_141.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_TTLIE,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_141.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_COTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_141.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_PROTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_141.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_PRETOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_141.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_CAPTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_142" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalPlanned_TTLTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_CORE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_PRORE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_PRERE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_CAPRE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_TTLRE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_COFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_PROFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_PREFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_CAPFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_TTLFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_COIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_PROIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_PREIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_CAPIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_TTLIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_COTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_PROTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_PRETOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_CAPTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalPlanned_TTLTOT)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-4">

            <p class="StepTitle"><a href="#" tooltip="Scheduled maintenance is work that has been scheduled for execution. The maintenance schedule is used to track the completion of scheduled maintenance work within the process unit. The measure of performance associated with this practice is called maintenance schedule compliance and is a reflection of the organization’s ability to adhere to a defined schedule. Maintenance scheduling is performed at least a week in advance.">Scheduled Maintenance</a> Work Orders</p>
            <p><i>Enter the total number of <a href="#" tooltip="Scheduled maintenance is work that has been scheduled for execution. The maintenance schedule is used to track the completion of scheduled maintenance work within the process unit. The measure of performance associated with this practice is called maintenance schedule compliance and is a reflection of the organization’s ability to adhere to a defined schedule. Maintenance scheduling is performed at least a week in advance.">scheduled maintenance</a> work orders completed during the reporting period for each equipment <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> category. Include both routine and <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnaround</a> work orders.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_PROC_MT_SCHED' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_PROC_150.1'><span class='numbering'>1.</span> <a href="#" tooltip="Corrective maintenance is work done to restore the function of an asset after failure or when failure is imminent.">Corrective Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_150.2'><span class='numbering'>2.</span> <a href="#" tooltip="Time-based maintenance activities performed at predetermined frequencies with established methods, tools, and equipment designed to avoid failure or extend equipment or asset life. Examples include lubrication or periodic adjustments. Tasks such as equipment overhauls scheduled via PM are to be considered Maintenance Events and included in statistics as a failure.">Preventive Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_150.3'><span class='numbering'>3.</span> Predictive Maintenance (CBM)</li>
                        <li class='gridrowtext' qid='QSTID_PROC_150.4'><span class='numbering'>4.</span> <a href="#" tooltip="Equipment that is properly maintained will operate with minimal interruption reaching their full potential life cycle. In contrast, poorly maintained assets fail frequently and reach the end of their useful life prematurely. Consequently, it is necessary to capture and report in-kind replacement of equipment as maintenance cost to avoid masking poor maintenance practices. If this cost is capitalized in the accounting system, report this amount as maintenance cost (either Routine Maintenance Cost or Turnaround Maintenance Cost as applicable).     See also Solomon guide, illustration, and decision tree on Maintenance Capital.">Maintenance Capital</a></li>
                        <li class='gridrowtext calctext' qid='QSTID_PROC_150.5'>Total</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_150.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_CORE,"IntTmpl",new {size="10" ,CalcTag="sumCorrSCHED sumRESCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_150.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_PRORE,"IntTmpl",new {size="10" ,CalcTag="sumProSCHED sumRESCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_150.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_PRERE,"IntTmpl",new {size="10" ,CalcTag="sumPredSCHED sumRESCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_150.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_CAPRE,"IntTmpl",new {size="10" ,CalcTag="sumCapSCHED sumRESCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_150.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_TTLRE,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_150.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_COFP,"IntTmpl",new {size="10" ,CalcTag="sumCorrSCHED sumFPSCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_150.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_PROFP,"IntTmpl",new {size="10" ,CalcTag="sumFPSCHED sumProSCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_150.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_PREFP,"IntTmpl",new {size="10" ,CalcTag="sumFPSCHED sumPredSCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_150.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_CAPFP,"IntTmpl",new {size="10" ,CalcTag="sumCapSCHED sumFPSCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_151" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_TTLFP,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_151.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_COIE,"IntTmpl",new {size="10" ,CalcTag="sumCorrSCHED sumEESCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_151.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_PROIE,"IntTmpl",new {size="10" ,CalcTag="sumEESCHED sumProSCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_151.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_PREIE,"IntTmpl",new {size="10" ,CalcTag="sumEESCHED sumPredSCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_151.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_CAPIE,"IntTmpl",new {size="10" ,CalcTag="sumCapSCHED sumEESCHED sumTOTSCHED" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_151.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_TTLIE,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_151.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_COTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_151.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_PROTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_151.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_PRETOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_151.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_CAPTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_152" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSched_TTLTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_CORE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_PRORE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_PRERE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_CAPRE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_TTLRE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_COFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_PROFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_PREFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_CAPFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_TTLFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_COIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_PROIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_PREIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_CAPIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_TTLIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_COTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_PROTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_PRETOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_CAPTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSched_TTLTOT)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-5">

            <p class="StepTitle">Maintenance Work Orders Completed as Scheduled</p>
            <p><i>Enter the number of planned maintenance work orders completed as scheduled (i.e. completed on or before the due date). Include both routine and <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnaround</a> work orders.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_PROC_MT_SCHCL' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_PROC_170.1'><span class='numbering'>1.</span> <a href="#" tooltip="Corrective maintenance is work done to restore the function of an asset after failure or when failure is imminent.">Corrective Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_170.2'><span class='numbering'>2.</span> <a href="#" tooltip="Time-based maintenance activities performed at predetermined frequencies with established methods, tools, and equipment designed to avoid failure or extend equipment or asset life. Examples include lubrication or periodic adjustments. Tasks such as equipment overhauls scheduled via PM are to be considered Maintenance Events and included in statistics as a failure.">Preventive Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_170.3'><span class='numbering'>3.</span> Predictive Maintenance (CBM)</li>
                        <li class='gridrowtext' qid='QSTID_PROC_170.4'><span class='numbering'>4.</span> <a href="#" tooltip="Equipment that is properly maintained will operate with minimal interruption reaching their full potential life cycle. In contrast, poorly maintained assets fail frequently and reach the end of their useful life prematurely. Consequently, it is necessary to capture and report in-kind replacement of equipment as maintenance cost to avoid masking poor maintenance practices. If this cost is capitalized in the accounting system, report this amount as maintenance cost (either Routine Maintenance Cost or Turnaround Maintenance Cost as applicable).     See also Solomon guide, illustration, and decision tree on Maintenance Capital.">Maintenance Capital</a></li>
                        <li class='gridrowtext calctext' qid='QSTID_PROC_170.5'>Total</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_170.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_CORE,"IntTmpl",new {size="10" ,CalcTag="sumCorrSCHCL sumRESCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_170.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_PRORE,"IntTmpl",new {size="10" ,CalcTag="sumProSCHCL sumRESCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_170.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_PRERE,"IntTmpl",new {size="10" ,CalcTag="sumPredSCHCL sumRESCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_170.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_CAPRE,"IntTmpl",new {size="10" ,CalcTag="sumCapSCHCL sumRESCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_170.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_TTLRE,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_170.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_COFP,"IntTmpl",new {size="10" ,CalcTag="sumCorrSCHCL sumFPSCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_170.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_PROFP,"IntTmpl",new {size="10" ,CalcTag="sumFPSCHCL sumProSCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_170.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_PREFP,"IntTmpl",new {size="10" ,CalcTag="sumFPSCHCL sumPredSCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_170.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_CAPFP,"IntTmpl",new {size="10" ,CalcTag="sumCapSCHCL sumFPSCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_171" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_TTLFP,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_171.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_COIE,"IntTmpl",new {size="10" ,CalcTag="sumCorrSCHCL sumEESCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_171.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_PROIE,"IntTmpl",new {size="10" ,CalcTag="sumEESCHCL sumProSCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_171.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_PREIE,"IntTmpl",new {size="10" ,CalcTag="sumEESCHCL sumPredSCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_171.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_CAPIE,"IntTmpl",new {size="10" ,CalcTag="sumCapSCHCL sumEESCHCL sumTOTSCHCL" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_171.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_TTLIE,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_171.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_COTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_171.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_PROTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_171.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_PRETOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_171.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_CAPTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_172" /><%: Html.EditorFor(model => model.SiteWorkOrdersTotalSchedCmpl_TTLTOT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_CORE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_PRORE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_PRERE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_CAPRE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_TTLRE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_COFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_PROFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_PREFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_CAPFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_TTLFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_COIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_PROIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_PREIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_CAPIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_TTLIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_COTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_PROTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_PRETOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_CAPTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersTotalSchedCmpl_TTLTOT)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-6">

            <p class="StepTitle"><a href="#" tooltip="Maintenance work that occurs within 72 hours of the equipment issue and/or disrupts the pre-planned (weekly) schedule.">Emergency Maintenance</a> Work Orders</p>
            <p><i>Enter the number of completed <a href="#" tooltip="Maintenance work that occurs within 72 hours of the equipment issue and/or disrupts the pre-planned (weekly) schedule.">emergency maintenance</a> work orders for each equipment <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> category.  Include both routine and <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnaround</a> work orders.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_PROC_MT_EMG' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_PROC_200.1'><span class='numbering'>1.</span> Total number of <a href="#" tooltip="Maintenance work that occurs within 72 hours of the equipment issue and/or disrupts the pre-planned (weekly) schedule.">emergency maintenance</a> work orders</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_200.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersEmergencyOrders_RE,"DoubleTmpl",new {size="10" ,CalcTag="sumWOEmerg" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_200.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersEmergencyOrders_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumWOEmerg" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_200.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersEmergencyOrders_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumWOEmerg" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_200.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersEmergencyOrders_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersEmergencyOrders_RE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersEmergencyOrders_FP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersEmergencyOrders_IE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersEmergencyOrders_Tot)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-7">

            <p class="StepTitle"><a href="#" tooltip="The combination of the quantity of work that has been fully planned for execution but is not ready to be scheduled, and the ready backlog. Planned backlog work may not be ready to be scheduled for various reasons, such as timing associated with environmental concerns or turnaround planning, waiting on materials, special tools needs unfulfilled, etc.">Planned Backlog</a></p>
            <p><i>Enter the average number of weeks of <a href="#" tooltip="The combination of the quantity of work that has been fully planned for execution but is not ready to be scheduled, and the ready backlog. Planned backlog work may not be ready to be scheduled for various reasons, such as timing associated with environmental concerns or turnaround planning, waiting on materials, special tools needs unfulfilled, etc.">planned backlog</a>.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_PROC_MT_AVGPL' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_PROC_182.1'><span class='numbering'>1.</span> <a href="#" tooltip="Corrective maintenance is work done to restore the function of an asset after failure or when failure is imminent.">Corrective Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_182.2'><span class='numbering'>2.</span> <a href="#" tooltip="Time-based maintenance activities performed at predetermined frequencies with established methods, tools, and equipment designed to avoid failure or extend equipment or asset life. Examples include lubrication or periodic adjustments. Tasks such as equipment overhauls scheduled via PM are to be considered Maintenance Events and included in statistics as a failure.">Preventive Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_182.3'><span class='numbering'>3.</span> Predictive Maintenance (CBM)</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_182.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgPlanBacklog_CORE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_182.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgPlanBacklog_PRORE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_182.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgPlanBacklog_PRERE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_182.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgPlanBacklog_COFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_182.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgPlanBacklog_PROFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_182.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgPlanBacklog_PREFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_182.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgPlanBacklog_COIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_182.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgPlanBacklog_PROIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_182.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgPlanBacklog_PREIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgPlanBacklog_CORE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgPlanBacklog_PRORE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgPlanBacklog_PRERE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgPlanBacklog_COFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgPlanBacklog_PROFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgPlanBacklog_PREFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgPlanBacklog_COIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgPlanBacklog_PROIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgPlanBacklog_PREIE)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-8">

            <p class="StepTitle"><a href="#" tooltip="The quantity of work that has been fully prepared for execution but has not yet been executed. It is work for which all planning has been done and materials procured, but the work is awaiting assigned labor for execution. Ready Backlog cannot be greater than Planned Backlog.">Ready Backlog</a></p>
            <p><i>Enter the average number of weeks of <a href="#" tooltip="The quantity of work that has been fully prepared for execution but has not yet been executed. It is work for which all planning has been done and materials procured, but the work is awaiting assigned labor for execution. Ready Backlog cannot be greater than Planned Backlog.">ready backlog</a>.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_PROC_MT_AVGRDY' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_PROC_190.1'><span class='numbering'>1.</span> <a href="#" tooltip="Corrective maintenance is work done to restore the function of an asset after failure or when failure is imminent.">Corrective Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_190.2'><span class='numbering'>2.</span> <a href="#" tooltip="Time-based maintenance activities performed at predetermined frequencies with established methods, tools, and equipment designed to avoid failure or extend equipment or asset life. Examples include lubrication or periodic adjustments. Tasks such as equipment overhauls scheduled via PM are to be considered Maintenance Events and included in statistics as a failure.">Preventive Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_PROC_190.3'><span class='numbering'>3.</span> Predictive Maintenance (CBM)</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_190.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgReadyBacklog_CORE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_190.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgReadyBacklog_PRORE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_190.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgReadyBacklog_PRERE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_190.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgReadyBacklog_COFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_190.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgReadyBacklog_PROFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_190.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgReadyBacklog_PREFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_190.7" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgReadyBacklog_COIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_190.8" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgReadyBacklog_PROIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_190.9" /><%: Html.EditorFor(model => model.SiteWorkOrdersAvgReadyBacklog_PREIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgReadyBacklog_CORE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgReadyBacklog_PRORE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgReadyBacklog_PRERE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgReadyBacklog_COFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgReadyBacklog_PROFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgReadyBacklog_PREFP)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgReadyBacklog_COIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgReadyBacklog_PROIE)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersAvgReadyBacklog_PREIE)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-9">

            <p class="StepTitle">Maintenance Job Plans <a href="#" tooltip="Maintenance work is repetitive over time as most maintenance jobs recur when the equipment fails again. The maintenance job plan library is a technical library where maintenance job plans are stored for easy retrieval and reuse.">Library</a></p>
            <p><i></i></p>



            <div class="questionblock" qid="">


                <div id='ST_PROC_MT_LBY' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_PROC_220.1'><span class='numbering'>1.</span> Total number of maintenance job plans stored in a <a href="#" tooltip="Maintenance work is repetitive over time as most maintenance jobs recur when the equipment fails again. The maintenance job plan library is a technical library where maintenance job plans are stored for easy retrieval and reuse.">library</a> for ease of retrieval and reuse</li>
                        <li class='gridrowtext' qid='QSTID_PROC_220.4'><span class='numbering'>2.</span> Percent of all job plans for the year reported created from a <a href="#" tooltip="Maintenance work is repetitive over time as most maintenance jobs recur when the equipment fails again. The maintenance job plan library is a technical library where maintenance job plans are stored for easy retrieval and reuse.">library</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Corrective Maintenance</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_220.1" /><%: Html.EditorFor(model => model.SiteWorkOrdersLibraryStorage_COTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_220.4" /><%: Html.EditorFor(model => model.SiteWorkOrdersLibraryReport_COTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Preventive Maintenance</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_220.2" /><%: Html.EditorFor(model => model.SiteWorkOrdersLibraryStorage_PROTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_220.5" /><%: Html.EditorFor(model => model.SiteWorkOrdersLibraryReport_PROTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Predictive Maintenance (CBM)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_220.3" /><%: Html.EditorFor(model => model.SiteWorkOrdersLibraryStorage_PRETOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_PROC_220.6" /><%: Html.EditorFor(model => model.SiteWorkOrdersLibraryReport_PRETOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersLibraryStorage_COTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersLibraryStorage_PROTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersLibraryStorage_PRETOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersLibraryReport_COTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersLibraryReport_PROTOT)%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersLibraryReport_PRETOT)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-10">

            <p class="StepTitle"><a href="#" tooltip="The maintenance schedule is used to track the completion of scheduled maintenance work within a process unit. The measure of performance associated with this practice is called maintenance schedule compliance and is a reflection of the organization’s ability to adhere to a defined schedule. A core metric for maintenance scheduling is measuring compliance on a daily or weekly basis.">Schedule Compliance</a></p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_PROC_220.8">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_PROC_220.8' /></span> <span class='numbering'>1.</span> What is the target percentage of <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> capacity <a href="#" tooltip="To allocate is to apportion for a specific purpose. For example, corporate RAM resources allocate their time to the plant sites they support. If six corporate reliability engineers are utilized to support three plants and their time is equally divided among the plant sites, then effectively two corporate reliability engineers support each plant.">allocated</a> to the schedule?
					 					 <span>
                                              <img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteWorkOrdersPcntCapacitySched,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersPcntCapacitySched)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_PROC_220.7">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_PROC_220.7' /></span> <span class='numbering'>2.</span> How frequently do you measure <a href="#" tooltip="The maintenance schedule is used to track the completion of scheduled maintenance work within a process unit. The measure of performance associated with this practice is called maintenance schedule compliance and is a reflection of the organization’s ability to adhere to a defined schedule. A core metric for maintenance scheduling is measuring compliance on a daily or weekly basis.">schedule compliance</a>? (select one)
					 					 <span>
                                              <img id="Img2" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <select id="SiteWorkOrdersFreqMeasureCmpl" name="SiteWorkOrdersFreqMeasureCmpl" width="30px">
                        <option value='-1'>SELECT ONE</option>
                        <option value='Daily' <%= Model.SiteWorkOrdersFreqMeasureCmpl != null ?(Model.SiteWorkOrdersFreqMeasureCmpl.Equals("Daily")? "SELECTED" : String.Empty):String.Empty %>>Daily</option>
                        <option value='Weekly' <%= Model.SiteWorkOrdersFreqMeasureCmpl != null ?(Model.SiteWorkOrdersFreqMeasureCmpl.Equals("Weekly")? "SELECTED" : String.Empty):String.Empty %>>Weekly</option>
                        <option value='Other' <%= Model.SiteWorkOrdersFreqMeasureCmpl != null ?(Model.SiteWorkOrdersFreqMeasureCmpl.Equals("Other")? "SELECTED" : String.Empty):String.Empty %>>Other</option>
                        <option value='DNM' <%= Model.SiteWorkOrdersFreqMeasureCmpl != null ?(Model.SiteWorkOrdersFreqMeasureCmpl.Equals("DNM")? "SELECTED" : String.Empty):String.Empty %>>Do Not Measure</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.SiteWorkOrdersFreqMeasureCmpl)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>



<script type = "text/javascript" > function loadHelpReferences() {
    var objHelpRefs = Section_HelpLst;
    for (i = objHelpRefs.length - 1; i >= 0; i--) {
        var qidNm = objHelpRefs[i]['HelpID'];
        if ($('li[qid^="' + qidNm + '"]').length) {
            var $elm = $('li[qid^="' + qidNm + '"]');
            $elm.append('<img id="Img3" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');
            $elm.find('img.tooltipBtn').css('visibility', 'visible');
        } else if ($('div[qid^="' + qidNm + '"]').length) {
            $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');
        }
    }
}

function formatResult(el, value) {
    var format = 'N';
    if (el.attr('NumFormat') == 'Int') format = 'n0';
    if (el.attr('DecPlaces') != null) {
        if (el.attr('NumFormat') == 'Number') format = 'n' + el.attr('DecPlaces');
    }
    if (value != undefined && isNaN(value)) value = 0;
    var tempval = Globalize.parseFloat(value.toString(), 'en');
    if (isFinite(tempval)) {
        el.val(Globalize.format(tempval, format));
        var idnm = '#_' + el.attr('id');
        var $f = $(idnm);
        if ($f.length) {
            $f.val(el.val());
        }
    } else {
        el.val('');
    }
}
$("input[CalcTag*='sumPCTtg']").sum({
    bind: "keyup",
    selector: "#SiteDocWorkflowPcntTT",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteDocWorkflowPcntTT').attr('readonly', 'readonly');
$('#SiteDocWorkflowPcntTT').change(function () {
    $(this).validate();
});
$("input[CalcTag*='sumCapPL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalPlanned_CAPTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalPlanned_CAPTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalPlanned_CAPTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumCapSCHED']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSched_CAPTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSched_CAPTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSched_CAPTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumCapSCHCL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSchedCmpl_CAPTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSchedCmpl_CAPTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSchedCmpl_CAPTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumCapWO']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalOrders_CAPTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalOrders_CAPTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalOrders_CAPTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumCorrPL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalPlanned_COTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalPlanned_COTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalPlanned_COTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumCorrSCHED']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSched_COTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSched_COTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSched_COTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumCorrSCHCL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSchedCmpl_COTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSchedCmpl_COTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSchedCmpl_COTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumCorrWO']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalOrders_COTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalOrders_COTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalOrders_COTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumEEPL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalPlanned_TTLIE",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalPlanned_TTLIE').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalPlanned_TTLIE').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumEESCHED']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSched_TTLIE",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSched_TTLIE').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSched_TTLIE').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumEESCHCL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSchedCmpl_TTLIE",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSchedCmpl_TTLIE').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSchedCmpl_TTLIE').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumEEWO']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalOrders_TTLIE",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalOrders_TTLIE').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalOrders_TTLIE').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumFPPL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalPlanned_TTLFP",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalPlanned_TTLFP').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalPlanned_TTLFP').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumFPSCHED']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSched_TTLFP",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSched_TTLFP').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSched_TTLFP').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumFPSCHCL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSchedCmpl_TTLFP",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSchedCmpl_TTLFP').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSchedCmpl_TTLFP').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumFPWO']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalOrders_TTLFP",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalOrders_TTLFP').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalOrders_TTLFP').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumPredPL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalPlanned_PRETOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalPlanned_PRETOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalPlanned_PRETOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumPredSCHED']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSched_PRETOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSched_PRETOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSched_PRETOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumPredSCHCL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSchedCmpl_PRETOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSchedCmpl_PRETOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSchedCmpl_PRETOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumPredWO']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalOrders_PRETOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalOrders_PRETOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalOrders_PRETOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumProPL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalPlanned_PROTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalPlanned_PROTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalPlanned_PROTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumProSCHED']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSched_PROTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSched_PROTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSched_PROTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumProSCHCL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSchedCmpl_PROTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSchedCmpl_PROTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSchedCmpl_PROTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumProWO']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalOrders_PROTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalOrders_PROTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalOrders_PROTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumREPL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalPlanned_TTLRE",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalPlanned_TTLRE').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalPlanned_TTLRE').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumRESCHED']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSched_TTLRE",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSched_TTLRE').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSched_TTLRE').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumRESCHCL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSchedCmpl_TTLRE",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSchedCmpl_TTLRE').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSchedCmpl_TTLRE').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumREWO']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalOrders_TTLRE",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalOrders_TTLRE').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalOrders_TTLRE').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumTOTPL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalPlanned_TTLTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalPlanned_TTLTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalPlanned_TTLTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumTOTSCHED']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSched_TTLTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSched_TTLTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSched_TTLTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumTOTSCHCL']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalSchedCmpl_TTLTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalSchedCmpl_TTLTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalSchedCmpl_TTLTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumTOTWO']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalOrders_TTLTOT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#SiteWorkOrdersTotalOrders_TTLTOT').attr('readonly', 'readonly');
$('#SiteWorkOrdersTotalOrders_TTLTOT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumWOEmerg']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersEmergencyOrders_Tot",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$("input[CalcTag*='sumRework']").sum({
    bind: "keyup",
    selector: "#SiteWorkOrdersTotalOrders_RWTT",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});

if ($("#SiteWorkOrdersTotalOrders_TTLRE").val() != null && !isNaN($("#SiteWorkOrdersTotalOrders_TTLRE").val()))
    if ($("#SiteWorkOrdersTotalOrders_RWRE").val() != null && !isNaN($("#SiteWorkOrdersTotalOrders_RWRE").val())) {
        $("#SiteWorkOrdersTotalOrders_TTLRE").trigger('keyup');
    } $("#SiteWorkOrdersTotalOrders_TTLRE,#SiteWorkOrdersTotalOrders_RWRE, #SiteWorkOrdersTotalOrders_CORE, #SiteWorkOrdersTotalOrders_PRORE, #SiteWorkOrdersTotalOrders_PRERE, #SiteWorkOrdersTotalOrders_CAPRE").bind("keyup", function () {
        var $result = $("#SiteWorkOrdersTotalOrders_RWRETT");
        $result.calc("v1/v2*v3", {
            v1: $("#SiteWorkOrdersTotalOrders_RWRE"),
            v2: $("#SiteWorkOrdersTotalOrders_TTLRE"),
            v3: 100
        });
        formatResult($result, $result.val()); /*$("#SiteMCptlContWageRateUSD_RT").trigger('blur');*/
    });
    if ($("#SiteWorkOrdersTotalOrders_TTLFP").val() != null && !isNaN($("#SiteWorkOrdersTotalOrders_TTLFP").val()))
        if ($("#SiteWorkOrdersTotalOrders_RWFW").val() != null && !isNaN($("#SiteWorkOrdersTotalOrders_RWFW").val())) {
            $("#SiteWorkOrdersTotalOrders_TTLFP").trigger('keyup');
        } $("#SiteWorkOrdersTotalOrders_TTLFP,#SiteWorkOrdersTotalOrders_RWFW, #SiteWorkOrdersTotalOrders_COFP, #SiteWorkOrdersTotalOrders_PROFP, #SiteWorkOrdersTotalOrders_PREFP, #SiteWorkOrdersTotalOrders_CAPFP").bind("keyup", function () {
            var $result = $("#SiteWorkOrdersTotalOrders_RWFWTT");
            $result.calc("v1/v2*v3", {
                v1: $("#SiteWorkOrdersTotalOrders_RWFW"),
                v2: $("#SiteWorkOrdersTotalOrders_TTLFP"),
                v3: 100
            });
            formatResult($result, $result.val()); /*$("#SiteMCptlContWageRateUSD_RT").trigger('blur');*/
        });
    if ($("#SiteWorkOrdersTotalOrders_TTLIE").val() != null && !isNaN($("#SiteWorkOrdersTotalOrders_TTLIE").val()))
        if ($("#SiteWorkOrdersTotalOrders_RWIEE").val() != null && !isNaN($("#SiteWorkOrdersTotalOrders_RWIEE").val())) {
            $("#SiteWorkOrdersTotalOrders_TTLIE").trigger('keyup');
        } $("#SiteWorkOrdersTotalOrders_TTLIE,#SiteWorkOrdersTotalOrders_RWIEE, #SiteWorkOrdersTotalOrders_COIE, #SiteWorkOrdersTotalOrders_PROIE, #SiteWorkOrdersTotalOrders_PREIE, #SiteWorkOrdersTotalOrders_CAPIE").bind("keyup", function () {
            var $result = $("#SiteWorkOrdersTotalOrders_RWIEETT");
            $result.calc("v1/v2*v3", {
                v1: $("#SiteWorkOrdersTotalOrders_RWIEE"),
                v2: $("#SiteWorkOrdersTotalOrders_TTLIE"),
                v3: 100
            });
            formatResult($result, $result.val()); /*$("#SiteMCptlContWageRateUSD_RT").trigger('blur');*/
        });
        if ($("#SiteWorkOrdersTotalOrders_TTLTOT").val() != null && !isNaN($("#SiteWorkOrdersTotalOrders_TTLTOT").val()))
            if ($("#SiteWorkOrdersTotalOrders_RWTT").val() != null && !isNaN($("#SiteWorkOrdersTotalOrders_RWTT").val())) {
                $("#SiteWorkOrdersTotalOrders_TTLTOT").trigger('keyup');
            } $("#SiteWorkOrdersTotalOrders_TTLTOT, #SiteWorkOrdersTotalOrders_RWTT, #SiteWorkOrdersTotalOrders_RWIEE, #SiteWorkOrdersTotalOrders_COIE, #SiteWorkOrdersTotalOrders_PROIE, #SiteWorkOrdersTotalOrders_PREIE, #SiteWorkOrdersTotalOrders_CAPIE, #SiteWorkOrdersTotalOrders_COFP, #SiteWorkOrdersTotalOrders_PROFP, #SiteWorkOrdersTotalOrders_PREFP, #SiteWorkOrdersTotalOrders_CAPFP,#SiteWorkOrdersTotalOrders_PRORE, #SiteWorkOrdersTotalOrders_PRERE, #SiteWorkOrdersTotalOrders_CAPRE, #SiteWorkOrdersTotalOrders_CORE, #SiteWorkOrdersTotalOrders_RWRE, #SiteWorkOrdersTotalOrders_RWFW").bind("keyup", function () {
                var $result = $("#SiteWorkOrdersTotalOrders_RWPCTTT");
                $result.calc("v1/v2*v3", {
                    v1: $("#SiteWorkOrdersTotalOrders_RWTT"),
                    v2: $("#SiteWorkOrdersTotalOrders_TTLTOT"),
                    v3: 100
                });
                formatResult($result, $result.val()); /*$("#SiteMCptlContWageRateUSD_RT").trigger('blur');*/
            });
$('#SiteWorkOrdersEmergencyOrders_Tot').attr('readonly', 'readonly');
$('#SiteWorkOrdersEmergencyOrders_Tot').change(function() {
    $(this).validate();
});
var secComments = Section_Comments;
var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';
for (i = secComments.length - 1; i >= 0; i--) {
    var qidNm = secComments[i]['CommentID'];
    if ($('img[qid="' + qidNm + '"]').length) {
        $('img[qid="' + qidNm + '"]').attr('src', redCmtImg);
    }
} <%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %> $('.rowdesc li div').prepend(function(index, html) {
    var gridId = $(this).closest('.grid').attr('id');
    if ($.trim(html).indexOf('nbsp', 0) == -1) {
        var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>';
    }
    $(this).html(newHTML);
});
$('img[suid]').click(function() {
    var suid = $(this).attr('suid');
    var qid = $(this).attr('qid');
    recordComment(suid, qid);
}); 
</script>
</asp:Content>
