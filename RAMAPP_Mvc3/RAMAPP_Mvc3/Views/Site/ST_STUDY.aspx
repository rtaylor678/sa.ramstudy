﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_STUDYModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 12/12/2013 10:03:54  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_ST_STUDY").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("ST_STUDY", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_ST_STUDY" class="stContainer">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Data Requirements</small> </span></a></li>
            <li><a href='#step-2'>
                <label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br />
                    <small>Data Accuracy</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle">Data Requirements</p>
            <p><i>Time and Resources for This Questionnaire</i></p>



            <div class="questionblock" qid="QSTID_STUDY_100.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_100.1' /></span> <span class='numbering'>1.</span> How many people were involved in data collection?
					 					 <span>
                                              <img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.StudyEffortDataCollectNumPers,"IntTmpl",new {size="10" ,CalcTag="entryPers" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.StudyEffortDataCollectNumPers)%>
                </div>

                <div class="ReportInUnit">Number </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_100.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_100.2' /></span> <span class='numbering'>2.</span> How many total work hours were required for data collection?
					 					 <span>
                                              <img id="Img2" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.StudyEffortDataCollectHrs,"IntTmpl",new {size="10" ,CalcTag="entryHrs" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.StudyEffortDataCollectHrs)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_100.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_100.3' /></span> <span class='numbering'>3.</span> How many people were involved in data entry?
					 					 <span>
                                              <img id="Img3" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.StudyEffortDataEntryNumPers,"IntTmpl",new {size="10" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.StudyEffortDataEntryNumPers)%>
                </div>

                <div class="ReportInUnit">Number </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_100.4">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_100.4' /></span> <span class='numbering'>4.</span> How many total work hours were required for data entry?
					 					 <span>
                                              <img id="Img4" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.StudyEffortDataEntryHrs,"IntTmpl",new {size="10" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.StudyEffortDataEntryHrs)%>
                </div>

                <div class="ReportInUnit">Hours </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_100.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_100.5' /></span> The average number of work hours per person required to collect the data.
					 					 <span>
                                              <img id="Img5" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.StudyEffortDataCollectHrs_Person,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.StudyEffortDataCollectHrs_Person)%>
                </div>

                <div class="ReportInUnit">(calculated) Hours/Person </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_100.6">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_100.6' /></span> The average number of work hours per person required to enter the data.
					 					 <span>
                                              <img id="Img6" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.StudyEffortDataEntryHrs_Person,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.StudyEffortDataEntryHrs_Person)%>
                </div>

                <div class="ReportInUnit">(calculated) Hours/Person </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-2">

            <p class="StepTitle">Data Accuracy</p>
            <p><i>Select the answer that best describes your data source.</i></p>



            <div class="questionblock" qid="QSTID_STUDY_120.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_120.1' /></span> <span class='numbering'>1.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> and Unit Characteristics
					 					 <span>
                                              <img id="Img7" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_Char, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_Char, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_Char, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_Char)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_120.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_120.2' /></span> <span class='numbering'>2.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> and Unit <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">Routine Maintenance</a> Expense
					 					 <span>
                                              <img id="Img8" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_RTCost, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_RTCost, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_RTCost, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_RTCost)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_120.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_120.3' /></span> <span class='numbering'>3.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> and Unit <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> Maintenance Expense
					 					 <span>
                                              <img id="Img9" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_TACost, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_TACost, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_TACost, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_TACost)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_120.4">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_120.4' /></span> <span class='numbering'>4.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> and Unit <a href="#" tooltip="Equipment that is properly maintained will operate with minimal interruption reaching their full potential life cycle. In contrast, poorly maintained assets fail frequently and reach the end of their useful life prematurely. Consequently, it is necessary to capture and report in-kind replacement of equipment as maintenance cost to avoid masking poor maintenance practices. If this cost is capitalized in the accounting system, report this amount as maintenance cost (either Routine Maintenance Cost or Turnaround Maintenance Cost as applicable).     See also Solomon guide, illustration, and decision tree on Maintenance Capital.">Maintenance Capital</a> Costs
					 					 <span>
                                              <img id="Img10" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MCptl, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MCptl, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MCptl, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_MCptl)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_120.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_120.5' /></span> <span class='numbering'>5.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> and Unit <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> Hours
					 					 <span>
                                              <img id="Img11" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MHrs, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MHrs, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MHrs, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_MHrs)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_120.6">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_120.6' /></span> <span class='numbering'>6.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> <a href="#" tooltip="Maintenance, repair, and operating materials (MRO) and spare parts. This excludes capital equipment staged for installation and consumable operating materials such as catalysts, chemicals, etc.">MRO</a> Inventory Data
					 					 <span>
                                              <img id="Img12" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MRO, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MRO, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MRO, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_MRO)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_120.7">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_120.7' /></span> <span class='numbering'>7.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> and Maintenance Function
					 					 <span>
                                              <img id="Img13" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MaintOrg, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MaintOrg, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_MaintOrg, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_MaintOrg)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_120.8">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_120.8' /></span> <span class='numbering'>8.</span> Site Maintenance Work Processes / Work Orders
					 					 <span>
                                              <img id="Img14" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_WorkProcess, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_WorkProcess, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_WorkProcess, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_WorkProcess)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_120.9">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_120.9' /></span> <span class='numbering'>9.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> and Unit <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> Data
					 					 <span>
                                              <img id="Img15" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_Reliability, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_Reliability, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_Reliability, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_Reliability)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_121">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_121' /></span> <span class='numbering'>10.</span> Unit Process Characteristics
					 					 <span>
                                              <img id="Img16" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_ProcessChar, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_ProcessChar, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_ProcessChar, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_ProcessChar)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_121.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_121.1' /></span> <span class='numbering'>11.</span> Unit Equipment Data
					 					 <span>
                                              <img id="Img17" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_EquipData, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_EquipData, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_EquipData, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_EquipData)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_STUDY_121.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_STUDY_121.2' /></span> <span class='numbering'>12.</span> Unit Maintenance Work Types
					 					 <span>
                                              <img id="Img18" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_WorkTypes, "MIS") %>&nbsp;Data extracted from computerized maintenance management system or similar system.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_WorkTypes, "EST") %>&nbsp;A combination of data from systems with estimates for select data.</br> <%= Html.RadioButtonFor(model => model.StudyDataCollectMethod_WorkTypes, "EXP") %>&nbsp;Relied heavily on estimates and assumptions to provide data.</br> <%: Html.ValidationMessageFor(model => model.StudyDataCollectMethod_WorkTypes)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>



    <script type="text/javascript">  function loadHelpReferences() { var objHelpRefs = Section_HelpLst; for (i = objHelpRefs.length - 1; i >= 0; i--) { var qidNm = objHelpRefs[i]['HelpID']; if ($('li[qid^="' + qidNm + '"]').length) { var $elm = $('li[qid^="' + qidNm + '"]'); $elm.append('<img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>'); $elm.find('img.tooltipBtn').css('visibility', 'visible'); } else if ($('div[qid^="' + qidNm + '"]').length) { $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible'); } } } function formatResult(el, value) { var format = 'N'; if (el.attr('NumFormat') == 'Int') format = 'n0'; if (el.attr('DecPlaces') != null) { if (el.attr('NumFormat') == 'Number') format = 'n' + el.attr('DecPlaces'); } if (value != undefined && isNaN(value)) value = 0; var tempval = Globalize.parseFloat(value.toString(), 'en'); if (isFinite(tempval)) { el.val(Globalize.format(tempval, format)); var idnm = '#_' + el.attr('id'); var $f = $(idnm); if ($f.length) { $f.val(el.val()); } } else { el.val(''); } } $("#StudyEffortDataCollectHrs,#StudyEffortDataCollectNumPers ").bind("keyup", function () { var $result = $("#StudyEffortDataCollectHrs_Person"); $result.calc("v1/v2", { v1: $("#StudyEffortDataCollectHrs"), v2: $("#StudyEffortDataCollectNumPers") }); formatResult($result, $result.val()); /*$("#StudyEffortDataCollectHrs_Person").trigger('blur');*/ }); if ($("#StudyEffortDataCollectHrs").val() != null && !isNaN($("#StudyEffortDataCollectHrs").val())) if ($("#StudyEffortDataCollectNumPers").val() != null && !isNaN($("#StudyEffortDataCollectNumPers").val())) { $("#StudyEffortDataCollectHrs").trigger('keyup'); } $("#StudyEffortDataEntryHrs,#StudyEffortDataEntryNumPers ").bind("keyup", function () { var $result = $("#StudyEffortDataEntryHrs_Person"); $result.calc("v1/v2", { v1: $("#StudyEffortDataEntryHrs"), v2: $("#StudyEffortDataEntryNumPers") }); formatResult($result, $result.val()); /*$("#StudyEffortDataEntryHrs_Person").trigger('blur');*/ }); if ($("#StudyEffortDataEntryHrs").val() != null && !isNaN($("#StudyEffortDataEntryHrs").val())) if ($("#StudyEffortDataEntryNumPers").val() != null && !isNaN($("#StudyEffortDataEntryNumPers").val())) { $("#StudyEffortDataEntryHrs").trigger('keyup'); } var secComments = Section_Comments; var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>'; for (i = secComments.length - 1; i >= 0; i--) { var qidNm = secComments[i]['CommentID']; if ($('img[qid="' + qidNm + '"]').length) { $('img[qid="' + qidNm + '"]').attr('src', redCmtImg); } }<%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %>$('.rowdesc li div').prepend(function (index, html) { var gridId = $(this).closest('.grid').attr('id'); if ($.trim(html).indexOf('nbsp', 0) == -1) { var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>'; } $(this).html(newHTML); }); $('img[suid]').click(function () { var suid = $(this).attr('suid'); var qid = $(this).attr('qid'); recordComment(suid, qid); });</script>

</asp:Content>
