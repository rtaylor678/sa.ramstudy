<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_CHARModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 02/03/2015 15:33:46  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_ST_CHAR").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("ST_CHAR", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_ST_CHAR" class="stContainer">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Site Workforce</small> </span></a></li>
            <li><a href='#step-2'>
                <label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br />
                    <small>Geographic Size of the Site</small> </span></a></li>
            <li><a href='#step-3'>
                <label class='stepNumber'>3</label><span class='stepDesc'>Section 3<br />
                    <small>Exchange Rate</small> </span></a></li>
            <li><a href='#step-4'>
                <label class='stepNumber'>4</label><span class='stepDesc'>Section 4<br />
                    <small>Site Replacement Value</small> </span></a></li>
            <li><a href='#step-5'>
                <label class='stepNumber'>5</label><span class='stepDesc'>Section 5<br />
                    <small>Routine Maintenance Safety Practices</small> </span></a></li>
            <li><a href='#step-6'>
                <label class='stepNumber'>6</label><span class='stepDesc'>Section 6<br />
                    <small>RAM Strategy</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle"><a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">Site</a> Workforce</p>
            <p><i></i></p>



            <div style="display: none;" class="questionblock" qid="QSTID_CH1X">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH1X' /></span> What is the total <a href="#" tooltip="Includes the total number of company and contract employees typically on-site for routine operations and maintenance. All shift personnel would be included, but contract employees assigned to turnarounds and capital projects would be excluded.">daily average number of employees</a> and contractors working at the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">site</a> excluding <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/As</a> and capital projects?
					 				
				
														
								
					
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CH1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH1' /></span> <span class='numbering'>1.</span> What is the total <a href="#" tooltip="Includes the total number of company and contract employees typically on-site for routine operations and maintenance. All shift personnel would be included, but contract employees assigned to turnarounds and capital projects would be excluded.">daily average number of employees</a> and contractors working at the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">site</a> excluding <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/As</a> and capital projects?
					 					 <span>
                                              <img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteNumPersInt,"IntTmpl",new {size="10" ,NumFormat="Int"})%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-2">

            <p class="StepTitle">Geographic Size of the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">Site</a></p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_CH10.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH10.1' /></span> <span class='numbering'>1.</span> What unit is used to measure geographic size?
					 					 <span>
                                              <img id="Img2" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Acres</td>
                            <td>Hectares</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteAreaUOM, "Acres") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteAreaUOM, "Hectares") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteAreaUOM)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CH10.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH10.2' /></span> <span class='numbering'>2.</span> What is the geographic size of the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">site</a>?
					 					 <span>
                                              <img id="Img3" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteAreaTotal,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CH10.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH10.3' /></span> <span class='numbering'>3.</span> What is the geographic size of the process areas only?
					 					 <span>
                                              <img id="Img4" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteAreaUnit,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-3">

            <p class="StepTitle"><a href="#" tooltip="The exchange rate is the rate at which two currencies will be exchanged for one another. For this study, the exchange rate is the rate between the local currency and the United States dollar (US�$). This is expressed as the amount of the local currency that is equivalent to US�$1.">Exchange Rate</a></p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_CH11a">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH11a' /></span> <span class='numbering'>1.</span> In which currency will costs be reported?
					 					 <span>
                                              <img id="Img5" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.LocalCurrency,"TextTmpl",new {size="25"})%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CH11b">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH11b' /></span> <span class='numbering'>2.</span> What was the average <a href="#" tooltip="The exchange rate is the rate at which two currencies will be exchanged for one another. For this study, the exchange rate is the rate between the local currency and the United States dollar (US�$). This is expressed as the amount of the local currency that is equivalent to US�$1.">exchange rate</a> (Local Currency per US$) for the period reported?
					 					 <span>
                                              <img id="Img6" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.ExchangeRate,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="6"})%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
        </div>
        <div class="SubSectionTab" id="step-4">

            <p class="StepTitle"><a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">Site</a> Replacement Value</p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_CST_100.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CST_100.1' /></span> <span class='numbering'>1.</span> What is the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">site</a> <a href="#" tooltip="The plant replacement value (PRV) is the original gross asset value of the plant facilities for which maintenance costs are reported, adjusted by the country�s industry sector engineering plant cost index change between the original construction date and the current date. The plant replacement value includes capitalized additions and excludes removed or demolished assets. The plant replacement values of leased assets that are maintained by the company are included. Included in the value are the value of improvements to the land upon which these assets are located and Capitalized Engineering costs. Excluded from the value are value of the land upon which the assets are located, Spare Parts and capitalized interest. Also known as asset replacement value (ARV), replacement asset value (RAV), or estimated replacement value (ERV).">plant replacement value</a> for which maintenance costs are reported?
					 					 <span>
                                              <img id="Img7" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SitePRV,"DoubleTmpl",new {size="15" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SitePRV)%>
                </div>

                <div class="ReportInUnit">Local Currency </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CST_100.1USD">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CST_100.1USD' /></span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">Site</a> <a href="#" tooltip="The plant replacement value (PRV) is the original gross asset value of the plant facilities for which maintenance costs are reported, adjusted by the country�s industry sector engineering plant cost index change between the original construction date and the current date. The plant replacement value includes capitalized additions and excludes removed or demolished assets. The plant replacement values of leased assets that are maintained by the company are included. Included in the value are the value of improvements to the land upon which these assets are located and Capitalized Engineering costs. Excluded from the value are value of the land upon which the assets are located, Spare Parts and capitalized interest. Also known as asset replacement value (ARV), replacement asset value (RAV), or estimated replacement value (ERV).">Plant Replacement Value</a> in US$
					 					 <span>
                                              <img id="Img8" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SitePRVUSD,"DoubleTmpl",new {size="15" ,NumFormat="Number" ,DecPlaces="0" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.SitePRVUSD)%>
                </div>

                <div class="ReportInUnit">US$ </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CST_100.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CST_100.2' /></span> <span class='numbering'>2.</span> What method was used to calculate the <a href="#" tooltip="The plant replacement value (PRV) is the original gross asset value of the plant facilities for which maintenance costs are reported, adjusted by the country�s industry sector engineering plant cost index change between the original construction date and the current date. The plant replacement value includes capitalized additions and excludes removed or demolished assets. The plant replacement values of leased assets that are maintained by the company are included. Included in the value are the value of improvements to the land upon which these assets are located and Capitalized Engineering costs. Excluded from the value are value of the land upon which the assets are located, Spare Parts and capitalized interest. Also known as asset replacement value (ARV), replacement asset value (RAV), or estimated replacement value (ERV).">plant replacement value</a> for the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">site</a> and process units?
					 					 <span>
                                              <img id="Img9" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <select id="PRVCalcMethod" name="PRVCalcMethod" width="30px">
                        <option value='-1'>SELECT ONE</option>
                        <option value='1' <%= Model.PRVCalcMethod != null ?(Model.PRVCalcMethod.Equals("1")? "SELECTED" : String.Empty):String.Empty %>>1 - Original capital construction cost inflated or indexed each year</option>
                        <option value='2' <%= Model.PRVCalcMethod != null ?(Model.PRVCalcMethod.Equals("2")? "SELECTED" : String.Empty):String.Empty %>>2 - Value based on insurance estimate of replacement value</option>
                        <option value='3' <%= Model.PRVCalcMethod != null ?(Model.PRVCalcMethod.Equals("3")? "SELECTED" : String.Empty):String.Empty %>>3 - Value based on acquisition or divestiture price</option>
                        <option value='4' <%= Model.PRVCalcMethod != null ?(Model.PRVCalcMethod.Equals("4")? "SELECTED" : String.Empty):String.Empty %>>4 - Value based on comparison to similar plant and process units</option>
                        <option value='5' <%= Model.PRVCalcMethod != null ?(Model.PRVCalcMethod.Equals("5")? "SELECTED" : String.Empty):String.Empty %>>5 - Provided by Solomon through Fuels or Olefin Study</option>
                        <option value='6' <%= Model.PRVCalcMethod != null ?(Model.PRVCalcMethod.Equals("6")? "SELECTED" : String.Empty):String.Empty %>>6 - Other estimate</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.PRVCalcMethod)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <%-- <div class="SubSectionTab" id="step-5">

            <p class="StepTitle"><a href="#" tooltip="An OSHA recordable injury is an occupational injury or illness that requires medical treatment more than simple first aid and must be reported. You must consider an injury or illness to meet the general recording criteria, and therefore to be recordable, if it results in any of the following: death, days away from work, restricted work or transfer to another job, medical treatment beyond first aid, or loss of consciousness. You must also consider a case to meet the general recording criteria if it involves a significant injury or illness diagnosed by a physician or other licensed health care professional, even if it does not result in death, days away from work, restricted work or job transfer, medical treatment beyond first aid, or loss of consciousness.">Recordable Injuries</a></p>
            <p><i>Please indicate the number of <a href="#" tooltip="An OSHA recordable injury is an occupational injury or illness that requires medical treatment more than simple first aid and must be reported. You must consider an injury or illness to meet the general recording criteria, and therefore to be recordable, if it results in any of the following: death, days away from work, restricted work or transfer to another job, medical treatment beyond first aid, or loss of consciousness. You must also consider a case to meet the general recording criteria if it involves a significant injury or illness diagnosed by a physician or other licensed health care professional, even if it does not result in death, days away from work, restricted work or job transfer, medical treatment beyond first aid, or loss of consciousness.">recordable injuries</a> in the maintenance function that occured while performing <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">routine maintenance</a> for the year reported excluding injuries that occured during <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A's</a>.  Include the following as recordable incidents: workplace related death, workplace injury resulting in time lost from work, and work-related injuries or illnesses requiring a physician's care.</i></p>



            <div class="questionblock" qid="QSTID_CH_104.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_104.1' /></span> <span class='numbering'>1.</span> Company
					 					 <span>
                                              <img id="Img10" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteSafetyCurrInjuryCntComp_RT,"IntTmpl",new {size="10" ,CalcTag="sumSafeRT1" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.SiteSafetyCurrInjuryCntComp_RT)%>
                </div>

                <div class="ReportInUnit">Count </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CH_104.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_104.2' /></span> <span class='numbering'>2.</span> Contractor
					 					 <span>
                                              <img id="Img11" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteSafetyCurrInjuryCntCont_RT,"IntTmpl",new {size="10" ,CalcTag="sumSafeRT1" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.SiteSafetyCurrInjuryCntCont_RT)%>
                </div>

                <div class="ReportInUnit">Count </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CH_104.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_104.3' /></span> Total
					 					 <span>
                                              <img id="Img12" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteSafetyCurrInjuryCntTot_RT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.SiteSafetyCurrInjuryCntTot_RT)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>--%>
        <div class="SubSectionTab" id="step-5">

            <p class="StepTitle"><a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">Routine Maintenance</a> Safety Practices</p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_CH_106.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_106.1' /></span> <span class='numbering'>1.</span> Do all <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> people participate in Pre-task Analysis for every job?
					 					 <span>
                                              <img id="Img13" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteSafetyPreTaskAnalysis, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteSafetyPreTaskAnalysis, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteSafetyPreTaskAnalysis)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CH_106.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_106.2' /></span> <span class='numbering'>2.</span> Do all <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> people participate in Safe Work Permitting for every job?
					 					 <span>
                                              <img id="Img14" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteSafetySafeWorkPermit, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteSafetySafeWorkPermit, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteSafetySafeWorkPermit)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CH_106.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_106.3' /></span> <span class='numbering'>3.</span> Does a <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> representative participate in Return to Operations for every job
					 					 <span>
                                              <img id="Img15" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteSafetyReturnToOperation, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteSafetyReturnToOperation, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteSafetyReturnToOperation)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-6">

            <p class="StepTitle"><a href="#" tooltip="An acronym for Reliability and Maintenance.">RAM</a> Strategy</p>
            <p><i></i></p>

            <%--    <div class="questionblock" qid="QSTID_CH_102.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_102.1' /></span> <span class='numbering'>1.</span> What percent of projects have <a href="#" tooltip="An analysis of all costs associated with the life cycle of equipment or assets including design, manufacture, operation, maintenance, and disposal. This analysis is used to determine the best equipment or material based on the total life cycle cost, not just the original purchase price.">Life Cycle Cost Analysis</a> (LCCA)?
					 					 <span>
                                              <img id="Img24" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteStrategyLCCA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteStrategyLCCA)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>--%>

            <div class="questionblock" qid="QSTID_CH_102.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_102.2' /></span> <span class='numbering'>1.</span> What percent of projects have <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> modeling?
					 					 <span>
                                              <img id="Img33" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteStrategyReliModeling,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteStrategyReliModeling)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <div class="questionblock" qid="QSTID_CH_107.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_107.1' /></span> <span class='numbering'>2.</span> For documented comprehensive multi-year <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> and maintenance <a href="#" tooltip="A multi-year guide to implementation of reliability and maintenance work processes and best practices required to support business goals and achieve operational objectives. The strategic plan provides clear purpose resulting in tangible benefits to the organization. Equally important is an understanding of roles and responsibilities in the strategic plan. Routine review and discussion of progress will confirm commitment and provide an opportunity to make periodic adjustments that may be required.">strategic plans</a>, how many years out is the plan?
					 					 <span>
                                              <img id="Img32" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 1px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteStrategyPlanDocYrs,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteStrategyPlanDocYrs)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <div class="questionblock" qid="QSTID_CH_107.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_107.2' /></span> <span class='numbering'>3.</span> What percentage of equipment has a documented Equipment <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> Strategy?
					 					 <span>
                                              <img id="Img17" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteStrategyEquipRelDocPcnt,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteStrategyEquipRelDocPcnt)%>
                </div>

                <div class="ReportInUnit">Percent </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <%--   <div class="questionblock" qid="QSTID_CH_103.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_103.2' /></span> <span class='numbering'>5.</span> Are there documented annual <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> and maintenance <a href="#" tooltip="A description of a desired outcome or accomplishment used for both organizations and individuals to drive behaviors. To be effective, goals and objectives must be measurable to discern changes in performance.">goals and objectives</a>?
					 					 <span>
                                              <img id="Img28" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyObjectivesDoc, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyObjectivesDoc, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyObjectivesDoc)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div> 

            <div class="questionblock" qid="QSTID_CH_103.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_103.3' /></span> <span class='numbering'>6.</span> Are the <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> and maintenance objectives directly aligned with and shared by production, engineering and supply chain objectives?
					 					 <span>
                                              <img id="Img25" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyMgrObjectives, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyMgrObjectives, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyMgrObjectives)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>--%>

            <div class="questionblock" qid="QSTID_CH_107.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_107.3' /></span> <span class='numbering'>4.</span> How many <a href="#" tooltip="An acronym for Reliability and Maintenance.">RAM</a> <a href="#" tooltip="A measure of performance used in business and industry associated with evaluating the success of a critical activity.">Key Performance Indicators</a> (KPI) do you monitor?
					 					 <span>
                                              <img id="Img27" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteStrategyNumKPI,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteStrategyNumKPI)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <div class="questionblock" qid="QSTID_CH_107.4">

                <div style="display: inline-block; margin: 1px 5px;" class="editor-field">


                    <div id='ST_CH_STR' class='grid editor-field'>
                        <ul>
                            <li>&nbsp;</li>
                            <li class='gridrowtext' qid='QSTID_CH_107.4'><span class='numbering'>5.</span> At what frequency do the <a href="#" tooltip="An acronym for Reliability and Maintenance.">RAM</a> <a href="#" tooltip="A measure of performance used in business and industry associated with evaluating the success of a critical activity.">Key Performance Indicators</a> get monitored.</li>
                        </ul>
                        </span>
						<ul class="gridbody">
                            <li class="columndesc">Daily</li>
                            <li class="gridrow">
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CH_107.4" /><%: Html.EditorFor(model => model.SiteStrategyNumKPIDaily,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        </ul>
                        <ul class="gridbody">
                            <li class="columndesc">Weekly</li>
                            <li class="gridrow">
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CH_107.5" /><%: Html.EditorFor(model => model.SiteStrategyNumKPIWeekly,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        </ul>
                        <ul class="gridbody">
                            <li class="columndesc">Monthly</li>
                            <li class="gridrow">
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CH_107.6" /><%: Html.EditorFor(model => model.SiteStrategyNumKPIMonthly,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        </ul>
                    </div>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyNumKPIDaily)%> <%: Html.ValidationMessageFor(model => model.SiteStrategyNumKPIWeekly)%> <%: Html.ValidationMessageFor(model => model.SiteStrategyNumKPIMonthly)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <div class="questionblock" qid="QSTID_CH_103.5">

                <span class='labelDesc'>Do personnel in the following functional groups have specific and measureable maintenance and <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> performance <a href="#" tooltip="A description of a desired outcome or accomplishment used for both organizations and individuals to drive behaviors. To be effective, goals and objectives must be measurable to discern changes in performance.">goals and objectives</a> tied directly to compensation?</span>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <div class="questionblock" qid="QSTID_CH_103.6">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_103.6' /></span> <span class='numbering'>6.</span> <a href="#" tooltip="An acronym for Reliability and Maintenance.">RAM</a> Staff
					 					 <span>
                                              <img id="Img22" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsRamStaff, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsRamStaff, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyGoalsRamStaff)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <div class="questionblock" qid="QSTID_CH_103.7">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_103.7' /></span> <span class='numbering'>7.</span> <a href="#" tooltip="An acronym for Reliability and Maintenance.">RAM</a> <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a>
                    <span>
                        <img id="Img21" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsRamCraft, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsRamCraft, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyGoalsRamCraft)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <div class="questionblock" qid="QSTID_CH_103.8">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_103.8' /></span> <span class='numbering'>8.</span> Production Staff
					 					 <span>
                                              <img id="Img19" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsProdStaff, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsProdStaff, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyGoalsProdStaff)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CH_103.9">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_103.9' /></span> <span class='numbering'>9.</span> Production Technicians
					 					 <span>
                                              <img id="Img20" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsProdTechs, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsProdTechs, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyGoalsProdTechs)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <div class="questionblock" qid="QSTID_CH_105.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_105.1' /></span> <span class='numbering'>10.</span> Engineering and Technical
					 					 <span>
                                              <img id="Img18" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsEngTech, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsEngTech, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyGoalsEngTech)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_CH_105.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_105.2' /></span> <span class='numbering'>11.</span> Supply Chain / Procurement
					 					 <span>
                                              <img id="Img23" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsSuppProc, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyGoalsSuppProc, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyGoalsSuppProc)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <%-- <div class="questionblock" qid="QSTID_CH_105.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_105.3' /></span> <span class='numbering'>15.</span> Are maintenance <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel qualified/certified in more than one of the following craft types: <a href="#" tooltip="Process equipment that is fixed versus rotating equipment, reciprocating equipment, or other moving equipment. Examples of fixed equipment include pipe, heat exchangers, process vessels, distillation towers, tanks, etc. Fixed equipment craft workers include pipefitters, welders, boilermakers, carpenters, painters, equipment operators, insulators, riggers, and other civil crafts. Include process equipment and exclude non-process equipment.">Fixed Equipment</a>, <a href="#" tooltip="Equipment that turns on an axis and is designed to impart kinetic energy on a process material. Common examples include centrifugal pumps and compressors, electric motors, steam turbines, etc. Rotating equipment craft include machinery mechanics such as millwrights and rotating equipment condition monitoring technicians. Include process equipment and exclude non-process equipment.">Rotating Equipment</a> or Instrument & Electrical?
					 					 <span>
                                              <img id="Img30" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyPersonnelCert, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyPersonnelCert, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyPersonnelCert)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>--%>

            <div class="questionblock" qid="QSTID_CH_105.4">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_105.4' /></span> <span class='numbering'>12.</span> Is an equipment "<a href="#" tooltip="List of assets providing the greatest adverse business impact. The impact may include cost of lost production, maintenance cost, safety / environmental impact, and/or number of failures. Typically a pareto analysis of actual maintenance history is performed to establish the list of �bad actors�.">bad actor</a>" list maintained and managed so as to focus on improving the <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> of <a href="#" tooltip="Equipment that has been evaluated and classified as critical due to its potential impact on safety, environment, quality, production, or cost.">critical equipment</a>?
					 					 <span>
                                              <img id="Img16" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyBadActorList, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyBadActorList, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyBadActorList)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <%--<div class="questionblock" qid="QSTID_CH_105.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_105.5' /></span> <span class='numbering'>14.</span> Bad Actor List - What percentage of the Top Ten issues on the list are resolved every year
					 					 <span>
                                              <img id="Img10" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyMOC, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyMOC, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyMOC)%>
                </div>
           </div>--%>
              <div class="questionblock" qid="QSTID_CH_105.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_PROC_220.7' /></span> <span class='numbering'>13.</span> Bad Actor List - What percentage of the Top Ten issues on the list are resolved every year
					 					 <span>
                                              <img id="Img11" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">
                    <select id="SiteStrategyBadActorPct" name="SiteStrategyBadActorPct" width="30px">
                        <option value='-1'>SELECT ONE</option>
                        <option value='0%' <%= Model.SiteStrategyBadActorPct != null ?(Model.SiteStrategyBadActorPct.Equals("0%")? "SELECTED" : String.Empty):String.Empty %>>0%</option>
                        <option value='>0% to 50%' <%= Model.SiteStrategyBadActorPct != null ?(Model.SiteStrategyBadActorPct.Equals(">0% to 50%")? "SELECTED" : String.Empty):String.Empty %>>>0% to 50%</option>
                        <option value='>50%' <%= Model.SiteStrategyBadActorPct != null ?(Model.SiteStrategyBadActorPct.Equals(">50%")? "SELECTED" : String.Empty):String.Empty %>>>50%</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyBadActorPct)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

              <%--<div class="questionblock" qid="QSTID_CH_105.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_105.5' /></span> <span class='numbering'>14.</span> Does the Management of Change (MOC) process require <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> analysis and subsequent reliability personnel approval if operating conditions change or equipment is added?
					 					 <span>
                                              <img id="Img26" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyMOC, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteStrategyMOC, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyMOC)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>--%>
            <div class="questionblock" qid="QSTID_CH_105.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_CH_105.5' /></span> <span class='numbering'>14.</span> Does the Management of Change (MOC) process require <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> analysis and subsequent reliability personnel approval if operating conditions change or equipment is added or modified?
					 					 <span>
                                              <img id="Img26" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">
                    <select id="SiteStrategyMOC" name="SiteStrategyMOC" width="30px">
                        <option value='-1'>SELECT ONE</option>
                        <option value='does not exist' <%= Model.SiteStrategyMOC != null ?(Model.SiteStrategyMOC.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                        <option value='exists but not documented' <%= Model.SiteStrategyMOC != null ?(Model.SiteStrategyMOC.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but not documented</option>
                        <option value='exists and documented' <%= Model.SiteStrategyMOC != null ?(Model.SiteStrategyMOC.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and documented</option>
                        <option value='documented and training provided' <%= Model.SiteStrategyMOC != null ?(Model.SiteStrategyMOC.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented and training provided</option>
                        <option value='documented, training provided, and internally audited' <%= Model.SiteStrategyMOC != null ?(Model.SiteStrategyMOC.Equals("documented, training provided, and internally audited")? "SELECTED" : String.Empty):String.Empty %>>documented, training provided, and internally audited</option>
                        <option value='Documented, training provided, and both internally and externally audited' <%= Model.SiteStrategyMOC != null ?(Model.SiteStrategyMOC.Equals("Documented, training provided, and both internally and externally audited")? "SELECTED" : String.Empty):String.Empty %>>Documented, training provided, and both internally and externally audited</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.SiteStrategyMOC)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>

<script type = "text/javascript" > function loadHelpReferences() {
     var objHelpRefs = Section_HelpLst;
     for (i = objHelpRefs.length - 1; i >= 0; i--) {
         var qidNm = objHelpRefs[i]['HelpID'];
         if ($('li[qid^="' + qidNm + '"]').length) {
             var $elm = $('li[qid^="' + qidNm + '"]');
             $elm.append('<img id="Img54" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');
             $elm.find('img.tooltipBtn').css('visibility', 'visible');
         } else if ($('div[qid^="' + qidNm + '"]').length) {
             $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');
         }
     }
 }

    function formatResult(el, value) {
        var format = 'N';
        if (el.attr('NumFormat') == 'Int') format = 'n0';
        if (el.attr('DecPlaces') != null) {
            if (el.attr('NumFormat') == 'Number') format = 'n' + el.attr('DecPlaces');
        }
        if (value != undefined && isNaN(value)) value = 0;
        var tempval = Globalize.parseFloat(value.toString(), 'en');
        if (isFinite(tempval)) {
            el.val(Globalize.format(tempval, format));
            var idnm = '#_' + el.attr('id');
            var $f = $(idnm);
            if ($f.length) {
                $f.val(el.val());
            }
        } else {
            el.val('');
        }
    }
    $("#UnitInfoPRV,#_UnitInfoExchangeRate ").bind("keyup", function () {
        var $result = $("#UnitInfoPRVUSD");
        $result.calc("v1/v2", {
            v1: $("#UnitInfoPRV"),
            v2: $("#_UnitInfoExchangeRate")
        });
        formatResult($result, $result.val()); /*$("#UnitInfoPRVUSD").trigger('blur');*/
    });
    if ($("#UnitInfoPRV").val() != null && !isNaN($("#UnitInfoPRV").val()))
        if ($("#_UnitInfoExchangeRate").val() != null && !isNaN($("#_UnitInfoExchangeRate").val())) {
            $("#UnitInfoPRV").trigger('keyup');
        } $("input[CalcTag*='sumUTINJ']").sum({
            bind: "keyup",
            selector: "#UnitSafetyCurrInjuryCntTot_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
    $('#UnitSafetyCurrInjuryCntTot_RT').attr('readonly', 'readonly');
    $('#UnitSafetyCurrInjuryCntTot_RT').change(function () {
        $(this).validate();
    });
    var secComments = Section_Comments;
    var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';
for (i = secComments.length - 1; i >= 0; i--) {
    var qidNm = secComments[i]['CommentID'];
    if ($('img[qid="' + qidNm + '"]').length) {
        $('img[qid="' + qidNm + '"]').attr('src', redCmtImg);
    }
} <%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %> $('.rowdesc li div').prepend(function (index, html) {
    var gridId = $(this).closest('.grid').attr('id');
    if ($.trim(html).indexOf('nbsp', 0) == -1) {
        var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>';
    }
    $(this).html(newHTML);
});
$('img[suid]').click(function () {
    var suid = $(this).attr('suid');
    var qid = $(this).attr('qid');
    recordComment(suid, qid);
});
</script>
</asp:Content>
