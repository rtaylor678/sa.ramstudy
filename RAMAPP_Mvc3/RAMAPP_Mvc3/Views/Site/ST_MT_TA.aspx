﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_MT_TAModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 12/12/2013 10:01:03  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_ST_MT_TA").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("ST_MT_TA", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <p style="font-size:medium;font-weight:bold"> Under Construction</p> 
    <div id="wizard_ST_MT_TA" class="stContainer">
     
<%--        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Site Maintenance Turnarounds</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle">Site Maintenance <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">Turnarounds</a></p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_MT_TA_100.0">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_TA_100.0' /></span> <span class='numbering'>1.</span> Do you take total site maintenance <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnarounds</a>?
					 					 <span>
                                              <img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteTATaken, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteTATaken, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteTATaken)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_MT_TA_100.2">

                <span class='labelDesc'>If no total site maintenance <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnarounds</a> have been taken in the past ten (10) years, please skip the rest of this section.</span>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_MT_TA_100.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_TA_100.1' /></span> <span class='numbering'>2.</span> How many times during the past ten (10) years has there been a total site maintenance <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnaround</a>?
					 					 <span>
                                              <img id="Img2" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteTA10YearCnt,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteTA10YearCnt)%>
                </div>

                <div class="ReportInUnit">Number </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_MT_TA_100.6">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_TA_100.6' /></span> <span class='numbering'>3.</span> What was the average number of down days for the last two (2) total <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">site</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnarounds</a>?
					 					 <span>
                                              <img id="Img3" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteTAAvgDaysDown,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteTAAvgDaysDown)%>
                </div>

                <div class="ReportInUnit">Days </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>--%>
        </div>
        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>



    <script type="text/javascript">  function loadHelpReferences() { var objHelpRefs = Section_HelpLst; for (i = objHelpRefs.length - 1; i >= 0; i--) { var qidNm = objHelpRefs[i]['HelpID']; if ($('li[qid^="' + qidNm + '"]').length) { var $elm = $('li[qid^="' + qidNm + '"]'); $elm.append('<img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>'); $elm.find('img.tooltipBtn').css('visibility', 'visible'); } else if ($('div[qid^="' + qidNm + '"]').length) { $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible'); } } } var secComments = Section_Comments; var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>'; for (i = secComments.length - 1; i >= 0; i--) { var qidNm = secComments[i]['CommentID']; if ($('img[qid="' + qidNm + '"]').length) { $('img[qid="' + qidNm + '"]').attr('src', redCmtImg); } }<%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %>$('.rowdesc li div').prepend(function (index, html) { var gridId = $(this).closest('.grid').attr('id'); if ($.trim(html).indexOf('nbsp', 0) == -1) { var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>'; } $(this).html(newHTML); }); $('img[suid]').click(function () { var suid = $(this).attr('suid'); var qid = $(this).attr('qid'); recordComment(suid, qid); });</script>

</asp:Content>
