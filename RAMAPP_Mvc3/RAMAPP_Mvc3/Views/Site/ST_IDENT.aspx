 
 
  


<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_IDENTModel>" %>
<%@ Import Namespace="System.Globalization" %>

 

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<%//------------------------------------------------------
  // Date Created : 12/12/2013 09:58:57  
  //-----------------------------------------------------%>
	 <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
	 <script type="text/javascript">
	     $(document).ready(function () {
	         // Smart Wizard 	
	         $("#wizard_ST_IDENT").smartTab({ transitionEffect: 'fade', keyNavigation: false });

	         /* $('li .stepDesc small').each(function (index) {
	         var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

	         $(this).text($(elementTitleId).text());

	         });*/

	     });
		</script>
	  <% Html.EnableClientValidation(); %>			   
	  <% using (Ajax.BeginForm("ST_IDENT",new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" })) { 
	     bool uomPref = true;
	  %>
	  	<div id="wizard_ST_IDENT" class="stContainer">
		    <ul class="tabContainer">
			<li><a href='#step-1'><label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br /><small>Site Identification</small> </span></a></li>
			</ul>
		
			    				  
				  
				  <div class="SubSectionTab"  id="step-1">
				   
					<p class="StepTitle"><a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">Site</a> Identification</p>
					<p><i></i></p> 
				 
		              
					 
			    <div  class="questionblock" qid="QSTID1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID1' /></span> <span class='numbering'>1.</span> Company
					 					 <span><img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.CompanyName,"TextTmpl",new {size="25"})%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID2' /></span> <span class='numbering'>2.</span> Plant/<a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">Site</a>
					 					 <span><img id="Img2" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteName,"TextTmpl",new {size="25"})%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID5">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID5' /></span> <span class='numbering'>3.</span> Country
					 					 <span><img id="Img3" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.DropDownListFor(model => model.SiteCountry, CultureInfo.GetCultures(CultureTypes.SpecificCultures & ~CultureTypes.NeutralCultures).Where(ci => ci.Name.Trim().Length > 0).OrderBy(ci=>new RegionInfo(ci.LCID).EnglishName ).Select(ci =>{   var r = new RegionInfo(ci.LCID);  return new SelectListItem() { Text = r.DisplayName, Value = r.TwoLetterISORegionName };}).Distinct(new RAMAPP_Mvc3.Models.SelectListItemComparer()),"Select Location")%> <%: Html.ValidationMessageFor(model => model.SiteCountry)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID3' /></span> <span class='numbering'>4.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">Site</a> Address�
					 					 <span><img id="Img4" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.StreetAddr1,"TextAreaTmpl",new {size="25" ,rows = "4",cols = "50"})%> <%: Html.ValidationMessageFor(model => model.StreetAddr1)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID4">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID4' /></span> <span class='numbering'>5.</span> Mail Address��
					 					 <span><img id="Img5" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.MailAddr1,"TextAreaTmpl",new {size="25" ,rows = "4",cols = "50"})%> <%: Html.ValidationMessageFor(model => model.MailAddr1)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID11">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID11' /></span> <span class='numbering'>6.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">Site</a> Manager Name
					 					 <span><img id="Img6" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteMgr,"TextTmpl",new {size="25"})%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID12">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID12' /></span> <span class='numbering'>7.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a �site� if it shares a common site management team and related site resources.">Site</a> Manager Email Address
					 					 <span><img id="Img7" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteMgrEmail,"TextTmpl",new {size="25"})%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID13">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID13' /></span> <span class='numbering'>8.</span> Maintenance Manager Name
					 					 <span><img id="Img8" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.MaintMgr,"TextTmpl",new {size="25"})%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID14">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID14' /></span> <span class='numbering'>9.</span> Maintenance Manager Email Address
					 					 <span><img id="Img9" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.MaintMgrEmail,"TextTmpl",new {size="25"})%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			    <%: Html.HiddenFor(model=>model.CompanySID) %>
  </div> <!-- for last step --> 
  </div>  <!-- for wizard container -->
<%
} //for form using clause 
%>
 


<script type="text/javascript">  function loadHelpReferences(){var  objHelpRefs = Section_HelpLst;for (i = objHelpRefs.length -1; i >= 0; i--) {      var qidNm = objHelpRefs[i]['HelpID'];      if ($('li[qid^="' + qidNm + '"]').length) {        var $elm = $('li[qid^="' + qidNm + '"]');        $elm.append('<img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');        $elm.find('img.tooltipBtn').css('visibility', 'visible');     }     else if ($('div[qid^="' + qidNm + '"]').length) {       $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');    }}}var secComments =  Section_Comments;var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';for(i = secComments.length-1;i >=0; i-- ){		var qidNm = secComments[i]['CommentID'];		if($('img[qid="' + qidNm + '"]').length){    		$('img[qid="' + qidNm + '"]').attr('src', redCmtImg);		}	}<%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %>$('.rowdesc li div').prepend(  function (index, html) {   var gridId = $(this).closest('.grid').attr('id');   if ($.trim(html).indexOf('nbsp', 0) == -1) {        var newHTML = '<span> ' +       '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' +       ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html +      '</span>';      }    $(this).html(newHTML);   }  ); $('img[suid]').click(function(){ var suid = $(this).attr('suid'); var qid = $(this).attr('qid') ; recordComment(suid,qid); });</script>

</asp:Content> 
