﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<div style="position: absolute; margin: auto auto; padding: 10px 10px; display: block;
        width: 500px; height: 400px;">
        <div style="margin: auto auto;">
            <h5>
               Suspicious Activity Detected</h5>
            <span class="style2">
                <br />
                <br />
                If this continues,  your account will be deactivated. </span>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
     <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
       <style type="text/css">
              .style1
              {
                  text-align: right;
              }
              .style2
              {
                  font-family: "Century Gothic";
                  color: #991000;
                  font-weight: 400;
                  font-size: 14pt;
              }
              #content
              {
                 
                  background: url('<%: Page.ResolveUrl("~/images/Background-rht.jpg") %>') right no-repeat;
                  position: relative;
                  top: 6px;
              }
          </style>
</asp:Content>



<asp:Content ID="Content8" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
