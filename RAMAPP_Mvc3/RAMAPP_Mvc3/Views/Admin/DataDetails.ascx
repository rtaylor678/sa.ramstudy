﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="RAMAPP_Mvc3.Entity" %>
<%@ Import Namespace="RAMAPP_Mvc3.Repositories.Cache" %>
<%
    
    var cachedNavigation = new CachedNavigationRepository();
    List<SiteUnits> sites = cachedNavigation.GetSiteUnitList();
    using (var db = new RAMEntities())
    {

        var siteUnitInfo = (from h in sites
                            from h2 in db.DatasetQuality
                            where h.DatasetID == h2.DatasetID
                            orderby h2.Grade 
                            select new
                                       {
                                           h.DatasetID,
                                           h.SiteName,
                                           h2.Grade,
                                           h2.PcntComplete,
                                           h2.ChecksNC,
                                           h2.ChecksNA,
                                           sectionSummary = (from sm in db.DatasetQualityBySection
                                                             where sm.DatasetID == h.DatasetID
                                                             orderby sm.Grade 
                                                             select sm),
                                           units = (from k in h.UnitsOfSite
                                                    from k2 in db.DatasetQuality
                                                    where k.DatasetID ==
                                                          k2.DatasetID
                                                    orderby k2.Grade 
                                                    select
                                                        new
                                                            {
                                                                k.DatasetID,
                                                                k.UnitName,
                                                                k2.Grade,
                                                                k2.PcntComplete,
                                                                k2.ChecksNC,
                                                                k2.ChecksNA,
                                                                unitSummary = (from us in db.DatasetQualityBySection
                                                                               where us.DatasetID == k.DatasetID
                                                                               orderby us.Grade 
                                                                               select us)
                                                            })
                                       }).FirstOrDefault(u => u.DatasetID == Convert.ToInt32(this.ViewContext.RouteData.Values["id"]));









%>
<div class="accordion">
    <h3>
        <a href="#">Overall</a></h3>
    <div>
        <div class="gridHorizontal">
            <ul class="gridbody">
                <%-- <li class=" columndesc gridrowtext">Site </li>--%>
                <li class="gridrowtext columndesc">Section</li><li class="gridrow columndesc">% Complete
                </li>
                <li class="gridrow columndesc">Quality of Data </li>
                <li class="gridrow columndesc">No. of Data Quality Issues</li><li class="gridrow columndesc">
                    No. of Pending Review Comment </li>
            </ul>
            <ul>
                <li class="rightPointerNav">
                    <ul class="gridbody" scale="<%= siteUnitInfo.Grade %>"  pctcm="<%= siteUnitInfo.PcntComplete %>">
                        <%-- <li class="gridrowtext site">
                    <%= siteUnitInfo.SiteName %></li>--%>
                        <li class="gridrowtext overall">Overall</li>
                        <li class="gridrow overall">
                            <%= String.Format("<span class='bar' style='width:{0:N1}%;'>{0:N1}</span>", siteUnitInfo.PcntComplete)%>
                           
                        </li>
                        <li class="gridrow overall"><span class="bigfont">
                            <%= siteUnitInfo.Grade %>
                        </span></li>
                        <li class="gridrow overall">
                            <%= siteUnitInfo.ChecksNC %></li>
                        <li class="gridrow overall">
                            <%= siteUnitInfo.ChecksNA %></li>
                    </ul>
                    <div class="unitsContainer2">
                    <!--ul class="gridbody"><li>&nbsp;</li><li class=" columndesc gridrowtext"> Section </li><li class="gridrow columndesc"> % Complete </li><li class="gridrow columndesc"> Quality of Data </li><li class="gridrow columndesc">No. of Data Quality Issues</li><li class="gridrow columndesc">No. of Pending Review Comment </li></ul-->
                    <%  foreach (var section in siteUnitInfo.sectionSummary)
                        {%>
                    <ul class="gridbody" scale="<%= section.Grade%>" pctcm="<%= section.PcntComplete %>">
                        <%-- <li class="gridrowtext">&nbsp;</li>--%>
                        <li class="gridrowtext site">
                            <%= section.Section%></li>
                        <li class="gridrow">
                            <%= String.Format("<span class='bar' style='width:{0:N1}%;'>{0:N1}</span>", section.PcntComplete)%> &nbsp;
                           
                        </li>
                        <li class="gridrow"><span class="bigfont">
                            <%= section.Grade%>
                        </span></li>
                        <li class="gridrow">
                            <%= section.ChecksNC%></li>
                        <li class="gridrow">
                            <%= section.ChecksNA%></li>
                    </ul>
                    <%  }%>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <%--<div class="headerIntro">
    Unit Summary
</div>--%>
    <% foreach (var unit in siteUnitInfo.units)
       { %>
    <h3>
        <a href="#">
            <%= unit.UnitName %></a></h3>
    <div>
        <div class="gridHorizontal">
            <ul class="gridbody">
                <li class="gridrowtext columndesc">Section</li><li class="gridrow columndesc">% Complete
                </li>
                <li class="gridrow columndesc">Quality of Data </li>
                <li class="gridrow columndesc">No. of Data Quality Issues</li><li class="gridrow columndesc">
                    No. of Pending Review Comment </li>
                <li class="gridrow columndesc">View Details</li></ul>
            <ul>
                <li>
                    <ul class="gridbody" scale="<%= unit.Grade %>" pctcm="<%= unit.PcntComplete %>">
                        <%--  <li class="gridrowtext site">
                            <%= unit.UnitName %></li>--%>
                        <li class="gridrowtext overall">Overall</li>
                        <li class="gridrow overall">
                            <%= String.Format("<span class='bar' style='width:{0:N1}%;'>{0:N1}</span>", siteUnitInfo.PcntComplete)%></li>
                        <li class="gridrow overall"><span class="bigfont">
                            <%= unit.Grade %>
                        </span></li>
                        <li class="gridrow overall">
                            <%= unit.ChecksNC %></li>
                        <li class="gridrow overall">
                            <%= unit.ChecksNA %></li>
                        <li class="gridrow overall">
                            <%= ((true)
                             ? "<a href='" +
                               Url.Action("ViewGradeDetails", "Admin", new {id = unit.DatasetID}) +
                               "?start=1&size=20&name=" + unit.UnitName + "'>View</a>"
                             : "") %>
                        </li>
                    </ul>
                    <% foreach (var section in unit.unitSummary)
                       {%>
                    <ul class="gridbody" scale="<%= section.Grade %>" pctcm="<%= section.PcntComplete %>">
                        <%--<li class="gridrowtext">&nbsp;</li>--%>
                        <li class="gridrowtext site">
                            <%= section.Section %></li>
                        <li class="gridrow">
                             <%= String.Format("<span class='bar' style='width:{0:N1}%;'>{0:N1}</span>", section.PcntComplete)%>
                           
                        </li>
                        <li class="gridrow"><span class="bigfont">
                            <%= section.Grade %>
                        </span></li>
                        <li class="gridrow">
                            <%= section.ChecksNC %></li>
                        <li class="gridrow">
                            <%= section.ChecksNA %></li>
                        <li class="gridrow">&nbsp;</li>
                    </ul>
                    <% }%>
                </li>
            </ul>
        </div>
    </div>
    <% } %>
</div>
<% } %>
<script type="text/javascript">
    $.ajaxSetup({
        cache: false
    });


    $(function () {

        $("ul.gridbody").each(function () {

            var scale = $(this).attr("scale");
            var pctcm = $(this).attr("pctcm");

            changeColor($(this).find('li:nth-child(3)'), scale);
            changeColor($(this).find('li:nth-child(2)').find('.bar'), pctcm);

        });
        $(".accordion").accordion({
            collapsible: true
        });


        $('.gridHorizontal div.unitsContainer2').toggle('fast');
        $('.gridHorizontal li ul.gridbody').not('.unitsContainer2').click(function() {

            if ($(this).parent().hasClass('rightPointerNav')) {
                $(this).parent().removeClass('rightPointerNav').addClass('downPointerNav');
                $(this).parent().children('div.unitsContainer2').slideDown('fast');
            } else if ($(this).parent().hasClass('downPointerNav')) {
                $(this).parent().removeClass('downPointerNav').addClass('rightPointerNav');
                $(this).parent().children('div.unitsContainer2').slideUp('fast');
            }

        });
    });
   

</script>
