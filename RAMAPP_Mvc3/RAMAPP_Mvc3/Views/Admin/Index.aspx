﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_DataCheck.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Src="~/Views/Shared/AdminMenu.ascx" TagPrefix="uc" TagName="AdminMenu" %>
<%@ Register Src="~/Views/Shared/AdminAvailUserControl.ascx" TagPrefix="uc" TagName="AvailUsrMenu" %>
<%@ Register Src="~/Views/Shared/AdminSiteUnitControl.ascx" TagPrefix="uc" TagName="AvlSitesUnits" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style type="text/css">
 
   div.navWindow2 ul 
        {
            width: 150px;
            list-style: none;
            line-height: 1.5;
            
        }
          .navWindow2  ul li a ,#quickLinks   ul li a{
          	
          	display:block;
          	
          }
        #quickLinks  ul li a:hover, .navWindow2  ul li a:hover
        {
        	border : thin  solid #991000 ;
        	color: #333;
        	background-color: #999;
        	
        	
        }
      
        .navWindow2 H5 
        {
           font-size:16px;
           margin-left: 20px;
        }
        
        .navWindow2 .unitsContainer {
        	margin: 0;
        }
        
        .navWindow2 .unitsContainer ul {
        	list-style: disc ;
        	margin-bottom: 10px;
        
        	
        }
         .navWindow2 ul li
        {
            padding-left: 10px;
            line-height: 1.5;
            width: 190px;
        }
        
        .navWindow2 div.unitsContainer ul li
        {
        	margin-bottom: 5px;
            padding-left: 10px !important;
        	width: 90% !important;
        }
        
        legend
        {
        	font-size: 90%;
        	text-align :center;
        }
        #tools ul ,#users ul
        {
        	list-style:disc;
        	width:80%
        }
  
        #wrapper {
        	
        	height: 100%;
        	width: 960px;
        	
        	
        }
         #content {
         	background: url('<%: Page.ResolveUrl("~/images/keyboard.jpg") %>')  center right no-repeat;
        	border: thin #ccc solid;
        	height: 100%;
        	min-width: 970px;
        }
        #quickLinks
        {
        	padding:2px;
        	background: rgba(255,255,255,.70);
        	height: 860px;
        	width: 775px;
        	
        }
        
 </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var isAdmin = SessionUtil.IsAdmin();
        var isSiteAdmin = SessionUtil.IsSiteAdmin();
    %>
    <div id="wrapper">
        <div id="quickLinks" style="font-size: 95%;">
           
            <p>&nbsp;</p>
            <div id='tools' class="grid_5" style="padding: 0 0px; min-height: 650px">
                <div style="border-right: thin #eee solid; min-height: 650px; padding-left: 5px;">
                    <div style="text-align:center;background:#991000;height:35px;width:98%">
                        <h5 style="color:white;padding-top:6px;">
                            TOOLS</h5>
                    </div>
                    <ul style="color: #991000; padding-left: 25px;">
                        <% if (isAdmin)
                           {  %>
                        <li>
                            <%: Html.ActionLink("Create Site", "CreateSite","Site") %></li>
                        <% } %>
                        <% if (isAdmin || isSiteAdmin)
                           {%>
                        <li>
                            <%: Html.ActionLink("Create User", "CreateUser", "User", new { id = SessionUtil.GetCompanySID() },null)%></li>
                        <% } %>
                    </ul>
                </div>
            </div>
            <div id='users' class="grid_5" style="padding: 0 0px; min-height: 650px;">
                <div style="border-right: thin #eee solid; min-height: 650px; padding-left: 5px;">
                    <uc:AvailUsrMenu runat="server" ID="mainAvailUsr" />
                </div>
            </div>
            <div id='sites' class="grid_5 navWindow2" style="padding: 0 0px; min-height: 650px;">
                <div style="border: none; min-height: 650px; width: 250px;">
                    <uc:AvlSitesUnits runat="server" ID="mainSiteUnits" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="LeftContent">
</asp:Content>
