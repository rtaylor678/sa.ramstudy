﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RAMAPP_Mvc3.Models
{
    public class DataCheckDetailsModel
    {
        public decimal CheckValue { get; set; }

        public decimal CompareValue { get; set; }

        public string DataCheckName { get; set; }

        public string DatacheckText { get; set; }

        public int DecimalPlaces { get; set; }

        public decimal RangeMax { get; set; }

        public decimal RangeMin { get; set; }

        public string StatusCode { get; set; }

        public string StatusImage { get; set; }

        public string StatusMessage { get; set; }

        public string StatusText { get; set; }

        public List<DataCheckTables> Tables { get; set; }

        public string SpellingSuggestions { get; set; }
    }

    public class DataCheckTables
    {
        public string TableHtml { get; set; }

        public byte TableNo { get; set; }
    }

    public class DataPropertyDetailModel
    {
        public string CheckType { set; get; }

        public decimal? CheckValue { set; get; }

        public decimal? CompareValue { set; get; }

        public int DatacheckID { set; get; }

        public string DataCheckName { set; get; }

        public string DataCheckText { set; get; }

        public int DatasetID { set; get; }

        public short PropertyNo { set; get; }

        public string QuestionSectionID { set; get; }

        public decimal? RangeMax { set; get; }

        public decimal? RangeMin { set; get; }

        public string Section { set; get; }

        public string SectionName { set; get; }

        public string StatusCode { set; get; }

        public string StatusImage { set; get; }

        public string StatusMessage { set; get; }

        public string StatusText { set; get; }

        public string Table1 { set; get; }

        public string Table2 { set; get; }

        public string Table3 { set; get; }
    }

    public class DataSectionSummaryModel
    {
        public string Datachecks { get; set; }

        public string Missing { get; set; }

        public string Overall { get; set; }
    }

    public class DataSectionSummaryModel2
    {
        public List<DataPropertyDetailModel> Datachecks { get; set; }

        public List<MissDataModel> Missing { get; set; }

        public List<OverallModel> Overall { get; set; }
    }

    public class DataValidationGradeModel
    {
        public int ChecksIN { set; get; }

        public int ChecksNA { set; get; }

        public int ChecksNC { set; get; }

        public int ChecksOK { set; get; }

        public int DatasetID { set; get; }

        public string FriendlyName { set; get; }

        public int Grade { set; get; }

        public int MissingData { set; get; }

        public decimal? numValue { set; get; }

        public string PropertyName { set; get; }

        public short PropertyNo { set; get; }

        public string Section { set; get; }

        public string strValue { set; get; }
    }

    public class DataValidationSectionModel
    {
        public int ChecksIN { set; get; }

        public int ChecksNA { set; get; }

        public int ChecksNC { set; get; }

        public int ChecksOK { set; get; }

        public int DatasetID { set; get; }

        public int Grade { set; get; }

        public int MissingData { set; get; }

        public decimal PcntComplete { set; get; }

        public short PropertyNo { set; get; }

        public int QuestionNo { set; get; }

        public string Section { set; get; }

        public string SectionID { set; get; }
    }

    public class DataValidationSummaryModel
    {
        public int ChecksIN { set; get; }

        public int ChecksNA { set; get; }

        public int ChecksNC { set; get; }

        public int ChecksOK { set; get; }

        public string DataLevel { set; get; }

        public int DatasetID { set; get; }

        public string FacilityName { set; get; }

        public int Grade { set; get; }

        public string LockedImg { set; get; }

        public int MissingData { set; get; }

        public int ParentID { set; get; }

        public decimal PcntComplete { set; get; }

        public IEnumerable<DataValidationSectionModel> SectionSummary { set; get; }
    }

    public class MissDataModel
    {
        public string FriendlyName { get; set; }

        public string QuestionSectionID { get; set; }

        public string Section { get; set; }
    }

    public class OverallModel
    {
        public int ChecksNA { get; set; }

        public int ChecksNC { get; set; }

        public int Grade { get; set; }

        public string name { get; set; }

        public decimal? PcntComplete { get; set; }
    }

    public class RelatedDataChecks
    {
        public int DatacheckID { get; set; }

        public string DatacheckName { get; set; }

        public short Priority { get; set; }

        public byte SeveritySort { get; set; }

        public string StatusCode { get; set; }
    }
}