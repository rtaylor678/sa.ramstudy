﻿

  
/*  

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using RAM_MvcApp.Annotations;
using System.Globalization;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Mvc;


namespace RAM_MvcApp.Models
{
					
    				
    				public partial class SiteCharacteristicsModel
    				{
					 
					 	public bool isUnderReview {get; set;}
						
			  		    // Table :  SiteChar 
						// Field : AcresOther
						[Required(ErrorMessage="This field is required")]
                        [Numeric(ErrorMessage = "Must be a number")]
                        public double  AcresOther {get; set;}
    							
					    // Table :  SiteChar 
						// Field : AcresUsedbyOffsite
						[Required(ErrorMessage="This field is required")]
                        [Numeric(ErrorMessage = "Must be a number")]
						public double  AcresUsedbyOffsite {get; set;}
    							
					    // Table :  SiteChar 
						// Field : AcresUsedbyUnit
						[Required(ErrorMessage="This field is required")]
                        [Numeric(ErrorMessage = "Must be a number")]
						public double  AcresUsedbyUnit {get; set;}
    							
					    // Table :  SiteChar 
						// Field : AcresUsedbyUtilities
						[Required(ErrorMessage="This field is required")]
                        [Numeric(ErrorMessage = "Must be a number")]
						public double  AcresUsedbyUtilities {get; set;}
    							
					    // Table :  SiteChar 
						// Field : NumPers
						[Required(ErrorMessage="This field is required")]
                        [Numeric(ErrorMessage = "Must be a number")]
						public string  NumPers {get; set;}
    							
					    // Table :  SiteChar 
						// Field : TotalArea
						[Required(ErrorMessage="This field is required")]
                        [Numeric(ErrorMessage = "Must be a number")]
						public double  TotalArea {get; set;}
    							
					}
										
    				
    				public partial class SiteCostModel
    				{
					 
					 	public bool isUnderReview {get; set;}
						
			  		    // Table :  SiteChar 
						// Field : ExchangeRate
						[Required(ErrorMessage="This field is required")]
						public double  ExchangeRate {get; set;}
    							
					    // Table :  SiteChar 
						// Field : LocalCurrency
						[Required(ErrorMessage="This field is required")]
						public string  LocalCurrency {get; set;}
    							
					    // Table :  SiteChar 
						// Field : PRV
						[Required(ErrorMessage="This field is required")]
						public double  PRV {get; set;}
    							
				      	
						// Table :  SiteMaintCosts 
						// Field : CoManHrs
						[Required(ErrorMessage="This field is required")]
						public double  CoManHrs_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : CoManHrs
						[Required(ErrorMessage="This field is required")]
						public double  CoManHrs_CostType_RT {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : ContManHrs
						[Required(ErrorMessage="This field is required")]
						public double  ContManHrs_CostType_RT {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : ContManHrs
						[Required(ErrorMessage="This field is required")]
						public double  ContManHrs_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : DirectMaintLaborExt
						[Required(ErrorMessage="This field is required")]
						public double  DirectMaintLaborExt_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : DirectMaintLaborExt
						[Required(ErrorMessage="This field is required")]
						public double  DirectMaintLaborExt_CostType_RT {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : DirectMaintLaborInt
						[Required(ErrorMessage="This field is required")]
						public double  DirectMaintLaborInt_CostType_RT {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : DirectMaintLaborInt
						[Required(ErrorMessage="This field is required")]
						public double  DirectMaintLaborInt_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : DirectMaintMatlInt
						[Required(ErrorMessage="This field is required")]
						public double  DirectMaintMatlInt_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : DirectMaintMatlInt
						[Required(ErrorMessage="This field is required")]
						public double  DirectMaintMatlInt_CostType_RT {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : IndirectMaintConsumables
						[Required(ErrorMessage="This field is required")]
						public double  IndirectMaintConsumables_CostType_RT {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : IndirectMaintConsumables
						[Required(ErrorMessage="This field is required")]
						public double  IndirectMaintConsumables_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : IndirectMaintSupportExt
						[Required(ErrorMessage="This field is required")]
						public double  IndirectMaintSupportExt_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : IndirectMaintSupportExt
						[Required(ErrorMessage="This field is required")]
						public double  IndirectMaintSupportExt_CostType_RT {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : IndirectMaintSupportInt
						[Required(ErrorMessage="This field is required")]
						public double  IndirectMaintSupportInt_CostType_RT {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : IndirectMaintSupportInt
						[Required(ErrorMessage="This field is required")]
						public double  IndirectMaintSupportInt_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : TotalDirectPlantMaintCosts
						[Required(ErrorMessage="This field is required")]
						public double  TotalDirectPlantMaintCosts_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : TotalDirectPlantMaintCosts
						[Required(ErrorMessage="This field is required")]
						public double  TotalDirectPlantMaintCosts_CostType_RT {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : TotalMaintCosts
						[Required(ErrorMessage="This field is required")]
						public double  TotalMaintCosts_CostType_RT {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : TotalMaintCosts
						[Required(ErrorMessage="This field is required")]
						public double  TotalMaintCosts_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : TotIndirectPlantMaintCosts
						[Required(ErrorMessage="This field is required")]
						public double  TotIndirectPlantMaintCosts_CostType_TA {get; set;}
								      	
						// Table :  SiteMaintCosts 
						// Field : TotIndirectPlantMaintCosts
						[Required(ErrorMessage="This field is required")]
						public double  TotIndirectPlantMaintCosts_CostType_RT {get; set;}
									}
										
    				[MetadataType(typeof(SiteIdentificationMetaData))]
    				public partial class SiteIdentificationModel
    				{
					 
					 	public bool isUnderReview {get; set;}
						
			  					    // Table :  CoordinatorInfo 
						// Field : CompanyName
						[Required(ErrorMessage="This field is required")]
						public string  CompanyName {get; set;}
    							
					    // Table :  CoordinatorInfo 
						// Field : Country
						[Required(ErrorMessage="This field is required")]
						public string  Country {get; set;}
    							
					    // Table :  CoordinatorInfo 
						// Field : EMail
						[Required(ErrorMessage="This field is required")]
                        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",ErrorMessage="Enter an email address")]
                        [DataType(DataType.EmailAddress)]
                        public string  EMail {get; set;}
    							
					    // Table :  CoordinatorInfo 
						// Field : FaxNo
						[Required(ErrorMessage="This field is required")]
						public string  FaxNo {get; set;}
    							
					    // Table :  CoordinatorInfo 
						// Field : JobTitle
						[Required(ErrorMessage="This field is required")]
						public string  JobTitle {get; set;}
    							
					    // Table :  CoordinatorInfo 
						// Field : MailAdd1
						[Required(ErrorMessage="This field is required")]
						public string  MailAdd1 {get; set;}
    							
					    // Table :  CoordinatorInfo 
						// Field : PhoneNo
						[Required(ErrorMessage="This field is required")]
						public string  PhoneNo {get; set;}
    							
					    // Table :  CoordinatorInfo 
						// Field : PlantCoordName
						[Required(ErrorMessage="This field is required")]
						public string  PlantCoordName {get; set;}
    							
					    // Table :  CoordinatorInfo 
						// Field : SiteName
						[Required(ErrorMessage="This field is required")]
						public string  SiteName {get; set;}
    							
					    // Table :  CoordinatorInfo 
						// Field : StreetAdd1
						[Required(ErrorMessage="This field is required")]
						public string  StreetAdd1 {get; set;}
    							
			       }
					public class SiteCharacteristicsMetaData
				    {
                      
                        // Table :  SiteChar 
						// Field : AcresOther
						[Required(ErrorMessage="This field is required")]
				        public object AcresOther { get; set; }    
					
                      
                        // Table :  SiteChar 
						// Field : AcresUsedbyOffsite
						[Required(ErrorMessage="This field is required")]
				        public object AcresUsedbyOffsite { get; set; }    
					
                      
                        // Table :  SiteChar 
						// Field : AcresUsedbyUnit
						[Required(ErrorMessage="This field is required")]
				        public object AcresUsedbyUnit { get; set; }    
					
                      
                        // Table :  SiteChar 
						// Field : AcresUsedbyUtilities
						[Required(ErrorMessage="This field is required")]
				        public object AcresUsedbyUtilities { get; set; }    
					
                      
                        // Table :  SiteChar 
						// Field : NumPers
						[Required(ErrorMessage="This field is required")]
				        public object NumPers { get; set; }    
					
                      
                        // Table :  SiteChar 
						// Field : TotalArea
						[Required(ErrorMessage="This field is required")]
				        public object TotalArea { get; set; }    
					
					} 	
				   
					public class SiteCostMetaData
				    {
                      
                        // Table :  SiteChar 
						// Field : ExchangeRate
						[Required(ErrorMessage="This field is required")]
				        public object ExchangeRate { get; set; }    
					
                      
                        // Table :  SiteChar 
						// Field : LocalCurrency
						[Required(ErrorMessage="This field is required")]
				        public object LocalCurrency { get; set; }    
					
                      
                        // Table :  SiteChar 
						// Field : PRV
						[Required(ErrorMessage="This field is required")]
				        public object PRV { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : CoManHrs
						[Required(ErrorMessage="This field is required")]
				        public object CoManHrs { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : ContManHrs
						[Required(ErrorMessage="This field is required")]
				        public object ContManHrs { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : DirectMaintLaborExt
						[Required(ErrorMessage="This field is required")]
				        public object DirectMaintLaborExt { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : DirectMaintLaborInt
						[Required(ErrorMessage="This field is required")]
				        public object DirectMaintLaborInt { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : DirectMaintMatlInt
						[Required(ErrorMessage="This field is required")]
				        public object DirectMaintMatlInt { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : IndirectMaintConsumables
						[Required(ErrorMessage="This field is required")]
				        public object IndirectMaintConsumables { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : IndirectMaintSupportExt
						[Required(ErrorMessage="This field is required")]
				        public object IndirectMaintSupportExt { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : IndirectMaintSupportInt
						[Required(ErrorMessage="This field is required")]
				        public object IndirectMaintSupportInt { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : TotalDirectPlantMaintCosts
						[Required(ErrorMessage="This field is required")]
				        public object TotalDirectPlantMaintCosts { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : TotalMaintCosts
						[Required(ErrorMessage="This field is required")]
				        public object TotalMaintCosts { get; set; }    
					
                      
                        // Table :  SiteMaintCosts 
						// Field : TotIndirectPlantMaintCosts
						[Required(ErrorMessage="This field is required")]
				        public object TotIndirectPlantMaintCosts { get; set; }    
					
					} 	
				   
					public class SiteIdentificationMetaData
				    {
                      
                        // Table :  CoordinatorInfo 
						// Field : CompanyName
						[Required(ErrorMessage="This field is required")]
				        public object CompanyName { get; set; }    
					
                      
                        // Table :  CoordinatorInfo 
						// Field : Country
						[Required(ErrorMessage="This field is required")]
				        public object Country { get; set; }    
					
                      
                        // Table :  CoordinatorInfo 
						// Field : EMail
						[Required(ErrorMessage="This field is required")]
				        public object EMail { get; set; }    
					
                      
                        // Table :  CoordinatorInfo 
						// Field : FaxNo
						[Required(ErrorMessage="This field is required")]
				        public object FaxNo { get; set; }    
					
                      
                        // Table :  CoordinatorInfo 
						// Field : JobTitle
						[Required(ErrorMessage="This field is required")]
				        public object JobTitle { get; set; }    
					
                      
                        // Table :  CoordinatorInfo 
						// Field : MailAdd1
						[Required(ErrorMessage="This field is required")]
				        public object MailAdd1 { get; set; }    
					
                      
                        // Table :  CoordinatorInfo 
						// Field : PhoneNo
						[Required(ErrorMessage="This field is required")]
				        public object PhoneNo { get; set; }    
					
                      
                        // Table :  CoordinatorInfo 
						// Field : PlantCoordName
						[Required(ErrorMessage="This field is required")]
				        public object PlantCoordName { get; set; }    
					
                      
                        // Table :  CoordinatorInfo 
						// Field : SiteName
						[Required(ErrorMessage="This field is required")]
				        public object SiteName { get; set; }    
					
                      
                        // Table :  CoordinatorInfo 
						// Field : StreetAdd1
						[Required(ErrorMessage="This field is required")]
				        public object StreetAdd1 { get; set; }    
					
				}		

}
*/