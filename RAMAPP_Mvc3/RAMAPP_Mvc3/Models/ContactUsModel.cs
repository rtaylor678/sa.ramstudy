﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RAMAPP_Mvc3.Models
{
    public class ContactUsModel
    {
        [Required(ErrorMessage = "Name is required")]
        [DisplayName("Name")]
        public string authorsName { get; set; }

        
        [DisplayName("Company")]
        public string companyName { get; set; }


        [DisplayName("Location")]
        public string location { get; set; }


        [Required(ErrorMessage = "Email address is required")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Valid Email Address is required.")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email Address")]
        public string emailAddress { get; set; }

        [Required(ErrorMessage = "Subject is required")]
        [DisplayName("Subject")]
        public string emailSubject{ get; set; }
        
        [Required(ErrorMessage="Message is required")]
        [DisplayName("Body")]
        public string emailBody { get; set; }

       
    }


}
