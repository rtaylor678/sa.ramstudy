﻿


 
  


using System.ComponentModel.DataAnnotations;


namespace  RAMAPP_Mvc3.Models
{

					
					/*[MetadataType(typeof(ST_CHARMetaData))]*/
					public partial class ST_CHARModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : SiteChar 
						// Field : ExchangeRate
												public decimal?  ExchangeRate {get; set;}
				        
											
						// Table : SiteChar 
						// Field : LocalCurrency
												public string  LocalCurrency {get; set;}
				        
											
						// Table : SiteChar 
						// Field : PRVCalcMethod
												public string  PRVCalcMethod {get; set;}
				        
											
						// Table : SiteChar 
						// Field : TotalArea
												public decimal?  SiteAreaTotal {get; set;}
				        
											
						// Table : SiteChar 
						// Field : AcresUsedbyUnit
												public decimal?  SiteAreaUnit {get; set;}
				        
											
						// Table : SiteChar 
						// Field : 
												public string  SiteAreaUOM {get; set;}
				        
											
						// Table : SiteChar 
						// Field : NumPers
												public string  SiteNumPers {get; set;}
				        
											
						// Table : SiteChar 
						// Field : NumPersInt
												public int?  SiteNumPersInt {get; set;}
				        
											
						// Table : SiteChar 
						// Field : SitePRV
												public decimal?  SitePRV {get; set;}
				        
											
						// Table : SiteChar 
						// Field : SitePRVUSD
												public decimal?  SitePRVUSD {get; set;}
				        
											
						// Table : SiteSafetyInjury 
						// Field : CoNumInjuries
												public int?  SiteSafetyCurrInjuryCntComp_RT {get; set;}
				        
											
						// Table : SiteSafetyInjury 
						// Field : ContNumInjuries
												public int?  SiteSafetyCurrInjuryCntCont_RT {get; set;}
				        
											
						// Table : SiteSafetyInjury 
						// Field : TotNumInjuries
												public int?  SiteSafetyCurrInjuryCntTot_RT {get; set;}
				        
											
						// Table : SiteSafety 
						// Field : 
												public string  SiteSafetyPreTaskAnalysis {get; set;}
				        
											
						// Table :  
						// Field : 
												public string  SiteSafetyReturnToOperation {get; set;}
				        
											
						// Table :  
						// Field : 
												public string  SiteSafetySafeWorkPermit {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : BadActorList
												public string  SiteStrategyBadActorList {get; set;}
				        
											
						// Table : SiteCharacteristics 
						// Field : 
												public decimal?  SiteStrategyEquipRelDocPcnt {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : GoalsEngTech
												public string  SiteStrategyGoalsEngTech {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : GoalsProdStaff
												public string  SiteStrategyGoalsProdStaff {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : GoalsProdTechs
												public string  SiteStrategyGoalsProdTechs {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : GoalsRAMCraft
												public string  SiteStrategyGoalsRamCraft {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : GoalsRAMStaff
												public string  SiteStrategyGoalsRamStaff {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : GoalsSuppProc
												public string  SiteStrategyGoalsSuppProc {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : LCCA
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteStrategyLCCA {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : MgrObjective
												public string  SiteStrategyMgrObjectives {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : MOC
												public string  SiteStrategyMOC {get; set;}


                        // Table : SiteMaintStrategy 
                        // Field : BadActorPct
                                                public string SiteStrategyBadActorPct { get; set; }
											
						// Table : SiteMaintStrategy 
						// Field : NumKPI
												public decimal?  SiteStrategyNumKPI {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : NumKPIDaily
												public decimal?  SiteStrategyNumKPIDaily {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : NumKPIMonthly
												public decimal?  SiteStrategyNumKPIMonthly {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : NumKPIWeekly
												public decimal?  SiteStrategyNumKPIWeekly {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : DocObjective
												public string  SiteStrategyObjectivesDoc {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : PrefIndicators
												public string  SiteStrategyPerfIndicators {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : PersonnelCert
												public string  SiteStrategyPersonnelCert {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : DocPlan
												public string  SiteStrategyPlanDoc {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : DocPlanYrs
												public decimal?  SiteStrategyPlanDocYrs {get; set;}
				        
											
						// Table : SiteMaintStrategy 
						// Field : ReliModeling
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteStrategyReliModeling {get; set;}
				        
															}
										
					/*[MetadataType(typeof(ST_COSTMetaData))]*/
					public partial class ST_COSTModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : SiteChar 
						// Field : ExchangeRate
												public decimal?  _ExchangeRate {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborInt
												public decimal?  _SiteExpCompLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborInt
												public decimal?  _SiteExpCompLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborExt
												public decimal?  _SiteExpContLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborExt
												public decimal?  _SiteExpContLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotLabor
												public decimal?  _SiteExpTotLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotLabor
												public decimal?  _SiteExpTotLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborInt
												public decimal?  _SiteMCptlCompLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborInt
												public decimal?  _SiteMCptlCompLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborExt
												public decimal?  _SiteMCptlContLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborExt
												public decimal?  _SiteMCptlContLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborExt
												public decimal?  _SiteMCptlTotLabor_RT {get; set;}
                        
				                                // Table : SiteMaintCapCosts 
                                                // Field : Metric
                                                public decimal? SiteMCptlMetricLabor_RT { get; set; }

                                                // Table : SiteMaintCapCosts 
                                                // Field : Metric
                                                public decimal? SiteMCptlMetricManHrs_RT { get; set; }

								                // Table : SiteMaintCapCosts 
                                                // Field : Metric
                                                public decimal? SiteMCptlMetricWageRate_RT { get; set; }

                                                // Table : SiteMaintCapCosts 
                                                // Field : Metric
                                                public decimal? SiteMCptlMetricWageRateUSD_RT { get; set; }
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborExt
												public decimal?  _SiteMCptlTotLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborInt
												public decimal?  SiteCostsCompLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborInt
												public decimal?  SiteCostsCompLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintMatlInt
												public decimal?  SiteCostsCompMatl_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintMatlInt
												public decimal?  SiteCostsCompMatl_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborExt
												public decimal?  SiteCostsContLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborExt
												public decimal?  SiteCostsContLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintMatlExt
												public decimal?  SiteCostsContMatl_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintMatlExt
												public decimal?  SiteCostsContMatl_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : IndirectMaintSupportInt
												public decimal?  SiteCostsIndirectCompSupport_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : IndirectMaintSupportInt
												public decimal?  SiteCostsIndirectCompSupport_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : IndirectMaintSupportExt
												public decimal?  SiteCostsIndirectContSupport_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : IndirectMaintSupportExt
												public decimal?  SiteCostsIndirectContSupport_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotalDirectPlantMaintCosts
												public decimal?  SiteCostsTotDirect_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotalDirectPlantMaintCosts
												public decimal?  SiteCostsTotDirect_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotIndirectPlantMaintCosts
												public decimal?  SiteCostsTotIndirect_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotIndirectPlantMaintCosts
												public decimal?  SiteCostsTotIndirect_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotLabor
												public decimal?  SiteCostsTotLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotLabor
												public decimal?  SiteCostsTotLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotMaintCosts
												public decimal?  SiteCostsTotMaint_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotMaintCosts
												public decimal?  SiteCostsTotMaint_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotMatl
												public decimal?  SiteCostsTotMatl_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotMatl
												public decimal?  SiteCostsTotMatl_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : AvgWage
												public decimal?  SiteExpAvgWageRate_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : AvgWage
												public decimal?  SiteExpAvgWageRate_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : AvgWageUSD
												public decimal?  SiteExpAvgWageRateUSD_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : AvgWageUSD
												public decimal?  SiteExpAvgWageRateUSD_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborInt
												public decimal?  SiteExpCompLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborInt
												public decimal?  SiteExpCompLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : CoManHrs
												public decimal?  SiteExpCompManHrs_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : CoManHrs
												public decimal?  SiteExpCompManHrs_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintMatlInt
												public decimal?  SiteExpCompMatl_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintMatlInt
												public decimal?  SiteExpCompMatl_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : CoWage
												public decimal?  SiteExpCompWageRate_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : CoWage
												public decimal?  SiteExpCompWageRate_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : CoWageUSD
												public decimal?  SiteExpCompWageRateUSD_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : CoWageUSD
												public decimal?  SiteExpCompWageRateUSD_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborExt
												public decimal?  SiteExpContLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintLaborExt
												public decimal?  SiteExpContLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : ContManHrs
												public decimal?  SiteExpContManHrs_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : ContManHrs
												public decimal?  SiteExpContManHrs_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintMatlExt
												public decimal?  SiteExpContMatl_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : DirectMaintMatlExt
												public decimal?  SiteExpContMatl_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : ContWage
												public decimal?  SiteExpContWageRate_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : ContWage
												public decimal?  SiteExpContWageRate_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : ContWageUSD
												public decimal?  SiteExpContWageRateUSD_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : ContWageUSD
												public decimal?  SiteExpContWageRateUSD_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : IndirectMaintSupportInt
												public decimal?  SiteExpIndirectCompSupport_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : IndirectMaintSupportInt
												public decimal?  SiteExpIndirectCompSupport_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : IndirectMaintSupportExt
												public decimal?  SiteExpIndirectContSupport_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : IndirectMaintSupportExt
												public decimal?  SiteExpIndirectContSupport_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotalDirectPlantMaintCosts
												public decimal?  SiteExpTotDirect_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotalDirectPlantMaintCosts
												public decimal?  SiteExpTotDirect_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotIndirectPlantMaintCosts
												public decimal?  SiteExpTotIndirect_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotIndirectPlantMaintCosts
												public decimal?  SiteExpTotIndirect_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotLabor
												public decimal?  SiteExpTotLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotLabor
												public decimal?  SiteExpTotLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotMaintCosts
												public decimal?  SiteExpTotMaint_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotMaintCosts
												public decimal?  SiteExpTotMaint_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotManHrs
												public decimal?  SiteExpTotManHrs_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotManHrs
												public decimal?  SiteExpTotManHrs_TA {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotMatl
												public decimal?  SiteExpTotMatl_RT {get; set;}
				        
											
						// Table : SiteMaintCosts 
						// Field : TotMatl
												public decimal?  SiteExpTotMatl_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : AvgWage
												public decimal?  SiteMCptlAvgWageRate_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : AvgWage
												public decimal?  SiteMCptlAvgWageRate_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : AvgWageUSD
												public decimal?  SiteMCptlAvgWageRateUSD_RT {get; set;}

                        // Table : SiteMaintCapCosts 
                        // Field : PctFTEContractors
                                                public decimal? SiteMCptlPctFTEContractors { get; set; }
				        
											
						// Table : SiteMaintCapCosts 
						// Field : AvgWageUSD
												public decimal?  SiteMCptlAvgWageRateUSD_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : IndirectMaintSupportInt
												public decimal?  SiteMCptlCompIndirectCost_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : IndirectMaintSupportInt
												public decimal?  SiteMCptlCompIndirectCost_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborInt
												public decimal?  SiteMCptlCompLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborInt
												public decimal?  SiteMCptlCompLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : CoManHrs
												public decimal?  SiteMCptlCompManHrs_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : CoManHrs
												public decimal?  SiteMCptlCompManHrs_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintMatlInt
												public decimal?  SiteMCptlCompMatl_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintMatlInt
												public decimal?  SiteMCptlCompMatl_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : CoWage
												public decimal?  SiteMCptlCompWageRate_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : CoWage
												public decimal?  SiteMCptlCompWageRate_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : CoWageUSD
												public decimal?  SiteMCptlCompWageRateUSD_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : CoWageUSD
												public decimal?  SiteMCptlCompWageRateUSD_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : IndirectMaintSupportExt
												public decimal?  SiteMCptlContIndirectCost_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : IndirectMaintSupportExt
												public decimal?  SiteMCptlContIndirectCost_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborExt
												public decimal?  SiteMCptlContLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborExt
												public decimal?  SiteMCptlContLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : ContManHrs
												public decimal?  SiteMCptlContManHrs_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : ContManHrs
												public decimal?  SiteMCptlContManHrs_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintMatlExt
												public decimal?  SiteMCptlContMatl_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintMatlExt
												public decimal?  SiteMCptlContMatl_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : ContWage
												public decimal?  SiteMCptlContWageRate_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : ContWage
												public decimal?  SiteMCptlContWageRate_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : ContWageUSD
												public decimal?  SiteMCptlContWageRateUSD_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : ContWageUSD
												public decimal?  SiteMCptlContWageRateUSD_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : TotPlantMaintCosts
												public decimal?  SiteMCptlTotCost_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : TotPlantMaintCosts
												public decimal?  SiteMCptlTotCost_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : TotalDirectPlantMaintCosts
												public decimal?  SiteMCptlTotDirectCost_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : TotalDirectPlantMaintCosts
												public decimal?  SiteMCptlTotDirectCost_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : TotIndirectPlantMaintCosts
												public decimal?  SiteMCptlTotIndirectCost_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : TotIndirectPlantMaintCosts
												public decimal?  SiteMCptlTotIndirectCost_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborExt
												public decimal?  SiteMCptlTotLabor_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : DirectMaintLaborExt
												public decimal?  SiteMCptlTotLabor_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : TotManHrs
												public decimal?  SiteMCptlTotManHrs_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : TotManHrs
												public decimal?  SiteMCptlTotManHrs_TA {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : TotMatl
												public decimal?  SiteMCptlTotMatl_RT {get; set;}
				        
											
						// Table : SiteMaintCapCosts 
						// Field : TotMatl
												public decimal?  SiteMCptlTotMatl_TA {get; set;}
				        
															}
										
					/*[MetadataType(typeof(ST_CRAFTCHARMetaData))]*/
					public partial class ST_CRAFTCHARModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : SiteWorkEnvironChar 
						// Field : AvgTrainHrs
												public string  SiteWorkEnvirAvgTrainHrs_COFP {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : AvgTrainHrs
												public string  SiteWorkEnvirAvgTrainHrs_COIE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : AvgTrainHrs
												public string  SiteWorkEnvirAvgTrainHrs_CORE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : AvgTrainHrs
												public string  SiteWorkEnvirAvgTrainHrs_CRFP {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : AvgTrainHrs
												public string  SiteWorkEnvirAvgTrainHrs_CRIE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : AvgTrainHrs
												public string  SiteWorkEnvirAvgTrainHrs_CRRE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : ExperInJob
												public string  SiteWorkEnvirExperInJob_COFP {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : ExperInJob
												public string  SiteWorkEnvirExperInJob_COIE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : ExperInJob
												public string  SiteWorkEnvirExperInJob_CORE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : ExperInJob
												public string  SiteWorkEnvirExperInJob_CRFP {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : ExperInJob
												public string  SiteWorkEnvirExperInJob_CRIE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : ExperInJob
												public string  SiteWorkEnvirExperInJob_CRRE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : LaborUnion
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirLaborUnion_CO {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : LaborUnion
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirLaborUnion_CR {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : OTPcnt
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirOTPcnt_CO {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : OTPcnt
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirOTPcnt_CR {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : OTPcntTarget
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirOTTarget_CO {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : OTPcntTarget
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirOTTarget_CR {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : PlanAvgTrainHrs
												public decimal?  SiteWorkEnvirPlanTrainHrs_COFP {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : PlanAvgTrainHrs
												public decimal?  SiteWorkEnvirPlanTrainHrs_COIE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : PlanAvgTrainHrs
												public decimal?  SiteWorkEnvirPlanTrainHrs_CORE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : PlanAvgTrainHrs
												public decimal?  SiteWorkEnvirPlanTrainHrs_CRFP {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : PlanAvgTrainHrs
												public decimal?  SiteWorkEnvirPlanTrainHrs_CRIE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : PlanAvgTrainHrs
												public decimal?  SiteWorkEnvirPlanTrainHrs_CRRE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : RCARCFA
												public decimal?  SiteWorkEnvirRCARCFA_CO {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : RCARCFA
												public decimal?  SiteWorkEnvirRCARCFA_CR {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : RegsCert
												public string  SiteWorkEnvirRegsCert_CO {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : RegsCert
												public string  SiteWorkEnvirRegsCert_CR {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : ReqDocFails
												public string  SiteWorkEnvirReqDocFail_CO {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : ReqDocFails
												public string  SiteWorkEnvirReqDocFail_CR {get; set;}

                                                // Table : SiteWorkEnvironChar 
                                                // Field : AvgTrainHrs_Range
                                                public string SiteWorkEnvirAvgTrainHrs_CORE_Range { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : ExperInJob_Range
                                                public string SiteWorkEnvirExperInJob_CORE_Range { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : AvgTrainHrs_Range
                                                public string SiteWorkEnvirAvgTrainHrs_CRRE_Range { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : ExperInJob_Range
                                                public string SiteWorkEnvirExperInJob_CRRE_Range { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : ExperInJob
                                                public string SiteWorkEnvirExperInJob_CRFP_Rg { get; set; }


                                                // Table : SiteWorkEnvironChar 
                                                // Field : Sector
                                                public string SiteWorkEnvirSector_COFP_Range { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : PlanTrainHrs
                                                public string SiteWorkEnvirPlanTrainHrs_COFP_Rg { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : AvgTrainHrs
                                                public string SiteWorkEnvirAvgTrainHrs_CRFP_Rg { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : AvgTrainHrs
                                                public string SiteWorkEnvirAvgTrainHrs_COIE_Rg { get; set; }


                                                // Table : SiteWorkEnvironChar 
                                                // Field : ExperInJob
                                                public string SiteWorkEnvirExperInJob_COIE_Rg { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : AvgTrainHrs
                                                public string SiteWorkEnvirAvgTrainHrs_CRIE_Rg { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : ExperInJob
                                                public string SiteWorkEnvirExperInJob_CRIE_Rg { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARCFA
                                                public string SiteWorkEnvirRCARCFA_CO_Rg { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARCFA
                                                public string SiteWorkEnvirRCARCFA_CR_Rg { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARCPQ
                                                public string SiteWorkEnvirRCARCPQ_CO { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARCPQ
                                                public string SiteWorkEnvirRCARCPQ_CR { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARMWPO
                                                public string SiteWorkEnvirRCARMWPO_CO { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARMWPO
                                                public string SiteWorkEnvirRCARMWPO_CR { get; set; }
                        
                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARACTB
                                                public string SiteWorkEnvirRCARACTB_CO { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARACTB
                                                public string SiteWorkEnvirRCARACTB_CR { get; set; }
                        
                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARWTSP
                                                public string SiteWorkEnvirRCARWTSP_CO { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARWTSP
                                                public string SiteWorkEnvirRCARWTSP_CR { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARWPA
                                                public string SiteWorkEnvirRCARWPA_CO { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARWPA
                                                public string SiteWorkEnvirRCARWPA_CR { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARCPA
                                                public string SiteWorkEnvirRCARCPA_CO { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARCPA
                                                public int? SiteWorkEnvirRCARCPA_CR { get; set; }
                        
                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARCWQA
                                                public string SiteWorkEnvirRCARCWQA_CO { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARCWQA
                                                public int? SiteWorkEnvirRCARCWQA_CR { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARRPRM
                                                public string SiteWorkEnvirRCARRPRM_CO { get; set; }

                                                // Table : SiteWorkEnvironChar 
                                                // Field : RCARRPRM
                                                public int? SiteWorkEnvirRCARRPRM_CR { get; set; }
						// Table : SiteWorkEnvironChar 
						// Field : Sector
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirSector_COFP {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : Sector
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirSector_COIE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : Sector
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirSector_CORE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : Sector
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirSector_CRFP {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : Sector
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirSector_CRIE {get; set;}
				        
											
						// Table : SiteWorkEnvironChar 
						// Field : Sector
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkEnvirSector_CRRE {get; set;}
				        
															}
										
					/*[MetadataType(typeof(ST_IDENTMetaData))]*/
					public partial class ST_IDENTModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : CoordinatorInfo 
						// Field : CompanyName
												public string  CompanyName {get; set;}
				        
											
						// Table : CoordinatorInfo 
						// Field : MailAdd1
												public string  MailAddr1 {get; set;}
				        
											
						// Table : CoordinatorInfo 
						// Field : MaintenanceManager
												public string  MaintMgr {get; set;}
				        
											
						// Table : CoordinatorInfo 
						// Field : MaintenanceManagerEmail
												public string  MaintMgrEmail {get; set;}
				        
											
						// Table : SiteInfo 
						// Field : Country
												public string  SiteCountry {get; set;}
				        
											
						// Table : CoordinatorInfo 
						// Field : SiteManager
												public string  SiteMgr {get; set;}
				        
											
						// Table : CoordinatorInfo 
						// Field : SiteManagerEmail
												public string  SiteMgrEmail {get; set;}
				        
											
						// Table : CoordinatorInfo 
						// Field : SiteName
												public string  SiteName {get; set;}
				        
											
						// Table : CoordinatorInfo 
						// Field : StreetAdd1
												public string  StreetAddr1 {get; set;}
				        
															}
										
					/*[MetadataType(typeof(ST_INVENMetaData))]*/
					public partial class ST_INVENModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : SiteSparesInventory 
						// Field : CatalogLineItems
												public decimal?  SiteSparesCatalogLineItemCnt {get; set;}
				        
											
						// Table : SiteSparesInventory 
						// Field : CosignOrVendor
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteSparesCosignOrVendorPcnt {get; set;}
				        
											
						// Table : SiteSparesInventory 
						// Field : PlantInvenVal
												public decimal?  SiteSparesInventory {get; set;}
				        
											
						// Table : SiteSparesInventory 
						// Field : PlantInvenValUSD
												public decimal?  SiteSparesInventoryUSD {get; set;}
				        
											
						// Table : SiteSparesInventory 
						// Field : PlantStoresIssuesVal
												public decimal?  SiteSparesPlantStoresIssues {get; set;}
				        
											
						// Table : SiteSparesInventory 
						// Field : PlantStoresIssuesValUSD
												public decimal?  SiteSparesPlantStoresIssuesUSD {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : UnsatisfiedFirstRequest
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWhsOrgAvgStockOutPcnt {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : ClosedStorePolicy
												public string  SiteWhsOrgClosedStoredPolicy {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : LCCAStrRmItems
												public string  SiteWhsOrgLCCAStrRmItems {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : LwCstHiVolMatrl
												public string  SiteWhsOrgLowCostFreeIssue {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : OperatingCst
												public decimal?  SiteWhsOrgOpCost {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : OperatingCstUSD
												public decimal?  SiteWhsOrgOpCostUSD {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : StorermOutsource
												public string  SiteWhsOrgOutsourced {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : PcntCriticalRank
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWhsOrgPcntCriticalRank {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : PctMatlDelivered
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWhsOrgPcntDelivered {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : PctKitted
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWhsOrgPcntKitted {get; set;}
				        
											
						// Table : SiteWarehouseOrg 
						// Field : PrevMaintStrRmItems
												public string  SiteWhsOrgPrevMaintStrRmItems {get; set;}
				        
															}
										
					/*[MetadataType(typeof(ST_MAINTMetaData))]*/
					public partial class ST_MAINTModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : SiteMaintCategories 
						// Field : Insulation
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  CostCatInsulation_RT {get; set;}
				        
											
						// Table : SiteMaintCategories 
						// Field : Insulation
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  CostCatInsulation_TA {get; set;}
				        
											
						// Table : SiteMaintCategories 
						// Field : Painting
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  CostCatPainting_RT {get; set;}
				        
											
						// Table : SiteMaintCategories 
						// Field : Painting
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  CostCatPainting_TA {get; set;}
				        
											
						// Table : SiteMaintCategories 
						// Field : Scaffolding
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  CostCatScaffolding_RT {get; set;}
				        
											
						// Table : SiteMaintCategories 
						// Field : Scaffolding
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  CostCatScaffolding_TA {get; set;}
				        
											
						// Table : SiteMaintCategories 
						// Field : TotGeneralMaintCosts
													[Range(100,100, ErrorMessage=" The total should equal to 100 percent")] 
												public decimal?  CostCatTotGeneral_RT {get; set;}
				        
											
						// Table : SiteMaintCategories 
						// Field : TotGeneralMaintCosts
													[Range(100,100, ErrorMessage=" The total should equal to 100 percent")]
                                                    public decimal? CostCatTotGeneral_TA { get; set; }


                        // Table : SiteMaintCategories 
                        // Field : MaterialCost
                                                    [Range(0, 100, ErrorMessage = "The field value is outside the range of 0 to 100 percent.")]
                                                    public decimal? CostCatMaterialCost_RT { get; set; }


                        // Table : SiteMaintCategories 
                        // Field : MaterialCost
                                                    [Range(0, 100, ErrorMessage = "The field value is outside the range of 0 to 100 percent.")]
                                                    public decimal? CostCatMaterialCost_TA { get; set; }
															}
										
					/*[MetadataType(typeof(ST_MT_TAMetaData))]*/
					public partial class ST_MT_TAModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : SiteMaintTA 
						// Field : 
												public decimal?  SiteTA10YearCnt {get; set;}
				        
											
						// Table : SiteMaintTA 
						// Field : AvgDurShutdwn
												public decimal?  SiteTAAvgDaysDown {get; set;}
				        
											
						// Table : SiteMaintTA 
						// Field : DoSiteShutdowns
												public string  SiteTATaken {get; set;}
				        
															}
										
					/*[MetadataType(typeof(ST_ORGMetaData))]*/
					public partial class ST_ORGModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : SiteCorpPersSupport 
						// Field : MaintEng
												public decimal?  CorpPersMaintEngr_CO {get; set;}
				        
											
						// Table : SiteCorpPersSupport 
						// Field : MaintEng
												public decimal?  CorpPersMaintEngr_CR {get; set;}
				        
											
						// Table : SiteCorpPersSupport 
						// Field : MaintEng
												public decimal?  CorpPersMaintEngr_Tot {get; set;}
				        
											
						// Table : SiteCorpPersSupport 
						// Field : DirectorsMgrs
												public decimal?  CorpPersMaintMgr_CO {get; set;}
				        
											
						// Table : SiteCorpPersSupport 
						// Field : DirectorsMgrs
												public decimal?  CorpPersMaintMgr_CR {get; set;}
				        
											
						// Table : SiteCorpPersSupport 
						// Field : DirectorsMgrs
												public decimal?  CorpPersMaintMgr_Tot {get; set;}
				        
											
						// Table : SiteCorpPersSupport 
						// Field : ReliabilityEng
												public decimal?  CorpPersRelEngr_CO {get; set;}
				        
											
						// Table : SiteCorpPersSupport 
						// Field : ReliabilityEng
												public decimal?  CorpPersRelEngr_CR {get; set;}
				        
											
						// Table : SiteCorpPersSupport 
						// Field : ReliabilityEng
												public decimal?  CorpPersRelEngr_Tot {get; set;}
				        
											
						// Table : SiteMaintOrg 
						// Field : CoMaintOrg
												public string  SiteOrgCompMaintOrg {get; set;}
				        
											
						// Table : SiteMaintOrg 
						// Field : CoReportsTo
												public string  SiteOrgCompMaintOrgReportsTo {get; set;}
				        
											
						// Table : SiteMaintOrg 
						// Field : TotalLvls
												public int?  SiteOrgLevelsCEOtoCraft {get; set;}
				        
											
						// Table : SiteMaintOrg 
						// Field : OrgLevels
												public int?  SiteOrgLevelsGMtoCraft {get; set;}
				        
											
						// Table : SiteMaintOrg 
						// Field : OverallPlantOrg
												public string  SiteOrgOverallPlant {get; set;}
				        
											
						// Table : SiteMaintOrg 
						// Field : ReportsTo
												public string  SiteOrgPrimaryLeader {get; set;}
				        
											
						// Table : SiteMaintOrg 
						// Field : UpperOrgLevels
												public int?  SiteOrgUpperLevels {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : Clerical
												public decimal?  SitePersMaintAdmin_CO {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : Clerical
												public decimal?  SitePersMaintAdmin_CR {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : Clerical
												public decimal?  SitePersMaintAdmin_Tot {get; set;}

                                                // Table : SitePersSupport 
                                                // Field : Maint
                                                public string CorpPersRelEngrExpr { get; set; }
				        
											
						// Table : SitePersSupport 
						// Field : MaintEng
												public decimal?  SitePersMaintEngr_CO {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintEng
												public decimal?  SitePersMaintEngr_CR {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintEng
												public decimal?  SitePersMaintEngr_Tot {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintManager
												public decimal?  SitePersMaintMgr_CO {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintManager
												public decimal?  SitePersMaintMgr_CR {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintManager
												public decimal?  SitePersMaintMgr_Tot {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MSPMStaff
												public decimal?  SitePersMaintMSPMStaff_CO {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MSPMStaff
												public decimal?  SitePersMaintMSPMStaff_CR {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MSPMStaff
												public decimal?  SitePersMaintMSPMStaff_TOT {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : Planners
												public decimal?  SitePersMaintPlanner_CO {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : Planners
												public decimal?  SitePersMaintPlanner_CR {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : Planners
												public decimal?  SitePersMaintPlanner_Tot {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintSched
												public decimal?  SitePersMaintSched_CO {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintSched
												public decimal?  SitePersMaintSched_CR {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintSched
												public decimal?  SitePersMaintSched_Tot {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : StoreroomMgrs
												public decimal?  SitePersMaintStoreroomMgrs_CO {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : StoreroomMgrs
												public decimal?  SitePersMaintStoreroomMgrs_CR {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : StoreroomMgrs
												public decimal?  SitePersMaintStoreroomMgrs_TOT {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintSupervisor
												public decimal?  SitePersMaintSupv_CO {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintSupervisor
												public decimal?  SitePersMaintSupv_CR {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : MaintSupervisor
												public decimal?  SitePersMaintSupv_Tot {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : Technical
												public decimal?  SitePersTechnical_CO {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : Technical
												public decimal?  SitePersTechnical_CR {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : Technical
												public decimal?  SitePersTechnical_Tot {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : TotalSupport
												public decimal?  SitePersTotSupport_CO {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : TotalSupport
												public decimal?  SitePersTotSupport_CR {get; set;}
				        
											
						// Table : SitePersSupport 
						// Field : TotalSupport
												public decimal?  SitePersTotSupport_Tot {get; set;}
				        
															}
										
					/*[MetadataType(typeof(ST_PROCESSMetaData))]*/
					public partial class ST_PROCESSModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : SiteDocumentation 
						// Field : Defined
												public string  SiteDocDefinedWorkflow_RT {get; set;}
				        
											
						// Table : SiteDocumentation 
						// Field : Defined
												public string  SiteDocDefinedWorkflow_TA {get; set;}

                                                // Table : SiteDocumentation 
                                                // Field : Defined
                                                public string SiteDocDefinedWorkflow1_RT { get; set; }


                                                // Table : SiteDocumentation 
                                                // Field : Defined
                                                public string SiteDocDefinedWorkflow1_TA { get; set; }

                                                // Table : SiteDocumentation 
                                                // Field : PcntTM
                                                public decimal? SiteDocWorkflowPcntTM { get; set; }


                                                // Table : SiteDocumentation 
                                                // Field : PcntMR
                                                public decimal? SiteDocWorkflowPcntMR { get; set; }

                                                // Table : SiteDocumentation 
                                                // Field : PcntFP
                                                public decimal? SiteDocWorkflowPcntFP { get; set; }


                                                // Table : SiteDocumentation 
                                                // Field : PcntAE
                                                public decimal? SiteDocWorkflowPcntAE { get; set; }

                                                // Table : SiteDocumentation 
                                                // Field : PcntOT
                                                public decimal? SiteDocWorkflowPcntOT { get; set; }


                                                // Table : SiteDocumentation 
                                                // Field : PcntTT
                                                public decimal? SiteDocWorkflowPcntTT { get; set; }

                                                // Table : SiteDocumentation 
                                                // Field : WorkPlan
                                                public string SiteDocWorkflowWorkPlan { get; set; }

                                                // Table : SiteDocumentation 
                                                // Field : WorkSchedule
                                                public string SiteDocWorkflowWorkSchedule { get; set; }

                                                // Table : SiteDocumentation 
                                                // Field : JobPlan
                                                public string SiteWorkOrdersJobPlan { get; set; }
											
						// Table : SiteDocumentation 
						// Field : Accessible
												public string  SiteDocWorkflowExtAssessed_RT {get; set;}
				        
											
						// Table : SiteDocumentation 
						// Field : Accessible
												public string  SiteDocWorkflowExtAssessed_TA {get; set;}
				        
											
						// Table : SiteDocumentation 
						// Field : Followed
												public string  SiteDocWorkflowFollowed_RT {get; set;}
				        
											
						// Table : SiteDocumentation 
						// Field : Followed
												public string  SiteDocWorkflowFollowed_TA {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgPlannedBckLog
												public decimal?  SiteWorkOrdersAvgPlanBacklog_COFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgPlannedBckLog
												public decimal?  SiteWorkOrdersAvgPlanBacklog_COIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgPlannedBckLog
												public decimal?  SiteWorkOrdersAvgPlanBacklog_CORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgPlannedBckLog
												public decimal?  SiteWorkOrdersAvgPlanBacklog_PREFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgPlannedBckLog
												public decimal?  SiteWorkOrdersAvgPlanBacklog_PREIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgPlannedBckLog
												public decimal?  SiteWorkOrdersAvgPlanBacklog_PRERE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgPlannedBckLog
												public decimal?  SiteWorkOrdersAvgPlanBacklog_PROFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgPlannedBckLog
												public decimal?  SiteWorkOrdersAvgPlanBacklog_PROIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgPlannedBckLog
												public decimal?  SiteWorkOrdersAvgPlanBacklog_PRORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgReadyBckLog
												public decimal?  SiteWorkOrdersAvgReadyBacklog_COFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgReadyBckLog
												public decimal?  SiteWorkOrdersAvgReadyBacklog_COIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgReadyBckLog
												public decimal?  SiteWorkOrdersAvgReadyBacklog_CORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgReadyBckLog
												public decimal?  SiteWorkOrdersAvgReadyBacklog_PREFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgReadyBckLog
												public decimal?  SiteWorkOrdersAvgReadyBacklog_PREIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgReadyBckLog
												public decimal?  SiteWorkOrdersAvgReadyBacklog_PRERE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgReadyBckLog
												public decimal?  SiteWorkOrdersAvgReadyBacklog_PROFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgReadyBckLog
												public decimal?  SiteWorkOrdersAvgReadyBacklog_PROIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : AvgReadyBckLog
												public decimal?  SiteWorkOrdersAvgReadyBacklog_PRORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : EmergencyOrders
												public decimal?  SiteWorkOrdersEmergencyOrders_FP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : EmergencyOrders
												public decimal?  SiteWorkOrdersEmergencyOrders_IE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : EmergencyOrders
												public decimal?  SiteWorkOrdersEmergencyOrders_RE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : EmergencyOrders
												public decimal?  SiteWorkOrdersEmergencyOrders_Tot {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : FrqMesureCmpl
												public string  SiteWorkOrdersFreqMeasureCmpl {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : LibraryReport
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkOrdersLibraryReport_COTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : LibraryReport
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkOrdersLibraryReport_PRETOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : LibraryReport
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkOrdersLibraryReport_PROTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : LibraryStorage
												public decimal?  SiteWorkOrdersLibraryStorage_COTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : LibraryStorage
												public decimal?  SiteWorkOrdersLibraryStorage_PRETOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : LibraryStorage
												public decimal?  SiteWorkOrdersLibraryStorage_PROTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : PcntCapacitySched
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  SiteWorkOrdersPcntCapacitySched {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_CAPFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_CAPIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_CAPRE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_CAPTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_COFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_COIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_CORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_COTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_PREFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_PREIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_PRERE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_PRETOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_PROFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_PROIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_PRORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_PROTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_TTLFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_TTLIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_TTLRE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalOrders
												public int?  SiteWorkOrdersTotalOrders_TTLTOT {get; set;}

                                                // Table : SiteWorkOrders 
                                                // Field : TotalOrders
                                                public string SiteWorkOrdersTotalOrders_RM { get; set; }

                                                // Table : SiteWorkOrders 
                                                // Field : TotalOrders
                                                public int? SiteWorkOrdersTotalOrders_RWRE { get; set; }

                                                // Table : SiteWorkOrders 
                                                // Field : TotalOrders
                                                public int? SiteWorkOrdersTotalOrders_RWFW { get; set; }

                                                // Table : SiteWorkOrders 
                                                // Field : TotalOrders
                                                public int? SiteWorkOrdersTotalOrders_RWIEE { get; set; }

                                                // Table : SiteWorkOrders 
                                                // Field : TotalOrders
                                                public decimal? SiteWorkOrdersTotalOrders_RWRETT { get; set; }

                                                // Table : SiteWorkOrders 
                                                // Field : TotalOrders
                                                public decimal? SiteWorkOrdersTotalOrders_RWFWTT { get; set; }

                                                // Table : SiteWorkOrders 
                                                // Field : TotalOrders
                                                public decimal? SiteWorkOrdersTotalOrders_RWIEETT { get; set; }

                                                // Table : SiteWorkOrders 
                                                // Field : TotalOrders
                                                public int? SiteWorkOrdersTotalOrders_RWTT { get; set; }

                                                // Table : SiteWorkOrders 
                                                // Field : TotalOrders
                                                public decimal? SiteWorkOrdersTotalOrders_RWPCTTT { get; set; }
                        

											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_CAPFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_CAPIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_CAPRE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_CAPTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_COFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_COIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_CORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_COTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_PREFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_PREIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_PRERE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_PRETOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_PROFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_PROIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_PRORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_PROTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_TTLFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_TTLIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_TTLRE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalPlanned
												public int?  SiteWorkOrdersTotalPlanned_TTLTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_CAPFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_CAPIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_CAPRE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_CAPTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_COFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_COIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_CORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_COTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_PREFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_PREIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_PRERE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_PRETOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_PROFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_PROIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_PRORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_PROTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_TTLFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_TTLIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_TTLRE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSched
												public int?  SiteWorkOrdersTotalSched_TTLTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_CAPFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_CAPIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_CAPRE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_CAPTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_COFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_COIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_CORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_COTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_PREFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_PREIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_PRERE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_PRETOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_PROFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_PROIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_PRORE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_PROTOT {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_TTLFP {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_TTLIE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_TTLRE {get; set;}
				        
											
						// Table : SiteWorkOrders 
						// Field : TotalSchedCmpl
												public int?  SiteWorkOrdersTotalSchedCmpl_TTLTOT {get; set;}

				        
															}
										
					/*[MetadataType(typeof(ST_RELMetaData))]*/
					public partial class ST_RELModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table :  
						// Field : SuppMgmtChng
												public string  SiteRelCMMSSuppMgmtChng {get; set;}

                                                // Table :  
                                                // Field : ReworkMeasure
                                                public string SiteRelCMMSReworkMeasure { get; set; }
                                                // Table :  
                                                // Field : ReworkMeasure
                                                //public decimal? SiteRelCMMSMaintRework { get; set; }
                                                // Table :  
                                                // Field : ReworkMeasure
                                                //public decimal? SiteRelCMMSPctRework { get; set; }
					
						// Table :  
						// Field : SuppPlanning
												public string  SiteRelCMMSSuppPlanning {get; set;}
				        
											
						// Table :  
						// Field : SuppRAMProcess
												public string  SiteRelCMMSSuppRAMProcess {get; set;}
				        
											
						// Table :  
						// Field : SuppScheduling
												public string  SiteRelCMMSSuppScheduling {get; set;}
				        
											
						// Table :  
						// Field : CritEqMeth
												public string  SiteRelCritEquipMeth_FP {get; set;}
				        
											
						// Table :  
						// Field : CritEqMeth
												public string  SiteRelCritEquipMeth_IE {get; set;}
				        
											
						// Table :  
						// Field : CritEqMeth
												public string  SiteRelCritEquipMeth_RE {get; set;}
				        
											
						// Table :  
						// Field : Downtime
												public string  SiteRelDowntimeAutoRec_FP {get; set;}
				        
											
						// Table :  
						// Field : Downtime
												public string  SiteRelDowntimeAutoRec_IE {get; set;}
				        
											
						// Table :  
						// Field : Downtime
												public string  SiteRelDowntimeAutoRec_RE {get; set;}
				        
											
						// Table :  
						// Field : EgStds
												public string  SiteRelEquipEngrStd_FP {get; set;}
				        
											
						// Table :  
						// Field : EgStds
												public string  SiteRelEquipEngrStd_IE {get; set;}
				        
											
						// Table :  
						// Field : EgStds
												public string  SiteRelEquipEngrStd_RE {get; set;}
				        
											
						// Table :  
						// Field : FailAnalysis
												public string  SiteRelFailureAnalysis_FP {get; set;}
				        
											
						// Table :  
						// Field : FailAnalysis
												public string  SiteRelFailureAnalysis_IE {get; set;}
				        
											
						// Table :  
						// Field : FailAnalysis
												public string  SiteRelFailureAnalysis_RE {get; set;}
				        
											
						// Table :  
						// Field : IntSpareEq
												public string  SiteRelInstalledSpares_FP {get; set;}
				        
											
						// Table :  
						// Field : IntSpareEq
												public string  SiteRelInstalledSpares_IE {get; set;}
				        
											
						// Table :  
						// Field : IntSpareEq
												public string  SiteRelInstalledSpares_RE {get; set;}
				        
											
						// Table :  
						// Field : CritEqInd
												public string  SiteRelKPIs_FP {get; set;}
				        
											
						// Table :  
						// Field : CritEqInd
												public string  SiteRelKPIs_IE {get; set;}
				        
											
						// Table :  
						// Field : CritEqInd
												public string  SiteRelKPIs_RE {get; set;}
				        
											
						// Table :  
						// Field : LCCA
												public string  SiteRelLCCA_FP {get; set;}
				        
											
						// Table :  
						// Field : LCCA
												public string  SiteRelLCCA_IE {get; set;}
				        
											
						// Table :  
						// Field : LCCA
												public string  SiteRelLCCA_RE {get; set;}
				        
											
						// Table :  
						// Field : 5S
												public string  SiteRelPIP5S {get; set;}
				        
											
						// Table :  
						// Field : 6SigmaBlackBelt
												public decimal?  SiteRelPIP6SigmaBlackBelt {get; set;}
				        
											
						// Table :  
						// Field : 6SigmaGreenBelt
												public decimal?  SiteRelPIP6SigmaGreenBelt {get; set;}
				        
											
						// Table :  
						// Field : MonteCarloSim
												public string  SiteRelPIPMonteCarloSim {get; set;}
				        
											
						// Table :  
						// Field : PoissonDist
												public string  SiteRelPIPPoissonDist {get; set;}
				        
											
						// Table :  
						// Field : TPM
												public string  SiteRelPIPTPM {get; set;}

                                                // Table :  
                                                // Field : RBM
                                                public string SiteRelPIPRBM { get; set; }
				        
											
						// Table :  
						// Field : WeibullAnalysis
												public string  SiteRelPIPWeibullAnalysis {get; set;}
				        
											
						// Table :  
						// Field : Predictive
												public string  SiteRelPredictive_FP {get; set;}
				        
											
						// Table :  
						// Field : Predictive
												public string  SiteRelPredictive_IE {get; set;}
				        
											
						// Table :  
						// Field : Predictive
												public string  SiteRelPredictive_RE {get; set;}
				        
											
						// Table :  
						// Field : Preventive
												public string  SiteRelPreventive_FP {get; set;}
				        
											
						// Table :  
						// Field : Preventive
												public string  SiteRelPreventive_IE {get; set;}
				        
											
						// Table :  
						// Field : Preventive
												public string  SiteRelPreventive_RE {get; set;}
				        
											
						// Table :  
						// Field : RCM
												public string  SiteRelRCM_FP {get; set;}
				        
											
						// Table :  
						// Field : RCM
												public string  SiteRelRCM_IE {get; set;}
				        
											
						// Table :  
						// Field : RCM
												public string  SiteRelRCM_RE {get; set;}
				        
											
						// Table :  
						// Field : MaintProc
												public string  SiteRelRepairProc_FP {get; set;}
				        
											
						// Table :  
						// Field : MaintProc
												public string  SiteRelRepairProc_IE {get; set;}
				        
											
						// Table :  
						// Field : MaintProc
												public string  SiteRelRepairProc_RE {get; set;}
				        
											
						// Table :  
						// Field : RiskInsp
												public string  SiteRelRiskBasedInsp_FP {get; set;}
				        
											
						// Table :  
						// Field : RiskInsp
												public string  SiteRelRiskBasedInsp_IE {get; set;}
				        
											
						// Table :  
						// Field : RiskInsp
												public string  SiteRelRiskBasedInsp_RE {get; set;}
				        
											
						// Table :  
						// Field : 
												public string  SiteRelRootCause_FP {get; set;}
				        
											
						// Table :  
						// Field : 
												public string  SiteRelRootCause_IE {get; set;}
				        
											
						// Table :  
						// Field : 
												public string  SiteRelRootCause_RE {get; set;}
				        
											
						// Table :  
						// Field : CritEqPlan
												public string  SiteRelStrategicPlan_FP {get; set;}
				        
											
						// Table :  
						// Field : CritEqPlan
												public string  SiteRelStrategicPlan_IE {get; set;}
				        
											
						// Table :  
						// Field : CritEqPlan
												public string  SiteRelStrategicPlan_RE {get; set;}
				        
															}
										
					/*[MetadataType(typeof(ST_STUDYMetaData))]*/
					public partial class ST_STUDYModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_Char {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_EquipData {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_MaintOrg {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_MCptl {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_MHrs {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_MRO {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_ProcessChar {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_Reliability {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_RTCost {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_TACost {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_WorkProcess {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : GatheringMeth
												public string  StudyDataCollectMethod_WorkTypes {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : DataGatheringHrs
												public int?  StudyEffortDataCollectHrs {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : AvgCollectingHrs
												public decimal?  StudyEffortDataCollectHrs_Person {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : NumDataCollectors
												public int?  StudyEffortDataCollectNumPers {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : DataEntererHrs
												public int?  StudyEffortDataEntryHrs {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : AvgEnteringHrs
												public decimal?  StudyEffortDataEntryHrs_Person {get; set;}
				        
											
						// Table : SiteStudyInfo 
						// Field : NumDataEnterers
												public int?  StudyEffortDataEntryNumPers {get; set;}
				        
															}
										
					/*[MetadataType(typeof(UT_CHARMetaData))]*/
					public partial class UT_CHARModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : UnitGenData 
						// Field : ExchangeRate
												public decimal?  _UnitInfoExchangeRate {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : AgitatorsInVessels
												public decimal?  EquipCntAgitatorsInVessels {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : OnStreamAnalyzersBlend
												public decimal?  EquipCntAnalyzerBlending {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : OnStreamAnalyzersContinuous
                                                //public decimal?  EquipCntAnalyzerEmissions {get; set;}

                                                public decimal? EquipCntAnylyzerEnv { get; set; }

                                                // Table : UnitEquipCounts 
                                                // Field : MotorsOthers
                                                public decimal? EquipCntMotorsOthers { get; set; }

                                                // Table : UnitEquipCounts 
                                                // Field : EquipCntAnalyzerSafty
                                                public decimal? EquipCntAnalyzerSafty { get; set; }
											
						// Table : UnitEquipCounts 
						// Field : OnStreamAnalyzersProcCtrl
												public decimal?  EquipCntAnalyzerProcCtrl {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : BlowersFans
												public decimal?  EquipCntBlowersFans {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : CentrifugesMain
												public decimal?  EquipCntCentrifugesMain {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : CompressorsRecipMainStdBy
												public decimal?  EquipCntCompressorsRecip {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : CompressorsRecipSpares
												public decimal?  EquipCntCompressorsRecipSpares {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : CompressorsRotatingMainStdBy
												public decimal?  EquipCntCompressorsRotating {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : CompressorsRotatingSpares
												public decimal?  EquipCntCompressorsRotatingSpares {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : ControlValves
												public decimal?  EquipCntControlValves {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : CoolingTowers
												public decimal?  EquipCntCoolingTowers {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : Crushers
												public decimal?  EquipCntCrushers {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : DistillationTowers
												public decimal?  EquipCntDistTowers {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : DistributionSystemVoltage
												public string  EquipCntDistVoltage {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : FiredFurnaceBlowersMain
												public decimal?  EquipCntFurnaceBoilers {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : HeatExchangers
												public decimal?  EquipCntHeatExch {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : FinFanHeatExchangers
												public decimal?  EquipCntHeatExchFinFan {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : HeatExchangersOth
												public decimal?  EquipCntHeatExchOth {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : HeatExchSpares
												public decimal?  EquipCntHeatExchSpares {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : ISBLSubStationTransformer
												public decimal?  EquipCntISBLSubStationTransformer {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : MotorsMain
												public decimal?  EquipCntMotorsMain {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : CentrifugalPumpMainStdBy
												public decimal?  EquipCntPumpsCentrifugal {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : PumpsCentrifugalSpares
												public decimal?  EquipCntPumpsCentrifugalSpares {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : CentrifugalPosDispStdBy
												public decimal?  EquipCntPumpsPosDisp {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : RefrigChillerUnits
												public decimal?  EquipCntRefrigUnits {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : RotaryDryers
												public decimal?  EquipCntRotaryDryers {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : SafetyInstrumentedSys
												public decimal?  EquipCntSafetyInstrSys {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : SafetyValves
												public decimal?  EquipCntSafetyValves {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : SilosISBL
												public decimal?  EquipCntSilosISBL {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : StorageTanksISBL
												public decimal?  EquipCntStorageTanksISBL {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : TurbinesMainStdBy
												public decimal?  EquipCntTurbines {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : TurbinesSpares
												public decimal?  EquipCntTurbinesSpares {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : VariableSpeedDrives
												public decimal?  EquipCntVarSpeedDrives {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : Vessels
												public decimal?  EquipCntVessels {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : MaterialOfConstruction
												public string  SeverityConstMaterials {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : Corrosivity
												public string  SeverityCorrosivity {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : Erosivity
												public string  SeverityErosivity {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : Explosivity
												public string  SeverityExplosivity {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : Flammability
												public string  SeverityFlammability {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : FreezePoint
												public string  SeverityFreezePt {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : Press
												public string  SeverityPressure {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : ProcessComplexity
												public string  SeverityProcessComplexity {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : Temperature
												public string  SeverityTemperature {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : Toxicity
												public string  SeverityToxicity {get; set;}
				        
											
						// Table : UnitSeverity 
						// Field : Viscosity
												public string  SeverityViscosity {get; set;}
				        
											
						// Table : UnitGenData 
						// Field : ContinuosOrBatch
												public string  UnitInfoContinuosOrBatch {get; set;}
				        
											
						// Table : UnitGenData 
						// Field : MaxDailyProdRate
												public decimal?  UnitInfoMaxDailyProdRate {get; set;}
				        
											
						// Table : UnitGenData 
						// Field : PhasePredominentFeedstock
												public string  UnitInfoPhasePredominentFeedstock {get; set;}
				        
											
						// Table : UnitGenData 
						// Field : PhasePredominentProduct
												public string  UnitInfoPhasePredominentProduct {get; set;}
				        
											
						// Table : UnitGenData 
						// Field : AvgPlantAge
												public decimal?  UnitInfoPlantAge {get; set;}
				        
											
						// Table : UnitGenData 
						// Field : ProdRateUOM
												public string  UnitInfoProdRateUOM {get; set;}
				        
											
						// Table : UnitGenData 
						// Field : PRV
												public decimal?  UnitInfoPRV {get; set;}
				        
											
						// Table : UnitGenData 
						// Field : PRVUSD
												public decimal?  UnitInfoPRVUSD {get; set;}
				        
											
						// Table : UnitGenData 
						// Field : 
												public decimal?  UnitInfoTrainCnt {get; set;}
				        
											
						// Table : UnitCharacteristics 
						// Field : SiteSafetyCurrInjuryCntComp_RT
												public int?  UnitSafetyCurrInjuryCntComp_RT {get; set;}
				        
											
						// Table : UnitCharacteristics 
						// Field : SiteSafetyCurrInjuryCntCont_RT
												public int?  UnitSafetyCurrInjuryCntCont_RT {get; set;}
				        
											
						// Table : UnitCharacteristics 
						// Field : SiteSafetyCurrInjuryCntTot_RT
												public int?  UnitSafetyCurrInjuryCntTot_RT {get; set;}
				        
															}
										
					/*[MetadataType(typeof(UT_COSTMetaData))]*/
					public partial class UT_COSTModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatl
												public decimal?  AnnTAMatlFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatl
												public decimal?  AnnTAMatlIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatlMtCap
												public decimal?  AnnTAMatlMtCapFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatlMtCap
												public decimal?  AnnTAMatlMtCapIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatlMtCap
												public decimal?  AnnTAMatlMtCapRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatlMtCap
												public decimal?  AnnTAMatlMtCapTOT {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatl
												public decimal?  AnnTAMatlRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatl
												public decimal?  AnnTAMatlTOT {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatlTot
												public decimal?  AnnTAMatlTotFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatlTot
												public decimal?  AnnTAMatlTotIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatlTot
												public decimal?  AnnTAMatlTotRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : AnnTAMatlTot
												public decimal?  AnnTAMatlTotTOT {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanMtCapWhr
												public decimal?  RoutCraftmanMtCapWhrFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanMtCapWhr
												public decimal?  RoutCraftmanMtCapWhrIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanMtCapWhr
												public decimal?  RoutCraftmanMtCapWhrRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanMtCapWhr
												public decimal?  RoutCraftmanMtCapWhrTOT {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanTotWhr
												public decimal?  RoutCraftmanTotWhrFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanTotWhr
												public decimal?  RoutCraftmanTotWhrIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanTotWhr
												public decimal?  RoutCraftmanTotWhrRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanTotWhr
												public decimal?  RoutCraftmanTotWhrTOT {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanWhr
												public decimal?  RoutCraftmanWhrFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanWhr
												public decimal?  RoutCraftmanWhrIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanWhr
												public decimal?  RoutCraftmanWhrRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutCraftmanWhr
												public decimal?  RoutCraftmanWhrTOT {get; set;}

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutCraftmanTltHrsRE
                                                public decimal? RoutCraftmanTltHrsRE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutCraftmanTltHrsFP
                                                public decimal? RoutCraftmanTltHrsFP { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutCraftmanTltHrsIE
                                                public decimal? RoutCraftmanTltHrsIE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutCraftmanTltHrsTOT
                                                public decimal? RoutCraftmanTltHrsTOT { get; set; }
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatl
												public decimal?  RoutMatlFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatl
												public decimal?  RoutMatlIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatlMtCap
												public decimal?  RoutMatlMtCapFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatlMtCap
												public decimal?  RoutMatlMtCapIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatlMtCap
												public decimal?  RoutMatlMtCapRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatlMtCap
												public decimal?  RoutMatlMtCapTOT {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatl
												public decimal?  RoutMatlRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatl
												public decimal?  RoutMatlTOT {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatlTot
												public decimal?  RoutMatlTotFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatlTot
												public decimal?  RoutMatlTotIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatlTot
												public decimal?  RoutMatlTotRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : RoutMatlTot
												public decimal?  RoutMatlTotTOT {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanMtCapWhr
												public decimal?  TACraftmanMtCapWhrFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanMtCapWhr
												public decimal?  TACraftmanMtCapWhrIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanMtCapWhr
												public decimal?  TACraftmanMtCapWhrRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanMtCapWhr
												public decimal?  TACraftmanMtCapWhrTOT {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanTotWhr
												public decimal?  TACraftmanTotWhrFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanTotWhr
												public decimal?  TACraftmanTotWhrIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanTotWhr
												public decimal?  TACraftmanTotWhrRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanTotWhr
												public decimal?  TACraftmanTotWhrTOT {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanWhr
												public decimal?  TACraftmanWhrFP {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanWhr
												public decimal?  TACraftmanWhrIE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanWhr
												public decimal?  TACraftmanWhrRE {get; set;}
				        
											
						// Table : UnitEquipMaterialCosts 
						// Field : TACraftmanWhr
												public decimal?  TACraftmanWhrTOT {get; set;}
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : TACraftmanWhrTltHrsRE

                                                public decimal? TACraftmanWhrTltHrsRE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : TACraftmanWhrTltHrsFP
                                                public decimal? TACraftmanWhrTltHrsFP { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : TACraftmanWhrTltHrsIE
                                                public decimal? TACraftmanWhrTltHrsIE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : TACraftmanWhrTltHrsTOT
                                                public decimal? TACraftmanWhrTltHrsTOT { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutMatlTltHrsRE

                                                public decimal? RoutMatlTltHrsRE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutMatlTltHrsFP
                                                public decimal? RoutMatlTltHrsFP { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutMatlTltHrsIE
                                                public decimal? RoutMatlTltHrsIE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutMatlTltHrsTOT
                                                public decimal? RoutMatlTltHrsTOT { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutMatlTltHrsRE

                                                public decimal? AnnTAMatlTltHrsRE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutMatlTltHrsFP
                                                public decimal? AnnTAMatlTltHrsFP { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutMatlTltHrsIE
                                                public decimal? AnnTAMatlTltHrsIE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RoutMatlTltHrsTOT
                                                public decimal? AnnTAMatlTltHrsTOT { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : UnitMaintCostRE

                                                public decimal? UnitMaintCostRE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : UnitMaintCostFP
                                                public decimal? UnitMaintCostFP { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : UnitMaintCostIE
                                                public decimal? UnitMaintCostIE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : UnitMaintCostTOT
                                                public decimal? UnitMaintCostTOT { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : UnitTAMaintCostRE

                                                public decimal? UnitTAMaintCostRE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : UnitTAMaintCostFP
                                                public decimal? UnitTAMaintCostFP { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : UnitTAMaintCostIE
                                                public decimal? UnitTAMaintCostIE { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : UnitTAMaintCostTOT
                                                public decimal? UnitTAMaintCostTOT { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcCLWRCOHrs
                                                public decimal? NRProcCLWRCOHrs { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcCLWRCTHrs
                                                public decimal? NRProcCLWRCTHrs { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcCLWRCOMatl
                                                public decimal? NRProcCLWRCOMatl { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcCLWRCTMatl
                                                public decimal? NRProcCLWRCTMatl { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcCLWDCOHrs

                                                public decimal? NRProcCLWDCOHrs { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcCLWDCTHrs
                                                public decimal? NRProcCLWDCTHrs { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcCLWDCOMatl
                                                public decimal? NRProcCLWDCOMatl { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcCLWDCTMatl
                                                public decimal? NRProcCLWDCTMatl { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcRLWRCOHrs
                                                public decimal? NRProcRLWRCOHrs { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcRLWRCTHrs
                                                public decimal? NRProcRLWRCTHrs { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcRLWRCOMatl
                                                public decimal? NRProcRLWRCOMatl { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcRLWRCTMatl
                                                public decimal? NRProcRLWRCTMatl { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcRLWDCOHrs
                                                public decimal? NRProcRLWDCOHrs { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcRLWDCTHrs
                                                public decimal? NRProcRLWDCTHrs { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcRLWDCOMatl
                                                public decimal? NRProcRLWDCOMatl { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRProcRLWDCTMatl
                                                public decimal? NRProcRLWDCTMatl { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRUnitRLMTCOHrs
                                                public decimal? NRUnitRLMTCOHrs { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRUnitRLMTCTHrs
                                                public decimal? NRUnitRLMTCTHrs { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRUnitRLMTCOMatl
                                                public decimal? NRUnitRLMTCOMatl { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRUnitRLMTCTMatl
                                                public decimal? NRUnitRLMTCTMatl { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 

                                                public decimal? CostTAWKEXEPCT_CT { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 

                                                public decimal? CostTAWKEXEPCT_CRC { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 

                                                public decimal? CostTAWKEXEPCT_CMC { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 

                                                public decimal? CostTAWKEXEPCT_CPC { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 

                                                public decimal? CostTAWKEXEPCT_CAC { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 
                                                public decimal? CostTAWKEXEPCT_TT { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 
                                                public string CostTAWKEXE_RM { get; set; }

                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 
                                                public decimal? RTWKRoutCT_CT { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 
                                                public decimal? RTWKRoutC_CRC { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 
                                                public decimal? RTWKRoutC_CMC { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 
                                                public decimal? RTWKRoutC_CPC { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 
                                                public decimal? RTWKRoutC_CAC { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : 
                                                public decimal? RTWKRoutC_TT { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRUnitCost
                                                public decimal? NRUnitTotal_HR { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : NRUnitCost
                                                public decimal? NRUnitTotal_SP { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RAMNRUnitCost
                                                public decimal? RAMNRUnitTotal_HR { get; set; }
                                                // Table : UnitEquipMaterialCosts 
                                                // Field : RAMNRUnitCost
                                                public decimal? RAMNRUnitTotal_SP { get; set; }


															}
										
					/*[MetadataType(typeof(UT_DTMetaData))]*/
					public partial class UT_DTModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : UnitShortOHChar 
						// Field : 
												public decimal?  _ShortOHAnnHrsDown {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : 
												public decimal?  _TAAvgAnnHrsDown {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : AnnSchedMaintHrs
												public decimal?  LossHrsForAnnTA {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : ProcessReliabilityHrs
												public decimal?  LossHrsForProcessReliability {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : ShortOHHrs
												public decimal?  LossHrsForShortOH {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : TotalProdLossHrs
												public decimal?  LossHrsForTotalProdLoss {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : UnSchedMaintHrs
												public decimal?  LossHrsForUnschedMaint {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : TotalAnnProdLossHrs
												public decimal?  LossHrsTotalAnnProdLoss {get; set;}
				        
											
						// Table : LostProduction 
						// Field : Outages
												public decimal?  LostProductionOutages {get; set;}
				        
											
						// Table : LostProduction 
						// Field : RateReductions
												public decimal?  LostProductionRateReductions {get; set;}
				        
											
						// Table :  
						// Field : MaintEquivHours
												public decimal?  NonMaintOutagesMaintEquivHours {get; set;}
				        
											
						// Table :  
						// Field : RAMFixedPlantPipeWorkCivil
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  RAMCausePcnt_FP {get; set;}
				        
											
						// Table :  
						// Field : RAMElectricalInstEquip
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  RAMCausePcnt_IE {get; set;}
				        
											
						// Table :  
						// Field : RAMRotatingEquip
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  RAMCausePcnt_RE {get; set;}
				        
											
						// Table :  
						// Field : RAMTotal
													[Range(100,100, ErrorMessage=" The total should equal to 100 percent")] 
												public decimal?  RAMCausePcnt_Tot {get; set;}
				        
											
						// Table : UnitShortOHChar 
						// Field : 
												public decimal?  ShortOH2YrCnt {get; set;}
				        
											
						// Table : UnitShortOHChar 
						// Field : 
												public decimal?  ShortOHAnnHrsDown {get; set;}
				        
											
						// Table : UnitShortOHChar 
						// Field : AvgSOHrsDown
												public decimal?  ShortOHAvgHrsDown {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : SchShrtFixedPlantPipeWorkCivil
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ShortOHCausePcnt_FP {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : SchSchrtElectricalInstEquip
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ShortOHCausePcnt_IE {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : SchShrtRotatingEquip
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ShortOHCausePcnt_RE {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : SchShrtTotal
													[Range(100,100, ErrorMessage=" The total should equal to 100 percent")] 
												public decimal?  ShortOHCausePcnt_Tot {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : FixedPlantPipeWorkCivil
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  StoppagePcnt_FP {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : ElecInstrumentEquip
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  StoppagePcnt_IE {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : RotatingEquip
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  StoppagePcnt_RE {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : Total
													[Range(100,100, ErrorMessage=" The total should equal to 100 percent")] 
												public decimal?  StoppagePcnt_Tot {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : 
												public decimal?  TAAvgAnnHrsDown {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : AvgTAHrsDown
												public decimal?  TAAvgHrsDown {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : AvgTAHrsExecution
												public decimal?  TAAvgHrsExecution {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : AvgTAHrsShutdown
												public decimal?  TAAvgHrsShutdown {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : AvgTAHrsStartup
												public decimal?  TAAvgHrsStartup {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : AvgInterval
												public decimal?  TAAvgInterval {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : PlanTAHrsDown
												public decimal?  TAAvgPlanHrsDown {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : SchTAFixedPlantPipeWorkCivil
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TACausePcnt_FP {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : SchTAElectricalInstEquip
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TACausePcnt_IE {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : SchTARotatingEquip
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TACausePcnt_RE {get; set;}
				        
											
						// Table : UnitLossesStoppage 
						// Field : SchTATotal
													[Range(100,100, ErrorMessage=" The total should equal to 100 percent")] 
												public decimal?  TACausePcnt_Tot {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : ProcessCompressorsOH
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TACompressors {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : ContAvailability
												public string  TAContAvail {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : ControlValvesOH
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TAControlValves {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : CptlProject
												public string  TACptlProject {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : TACritPath
												public string  TACritPath {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : TACritPathOth
												public string  TACritPathOth {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : DeteriorizationPlantorPerf
												public string  TADeterioration {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : DiscretionaryNeedForInspection
												public string  TADiscretionaryInspect {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : DistTowersOpened
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TADistTowersOpened {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : FiredFurnaceBoilersOH
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TAFiredFurnaces {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : RegSchedFixedByCompany
												public string  TAFixedSchedule {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : HeatExchangersOpened
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TAHeatExchOpened {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : DropOffInProductDemand
												public string  TALowDemand {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : ProcessPumpsOH
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TAPumps {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : RegulatoryInspectionRequired
												public string  TARegulatoryInspect {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : UnplannedEqFail
												public string  TAUnplannedFailure {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : PressureValvesCalibrated
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TAValvesCalibrated {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : ProcessValvesOpened
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  TAValvesOpened {get; set;}
				        
											
						// Table : UnitStopTAChar 
						// Field : LocalWeatherConditions
												public string  TAWeather {get; set;}

                                                // Table : UnitStopTAChar 
                                                // Field : TAMGM
                                                public string TAManagement { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAMGMOth
                                                public string TAManagementOth { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyMon
                                                public decimal? TAStrategyMon_TSD { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyMon
                                                public decimal? TAStrategyMon_SSF { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyMon
                                                public decimal? TAStrategyMon_ASF { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyMon
                                                public decimal? TAStrategyMon_MCC { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyMon
                                                public decimal? TAStrategyMon_BDC { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyMon
                                                public decimal? TAStrategyMon_IDSP { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyMon
                                                public decimal? TAStrategyMon_IDSA { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyMon
                                                public decimal? TAStrategyMon_MMP { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyMon
                                                public decimal? TAStrategyMon_NDW { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyMon
                                                public decimal? TAStrategyMon_DWA { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyWP
                                                public string TAStrategyWP_RFW { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAStrategyWP
                                                public decimal? TAStrategyWP_MCW { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAExecute
                                                public decimal? TAWKEXEPCT_CT { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAExecute
                                                public decimal? TAWKEXEPCT_CRC { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAExecute
                                                public decimal? TAWKEXEPCT_CMC { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAExecute
                                                public decimal? TAWKEXEPCT_CPC { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAExecute
                                                public decimal? TAWKEXEPCT_CAC { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAExecute
                                                public decimal? TAWKEXEPCT_TT { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TAExecute
                                                public string TAWKEXE_RM { get; set; }

                                                // Table : UnitStopTAChar 
                                                // Field : TARoutine
                                                public decimal? TAWKRoutCT_CT { get; set; }
															
                                                // Table : UnitStopTAChar 
                                                // Field : TARoutine
                                                public decimal? TAWKRoutCT_CRC { get; set; }
                                                // Table : UnitStopTAChar 
                                                // Field : TARoutine
                                                public decimal? TAWKRoutCT_CMC { get; set; }
                                                // Table : UnitStopTAChar 
                                                // Field : TARoutine
                                                public decimal? TAWKRoutCT_CPC { get; set; }
                                                // Table : UnitStopTAChar 
                                                // Field : TARoutine
                                                public decimal? TAWKRoutCT_CAC { get; set; }
                                                // Table : UnitStopTAChar 
                                                // Field : TARoutine
                                                public decimal? TAWKRoutCT_TT { get; set; }
															}
										
					/*[MetadataType(typeof(UT_PROCESSMetaData))]*/
					public partial class UT_PROCESSModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : UnitMaintTaskCategories 
						// Field : ConditionMonitoringHrs
												public decimal?  TaskConditionMonitor_FP {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : ConditionMonitoringHrs
												public decimal?  TaskConditionMonitor_IE {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : ConditionMonitoringHrs
												public decimal?  TaskConditionMonitor_RE {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : ConditionMonitoringHrs
												public decimal?  TaskConditionMonitor_Tot {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : EmergencyHrs
												public decimal?  TaskEmergency_FP {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : EmergencyHrs
												public decimal?  TaskEmergency_IE {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : EmergencyHrs
												public decimal?  TaskEmergency_RE {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : EmergencyHrs
												public decimal?  TaskEmergency_Tot {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : PreventativeHrs
												public decimal?  TaskPreventive_FP {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : PreventativeHrs
												public decimal?  TaskPreventive_IE {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : PreventativeHrs
												public decimal?  TaskPreventive_RE {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : PreventativeHrs
												public decimal?  TaskPreventive_Tot {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : RoutHrs
												public decimal?  TaskRoutCorrective_FP {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : RoutHrs
												public decimal?  TaskRoutCorrective_IE {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : RoutHrs
												public decimal?  TaskRoutCorrective_RE {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : RoutHrs
												public decimal?  TaskRoutCorrective_Tot {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : TotalHrs
												public decimal?  TaskTotal_FP {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : TotalHrs
												public decimal?  TaskTotal_IE {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : TotalHrs
												public decimal?  TaskTotal_RE {get; set;}
				        
											
						// Table : UnitMaintTaskCategories 
						// Field : TotalHrs
												public decimal?  TaskTotal_Tot {get; set;}

                                                // Table : UnitMaintTaskCategories 
                                                // Field : IMPCT
                                                public decimal? NonEmergencyPCT_RE { get; set; }

                                                // Table : UnitMaintTaskCategories 
                                                // Field : IMPCT
                                                public decimal? NonEmergencyPCT_FP { get; set; }

                                                // Table : UnitMaintTaskCategories 
                                                // Field : IMPCT
                                                public decimal? NonEmergencyPCT_IE { get; set; }

                                                // Table : UnitMaintTaskCategories 
                                                // Field : IMPCT
                                                public decimal? NonEmergencyPCT_Tot { get; set; }
                                                // Table : UnitMaintTaskCategories 
                                                // Field : IMPCT
                                                //public decimal? Rework_RE { get; set; }

                                                // Table : UnitMaintTaskCategories 
                                                // Field : IMPCT
                                                //public decimal? Rework_FP { get; set; }

                                                // Table : UnitMaintTaskCategories 
                                                // Field : IMPCT
                                                //public decimal? Rework_IE { get; set; }

                                                // Table : UnitMaintTaskCategories 
                                                // Field : IMPCT
                                                //public decimal? Rework_Tot { get; set; }
				        
				        
															}
										
					/*[MetadataType(typeof(UT_RELMetaData))]*/
					public partial class UT_RELModel
					{
					    [ScaffoldColumn(true)]
						public bool isUnderReview {get; set;}
						
						[ScaffoldColumn(true)]
						public string CompanySID  {get;set;}
						
			  	
						// Table : UnitEquipCounts 
						// Field : OnStreamAnalyzersBlend
												public decimal?  _EquipCntAnalyzerBlending {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : OnStreamAnalyzersContinuous
                                                public decimal? _EquipCntAnylyzerEnv { get; set; }
				        
											
						// Table : UnitEquipCounts 
						// Field : OnStreamAnalyzersProcCtrl
												public decimal?  _EquipCntAnalyzerProcCtrl {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : ControlValves
												public decimal?  _EquipCntControlValves {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : DistillationTowers
												public decimal?  _EquipCntDistTowers {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : FiredFurnaceBlowersMain
												public decimal?  _EquipCntFurnaceBoilers {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : MotorsMain
												public decimal?  _EquipCntMotorsMain {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : CentrifugalPosDispStdBy
												public decimal?  _EquipCntPumpsPosDisp {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : SafetyInstrumentedSys
												public decimal?  _EquipCntSafetyInstrSys {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : VariableSpeedDrives
												public decimal?  _EquipCntVarSpeedDrives {get; set;}
				        
											
						// Table : UnitEquipCounts 
						// Field : Vessels
												public decimal?  _EquipCntVessels {get; set;}
				        
											
						// Table :  
						// Field : _EquipCntCompressorsRecip2
												public decimal?  EquipCntCompressorsRecipTOT {get; set;}
				        
											
						// Table :  
						// Field : _EquipCntCompressorsRotating2
												public decimal?  EquipCntCompressorsRotatingTOT {get; set;}
				        
											
						// Table :  
						// Field : _EquipCntHeatExch2
												public decimal?  EquipCntHeatExchTOT {get; set;}
				        
											
						// Table :  
						// Field : _EquipCntPumpsCentrifugalSpares2
												public decimal?  EquipCntPumpsCentrifugalTOT {get; set;}
				        
											
						// Table :  
						// Field : _EquipCntTurbines2
												public decimal?  EquipCntTurbinesTOT {get; set;}
				        
											
						// Table :  
						// Field : CriticalityRanking
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  EquipCriticalityRanking_FP {get; set;}
				        
											
						// Table :  
						// Field : CriticalityRanking
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  EquipCriticalityRanking_IE {get; set;}
				        
											
						// Table :  
						// Field : CriticalityRanking
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  EquipCriticalityRanking_RE {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : AnalyzerBlending
												public decimal?  EventsAnalyzerBlending {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : AnalyzerContEmsMont
												public decimal?  EventsAnalyzerEmissions {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : AnalyzerProcCtrl
												public decimal?  EventsAnalyzerProcCtrl {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : CompressorsRecip
												public decimal?  EventsCompressorsRecip {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : CompressorsRotating
												public decimal?  EventsCompressorsRotating {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : ControlValves
												public decimal?  EventsControlValves {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : DistillTwrs
												public decimal?  EventsDistTowers {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : FiredBoilers
												public decimal?  EventsFurnaceBoilers {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : HeatExch
												public decimal?  EventsHeatExch {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : MotorsElectric
												public decimal?  EventsMotorsMain {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : ProcessPumpsCntrgl
												public decimal?  EventsPumpsCentrifugal {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : ProcessPumpsPosDsp
												public decimal?  EventsPumpsPosDisp {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : SafetyInstrumentSys
												public decimal?  EventsSafetyInstrSys {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : Turbines
												public decimal?  EventsTurbines {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : VarSpdDrvs
												public decimal?  EventsVarSpeedDrives {get; set;}
				        
											
						// Table : UnitEvents 
						// Field : ProcVsl
												public decimal?  EventsVessels {get; set;}


                                                // Table : UnitEvents 
                                                // Field : Turbines                      
                                                public string PDataTurbines { get; set; }

                                                // Table : UnitEvents 
                                                // Field : CompressorsRecip              
                                                public string PDataCompressorsRecip { get; set; }

                                                // Table : UnitEvents 
                                                // Field : CompressorsRotating           
                                                public string PDataCompressorsRotating { get; set; }

                                                // Table : UnitEvents 
                                                // Field : ProcessPumpsCntrgl            
                                                public string PDataPumpsCentrifugal { get; set; }

                                                // Table : UnitEvents 
                                                // Field : ProcessPumpsPosDsp            
                                                public string PDataPumpsPosDisp { get; set; }

                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? PEventsTurbines { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? PEventsCompressorsRecip { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? PEventsCompressorsRotating { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? PEventsPumpsCentrifugal { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? PEventsPumpsPosDisp { get; set; }

                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? REventsTurbines { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? REventsCompressorsRecip { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? REventsCompressorsRotating { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? REventsPumpsCentrifugal { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? REventsPumpsPosDisp { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? PEventsTurbinesMTBE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? PEventsCompressorsRecipMTBE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? PEventsCompressorsRotatingMTBE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl
                                                public decimal? PEventsPumpsCentrifugalMTBE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : Turbines
                                                public decimal? PEventsPumpsPosDispMTBE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : CompressorsRecip                                    
                                                public decimal? PEventsTurbinesMTBE_Rct { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcessPumpsCntrgl                                                   
                                                public decimal? PEventsCompressorsRecipMTBE_Rct { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcessPumpsCntrgl            
                                                public decimal? PEventsCompressorsRotatingMTBE_Rct { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcessPumpsCntrgl            
                                                public decimal? PEventsPumpsCentrifugalMTBE_Rct { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PEventsPumpsPosDisp
                                                public decimal? PEventsPumpsPosDispMTBE_Rct { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents
                                                public decimal? PctEventsDsgIsu { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents
                                                public decimal? PctEventsConsIsu { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents
                                                public decimal? PctEventsMaintIsu { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents
                                                public decimal? PctEventsOprtIsu { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents
                                                public decimal? PctEventsNmlEndLife { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents
                                                public decimal? PctEventsTotalRE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents
                                                public decimal? PctEventsOther { get; set; }
                                                // Table : UnitEvents 
                                                // Field : FiredBoilers                  
                                                public string PDataEventsFurnaceBoilers { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl                       
                                                public string PDataEventsVessels { get; set; }
                                                // Table : UnitEvents 
                                                // Field : DistillTwrs                   
                                                public string PDataEventsDistTowers { get; set; }
                                                // Table : UnitEvents 
                                                // Field : HeatExch                      
                                                public string PDataEventsHeatExch { get; set; }
                                                // Table : UnitEvents 
                                                // Field : FiredBoilers                  
                                                public decimal? PEventsFurnaceBoilers { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl                       
                                                public decimal? PEventsVessels { get; set; }
                                                // Table : UnitEvents 
                                                // Field : DistillTwrs                   
                                                public decimal? PEventsDistTowers { get; set; }
                                                // Table : UnitEvents 
                                                // Field : HeatExch                      
                                                public decimal? PEventsHeatExch { get; set; }
                                                // Table : UnitEvents 
                                                // Field : FiredBoilers                  
                                                public decimal? REventsFurnaceBoilers { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl                       
                                                public decimal? REventsVessels { get; set; }
                                                // Table : UnitEvents 
                                                // Field : DistillTwrs                   
                                                public decimal? REventsDistTowers { get; set; }
                                                // Table : UnitEvents 
                                                // Field : HeatExch                      
                                                public decimal? REventsHeatExch { get; set; }
                                                // Table : UnitEvents 
                                                // Field : FiredBoilers                  
                                                public decimal? PEventsMTBEFurnaceBoilers { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl                       
                                                public decimal? PEventsMTBEVessels { get; set; }
                                                // Table : UnitEvents 
                                                // Field : DistillTwrs                   
                                                public decimal? PEventsMTBEDistTowers { get; set; }
                                                // Table : UnitEvents 
                                                // Field : HeatExch                      
                                                public decimal? PEventsMTBEHeatExch { get; set; }
                                                // Table : UnitEvents 
                                                // Field : FiredBoilers                  
                                                public decimal? REventsMTBEFurnaceBoilers { get; set; }
                                                // Table : UnitEvents 
                                                // Field : ProcVsl                       
                                                public decimal? REventsMTBEVessels { get; set; }
                                                // Table : UnitEvents 
                                                // Field : DistillTwrs                   
                                                public decimal? REventsMTBEDistTowers { get; set; }
                                                // Table : UnitEvents 
                                                // Field : HeatExch                      
                                                public decimal? REventsMTBEHeatExch { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsDsgIsu_FP { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsConsIsu_FP { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsMaintIsu_FP { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsOprtIsu_FP { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsNmlEndLife_FP { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsOther_FP { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents      
                                                public decimal? PctEventsTotalRE_FP { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsDsgIsu_IE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsConsIsu_IE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsMaintIsu_IE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsOprtIsu_IE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsNmlEndLife_IE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents                      
                                                public decimal? PctEventsOther_IE { get; set; }
                                                // Table : UnitEvents 
                                                // Field : PctEvents             
                                                public decimal? PctEventsTotal_IE { get; set; }
											
						// Table : MTBF 
						// Field : AnalyzerBlending
												public decimal?  MTBFAnalyzerBlending {get; set;}
				        
											
						// Table : MTBF 
						// Field : AnalyzerEmissions
												public decimal?  MTBFAnalyzerEmissions {get; set;}
				        
											
						// Table : MTBF 
						// Field : AnalyzerProcCtrl
												public decimal?  MTBFAnalyzerProcCtrl {get; set;}
				        
											
						// Table : MTBF 
						// Field : CompressorsRecip
												public decimal?  MTBFCompressorsRecip {get; set;}
				        
											
						// Table : MTBF 
						// Field : CompressorsRotating
												public decimal?  MTBFCompressorsRotating {get; set;}
				        
											
						// Table : MTBF 
						// Field : ControlValves
												public decimal?  MTBFControlValves {get; set;}
				        
											
						// Table : MTBF 
						// Field : DistTowers
												public decimal?  MTBFDistTowers {get; set;}
				        
											
						// Table : MTBF 
						// Field : FurnaceBoilers
												public decimal?  MTBFFurnaceBoilers {get; set;}
				        
											
						// Table : MTBF 
						// Field : HeatExch
												public decimal?  MTBFHeatExch {get; set;}
				        
											
						// Table : MTBF 
						// Field : MotorsMain
												public decimal?  MTBFMotorsMain {get; set;}
				        
											
						// Table : MTBF 
						// Field : PumpsCentrifugal
												public decimal?  MTBFPumpsCentrifugal {get; set;}
				        
											
						// Table : MTBF 
						// Field : PumpsPosDisp
												public decimal?  MTBFPumpsPosDisp {get; set;}
				        
											
						// Table : MTBF 
						// Field : SafetyInstrSys
												public decimal?  MTBFSafetyInstrSys {get; set;}
				        
											
						// Table : MTBF 
						// Field : Turbines
												public decimal?  MTBFTurbines {get; set;}
				        
											
						// Table : MTBF 
						// Field : VarSpeedDrives
												public decimal?  MTBFVarSpeedDrives {get; set;}
				        
											
						// Table : MTBF 
						// Field : Vessels
												public decimal?  MTBFVessels {get; set;}

                                                // Table : MTBF 
                                                // Field : MotorsMain
                                                public string PDataEventsMotorsMain { get; set; }


                                                // Table : MTBF 
                                                // Field : VarSpeedDrives
                                                public string PDataEventsVarSpeedDrives { get; set; }

                                                // Table : MTBF 
                                                // Field : ControlValves
                                                public string PDataEventsControlValves { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerProcCtrl
                                                public string PDataEventsAnalyzerProcCtrl { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerBlending
                                                public string PDataEventsAnalyzerBlending { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerEmissions
                                                public string PDataEventsAnalyzerEmissions { get; set; }

                                                // Table : MTBF 
                                                // Field : MotorsMain
                                                public decimal? PEventsMotorsMain { get; set; }


                                                // Table : MTBF 
                                                // Field : VarSpeedDrives
                                                public decimal? PEventsVarSpeedDrives { get; set; }

                                                // Table : MTBF 
                                                // Field : ControlValves
                                                public decimal? PEventsControlValves { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerProcCtrl
                                                public decimal? PEventsAnalyzerProcCtrl { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerBlending
                                                public decimal? PEventsAnalyzerBlending { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerEmissions
                                                public decimal? PEventsAnalyzerEmissions { get; set; }
                                                // Table : MTBF 
                                                // Field : MotorsMain
                                                public decimal? REventsMotorsMain { get; set; }


                                                // Table : MTBF 
                                                // Field : VarSpeedDrives
                                                public decimal? REventsVarSpeedDrives { get; set; }

                                                // Table : MTBF 
                                                // Field : ControlValves
                                                public decimal? REventsControlValves { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerProcCtrl
                                                public decimal? REventsAnalyzerProcCtrl { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerBlending
                                                public decimal? REventsAnalyzerBlending { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerEmissions
                                                public decimal? REventsAnalyzerEmissions { get; set; }
                                                // Table : MTBF 
                                                // Field : MotorsMain
                                                public decimal? PMTBEMotorsMain { get; set; }

                                                // Table : MTBF 
                                                // Field : VarSpeedDrives
                                                public decimal? PMTBEVarSpeedDrives { get; set; }

                                                // Table : MTBF 
                                                // Field : ControlValves
                                                public decimal? PMTBEControlValves { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerProcCtrl
                                                public decimal? PMTBEAnalyzerProcCtrl { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerBlending
                                                public decimal? PMTBEAnalyzerBlending { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerEmissions
                                                public decimal? PMTBEAnalyzerEmissions { get; set; }
                                                // Table : MTBF 
                                                // Field : MotorsMain
                                                public decimal? RMTBEMotorsMain { get; set; }

                                                // Table : MTBF 
                                                // Field : VarSpeedDrives
                                                public decimal? RMTBEVarSpeedDrives { get; set; }

                                                // Table : MTBF 
                                                // Field : ControlValves
                                                public decimal? RMTBEControlValves { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerProcCtrl
                                                public decimal? RMTBEAnalyzerProcCtrl { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerBlending
                                                public decimal? RMTBEAnalyzerBlending { get; set; }

                                                // Table : MTBF 
                                                // Field : AnalyzerEmissions
                                                public decimal? RMTBEAnalyzerEmissions { get; set; }

                                                // Table : MTBF 
                                                // Field : EventsPerYear
                                                public decimal? NumCauseShutDownPU_EY { get; set; }

                                                // Table : MTBF 
                                                // Field : EventsPerYear
                                                public decimal? NumRsltGovSpillPU_EY { get; set; }

                                                // Table : MTBF 
                                                // Field : EventsPerYear
                                                public decimal? NumExPipeAstLocPU_EY { get; set; }

                                                // Table : MTBF 
                                                // Field : EventsPerYear
                                                public decimal? PipeConnectionPU_Pct { get; set; }


                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string NumCauseShutDownPU_CC { get; set; }

                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string NumRsltGovSpillPU_CC { get; set; }

                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string NumExPipeAstLocPU_CC { get; set; }

                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string PipeConnectionPU_CC { get; set; }

                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsDsgIsu_PU { get; set; }

                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsConsIsu_PU { get; set; }

                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsMaintIsu_PU { get; set; }

                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsOprtIsu_PU { get; set; }

                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsNmlEndLife_PU { get; set; }

                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsOther_PU { get; set; }

                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsTotal_PU { get; set; }

                                                // Table : MTBF 
                                                // Field : EventsPerYear
                                                public decimal? NumCauseShutDownPP_EY { get; set; }

                                                // Table : MTBF 
                                                // Field : EventsPerYear
                                                public decimal? NumRsltGovSpillPP_EY { get; set; }

                                                // Table : MTBF 
                                                // Field : EventsPerYear
                                                public decimal? NumExPipeAstLocPP_EY { get; set; }

                                                // Table : MTBF 
                                                // Field : EventsPerYear
                                                public decimal? PipeConnectionPP_Pct { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string NumCauseShutDownPP_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string NumRsltGovSpillPP_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string NumExPipeAstLocPP_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string PipeConnectionPP_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsDsgIsu_PP { get; set; }
                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsConsIsu_PP { get; set; }
                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsMaintIsu_PP { get; set; }
                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsOprtIsu_PP { get; set; }
                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsNmlEndLife_PP { get; set; }
                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsOther_PP { get; set; }
                                                // Table : MTBF 
                                                // Field : PctEvents
                                                public decimal? PctEventsTotal_PP { get; set; }
                                                // Table : MTBF 
                                                // Field : < 2 years
                                                public decimal? AssetInspPresVes_2Y { get; set; }
                                                // Table : MTBF 
                                                // Field : < 2 years
                                                public decimal? AssetInspDistCol_2Y { get; set; }
                                                // Table : MTBF 
                                                // Field : < 2 years
                                                public decimal? AssetInspHeatExg_2Y { get; set; }
                                                // Table : MTBF 
                                                // Field : < 2 years
                                                public decimal? AssetInspProcPipe_2Y { get; set; }
                                                // Table : MTBF 
                                                // Field : < 2 years
                                                public decimal? AssetInspUtilPipe_2Y { get; set; }
                                                // Table : MTBF 
                                                // Field : < 2 years
                                                public decimal? AssetInspReliefVal_2Y { get; set; }
                                                // Table : MTBF 
                                                // Field : < 2 years
                                                public decimal? AssetInspOthProDev_2Y { get; set; }
                                                // Table : MTBF 
                                                // Field : < 2 years
                                                public decimal? AssetInspMachines_2Y { get; set; }
                                                // Table : MTBF 
                                                // Field : < 2 years
                                                public decimal? AssetInspStructures_2Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 2-5 years
                                                public decimal? AssetInspPresVes_5Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 2-5 years
                                                public decimal? AssetInspDistCol_5Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 2-5 years
                                                public decimal? AssetInspHeatExg_5Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 2-5 years
                                                public decimal? AssetInspProcPipe_5Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 2-5 years
                                                public decimal? AssetInspUtilPipe_5Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 2-5 years
                                                public decimal? AssetInspReliefVal_5Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 2-5 years
                                                public decimal? AssetInspOthProDev_5Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 2-5 years
                                                public decimal? AssetInspMachines_5Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 2-5 years
                                                public decimal? AssetInspStructures_5Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 6-9 years
                                                public decimal? AssetInspPresVes_9Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 6-9 years
                                                public decimal? AssetInspDistCol_9Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 6-9 years
                                                public decimal? AssetInspHeatExg_9Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 6-9 years
                                                public decimal? AssetInspProcPipe_9Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 6-9 years
                                                public decimal? AssetInspUtilPipe_9Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 6-9 years
                                                public decimal? AssetInspReliefVal_9Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 6-9 years
                                                public decimal? AssetInspOthProDev_9Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 6-9 years
                                                public decimal? AssetInspMachines_9Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 6-9 years
                                                public decimal? AssetInspStructures_9Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 10+ years
                                                public decimal? AssetInspPresVes_10Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 10+ years
                                                public decimal? AssetInspDistCol_10Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 10+ years
                                                public decimal? AssetInspHeatExg_10Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 10+ years
                                                public decimal? AssetInspProcPipe_10Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 10+ years
                                                public decimal? AssetInspUtilPipe_10Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 10+ years
                                                public decimal? AssetInspReliefVal_10Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 10+ years
                                                public decimal? AssetInspOthProDev_10Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 10+ years
                                                public decimal? AssetInspMachines_10Y { get; set; }
                                                // Table : MTBF 
                                                // Field : 10+ years
                                                public decimal? AssetInspStructures_10Y { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string AssetInspPresVes_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string AssetInspDistCol_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string AssetInspHeatExg_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string AssetInspProcPipe_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string AssetInspUtilPipe_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string AssetInspReliefVal_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string AssetInspOthProDev_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string AssetInspMachines_CC { get; set; }
                                                // Table : MTBF 
                                                // Field : CurrentlyCaptured
                                                public string AssetInspStructures_CC { get; set; }
						// Table :  
						// Field : PctAcousEmiCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramAcousticEmission_FP {get; set;}
				        
											
						// Table :  
						// Field : PctAcousEmiCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramAcousticEmission_IE {get; set;}
				        
											
						// Table :  
						// Field : PctAcousEmiCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramAcousticEmission_RE {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : PctCondCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramCBM_FP {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : PctCondCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramCBM_IE {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : PctCondCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramCBM_RE {get; set;}
				        
											
						// Table :  
						// Field : PctCUICvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramCUI_FP {get; set;}
				        
											
						// Table :  
						// Field : PctCUICvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramCUI_IE {get; set;}
				        
											
						// Table :  
						// Field : PctCUICvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramCUI_RE {get; set;}
				        
											
						// Table :  
						// Field : PctIRThermCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramIRThermography_FP {get; set;}
				        
											
						// Table :  
						// Field : PctIRThermCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramIRThermography_IE {get; set;}
				        
											
						// Table :  
						// Field : PctIRThermCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramIRThermography_RE {get; set;}
				        
											
						// Table :  
						// Field : PctOilAnalysisCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramOilAnalysis_FP {get; set;}
				        
											
						// Table :  
						// Field : PctOilAnalysisCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramOilAnalysis_IE {get; set;}
				        
											
						// Table :  
						// Field : PctOilAnalysisCvr
                                                    [Range(0, 100, ErrorMessage = "The field value is outside the range of 0 to 100 percent.")]
                                                    public decimal? ProgramOilAnalysis_RE { get; set; }

                        // Table :  
						// Field : MachineToolPct_RE
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")]
						   						public decimal?  MachineToolPct_RE {get; set;}

                        // Table :  
						// Field : MachineToolPct_FP
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  MachineToolPct_FP {get; set;}

                        // Table :  
						// Field : MachineToolPct_IE
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  MachineToolPct_IE {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : PctRCMCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramRCM_FP {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : PctRCMCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramRCM_IE {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : PctRCMCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramRCM_RE {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : PctTimeCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramTimeBased_FP {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : PctTimeCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramTimeBased_IE {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : PctTimeCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramTimeBased_RE {get; set;}
				        
											
						// Table :  
						// Field : PctVibrAnaCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramVibrationAnalysis_FP {get; set;}
				        
											
						// Table :  
						// Field : PctVibrAnaCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramVibrationAnalysis_IE {get; set;}
				        
											
						// Table :  
						// Field : PctVibrAnaCvr
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  ProgramVibrationAnalysis_RE {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : ImprovementProg
												public string  RelProcessImproveProg_FP {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : ImprovementProg
												public string  RelProcessImproveProg_IE {get; set;}
				        
											
						// Table : UnitRelOrgProcesses 
						// Field : ImprovementProg
												public string  RelProcessImproveProg_RE {get; set;}

                                                // Table : UnitRelOrgProcesses 
                                                // Field : MachineToolMVal
                                                public string MachineToolMVal_RE { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : MachineToolMVal
                                                public string MachineToolMVal_FP { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : MachineToolMVal
                                                public string MachineToolMVal_IE { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : EquipmentRelStg
                                                public string EquipmentRelStg_RE { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : EquipmentRelStg
                                                public string EquipmentRelStg_FP { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : EquipmentRelStg
                                                public string EquipmentRelStg_IE { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : RootCauseAls
                                                public string RootCauseAls_ARI { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : RootCauseAls
                                                public string RootCauseAls_Pct { get; set; }


                                                // Table : UnitRelOrgProcesses 
                                                // Field : RootCauseAls
                                                public string RootCauseAls_No { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : RootCauseAls
                                                public string RootCauseAls_AFNO { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : RootCauseAls
                                                public string RootCauseAls_AFO { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : RootCauseAls
                                                public string RootCauseAls_QOF { get; set; }

                                                // Table : UnitRelOrgProcesses 
                                                // Field : RootCauseAls
                                                public string RootCauseAls_BAL { get; set; }
                       
											
						// Table : UnitRiskMgmt 
						// Field : EquipRBI_FP
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  RiskMgmtEquipRBI_FP {get; set;}
				        
											
						// Table : UnitRiskMgmt 
						// Field : EquipRBI_IE
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  RiskMgmtEquipRBI_IE {get; set;}
				        
											
						// Table : UnitRiskMgmt 
						// Field : EquipRBI_RE
											    	[Range(0,100,ErrorMessage="The field value is outside the range of 0 to 100 percent.")] 
						   						public decimal?  RiskMgmtEquipRBI_RE {get; set;}
				        
														   }

}
