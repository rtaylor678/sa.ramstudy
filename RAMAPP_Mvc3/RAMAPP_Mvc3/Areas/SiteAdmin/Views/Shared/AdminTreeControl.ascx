﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%

    using (RAMAPP_Mvc3.Entity.RAMEntities db = new RAMAPP_Mvc3.Entity.RAMEntities())
    {

        RAMAPP_Mvc3.Entity.LogIn userinfo = (RAMAPP_Mvc3.Entity.LogIn)HttpContext.Current.Session["UserInfo"];
        if (userinfo != null)
        {

            var sites = (from s in db.SiteInfo
                         where s.CompanyID == userinfo.CompanyID
                         select new
                         {
                             s.SiteName,
                             s.SiteNbr,
                             units = (from u in db.UnitInfo
                                      where u.CompanyID == s.CompanyID && u.PlantSiteName == s.SiteName
                                      select new { u.CoNameForUnit, u.PrdctLneNbr, u.ProductGenericName, u.ProcessType })
                         }).ToList();

            TreeNode siteRoot = TreeView1.FindNode("Sites");
            foreach (var parent in sites)
            {
                TreeNode siteNode = new TreeNode(parent.SiteName, parent.SiteNbr.ToString());
                siteRoot.ChildNodes.Add(siteNode);
                //TreeView1.Add(siteNode);
                foreach (var child in parent.units)
                {
                    TreeNode unitNode = new TreeNode(child.CoNameForUnit, child.PrdctLneNbr.ToString());
                    siteNode.ChildNodes.Add(unitNode);
                }
            }
        }
    }

%>
<asp:TreeView ID="TreeView1" runat="server" Height="169px" ImageSet="XPFileExplorer"
    LineImagesFolder="~/TreeLineImages" NodeIndent="15" ShowLines="True" Width="121px">
    <HoverNodeStyle Font-Underline="True" ForeColor="#6666AA" />
    <Nodes>
        <asp:TreeNode Text="Sites" Value="Sites"></asp:TreeNode>
        <asp:TreeNode Text="Users" Value="Users"></asp:TreeNode>
    </Nodes>
    <NodeStyle Font-Names="Tahoma" Font-Size="8pt" ForeColor="Black" HorizontalPadding="2px"
        NodeSpacing="0px" VerticalPadding="2px" />
    <ParentNodeStyle Font-Bold="False" />
    <SelectedNodeStyle BackColor="#B5B5B5" Font-Underline="False" HorizontalPadding="0px"
        VerticalPadding="0px" />
</asp:TreeView>