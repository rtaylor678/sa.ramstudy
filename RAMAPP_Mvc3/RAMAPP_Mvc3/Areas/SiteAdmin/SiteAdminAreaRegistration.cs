﻿using System.Web.Mvc;

namespace RAMAPP_Mvc3.Areas.SiteAdmin
{
    public class SiteAdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SiteAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SiteAdmin_Default",
                "SiteAdmin/{action}/{companyId}",
                new { area = "SiteAdmin", controller = "SiteAdmin", action = "Index", companyId = UrlParameter.Optional }
            );

            // routes.MapRoute("SiteAmin", "Admin/{companyid}",
            //      new { area = "SiteAdmin", controller = "SiteAdmin", action = "Index", companyId = "" });
        }
    }
}