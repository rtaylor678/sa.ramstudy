﻿using System.Linq;
using System.Web.Mvc;
using RAMAPP_Mvc3.Entity;

namespace RAMAPP_Mvc3.Controllers
{
    public class ToolsController : Controller
    {
        //
        // GET: /Tools/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GridConfigurator()
        {
            return View();
        }

        public JsonResult GetGrid(string gridId)
        {
            using (var db = new RAMEntities())
            {
                string result1 =
                    db.ExecuteStoreQuery<string>("select dbo.CreateGridList_A({0})", gridId).SingleOrDefault();
                string result2 =
                    db.ExecuteStoreQuery<string>("select dbo.EditGridList_B({0})", gridId).SingleOrDefault();

                string tablebldr = result1 + result2;

                return Json(tablebldr, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public void AddCalcGroup()
        {
        }
    }
}