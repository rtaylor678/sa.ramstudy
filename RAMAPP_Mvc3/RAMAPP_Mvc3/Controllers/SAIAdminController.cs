﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Models;
using RAMAPP_Mvc3.Localization;

namespace RAMAPP_Mvc3.Controllers
{
    public class SAIAdminController : Controller
    {
        //
        // GET: /SAIAdmin/

        public ActionResult Index()
        {
            
            Session["Mode"] = SessionUtil.AdminKey();

            return View();
        }

        [HttpGet]
        public ActionResult ViewCompanies()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ViewCompaniesInner(string id)
        {
            var sb = new StringBuilder();
            var enClt = ClientCultureInfo.GetEnglishCulture();
            const string innerTemplate = "<ul  class=\"gridbody\" scale=\"{1}" +
                             "\"  pctcm='{2:N1}'><li class=\"gridrowtext\" style=\"text-indent:30px;\">{0}</li><li class=\"gridrow\">" + "<span class='bar' style='width:{4}%;'>{2:N1}%</span>" +
                             "</li><li class=\"gridrow\"><span class=\"bigfont\">{1}" +
                             "</span></li> <li class=\"gridrow\">{3}" +
                             "</li></ul>";
            using (var db = new RAMEntities())
            {
                var comps = (
                             from h in db.SiteInfo
                             join v in db.DatasetQualitySiteSummary on h.DatasetID equals v.DatasetID
                             where h.CompanySID.Equals(id)
                             select new { h.DatasetID, h.SiteName, h.CompanySID, HasSubmitted = db.Reviewable.Any(g => (g.SiteDatsetID == h.DatasetID) || (g.CompanySID == h.CompanySID && g.DataLevel == "Co")), v.PcntComplete, v.Grade });

                var imgPath = Url.Content("~/images");
                foreach (var cp in comps)
                {
                    sb.AppendFormat(innerTemplate, cp.SiteName, cp.Grade, cp.PcntComplete,
                        "<img class=\"lockbtn\" level=\"Si\"  sid=\"" + cp.CompanySID + "\"  hasSubmitted=" + cp.HasSubmitted + " eid=\"" + cp.DatasetID + "\" alt=\"" + (cp.HasSubmitted ? "Ready" : "Not Ready") + "\" src=\"" + imgPath + (cp.HasSubmitted ? "/disable" : "/enable") + ".gif\" />", String.Format(enClt, "{0:N1}", cp.PcntComplete)
                        );
                }
            }

            return Content(sb.ToString());
        }

        //
        // GET: /SAIAdmin/Create

        public ActionResult CreateCompany()
        {
            return View();
        }

        //
        // POST: /SAIAdmin/Create

        [HttpPost]
        public ActionResult CreateCompany(CompanyModel m)
        {
            try
            {
                var studyYear = 2011;
                //const string studyYearExt = "11";
                using (var db = new RAMEntities())
                {
                    DateTime? curStudyYear = db.GetCurrentStudy().Single();
                    studyYear = curStudyYear.Value.Year;
                    string studyYearExt = studyYear.ToString().Remove(0, 2);
                    var result = db.AddCompany(m.Name, m.CompanyID + studyYearExt, m.CompanyID, (short)studyYear, null,
                                               null, null, null, null, null, null, null,
                                               (short?)m.SiteLimit);

                    int status = result.Single() ?? -99;

                    if (status < 0)
                    {
                        TempData["CreateMsg"] = "The company identification number already exists. Please enter a different company identification number.";
                        return View();
                    }
                }
                var coordinator = new UserDetails() { CompanyID = m.CompanyID, Role = 2 };

                return View("AddCompanyCoordinator", coordinator);
            }
            catch (Exception ex)
            {
                TempData["CreateMsg"] = "Error just occurred. Could not create company.";
                return View();
            }
        }

        public ActionResult CompanySummary(int companyId, string email)
        {


            ViewData["compid"] = companyId;
            ViewData["email"] = email;
           // CompanyDetails(companyId);
           // DataCoordinatorDetails(email);

            return View();
        }

        [HttpGet]
        public ActionResult AssignCoordinator()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AssignCoordinator(UserDetails um)
        {
            if (!um.CompanyID.ToUpper().Contains("00SAI"))
                um.Role = 2;
            else
                um.Role = 32000;

            return View("AddCompanyCoordinator", um);
        }

        [HttpGet]
        public ActionResult AddCompanyCoordinator(UserDetails m)
        {
            return View(m);
        }

        //[HttpGet]
        //public ActionResult AddCompanyCoordinator()
        //{
        //    return View();
        //}

        [HttpPost]
        public ActionResult CreateCompanyCoordinator(UserDetails um)
        {
            TempData["UserCreateMsg"] = "User " + um.Firstname + " " + um.Lastname + " was sucessfully created";
            var memProvider = new RAMMembershipProvider();
            MembershipCreateStatus status;

            using (var db = new RAMEntities())
            {
                try
                {
                    DateTime? curStudyYear = db.GetCurrentStudy().Single();

                    //var comp = db.Company_LU.Where(cl => cl.CompanyID == um.CompanyID && cl.StudyYear == 2011).Single();
                    var comp = db.Company_LU.Where(cl => cl.CompanyID == um.CompanyID && cl.StudyYear == curStudyYear.Value.Year).Single();

                    um.ScreenName = um.EmailAddress.Split("@".ToCharArray())[0];

                    um.UserID = um.ScreenName + "/" + comp.CompanySID;

                    TempData["CompanyName"] = comp.CompanyName;

                    var user = memProvider.CreateUser(um.CompanyID.Trim(), um.UserID, um.Password, 2, um.Firstname,
                        um.Lastname, um.ScreenName, (um.Culture ?? "en"), um.EmailAddress, um.JobTile != null ? um.JobTile.Trim() : "", um.Telephone != null ? um.Telephone.Trim() : "",
                                           out status);
                    if (status == MembershipCreateStatus.DuplicateUserName)
                        TempData["UserCreateMsg"] = "User is already in the system.";
                    else
                    {
                        ViewData["DCcompanyid"] = comp.CompanyID;
                        ViewData["DCfirstname"] = um.Firstname;
                        ViewData["DClastname"] = um.Lastname;
                        ViewData["DCjobtitle"] = um.JobTile;
                        ViewData["DCemail"] = um.EmailAddress;
                        ViewData["DCphone"] = um.Telephone;
                        ViewData["DCculture"] = um.Culture;
                        ViewData["DCcompanyname"] = comp.CompanyName;
                        return View("CompanySummary", new { companyId = comp.CompanyID, userEmail = um.EmailAddress });
                    }
                }
                catch
                {
                    TempData["UserCreateMsg"] = "A system error occurred." + um.Firstname + " " + um.Lastname +
                                                " was not created";
                }

                return View("AddCompanyCoordinator", um);
            }
        }

        [HttpPost]
        public JsonResult ToggleSubmit(int id, string sid, string level)
        {
            var result = true;
            try
            {
                using (var db = new RAMEntities())
                {
                    sid = sid.Trim();
                   
                    if (!db.Reviewable.Any(r => (r.SiteDatsetID == id && r.DataLevel == "Si") || (r.CompanySID == sid && r.DataLevel == "Co")))
                    {
                        var rv = new Reviewable();
                        rv.DataLevel = level;
                        rv.CompanySID = sid;
                        rv.SiteDatsetID = id;
                     
                        db.AddToReviewable(rv);
                        db.SaveChanges();
                    }
                    else
                    {
                        var rv = null as Reviewable;
                        if (level == "Si")
                        {
                            rv = db.Reviewable.FirstOrDefault(r => (r.SiteDatsetID == id && r.DataLevel == "Si"));
                          
                        }
                        else
                        {
                            rv = db.Reviewable.FirstOrDefault(r => (r.CompanySID == sid && r.DataLevel == "Co"));
                        }

                        if (rv != null)
                        {
                            db.DeleteObject(rv);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch
            {
                result = false;
            }
            return this.Json(new { ret = result });
        }

        public PartialViewResult CompanyDetails(int id)
        {
            using (var db = new RAMEntities())
            {
                var cm = db.Company_LU.FirstOrDefault(c => c.DatasetID == id);
                var comModel = new CompanyModel() { CompanyID = cm.CompanyID, DatasetID = cm.DatasetID, Name = cm.CompanyName };
                return PartialView("CompanyDetails", comModel);
            }
        }

        public PartialViewResult DataCoordinatorDetails(string email)
        {
            var memProvider = new RAMMembershipProvider();
       
            var cm = memProvider.GetUser(email); 
            var comModel = new UserDetails()
            {
                Culture = cm.UserLanguage,
                EmailAddress = cm.Email,
                ScreenName = cm.ScreenName,
                JobTile = cm.JobTitle,
                Firstname = cm.FirstName,
                Lastname = cm.LastName
            };
            return PartialView("DataCoordinatorDetails", new UserDetails());
           
        }
    }
}