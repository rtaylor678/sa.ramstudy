﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using RAMAPP_Mvc3.ActionFilter;
using RAMAPP_Mvc3.Documents;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Models;
using RAMAPP_Mvc3.Repositories.Cache;



namespace RAMAPP_Mvc3.Controllers
{
    [Authorize]
    [ReplaceCurrencyAndBreadcrumb]
    public class AppController : Controller
    {
        //
        // GET: /App/

        private const short CacheExprirationTime = 150;
        private static readonly object CacheLockObject = new object();

        [HttpGet]
        public ActionResult Comment(string id, string qid)
        {
            return View();
        }

        [HttpPost]
        public void Comment(CommentModel m, int id, string qid)
        {
            qid = qid.Replace("___", ".");
            if (SessionUtil.IsSessionExpired() != null)
            {
                var userinfo = (LogIn)Session["UserInfo"];

                if (userinfo != null && qid != "" && id != 0)
                {
                    string newComment = Request.Form["newComment.Comment"].Trim();
                    var cm = new Comments
                                 {
                                     UserNo = userinfo.UserNo,
                                     DatasetID = id,
                                     Posted = DateTime.Now,
                                     QuestionInventoryID = qid.Trim(),
                                     CommentText = newComment
                                 };

                    using (var db = new RAMEntities())
                    {
                        if (
                            !db.Comments.Any(
                                c =>
                                c.QuestionInventoryID == qid.Trim() && c.DatasetID == id && c.CommentText == newComment))
                        {
                            db.AddToComments(cm);
                            db.SaveChanges();
                        }
                    }
                }
            }
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult CommentsForQuestion(string id, string qid)
        {
            return View();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        [HttpPost]
        public void DeleteComment(int id)
        {
            var user = (LogIn)Session["UserInfo"];
            using (var db = new RAMEntities())
            {
                Comments o = (from g in db.Comments
                              where g.CommentID == id
                              select g).Single();
                db.DeleteObject(o);
                db.SaveChanges();
            }
        }


        [OutputCache(Duration = 600, VaryByParam = "None")]
        public ActionResult Glossary()
        {
            string filePath = Server.MapPath("~/Documents/2011 RAM Study Glossary.docx");
            ViewData["Glossary"] = DocxReader.ConvertDocxToHtml(filePath);
            return View();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Introduction()
        {
            return View();
        }

        public JsonResult JsonValidation(ModelStateDictionary state)
        {
            return new JsonResult
                       {
                           Data = new
                                      {
                                          Tag = "ValidationError",
                                          State = from e in state
                                                  where e.Value.Errors.Count > 0
                                                  select new
                                                             {
                                                                 Name = e.Key,
                                                                 Errors = e.Value.Errors.Select(x => x.ErrorMessage)
                                                      .Concat(
                                                          e.Value.Errors.Where(x => x.Exception != null).Select(
                                                              x => x.Exception.Message))
                                                             }
                                      }
                       };
        }

        /// <summary>
        /// Loads the tooltip and help whenever a question reference button is clicked or toggled
        /// </summary>
        /// <param name="qid">The question's id in the database </param>
        /// <returns></returns>
        public JsonResult LoadQuestionReferences(string qid)
        {
            using (var db = new RAMEntities())
            {
                object ret = null;
                QuestionReference tip = db.QuestionReference.FirstOrDefault(q => q.QuestionInventoryID == qid);
                if (tip == null)
                    ret = new { ToolTip = "Help is unavailable.", Help = "Help is unavailable." };
                else
                    ret = new { ToolTip = tip.Tip.TrimEnd(), Help = tip.Help.TrimEnd() };

                return Json(ret, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LoggedIn(string email)
        {
            if (email != "")
            {
                var provider = new RAMMembershipProvider();
                var userinfo = provider.GetUser(email);

                Session["UserInfo"] = userinfo;
                Session["IsAdmin"] = (userinfo.SecurityLevel == 2 || userinfo.SecurityLevel >= 32000);
              
                
                using (var db = new RAMEntities())
                {
                   /* var user = (from a in db.CurrentStudyForUsers 
                                where a.UserID.Trim() == userinfo.UserID.Trim() 
                                select new { CompSID = a.CompanySID, IsSiteAdmin = db.SitePermissions.Any(p => userinfo.UserID.Trim() == p.UserID.Trim() && p.SecurityLevel == 1) }).First();
                    */

                    Session["CompanySID"] = db.CurrentStudyForUsers.FirstOrDefault(u => u.UserID.Trim() == userinfo.UserID.Trim()).CompanySID.Trim(); //user.CompSID.Trim();
                      /*  (from a in db.CurrentStudyForUsers where a.UserID == userinfo.UserID select a.CompanySID).Single
                            ().Trim();*/

                    Session["IsSiteAdmin"] = db.SitePermissions.Any(p => userinfo.UserID.Trim() == p.UserID.Trim() && p.SecurityLevel == 1); // user.IsSiteAdmin;
                     /*   db.SitePermissions.Any(p => p.UserID == userinfo.UserID && p.SecurityLevel == 1);*/

                    //Session.Timeout = 130;
                }
                
                if (userinfo.SecurityLevel < 32000)
                    SessionChecker.UpdateInsertSessionDictionary(userinfo.UserNo);

                if (userinfo.CompanyID.Contains("00SAI") && userinfo.SecurityLevel >= 32000)
                    return RedirectToAction("Index", "SAIAdmin");
            }

            return RedirectToAction("Index");
        }

        public ActionResult MyProfile()
        {
            return RedirectToAction("Index", "Profile");
        }

        [HttpGet]
        public ActionResult NotAllowed()
        {
            return View();
        }



        public ActionResult UserGuide()
        {
            return View();
        }



        protected ActionResult LoadView<TModel>(string id) where TModel : class
        {
            if (!SessionUtil.IsSessionExpired() && id != String.Empty)
            {
                short siteOrUnitId = Int16.Parse(id);

              

                //Called from the repository to make that the object is pulled from the cache
                var repository =
                    (ICachedSiteUnitModelsRepository)new CachedSiteUnitModelsRepository();
                //IoC.Initialize().GetInstance(typeof(Repositories.Cache.ICachedSiteModelsRepository));
                TModel siteOrUnitModel;
                repository.GetModel(out siteOrUnitModel, SessionUtil.GetCompanySID(), siteOrUnitId);

                return View(siteOrUnitModel);
            }

            return View();
        }

        protected JsonResult SaveView<TModel>(TModel m, string id) where TModel : class
        {
            bool result = false;
            string statusMsg = "Data has been saved";
            string companySID;
            int userNo;

            userNo = (Session["UserInfo"] as LogIn).UserNo;

            if (id != null)
            {
                companySID = SessionUtil.GetCompanySID();

                try
                {
                    Type type =
                        Type.GetType("RAMAPP_Mvc3.ModelPropertyHandlers." + typeof(TModel).Name + "PropertyHandler",
                                     true);
                    dynamic hnd = Activator.CreateInstance(type);

                    result = hnd.Save(m, Int32.Parse(id), userNo);
                }
                catch (Exception e)
                {
                    result = false;
                    statusMsg = e.Message;
                }
            }

            return Json(new { Status = result, Msg = statusMsg });
        }


    }
}