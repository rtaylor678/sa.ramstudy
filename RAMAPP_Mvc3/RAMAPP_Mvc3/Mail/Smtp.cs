﻿using System.Net.Mail;
using System;

/// <summary>
/// Summary description for Smtp.
/// </summary>
public sealed class Smtp
{
     

    public Smtp()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// Sends email messages
    /// </summary>
    /// <param name="from">Sender address</param>
    /// <param name="to">Recepient address</param>
    /// <param name="bcc">Bcc recepient</param>
    /// <param name="cc">Cc recepient</param>
    /// <param name="subject">Subject of mail message</param>
    /// <param name="body">Body of mail message</param>
    public static void SendMailMessage(string author, string from, string to, string bcc, string cc, string subject, string body)
    {
        //Create an instance of the MailMessage class
        string EmailServerAddress = System.Configuration.ConfigurationManager.AppSettings["MailServerAddress"].ToString();
        using (MailMessage objMM = new MailMessage())
        {
            //objMM.From = new MailAddress(from, author);
            objMM.From = new MailAddress(from);
            var recips = to.Split(";".ToCharArray());
            foreach (var recip in recips)
            {
                objMM.To.Add(new MailAddress(recip));
            }

            objMM.IsBodyHtml = false;
            objMM.Priority = MailPriority.Normal;
            objMM.BodyEncoding = System.Text.Encoding.UTF8;
            objMM.SubjectEncoding = System.Text.Encoding.UTF8;

            SmtpClient client = new SmtpClient(EmailServerAddress, int.Parse(System.Configuration.ConfigurationManager.AppSettings["MailServerPort"].ToString()));
            client.EnableSsl = true;
            client.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["MailServerUserName"].ToString(), System.Configuration.ConfigurationManager.AppSettings["MailServerPassword"].ToString());

            objMM.Subject = subject;

            //Set the body - use VbCrLf to insert a carriage return
            objMM.Body = body;
            try
            {
                //Send message
                client.Send(objMM);
            }
            catch (Exception ex)
            {

            }
        }
    }
}