﻿/*
*	Watermark plugin
*	Version: 0.6
*	jQuery 1.2.6
*	 
*	Author: Gonzalo Villar
*
*	TODO:
*		- Inherit all the watermarked element CSS
* 
*/
//(function (c) { function h(a, b, d) { var f = (b.watermarkText) ? b.watermarkText : a.attr('title'); var g = (d == 'INPUT') ? '<input type="text">' : '<textarea>'; var e = c(g).attr('id', a.attr('id') + '_0').addClass(b.watermarkCssClass).addClass('watermarkPluginCustomClass').val(f).css({ overflowY: a.css('overflow-y'), overflowX: a.css('overflow-x') }); if (a.height() > 0) e.css({ height: a.outerHeight(), width: a.outerWidth() }); e.hide(); a.before(e); return e } function i(g, e) { g.each(function () { var b = jQuery(this); var d = b.attr('tagName'); if (d == null || d.length == 0) d = b.prop('tagName'); var f = h(b, e, d.toUpperCase()); f.focus(function (a) { c(this).hide(); b.show().focus() }); b.blur(function (a) { if (c(this).val() == '') { c(this).hide(); f.show() } }); b.blur() }); return g } c(window).unload(function () { c('.watermarkPluginCustomClass').remove() }); c.fn.watermark = function (a) { return i(this, a) } })(jQuery);

;(function ($) {

    function CreateDummyInput(jElement, options, tagName) {
        var watermarkText = (options.watermarkText) ? options.watermarkText : jElement.attr('title');
        var dummyType = (tagName == 'INPUT') ? '<input type="text">' : '<textarea>';

        var dummyInput = $(dummyType)
            .attr('id', jElement.attr('id') + '_watermark')
            .addClass(options.watermarkCssClass)
			.addClass('watermarkPluginCustomClass') //workaround to fix some caching? problem in FF3. Used in window.unload hook to remove watermarks from the DOM
            .val(watermarkText)
			.css({ overflowY: jElement.css('overflow-y'), overflowX: jElement.css('overflow-x') });

        if (jElement.height() > 0)
            dummyInput.css({ height: jElement.outerHeight(), width: jElement.outerWidth() });

        dummyInput.hide();
        jElement.before(dummyInput);
        return dummyInput;
    }

    function MakeWatermark(element, options) {
        element.each(function () {
            var thisEl = jQuery(this);
            var tag = thisEl.attr('tagName');

            //jquery 1.6.1 fix
            if (tag == null || tag.length == 0)
                tag = thisEl.prop('tagName');



            var dummyInput = CreateDummyInput(thisEl, options, tag.toUpperCase());

            dummyInput.focus(function (e) {
                $(this).hide();
                thisEl.show().focus();
            });
            
            thisEl.blur(function (e) {
            if ($(this).val() == '') {
            $(this).hide();
            dummyInput.show();
            }
            });

          thisEl.blur();

        });

        return element;
    }

    $(window).unload(function () {
        $('.watermarkPluginCustomClass').remove();
    });

    $.fn.watermark = function (options) { return MakeWatermark(this, options); }

})(jQuery);;