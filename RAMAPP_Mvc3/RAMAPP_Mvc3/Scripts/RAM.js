﻿//Used after referencing globalize.js
function setupCulture(selectedCulture, globPath) {
    // var globPath = "/Scripts/cultures/globalize.culture." + selectedCulture + ".js";
    Globalize.culture(selectedCulture);
    //$.getScript(globPath, function () { alert(selectedCulture + '- language loaded successfully'); });

    //Configure validators
    $.validator.methods.number = function (value, element) {
        return this.optional(element) || !isNaN(Globalize.parseFloat(value, selectedCulture));
    }
}

function commentCallback(responseText, responseStatus, xhr) {
    if (responseStatus != 'success') {
        var errormsg;
        if (xhr && xhr.statusText)
            errormsg = xhr.statusText;
        if (errormsg == '' || errormsg == null)
            errormsg = $(responseText).text();

        errormsg += ' at ' + (new Date()).toLocaleTimeString();

        $('#commentDiv').html(responseText);
    }
}

//Opens comment box and records comments
function recordComment(suid, qid) {
    var $commentBox = $("#commentDiv");
    var $loader = $commentBox.find('#loading');
    $commentBox.html('');
    $commentBox.dialog({
        bgiframe: true,
        resizable: false,
        modal: true,
        height: 530,
        width: 500,
        buttons:
                {
                    'Save': function () {
                        $commentBox.find("form:first").submit();
                        return false;
                    },
                    'Close': function () {
                        $(this).dialog('close');
                    }
                }
    });

    $loader.show();

    $commentBox.load(appUrl + '/Comment/' + suid + '/' + qid.toString().replace('.', '___'), function (response, status, xhr) {
        if (status == "error") {
            $(this).html("Sorry an error occurred when loading comments");
        } else
            refreshComment(suid, qid);
    });

    $loader.hide();

    return false;
}

function refreshComment(id, qid) {
    var $commentList = $("#commentList");
    var $textBox = $('#commentDiv  textarea');
    $commentList.html('');
    $commentList.load(appUrl + '/CommentsForQuestion/' + id + '/' + qid.toString().replace('.', '___'), function (resp, status, xhr) {
        $commentList.html(resp);
    });

    $textBox.val('').focus();

    $('#myCounter').text('250');
}

function deleteComment(id, datasetID, qid) {
    $.post(appUrl + '/DeleteComment',
            {
                "id": id
            }, function (e) {
                refreshComment(datasetID, qid);
            }, 'json');
}