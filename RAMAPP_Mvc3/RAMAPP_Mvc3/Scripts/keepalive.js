﻿window.setInterval(keepAlive, 600000);

function keepAlive() {
    $.ajax(
            {
                url: "./KeepAlive.aspx",
                cache: false,
                success: function () { $('body').html(Date.toLocaleString()) }
            }
   );
}