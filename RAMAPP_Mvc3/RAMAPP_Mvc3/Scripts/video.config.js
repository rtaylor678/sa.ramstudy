   var videoConfig = {
        	"height": 320,
        	"width" : 640,
        	"videos": [
                       { "name": "Introduction_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 1 - Web Portal Admin", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-1.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-1.flv" },
        { "name": "Introduction_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 2 (Part 1) - Introduction", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-2-Part-1.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-2-Part-1.flv" },
        { "name": "Spreadsheet_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 2 (Part 2) - Introduction", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-2-Part-2.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-2-Part-2.flv" },
        { "name": "03 process and operations_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 3 - Site & Unit Characteristics", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-3.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-3.flv" },
        { "name": "04 process data_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 4 (Part 1) - Maintenance Costs", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-4.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-4.flv" },
        { "name": "04 process data_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 4 (Part 2) - Maintenance Capital", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-4-Part-2.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-4-Part-2.flv" },
        { "name": "investment and maintenance costs - table 11,11a_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 5 - Organization", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-5.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-5.flv" },
        { "name": "maint nonta_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 6 - MRO Inventory", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-6.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-6.flv" },
        { "name": "2010 dcs turnaround module_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 7 - Work Processes", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-7.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-7.flv" },
        { "name": "energy table 3_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 8 - Reliability", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-8.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-8.flv" },
        { "name": "energy table 3_controller.swf", "folder": "", "image": "FirstFrame.png", "title": "Module 9 - Input Data Review", "videoName": "https://webservices.solomononline.com/RAMStudy/Videos/Module-9.mp4", "videoFlv": "https://webservices.solomononline.com/RAMStudy/Videos/Module-9.flv" }
        ]
     };

     