﻿using System;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace RAMAPP_Mvc3.Documents
{
    public class DocxReader
    {
        private const string WordmlNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";

        public static string ConvertDocxToHtml(string filePath)
        {
            using (var document = WordprocessingDocument.Open(filePath, false))
            {
                int header = 0;
                bool isBulletList = false;
                XNamespace w = WordmlNamespace;

                var text = new StringBuilder();
                text.AppendLine("<!DOCTYPE html>");
                text.AppendLine("<html>");
                text.AppendLine("  <head>");
                text.AppendLine(" </head>");
                text.AppendLine("  <body><p>&nbsp;</p>");
                //t iterate through paragraphs
                var paragraphs = document.MainDocumentPart.Document.Body.Elements<Paragraph>();
                var ul = 1;

                foreach (Paragraph para in paragraphs)
                {
                    //t we export paragraphs as <p> tags

                    var stylenode = para.Elements<ParagraphProperties>().Where(prop => prop.ParagraphStyleId != null &&
                                                                                       (prop.ParagraphStyleId.Val.Value.Contains("Heading") ||
                                                                                        prop.ParagraphStyleId.Val.Value.Contains("BulletedList")))
                                                                                        .FirstOrDefault();

                    if (stylenode != null)
                    {
                        switch (stylenode.ParagraphStyleId.Val.Value)
                        {
                            case "BulletedList":

                                if (ul++ == 1)
                                {
                                    text.Append("<ul>");
                                }
                                text.Append("<li>");
                                isBulletList = true;
                                break;
                            default:
                                if (ul > 1)
                                {
                                    text.Append("</ul>");
                                    ul = 1;
                                }

                                text.Append("<p>");

                                if (stylenode.ParagraphStyleId.Val.Value.Contains("Heading"))
                                {
                                    header = Int16.Parse(stylenode.ParagraphStyleId.Val.Value.Remove(0, 7)) - 3; // the word heading length is 7 - Heading6
                                    text.Append("<h" + header + ">");
                                }
                                break;
                        }
                    }
                    else
                    {
                        if (ul > 1)
                        {
                            ul = 1;
                            text.Append("</ul>");
                        }
                        text.Append("<p>");
                    }

                    foreach (var r in para.Elements<Run>())
                    {
                        if (r.RunProperties != null)
                        {
                            //t ADD OPENNING TAGS HERE, if any

                            if (r.RunProperties.Bold != null)
                                text.Append("<b>");

                            if (r.RunProperties.Italic != null)
                                text.Append("<i>");

                            if (r.RunProperties.Underline != null)
                                text.Append("<u>");

                            if (r.RunProperties.Color != null && r.RunProperties.Color.Val != null)
                                text.Append("<span style=\"color:#" + r.RunProperties.Color.Val + "\">");

                            text.Append(r.RunProperties.Caps != null ? r.InnerText.ToUpper() : r.InnerText);

                            //t ADD CLOSING TAGS HERE
                            //t IN BACK ORDER OF OPENNING TAGS
                            if (r.RunProperties.Color != null && r.RunProperties.Color.Val != null)
                                text.Append("</span>");

                            if (r.RunProperties.Underline != null)
                                text.Append("</u>");

                            if (r.RunProperties.Italic != null)
                                text.Append("</i>");

                            if (r.RunProperties.Bold != null)
                                text.Append("</b>");
                        }
                        else
                        {
                            text.Append(r.InnerText);
                        }
                    }

                    if (header > 0)
                    {
                        text.Append("</h" + header + ">");
                        header = 0;
                    }
                    //if (ul == 1)
                    //    text.Append("</p>");

                    if (ul > 1)
                        text.Append("</li>");

                    if (ul == 1)
                        text.Append("</p>");
                }

                text.AppendLine("  </body>");
                text.AppendLine("</html>");

                return text.ToString();
            }
        }
    }
}