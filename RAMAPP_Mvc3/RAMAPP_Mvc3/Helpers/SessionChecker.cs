﻿  using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Web;

namespace RAMAPP_Mvc3.Helpers
{
  
    /// <summary>
    /// Class to get information about who is online
    /// </summary>
    public class SessionChecker
    {
        private static Dictionary<int, DateTime> dictSession;

        public static Dictionary<int, DateTime> CreatSessionDictionary()
        {
            var objToLock = new Object();
            var dictControl = HttpRuntime.Cache["UsersCountSession"] as Dictionary<int, DateTime>;
            try
            {
                if (dictControl == null)
                {
                    lock (objToLock)
                    {
                        dictSession = new Dictionary<int, DateTime>();
                        HttpRuntime.Cache.Insert("UsersCountSession", dictSession, null, DateTime.Now.AddMinutes(10100),
                        System.Web.Caching.Cache.NoSlidingExpiration,
                        System.Web.Caching.CacheItemPriority.NotRemovable, null);

                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "exception at CreatSessionDictionary: " + ex.Message, EventLogEntryType.Error);
            }
            return HttpRuntime.Cache["UsersCountSession"] as Dictionary<int, DateTime>;
        }

        public static void UpdateInsertSessionDictionary(int pUserId)
        {
            try
            {
                var objElseLock = new object();
                var dic = HttpRuntime.Cache["UsersCountSession"] as Dictionary<int, DateTime>;
                if (dic != null)
                {

                    if (dic.ContainsKey(pUserId))
                    {
                        if ((DateTime.Now - dic[pUserId]).Minutes > 2) // to reduce the lock load
                            lock (((IDictionary)dic).SyncRoot)
                            {
                                dic[pUserId] = DateTime.Now.AddMinutes(1);
                            }
                    }
                    else // insert
                    {
                        lock (objElseLock)
                        {
                            dic.Add(pUserId, DateTime.Now.AddMinutes(1));
                            HttpRuntime.Cache["UsersCountSession"] = dic;
                        }
                    }
                }
                else
                    CreatSessionDictionary();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "exception at UpadteInsertSessionDictionary: " + ex.Message, EventLogEntryType.Error);
            }
        }

        public static int GetOnlineUsersCount()
        {
            if (null != HttpRuntime.Cache["UsersCountSession"])
            {
                return (HttpRuntime.Cache["UsersCountSession"] as Dictionary<int, DateTime>).Count;
            }
            return 0;
        }
    }


}