﻿using System;
using ShrinkWrap.HttpHandler;

namespace ShrinkWrap.Exceptions
{
    /// <summary>
    /// Custom Exception thrown for ShrinkWrap specific errors.
    /// </summary>
    public class ShrinkWrapException : Exception
    {
        public CompositeUri Uri { get; private set;}

        public ShrinkWrapException(String message, Exception innerException, CompositeUri uri)
            : base(message, innerException)
        {
            Uri = uri;
        }
    }
}
