﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace ShrinkWrap.Configuration
{
    public class ShrinkWrapConfiguration : ConfigurationSection
    {
        /// <summary>
        /// We set expiry headers so that browsers can cache our responses.
        /// </summary>
        [ConfigurationProperty("expiry", DefaultValue = 0, IsRequired = false)]
        public virtual int Expiry
        {
            get
            {
                return (int)this["expiry"];
            }
        }

        /// <summary>
        /// We refuse to serve more than maxConstituents files aggregated together to prevent
        /// malicious requests chewing up resources.
        /// </summary>
        [ConfigurationProperty("maxConstituents", DefaultValue = 20, IsRequired = false)]
        public virtual int MaxConstituents
        {
            get
            {
                return (int)this["maxConstituents"];
            }
        }

        /// <summary>
        /// Builds a ShrinkWrapConfiguration object from web.config. If there's no <shrinkwrap> node, a configuration
        /// object with default values is returned.
        /// </summary>
        public static ShrinkWrapConfiguration Get()
        {
            var config = ConfigurationManager.GetSection("shrinkwrap") as ShrinkWrapConfiguration;
            return config ?? new ShrinkWrapConfiguration();
        }
    }
}
