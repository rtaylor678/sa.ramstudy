﻿using System;
namespace ShrinkWrap.HttpHandler
{
    public interface IHttpRequest
    {
        System.Collections.Specialized.NameValueCollection Headers { get; set; }
        Uri Uri { get; set; }
        string Verb { get; set; }
    }
}
