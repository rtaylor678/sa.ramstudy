using System;
using ShrinkWrap.Minifier;

namespace ShrinkWrap.HttpHandler
{
    /// <summary>
    /// Handles javascript files using minification
    /// </summary>
    public class JavascriptHandler : MinifyingHandler
    {
        IMinifier minifier = new JavascriptMinifier();
        public override IMinifier Minifier
        {
            get
            {
                return minifier;
            }
        }
        public override string ContentType
        {
            get
            {
                return "text/javascript";
            }
        }
    }
}