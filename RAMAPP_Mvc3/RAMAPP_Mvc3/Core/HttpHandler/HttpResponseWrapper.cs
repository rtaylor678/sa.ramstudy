﻿using System;
using System.Web;
using System.Collections.Specialized;
using System.IO;

namespace ShrinkWrap.HttpHandler
{
    public class HttpResponseWrapper : IHttpResponse
    {
        HttpResponse response;

        public HttpResponseWrapper(HttpResponse response)
        {
            this.response = response;
        }

        public void Write(String data)
        {
            response.Write(data);
        }

        public String GetHeader(String key)
        {
            return response.Headers[key];
        }

        public void AddHeader(String key, String value)
        {
            response.AddHeader(key, value);
        }

        public int StatusCode
        {
            get { return response.StatusCode; }
            set { response.StatusCode = value; }
        }

        public String ContentType
        {
            get { return response.ContentType; }
            set { response.ContentType = value; }
        }

        public Stream Filter
        {
            get { return response.Filter; }
            set { response.Filter = value; }
        }

        public int Expires
        {
            get { return response.Expires; }
            set { response.Expires = value; }
        }
    }
}
