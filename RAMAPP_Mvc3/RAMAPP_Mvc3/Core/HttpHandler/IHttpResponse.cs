﻿using System.Collections.Specialized;
using System;
using System.IO;

namespace ShrinkWrap.HttpHandler
{
    public interface IHttpResponse
    {
        int StatusCode { get; set; }
        void Write(String data);
        String ContentType { get; set; }
        Stream Filter { get; set; }
        int Expires { get; set; }
        void AddHeader(String key, String value);
        String GetHeader(String key);
    }
}
