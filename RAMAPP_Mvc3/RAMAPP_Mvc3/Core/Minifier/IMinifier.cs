﻿using System;

namespace ShrinkWrap.Minifier
{
    public interface IMinifier
    {
        String Minify( String input );
    }
}
