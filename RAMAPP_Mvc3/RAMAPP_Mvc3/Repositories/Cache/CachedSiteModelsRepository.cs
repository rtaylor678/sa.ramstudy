﻿using System;
using System.Linq;
using System.Reflection;
using System.Web;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;

namespace RAMAPP_Mvc3.Repositories.Cache
{
    internal interface ICachedSiteUnitModelsRepository
    {
        void GetModel<TK>(out TK result, string companyId, short siteId) where TK : class;
    }
    
    public class CachedSiteUnitModelsRepository : ICachedSiteUnitModelsRepository
    {
        private const short CacheExpirationTime = 35;
        private static readonly object CacheLockObject = new object();

        // Returns  a model either from cache or database. Used for returning data to to views within  the survey section
        public virtual void GetModel<TModel>(out TModel result, string companySID, short id) where TModel : class
        {
            string cacheKey = String.Format("{0}|{1}|{2}", companySID, typeof(TModel).Name.Replace("Model", ""), id);

            result = HttpRuntime.Cache[cacheKey] as TModel;

            result = null;

            lock (CacheLockObject)
            {
                if (result == null)
                {
                    Type type = Type.GetType("RAMAPP_Mvc3.ModelPropertyHandlers." + typeof(TModel).Name + "PropertyHandler", true);
                    dynamic hnd = Activator.CreateInstance(type);

                    result = hnd.Load(id);

                    if (result != null)
                    {
                        PropertyInfo compIdPropInfo = result.GetType().GetProperty("CompanySID");
                        if (compIdPropInfo != null)
                        {
                            if (companySID != (string)compIdPropInfo.GetValue(result, null))
                                compIdPropInfo.SetValue(result, companySID, null);
                        }

                        PropertyInfo myPropInfo = result.GetType().GetProperty("isUnderReview");
                        if (myPropInfo != null)
                        {
                            var cachedSitesUnits = new CachedNavigationRepository();
                            var csc = cachedSitesUnits.GetSiteUnitList();
                            var isSite = cachedSitesUnits.GetSiteUnitList().Select(s => s.DatasetID).Contains(id);
                            if (!isSite)
                            {

                                var sid = (short)csc.Single(s => s.UnitsOfSite.Select(u => u.DatasetID).Contains(id)).DatasetID;
                                myPropInfo.SetValue(result, IsUnderReview(companySID, sid), null);
                            }
                            else
                                myPropInfo.SetValue(result, IsUnderReview(companySID, id), null);
                        }
                        //Cached for 15 minutes
                        HttpRuntime.Cache.Insert(cacheKey, result, null, DateTime.Now.AddMinutes(CacheExpirationTime), TimeSpan.Zero);
                    }
                    else
                        throw new SystemException("Site or unit is not found");
                }
            }
        }

        /// <summary>
        /// Checks weather a site is locked cause it is under review or unlocked
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        private static bool IsUnderReview(string companyId, short siteDataSetID)
        {
            if (HttpContext.Current.Session["Mode"] != null && HttpContext.Current.Session["Mode"].ToString().Equals(SessionUtil.AdminKey()))
                return false;
           
            var siteID = siteDataSetID.ToString();

            HttpCookie cookie = HttpContext.Current.Request.Cookies.Get("UnderReview");
            if (cookie == null)
            {
                cookie = new HttpCookie("UnderReview");
                // Set cookie to expire in 10 minutes.
                cookie.Expires = DateTime.Now.AddMinutes(10d);
                // Insert the cookie in the current HttpResponse.
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
           
            if (!cookie.Values.AllKeys.Contains(siteID))
            {
                using (var db = new RAMEntities())
                {
                    bool isUnderReview = (from r in db.Reviewable
                                          join s in db.SiteInfo on r.SiteDatsetID equals s.DatasetID
                                          where r.CompanySID == companyId && ((r.DataLevel.Trim() == "Co") || (r.DataLevel.Trim() == "Si" && s.DatasetID == siteDataSetID))
                                          select r
                     ).Count() > 0;

                    cookie.Values.Add(siteID, isUnderReview.ToString());

                    return isUnderReview;
                   
                }
            }
            else
                return Convert.ToBoolean(cookie.Values[siteID]);
        }
    }
}