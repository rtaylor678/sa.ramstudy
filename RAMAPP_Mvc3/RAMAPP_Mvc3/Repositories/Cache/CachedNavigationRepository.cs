﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Encryption;
namespace RAMAPP_Mvc3.Repositories.Cache
{
    public class SiteUnits
    {
        public string SiteName { get; set; }
        public short Role { get; set; }
        public int DatasetID { get; set; }
        public string LockedImg { get; set; }
        public IEnumerable<Units> UnitsOfSite { get; set; }
    }

    public class Units
    {
        public string UnitName { get; set; }
        public int DatasetID { get; set; }
        public string GenericProductName { get; set; }
        public string ProcessType { get; set; }
    }

    interface ICachedNavigationRepository
    {
        List<SiteUnits> GetSiteUnitList();
        void ResetList();
    }
    public class CachedNavigationRepository : ICachedNavigationRepository
    {
        private const short CacheExpirationTime = 35; //Cached for 15 minutes
        private static readonly object CacheLockObject = new object();

       /* ########## PREVIOUS METHOD. WAS CAUSING SITE LISTS NOT BELONGING TO CURRENT USING TO BE PULLED UP. #############
        public List<SiteUnits> GetSiteUnitList()
        {
            var userinfo = ((LogIn)HttpContext.Current.Session["UserInfo"]);


            var cacheKey = userinfo.UserID.Trim();
            var result = HttpRuntime.Cache[cacheKey] as List<SiteUnits>;


            var cp = SessionUtil.AdminKey();

            if (result == null || HttpContext.Current.Request[cp] != null)
            {
                lock (CacheLockObject)
                {
                    using (var db = new RAMEntities())
                    {
                        var companySID = SessionUtil.GetCompanySID();
                        var isAdmin = SessionUtil.IsAdmin();
                       // var isSiteAdmin = SessionUtil.IsSiteAdmin();
                        //result = (from s in db.SiteInfo
                        //          where s.CompanySID == companySID &&
                        //         (isAdmin || db.SitePermissions.Any(p => p.UserID == userinfo.UserID && (p.SecurityLevel == 1 || p.SecurityLevel == 0) && p.DatasetID == s.DatasetID))
                        //          select new SiteUnits
                        //          {
                        //              SiteName = s.SiteName,
                        //              Role = (isAdmin ? (short)2 : db.SitePermissions.FirstOrDefault(p => p.UserID == userinfo.UserID && p.DatasetID == s.DatasetID).SecurityLevel),
                        //              DatasetID = s.DatasetID,
                        //              LockedImg = db.Reviewable.Any(r => r.CompanySID == s.CompanySID && (((r.DataLevel.Trim() == "Co") || (r.DataLevel.Trim() == "Si" && r.SiteDatsetID == s.DatasetID)))) ? "#lockimg#" : "",
                        //              UnitsOfSite = from u in db.UnitInfo
                        //                            where u.SiteDatasetID == s.DatasetID
                        //                            select new Units { UnitName = u.UnitName, DatasetID = u.DatasetID, GenericProductName = u.GenericProductName, ProcessType = u.ProcessType }
                        //          }).ToList();
                        result = Linqs.CompiledLinqs.GetSiteUnitsQuery(db, isAdmin, companySID, userinfo.UserID).ToList();

                    }

                    if (result != null)
                    {
                        HttpRuntime.Cache.Insert(cacheKey, result, null, DateTime.Now.AddMinutes(CacheExpirationTime), TimeSpan.Zero);
                    }
                }

            }

            return result;
        }
        */
        
        public List<SiteUnits> GetSiteUnitList()
        {
            var userinfo = ((LogIn)HttpContext.Current.Session["UserInfo"]);
            List<SiteUnits> result = new List<SiteUnits>();
            var cp = SessionUtil.AdminKey();

            using (var db = new RAMEntities())
            {
                var companySID = SessionUtil.GetCompanySID();
                var isAdmin = SessionUtil.IsAdmin();
                result = Linqs.CompiledLinqs.GetSiteUnitsQuery(db, isAdmin, companySID, userinfo.UserID).ToList();
            }
            return result;
        }
        

        public void ResetList()
        {
            var userinfo = ((LogIn)HttpContext.Current.Session["UserInfo"]);
            var cacheKey = userinfo.UserID.Trim();
            
            lock (CacheLockObject)
            {
                HttpRuntime.Cache.Remove(cacheKey);
            }
            this.GetSiteUnitList();
        }

    }
}