﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Threading;
using System.Globalization;
using RAMAPP_Mvc3.Helpers;
using NHunspell;

//using Rejuicer;
//using Rejuicer.Model;
//using Rejuicer.Configuration;


namespace RAMAPP_Mvc3
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.IgnoreRoute("{*allpdf}", new { allaspx = @".*\.pdf(/.*)?" });
           
            routes.Ignore("ChartImg.axd/{*pathInfo}");
            routes.Ignore("{controller}/ChartImg.axd/{*pathInfo}");
            routes.Ignore("{controller}/{action}/ChartImg.axd/{*pathInfo}");
            routes.Ignore("{controller}/{action}/ActiveSession/{*pathInfo}");
            routes.Ignore("*/KeepSessionAlive.aspx/{*pathInfo}");
            routes.Ignore("*/InputReport.aspx/{*pathInfo}");

           routes.MapRoute(
                 "DataCheckDetail",
                 "Review/ViewDataCheckDetail/{id}",
                 new { controller = "Review", action = "ViewDataCheckDetail", id = UrlParameter.Optional }
                 );

           routes.MapRoute(
               "Review",
               "Review/{action}/{id}/{usr}/{dcid}",
               new { controller = "Review", action = "Index", id = UrlParameter.Optional, usr = UrlParameter.Optional, dcid = UrlParameter.Optional }
               );


            routes.MapRoute(
                "Admin", 
                "Admin/{action}/{id}/{usr}/{dcid}",
                new { controller = "Admin", action = "Index", id = UrlParameter.Optional, usr = UrlParameter.Optional, dcid = UrlParameter.Optional }
                );

            
            
            routes.MapRoute(
                "Profile",
                "Profile/{action}/{id}",
                new { controller = "Profile", action = "Index", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                  "Study",
                  "App/{action}/{id}/{qid}",
                  new { controller = "App", action = "Index", id = UrlParameter.Optional, qid = UrlParameter.Optional }
                  );

            routes.MapRoute(
                "LoggedIn",
                "LoggedIn/{companyId}/{email}",
                new { controller = "App", action = "LoggedIn", companyId = UrlParameter.Optional, email = "" }
                );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );


        }

        protected void Application_Start()
        {
            //Starts the timer for the who is online feature 
            GlobalTimer.StartGlobalTimer();

            //OnRequest.ForJs("~/AppMasterCombined.js").Combine
            ////.File("~/Scripts/jquery.cookie.js")
            ////.File("~/Scripts/jquery-ui-1.8.15.custom.min.js")
            ////.File("~/Scripts/jquery.calculation.min.js")
            ////.File("~/Scripts/Watermark.js")
            ////.File("~/Scripts/SlidingNav.min.js")
            //.FilesIn("~/Scripts/").Matching("(jquery.cookie.js)*(jquery-ui-1.8.15.custom.min.js)*(jquery.calculation.min.js)*(Watermark.js)*(SlidingNav.min.js)*")
            ////.File("jquery.cookie.js")
            ////.File("jquery-ui-1.8.15.custom.min.js")
            ////.File("jquery.calculation.min.js")
            ////.File("Watermark.js")
            ////.File("SlidingNav.min.js")
            //.Configure(); 

            //OnRequest.ForCss("~/Combined.css")
            //.Compact
            //.FilesIn("~/Content/")
            //.Matching("*.css")
            //.Configure();   
            //}


            // Double Model Binder Handles culture number formats
            ModelBinders.Binders.Add(typeof(double), new ModelBinder.DoubleModelBinder());
            ModelBinders.Binders.Add(typeof(double?), new ModelBinder.DoubleModelBinder());
            ModelBinders.Binders.Add(typeof(float), new ModelBinder.FloatModelBinder());
            ModelBinders.Binders.Add(typeof(float?), new ModelBinder.FloatModelBinder());
            ModelBinders.Binders.Add(typeof(decimal), new ModelBinder.DoubleModelBinder());
            ModelBinders.Binders.Add(typeof(decimal?), new ModelBinder.DoubleModelBinder());
            ModelBinders.Binders.Add(typeof(int), new ModelBinder.DoubleModelBinder());
            ModelBinders.Binders.Add(typeof(int?), new ModelBinder.DoubleModelBinder());
            
            RegisterGlobalFilters(GlobalFilters.Filters);
            
            AreaRegistration.RegisterAllAreas();
            
            RegisterRoutes(RouteTable.Routes);


            System.Web.Mvc.AjaxHelper.GlobalizationScriptPath =
               "http://ajax.microsoft.com/ajax/4.0/1/globalization/";

            //string path = HttpContext.Current.Request.ApplicationPath;
           // HttpRuntime.Cache.Add("AppPath", path, null, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);

            try
            {
                NHunspell.Hunspell.NativeDllPath = Hunspell.NativeDllPath + @"\nhunspell";
                string dictionaryPath = Hunspell.NativeDllPath;

                spellEngine = new SpellEngine();
                LanguageConfig enConfig = new LanguageConfig();
                enConfig.LanguageCode = "en";
                enConfig.HunspellAffFile = System.IO.Path.Combine(dictionaryPath, "en_us.aff");
                enConfig.HunspellDictFile = System.IO.Path.Combine(dictionaryPath, "en_us.dic");
                enConfig.HunspellKey = "";
                enConfig.HyphenDictFile = System.IO.Path.Combine(dictionaryPath, "hyph_en_us.dic");
                enConfig.MyThesIdxFile = System.IO.Path.Combine(dictionaryPath, "th_en_us_new.idx");
                enConfig.MyThesDatFile = System.IO.Path.Combine(dictionaryPath, "th_en_us_new.dat");
                spellEngine.AddLanguage(enConfig);
            }
            catch (Exception ex)
            {
                if (spellEngine != null)
                    spellEngine.Dispose();

                throw;
            }
            
        }

        static public SpellEngine spellEngine;

        static public SpellEngine SpellEngine { get { return spellEngine; } }

        protected void Application_End(object sender, EventArgs e)
        {
            if (spellEngine != null)
                spellEngine.Dispose();
            spellEngine = null;

        }
    }
}