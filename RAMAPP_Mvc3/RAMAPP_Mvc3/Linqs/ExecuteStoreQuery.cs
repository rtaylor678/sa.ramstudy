﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Models;

namespace RAMAPP_Mvc3.Linqs
{
    public class ExecuteStoreQuery
    {
        public List<RelatedDataChecks> GetRelatedDataChecksQuery(RAMEntities db, int id, int datacheckId)
        {
            var relatedSql = "SELECT * FROM dbo.GetRelatedDataChecks({0},{1}) Order By RowNumber";
            var dataRelatedChecks = db.ExecuteStoreQuery<RelatedDataChecks>(relatedSql, id, datacheckId).ToList();
            return dataRelatedChecks;
        }


        public List<MissDataModel> GetMissingDataQuery(RAMEntities db, int id, string sectionID)
        {
            var relatedSql = "SELECT * FROM dbo.GetMissingData({0},{1})  ORDER BY SortKey";
            var missingData = db.ExecuteStoreQuery<MissDataModel>(relatedSql, id, sectionID).ToList();
            return missingData;
        }

        public List<DataPropertyDetailModel> GetDataChecksQuery(RAMEntities db, int id, string sectionID)
        {
            var relatedSql = "SELECT gd.*,lu.StatusText FROM dbo.GetDatachecks({0},{1},0) gd join dbo.DatacheckStatus_LU lu on lu.StatusCode=gd.StatusCode Order By RowNumber";
            var dataChecks = db.ExecuteStoreQuery<DataPropertyDetailModel>(relatedSql, id, sectionID).ToList();
            return dataChecks;
        }


        public void ChangeDataCheckStatusQuery(RAMEntities db, int id, int dcid, string status)
        {
            var relatedSql = "exec dbo.OverrideDatacheckStatus {0},{1},{2}";
            db.ExecuteStoreCommand(relatedSql, id, dcid, status);

        }
    }
}