﻿using System;
using System.Threading;
using System.Web.Mvc;

namespace RAMAPP_Mvc3.ModelBinder
{
    /// <summary>
    /// Formats the data to the proper culture when read from the presentation  to database 
    /// </summary>
    public class DoubleModelBinder : DefaultModelBinder
    {

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(double) || bindingContext.ModelType == typeof(double?) || bindingContext.ModelType == typeof(decimal?) || bindingContext.ModelType == typeof(decimal) || bindingContext.ModelType == typeof(int?) || bindingContext.ModelType == typeof(int))
            {
                var glob = new RAMAPP_Mvc3.Localization.ClientCultureInfo();

                return BindNumber(bindingContext);
            }
            else
            {
                return base.BindModel(controllerContext, bindingContext);
            }
        }

        private object BindNumber(ModelBindingContext bindingContext)
        {
            ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valueProviderResult == null)
            {
                return null;
            }

            try
            {
                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProviderResult);
                object value = null;
                Type t = bindingContext.ModelType;

                if (bindingContext.ModelType == typeof(int?))
                {
                    t = typeof(int);
                    value = Convert.ChangeType(valueProviderResult.AttemptedValue.Trim().Replace(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator, ""), t,
                                             Thread.CurrentThread.CurrentCulture);
                    return value;
                }
                if (bindingContext.ModelType == typeof(decimal?))
                {
                    t = typeof(decimal);
                }

                if (bindingContext.ModelType == typeof(double?))
                {
                    t = typeof(double);
                }

                value = Convert.ChangeType(valueProviderResult.AttemptedValue.Trim(), t,
                                               Thread.CurrentThread.CurrentCulture);

                return value;
            }
            catch
            {
                var ex = new InvalidOperationException("Invalid value", new Exception("Invalid value", new FormatException("Invalid value")));
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex);
                return null;
            }
        }
    }
}