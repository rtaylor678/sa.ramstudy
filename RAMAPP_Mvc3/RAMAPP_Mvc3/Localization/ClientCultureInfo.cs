﻿using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;

namespace RAMAPP_Mvc3.Localization
{

    /// <summary>
    /// Class used set the clients culture based on user preferences in their profile or the 
    /// browser setting when not specied in their profile. 
    /// </summary>
    public class ClientCultureInfo
    {
        private static CultureInfo englishCulture;

        public ClientCultureInfo()
        {
            SetClientCultureInfo();
            this.name = Thread.CurrentThread.CurrentCulture.Name;

            this.numberFormat = Thread.CurrentThread.CurrentCulture.NumberFormat;

            this.dateTimeFormat = Thread.CurrentThread.CurrentCulture.DateTimeFormat;
        }

        public DateTimeFormatInfo dateTimeFormat { get; set; }

        public string name { get; set; }

        public NumberFormatInfo numberFormat { get; set; }

        public static CultureInfo GetEnglishCulture()
        {
            if (englishCulture==null)
                englishCulture = new CultureInfo("en-US");
            return englishCulture;
        }

        public static string SerializedCulture(ClientCultureInfo info)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();

            return js.Serialize(info);
        }

        public void SetClientCultureInfo()
        {
            var lang = "";
            var isSessionAlive = !SessionUtil.IsSessionExpired();
            if (isSessionAlive && HttpContext.Current.Session["lang"] == null)
            {
                var request = HttpContext.Current.Request;
                if (isSessionAlive && HttpContext.Current.Session["UserInfo"] != null)
                {
                    var userinfo = (LogIn)HttpContext.Current.Session["UserInfo"];
                    if (userinfo.UserLanguage != null && userinfo.UserLanguage.Trim() != String.Empty)
                        lang = userinfo.UserLanguage.Trim();
                }

                if (lang == "" && request.UserLanguages != null)
                {
                    lang = request.UserLanguages[0];
                }
                HttpContext.Current.Session["lang"] = lang;
            }
            else
                lang = (string)HttpContext.Current.Session["lang"];

            //if (lang != Thread.CurrentThread.CurrentCulture.Name)
            //{
            if (lang != null)
                Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            //}
        }
    }
}