using System.Web.Mvc;
using StructureMap;

[assembly: WebActivator.PreApplicationStartMethod(typeof(RAMAPP_Mvc3.AppStart_Structuremap), "Start")]

namespace RAMAPP_Mvc3
{
    public static class AppStart_Structuremap
    {
        public static void Start()
        {
            var container = (IContainer)IoC.Initialize();
            DependencyResolver.SetResolver(new SmDependencyResolver(container));
        }
    }
}