﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Repositories.Cache;

namespace RAMAPP_Mvc3.ActionFilter
{
    public class BreadcrumbModel
    {
        public string LocalCurrency { get; set; }

        public string SiteName { get; set; }

        public string UnitName { get; set; }
    }



    /// <summary>
    ///   Locates breadcrumb ,client currency, checks access authorization ,and search for comments as well. 
    /// </summary>
    public class ReplaceCurrencyAndBreadcrumbAttribute : ActionFilterAttribute
    {
        private string Level;
        private string Breadcrumb;

        private bool ValidID;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var action = filterContext.RouteData.Values["Action"].ToString();
            var controller = filterContext.RouteData.Values["Controller"].ToString();
            var dataSetId = "";

            Breadcrumb = " ";
            Level = " ";


            if (action.Contains("GetComments") || action.Contains("GetStatusImage") || action.Contains("LoggedIn") || action == "UpdateUser" || action == "CreateUser")
            {
                //base.OnActionExecuting(filterContext);
                return;
            }

            if (filterContext.HttpContext.Request.Url.PathAndQuery.Contains("PIE.htc") || filterContext.HttpContext.Request.Url.PathAndQuery.Contains("images"))
                return;


            if (filterContext.RouteData.Values["id"] != null)
            {
                dataSetId = filterContext.RouteData.Values["id"].ToString();
                int dcid = Convert.ToInt32(dataSetId);

                if (filterContext.HttpContext.Session["Mode"] == null)// if solomon skip this
                    ValidID = validateDatasetID(dcid);
                else
                    ValidID = true;

                if (ValidID)
                {
                    //ST_ are site survey pages and UT_ are unit survey pages
                    if (action.StartsWith("ST_") || action.StartsWith("UT_")) 
                    {
                        string section = action;

                        SetHelpInSection(filterContext, dcid, section);
                        SetSectionComments(filterContext, dcid, section);

                    }
                    var level = filterContext.HttpContext.Request["sectiontype"];
                    Level = level != null ? level :controller == "Review" ? "review": "home";
                    if (filterContext.HttpContext.Session["InfoList" + dataSetId] == null)
                    {

                        SetInfo(dcid, Level, filterContext);//SETS CURRENCY PROPERTY
                        filterContext.HttpContext.Session["InfoList" + dataSetId] = filterContext.HttpContext.Items["info" + dataSetId];
                    }
                    else
                    {
                        filterContext.HttpContext.Items["info" + dataSetId] = (dynamic)filterContext.HttpContext.Session["InfoList" + dataSetId];
                    }

                    SetBreadcrumb(dataSetId, action, controller, Level, filterContext.Controller, filterContext);
                    SetCurrency(filterContext, dcid);
                }

            }
            else
                ValidID = true;

            if (!ValidID)
            {
                var notAllowedResult = new ViewResult
                {
                    ViewName = "~/Views/Shared/NotAllowed.aspx"
                };

                filterContext.Result = notAllowedResult; // new RedirectToRouteResult(new RouteValueDictionary { { "action", "NotAllowed" }, { "controller", "Shared" } });
            }

            if (action != "Index")
                UpdateSessionMonitor();
        }

        private void UpdateSessionMonitor()
        {
            var sess = HttpContext.Current.Session;
            if (sess != null)
            {
                var userinfo = sess["UserInfo"] as LogIn;
                if (userinfo != null)
                {
                    if (userinfo.SecurityLevel < 32000)
                        SessionChecker.UpdateInsertSessionDictionary(userinfo.UserNo);
                }
            }
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var action = filterContext.RouteData.Values["Action"].ToString();
            var controller = filterContext.RouteData.Values["Controller"].ToString();
            var dataSetId = "";

            var vHelpRef = filterContext.HttpContext.Items["HelpRefs"] as String ?? "";
            var vCurrency = filterContext.HttpContext.Items["Currency"] as String ?? "";
            var vComments = filterContext.HttpContext.Items["Comments"] as String ?? "";
            var removeSpaceOnly = true;
        

            Level = "";

            if (action.StartsWith("ST_") || action.StartsWith("UT_") || action.Equals("ViewDataCheckDetail") || action.Equals("DataSectionSummary") || action.Contains("LoggedIn"))
            {
                removeSpaceOnly = false;
            }


            if (filterContext.HttpContext.Request.Url.PathAndQuery.Contains("PIE.htc") || action.Contains("GetStatusImage"))
            {
                base.OnResultExecuted(filterContext);
                return;
            }

            var response = filterContext.HttpContext.Response;

            response.Filter = new ReplaceKeywordsInStream(response.Filter, vCurrency, vComments, vHelpRef, removeSpaceOnly);
        }

        private bool validateDatasetID(int dataSetId)
        {
            var cacheNav = new CachedNavigationRepository();

            //Check if this user can access this site or unit
            var validSite = cacheNav.GetSiteUnitList().Any(c => c.DatasetID == dataSetId || c.UnitsOfSite.Select(u => u.DatasetID).Contains(dataSetId));

            return validSite;
        }

        private void SetInfo(int pid, string level, ActionExecutingContext filterContext)
        {

            using (var db = new RAMEntities())
            {
                if (level == "site" || level == "review")
                {
                    filterContext.HttpContext.Items["info" + pid] = (from j in db.SiteInfo
                                                                     where j.DatasetID == pid
                                                                     select new BreadcrumbModel { LocalCurrency = j.LocalCurrency, SiteName = j.SiteName, UnitName = "" }).FirstOrDefault();

                    return;
                }
                if (level == "unit" || level == "review")
                {
                    filterContext.HttpContext.Items["info" + pid] = (from h in db.UnitInfo
                                                                     join j in db.SiteInfo on h.SiteDatasetID equals j.DatasetID
                                                                     where h.DatasetID == pid
                                                                     select new BreadcrumbModel { LocalCurrency = j.LocalCurrency, SiteName = j.SiteName, UnitName = h.UnitName }).FirstOrDefault();
                    return;
                }


            }
        }

        private void SetBreadcrumb(string pid, string action, string controller, string level, ControllerBase ctl, ActionExecutingContext filterContext)
        {
            

            var routes = new RouteCollection();
            MvcApplication.RegisterRoutes(routes);

           
            var urlHelper = new UrlHelper(filterContext.HttpContext.Request.RequestContext );
            var urlTemp = @"<a href='{0}'>{1}</a>";
            var homeUrl = String.Format(urlTemp, urlHelper.Action("Index", "App"), "Home");
            var info = "";
            var breadCrumb = homeUrl;

            if ((this.Level == "site" || this.Level == "unit") && pid != "")
            {
                var brModel = (BreadcrumbModel)filterContext.HttpContext.Items["info" + pid];
                info = brModel.SiteName.ToUpper() + "&nbsp;&raquo;&nbsp;";

                if (this.Level == "unit" && brModel.UnitName != null)
                    info += brModel.UnitName.ToUpper();
                
                breadCrumb = homeUrl + "&nbsp;&raquo;&nbsp;";
            }

            if (this.Level == "home" || this.Level == "")
                breadCrumb = homeUrl;
            

            ctl.ViewData["sitesunitsbreadcrumb"] = info;
            ctl.ViewData["breadcrumb"] = breadCrumb;
        }

        private void SetHelpInSection(ActionExecutingContext filterContext, int id, string section)
        {
            string companySID = SessionUtil.GetCompanySID();
            if (id != 0 && section != null)
            {
                var jSearializer = new JavaScriptSerializer();

                using (var db = new RAMEntities())
                {
                    var helpLst = (from h in db.QuestionProperties
                                   where h.QuestionSectionID == section && h.HelpID != null && h.HelpID != String.Empty
                                   select new { HelpID = h.QuestionInventoryID.Trim() }).ToList();

                    filterContext.HttpContext.Items["HelpRefs"] = jSearializer.Serialize(helpLst);
                }
            }
        }

        private void SetSectionComments(ActionExecutingContext filterContext, int id, string section)
        {
            string companySID = SessionUtil.GetCompanySID();
            if (id != 0 && section != null)
            {
                using (var db = new RAMEntities())
                {
                    var jSearializer = new JavaScriptSerializer();
                    var cm = (from h in db.Comments
                              join q in db.QuestionProperties on section equals q.QuestionSectionID
                              where h.DatasetID == id && h.QuestionInventoryID == q.QuestionInventoryID
                              select new { CommentID = q.QuestionInventoryID.Trim() }).Distinct().ToList();

                    filterContext.HttpContext.Items["Comments"] = jSearializer.Serialize(cm);
                }
            }
        }

        private void SetCurrency(ActionExecutingContext filterContext, int pid)
        {

            filterContext.HttpContext.Items["Currency"] = "Local Currency";
            var infoList = (BreadcrumbModel)filterContext.HttpContext.Items["info" + pid];
            if (infoList != null)
            {
                if (infoList.LocalCurrency != String.Empty)
                    filterContext.HttpContext.Items["Currency"] = infoList.LocalCurrency;

            }
        }
    }

    /// <summary>
    ///   Stream class that tnserts breadcrumb  and client currency. It get call from  class ReplaceCurrencyAndBreadcrumbAttribute
    /// </summary>
    public class ReplaceKeywordsInStream : MemoryStream
    {
        private Stream _shrink;
        private Func<string, string> _filter;
        private static readonly Regex RegexBetweenTags = new Regex(@">(?! )\s+", RegexOptions.Compiled);
        private static readonly Regex RegexLineBreaks = new Regex(@"([\n\s])+?(?<= {2,})<", RegexOptions.Compiled);

        public ReplaceKeywordsInStream(Stream shrink, string currency, string comments, string helpLst, bool removeSpaceOnly)
        {
            _filter = s =>
            {
               
                var ignoreCase = RegexOptions.IgnoreCase;
                if (!removeSpaceOnly)
                {
                    if (String.IsNullOrEmpty(currency))
                    {
                        currency = "Local Currency";
                    }
                    if (!String.IsNullOrEmpty(helpLst))
                        s = Regex.Replace(s, "Section_HelpLst", helpLst, ignoreCase);

                    if (!String.IsNullOrEmpty(comments))
                        s = Regex.Replace(s, "Section_Comments", comments, ignoreCase);

                    s = Regex.Replace(s, "Local Currency", currency, ignoreCase);
                }
                //Minify Html
                s = RegexBetweenTags.Replace(s, ">");

                s = RegexLineBreaks.Replace(s, "<");
               

                return s;
               
            };

            _shrink = shrink;
            
        }   

        public override void Write(byte[] buffer, int offset, int count)
        {
            var html = Encoding.UTF8.GetString(buffer);
            html = _filter(html);
            _shrink.Write(Encoding.UTF8.GetBytes(html), offset, Encoding.UTF8.GetByteCount(html));
           
        }
    }
}