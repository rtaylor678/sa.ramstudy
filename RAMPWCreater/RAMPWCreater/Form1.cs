﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace RAMPWCreater
{
    public partial class Form1 : Form
    {
        private const string EncryptionIV = "SACTW0S1G2L3C4D5B6R7M8F9";
        private const string PartialKey = "SOALCDMSCCHOOL1996DUSTYRHODES";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreateRAMPW();
        }

        public void CreateRAMPW()
        {
            string screenname = txtScreenName.Text;
            string companyID = txtCompanyID.Text;
            string password = txtPassword.Text;

            string salt = "dog" + screenname + "butt" + companyID + "steelers";
            string encPwd = Encrypt(password, salt);
            txtHashedPassword.Text = encPwd;
        }

        public void DecryptRAMPW()
        {
            string screenname = txtScreenName.Text;
            string companyID = txtCompanyID.Text;
            string password = txtPassword.Text;

            string salt = "dog" + screenname + "butt" + companyID + "steelers";
            string encPwd = Decrypt(password, salt);
            txtHashedPassword.Text = encPwd;
        }

        public string Encrypt(string aString, string salt)
        {
            // Note that the key and IV must be the same for the encrypt and decrypt calls.
            string results;
            TripleDESCryptoServiceProvider tdesEngine = new TripleDESCryptoServiceProvider();
            string key = salt + PartialKey;
            byte[] keyBytes = Encoding.ASCII.GetBytes(key.Substring(0, 24));
            byte[] ivBytes = Encoding.ASCII.GetBytes(EncryptionIV);

            ICryptoTransform transform = tdesEngine.CreateEncryptor(keyBytes, ivBytes);
            byte[] mesg = Encoding.ASCII.GetBytes(aString);
            byte[] ecn = transform.TransformFinalBlock(mesg, 0, mesg.Length);

            results = Convert.ToBase64String(ecn);

            return (results);
        }

        public string Decrypt(string aString, string salt)
        {
            // Note that the key and IV must be the same for the encrypt and decript calls.
            string results;
            aString = aString.Replace(" ", "+");
            TripleDESCryptoServiceProvider tdesEngine = new TripleDESCryptoServiceProvider();
            string key = salt + PartialKey;
            byte[] keyBytes = Encoding.ASCII.GetBytes(key.Substring(0, 24));
            byte[] ivBytes = Encoding.ASCII.GetBytes(EncryptionIV);
            ICryptoTransform transform = tdesEngine.CreateDecryptor(keyBytes, ivBytes);

            byte[] mesg = Convert.FromBase64String(aString);//Encoding.ASCII.GetBytes(aString);

            byte[] ecn = transform.TransformFinalBlock(mesg, 0, mesg.Length);

            results = Encoding.ASCII.GetString(ecn);
            // results = Convert.ToBase64String(ecn);

            return (results);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DecryptRAMPW();
        }
    }
}
